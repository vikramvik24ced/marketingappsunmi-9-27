package com.sunmi.printerhelper.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sunmi.printerhelper.Fragments.AddCustomerInfoFrag;
import com.sunmi.printerhelper.Fragments.ReportFargment;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.R;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportActivity extends AppCompatActivity {


    @BindViews({R.id.txtTime,R.id.txtDate})
    List<TextView> bk_listText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
        if (MakeView.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            bk_listText.get(0).setText(MakeView.showCurrentTime());
            bk_listText.get(1).setText(MakeView.showCuurentDate());   }
        Fragment fragment = new ReportFargment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction().replace(R.id.fl_report, fragment, "reportFrag");
        ft.commit();

    }



    @OnClick({R.id.img_home,R.id.img_inventery,R.id.img_purchase,R.id.img_receipt,R.id.img_billing})
    public void clickMe(View view)
    {
        Intent intent = null;
        switch (view.getId())
        {
            case R.id.img_home:
                intent = new Intent(ReportActivity.this,HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.img_inventery:
                intent = new Intent(ReportActivity.this,InventoryActivity.class);
                startActivity(intent);
                break;
            case R.id.img_purchase:
                intent = new Intent(ReportActivity.this,PurchaseActivity.class);
                startActivity(intent);
                break;
            case R.id.img_receipt:
                intent = new Intent(ReportActivity.this,ReceiptsActivity.class);
                startActivity(intent);
                break;
            case R.id.img_billing:
                intent = new Intent(ReportActivity.this,BillingActivity.class);
                startActivity(intent);
                break;}
    }



}
