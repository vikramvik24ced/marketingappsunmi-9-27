package com.sunmi.printerhelper.activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.content.Intent;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.RetrofitDetails.APIClient;
import com.sunmi.RetrofitDetails.RetrofitInterface;
import com.sunmi.printerhelper.Adapter.HomeAdapter;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.APIModel;
import com.sunmi.printerhelper.Model.CategoryModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import com.sunmi.printerhelper.Fragments.UserDetailsFrag;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Category_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Order_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Product_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Purchased_Table_Name;

public class HomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, ConnectivityReceiver.ConnectivityReceiverListener {

    //------------------------------------------------RecyclerView--------------------------------------------//
    @BindView(R.id.rv_Home)
    RecyclerView rv_Home;
    //-----------------------------------------------View--------------------------------------------//
    @BindView(R.id.img_WifiOnOff)
    ImageView img_WifiOnOff;
    @BindView(R.id.txt_WifiOnOff)
    TextView txt_WifiOnOff;
    TextView txtUserName, txtEmail, txtCompanyName;

    @BindViews({R.id.txtBillingCount,R.id.txtProductCount,R.id.txtSalesCount,R.id.txt_UserName,R.id.txt_Mail})
    List<TextView> bk_listText;


    //-----------------------------------------------Array Or ArrayList--------------------------------------------//
    int[] image = {R.drawable.home_billing, R.drawable.home_inventory, R.drawable.home_purchase, R.drawable.home_receipts, R.drawable.home_report, R.drawable.home_setting};
    int[] imageT1 = {R.drawable.home_billing, R.drawable.home_inventory, R.drawable.home_purchase, R.drawable.home_receipts,
            R.drawable.home_report, R.drawable.home_customer, R.drawable.home_profile, R.drawable.home_setting};
    String[] titleT1 = {"Billing", "Inventory", "Purchase Order", "Receipts", "Reports", "Customer", "Profile", "Settings"};
    String[] title = {"Billing", "Inventory", "Purchase Order", "Receipts", "Reports", "Settings"};
    ArrayList<Integer> homeInfo = new ArrayList<>();
    //------------------------------------------------------Variable----------------------------------------------//
    int user_id = 0, max_id = 0, size = 0;
    //------------------------------------------------------Classes----------------------------------------------//
    DBHelper dbHelper;
    HomeAdapter homeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        if (MakeView.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            bk_listText.get(3).setText(SharedPref.getInstance(this).getLoginDetails().get(5));
            bk_listText.get(4).setText(SharedPref.getInstance(this).getLoginDetails().get(7));

        }

        dbHelper = new DBHelper(this);
        init();
//        dbHelper.getNewCustomer();
        //  checkConnection();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View v = navigationView.getHeaderView(0);
        txtUserName = v.findViewById(R.id.txtUserName);
        txtEmail = v.findViewById(R.id.txtEmail);
        txtCompanyName = v.findViewById(R.id.txtCompanyName);
        txtUserName.setText(SharedPref.getInstance(this).getLoginDetails().get(5));
        txtEmail.setText(SharedPref.getInstance(this).getLoginDetails().get(7));
        txtCompanyName.setText(SharedPref.getInstance(this).getLoginDetails().get(1));
        findViewById(R.id.nav_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.openDrawer(GravityCompat.START);
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Fragment fragment;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            fragment = new UserDetailsFrag();
            ft.replace(R.id.rl_frame, fragment);
            ft.addToBackStack("null");
            ft.commit();

        } else if (id == R.id.nav_customers) {
            startActivity(new Intent(this, CustomerActivity.class));
        } else if (id == R.id.nav_sync) {

        } else if (id == R.id.nav_setting) {
            startActivity(new Intent(this, SettingActivity.class));
        } else if (id == R.id.nav_logout) {
            logout();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void init() {
        txtUserName = findViewById(R.id.txtUserName);
    }

    private void logout() {
        SharedPref.getInstance(this).setLogOut(true);
        Intent intent = new Intent(this, Splash.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Toast.makeText(this, "SignOut", Toast.LENGTH_SHORT).show();
        finish();
    }

    public void settingRecyclerView() {
        if (MakeView.isTablet(this)) {
            homeAdapter = new HomeAdapter(this, imageT1, titleT1, homeInfo);
            rv_Home.setLayoutManager(new GridLayoutManager(this, 4));
        } else {
            homeAdapter = new HomeAdapter(this, image, title, homeInfo);
            rv_Home.setLayoutManager(new GridLayoutManager(this, 2));
        }
        rv_Home.setAdapter(homeAdapter);
        homeAdapter.notifyDataSetChanged();
    }

    public void clickProfile(View view) {
        Intent intent = new Intent(this, ProfileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void onStart() {
        super.onStart(); }

    @Override
    protected void onResume() {
        super.onResume();
        getAllProducts();
        settingRecyclerView();
        // register connection status listener
        BaseApp.getInstance().setConnectivityListener(this);
        checkConnection();

    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        try {
            wifiStatus(isConnected);
        } catch (JSONException e) {
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        try {
            wifiStatus(isConnected);
        } catch (JSONException e) {
        }
    }

    private void wifiStatus(boolean isConnected) throws JSONException {
        if (isConnected) {
            if (MakeView.isTablet(this)) {
                txt_WifiOnOff.setText("Online");
                img_WifiOnOff.setImageResource(R.drawable.wifi_on_white);
            } else {
                txt_WifiOnOff.setText("Online");
                img_WifiOnOff.setImageResource(R.drawable.wifi_on_24dp);
            }
//            callingAppCategoryAPI(isConnected);
            //   callingProductAPI(isConnected);
        } else {
            if (MakeView.isTablet(this)) {
                txt_WifiOnOff.setText("Offline");
                img_WifiOnOff.setImageResource(R.drawable.wifi_off_white);
            } else {
                txt_WifiOnOff.setText("Offline");
                img_WifiOnOff.setImageResource(R.drawable.wifi_off_24dp);
            }

        }
    }

    public void callingAppCategoryAPI(final boolean isConnected) throws JSONException {

        if (isConnected) {
            ArrayList<CategoryModel> arrayListCategory = new ArrayList<>();
            // get userid and maxid from category table
            user_id = Integer.valueOf(SharedPref.getInstance(this).getLoginDetails().get(0));
            max_id = dbHelper.getLastRowId(Category_Table_Name, "Category_id");
            Log.e("categoryAPI SYNC ", "user_id: " + user_id + "  max_id: " + max_id + "");
            // GETTING ALL CATEGORY DATA WITH SYNC STATUS 0
            arrayListCategory = dbHelper.search_SyncStatusCate();


            JSONObject jsonFinalCatgry = new JSONObject();
            final JSONArray jsonArrayData = new JSONArray();
            for (int i = 0; i < arrayListCategory.size(); i++) {
                JSONObject jsonObjectData = new JSONObject();
                jsonObjectData.put("Category_id", arrayListCategory.get(i).getId());
                jsonObjectData.put("category_name", arrayListCategory.get(i).getCategegoryname());
                jsonArrayData.put(jsonObjectData);
                Log.e("category added ", jsonArrayData.toString());
            }

            jsonFinalCatgry.put("user_id", user_id);
            jsonFinalCatgry.put("max_id", max_id);
            jsonFinalCatgry.put("data", jsonArrayData);
            Log.e("categoryAPI SYNC ", "Json RequestData " + jsonFinalCatgry.toString());
            RequestBody body = RequestBody.create(MediaType.parse("text/plain"), jsonFinalCatgry.toString());

            RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
            Call<APIModel> modelCall = retrofitInterface.sendAndGetCategory(body);
            modelCall.enqueue(new Callback<APIModel>() {
                @Override
                public void onResponse(Call<APIModel> call, Response<APIModel> response) {

                    Log.e("categoryAPI ", "response: " + response.body().getMessage());

                    if (response.isSuccessful()) {

                        // CHANGE SYNC STATUS OF CATEGORY
                        Log.e("lengthh json array ", jsonArrayData.length() + "");
                        if (jsonArrayData.length() > 0) {
                            try {
                                dbHelper.updateSyncStatus(jsonArrayData, Category_Table_Name, "sync_status", "Category_id");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        // SYNCING NEW CATEGORY FROM REMOTE TO LOCAL ( INSERTING NEW CATEGORIES TO LOCAL )
                        Log.e("categoryAPI ", "No.Of New Categories: " + response.body().getCat_Data().size() + "");
                        size = response.body().getCat_Data().size();
                        if (size > 0) {
                            CategoryModel categoryModel;
                            for (int i = 0; i < size; i++) {
                                categoryModel = new CategoryModel(response.body().getCat_Data().get(i).getCategoryName(), 1);
                                Log.e("status ", categoryModel.getSyncStatus() + "");
                                dbHelper.insertCategory(categoryModel);
                            }
                            size = 0;
                        }

                        // SYNCING UPDATED CATEGORY FROM REMOTE TO LOCAL ( UPDATE CATEGORIES TO LOCAL)
                        Log.e("categoryAPI ", "No.Of UPDATED Categories: " + response.body().getCat_update_data().size() + "");
                        size = response.body().getCat_update_data().size();
                        if (size > 0) {
                            for (int i = 0; i < size; i++) {
                                try {
                                    dbHelper.updateCategoryName(response.body().getCat_update_data().get(i).getCategory_id(), response.body().getCat_update_data().get(i).getCategoryName());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            size = 0;
                        }

                        // SYNCING DELETED CATEGORY FROM REMOTE TO LOCAL ( DELETED CATEGORIES TO LOCAL)
                        Log.e("categoryAPI ", "No.Of Categories to be DELETED : " + response.body().getCat_delete_data().size() + "");
                        size = response.body().getCat_delete_data().size();
                        if (size > 0) {
                            for (int i = 0; i < size; i++) {
                                Log.e("category id ", response.body().getCat_delete_data().get(i).getCategory_id() + "");
                                dbHelper.delete_category(response.body().getCat_delete_data().get(i).getCategory_id());
                            }
                        }
                    }


                    try {
                        callingProductAPI(isConnected);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<APIModel> call, Throwable t) {
                }
            });
        }
    }


    public void callingProductAPI(boolean isConnected) throws JSONException {
        if (isConnected) {
            Log.e("callingProductAPI ", "Product API");

            // GETTING ALL CATEGORY DATA WITH SYNC STATUS 0
            final ArrayList<ProductModel> arr_getLocalPro = dbHelper.search_SyncStatus();
            Log.e("callingProductAPI ", arr_getLocalPro.size() + "");
            user_id = Integer.valueOf(SharedPref.getInstance(this).getLoginDetails().get(0));
            max_id = dbHelper.getLastRowId(Product_Table_Name, "Pro_id");
            Log.e("callingProductAPI ", "user_id: " + user_id + "  max_id: " + max_id + "");

            final JSONArray ja_NewProductFromLocal = new JSONArray();
            for (int i = 0; i < arr_getLocalPro.size(); i++) {
                JSONObject jsonObjectData = new JSONObject();
                jsonObjectData.put("Pro_id", arr_getLocalPro.get(i).getPro_id());
                jsonObjectData.put("category_id", arr_getLocalPro.get(i).getCategory_id());
                jsonObjectData.put("pro_name", arr_getLocalPro.get(i).getPro_name());
                jsonObjectData.put("quantity", arr_getLocalPro.get(i).getQty());
                jsonObjectData.put("price", arr_getLocalPro.get(i).getPrice());
                jsonObjectData.put("soldby_id", arr_getLocalPro.get(i).getSoldbyId());
                jsonObjectData.put("bar_code", arr_getLocalPro.get(i).getBarcode());
                jsonObjectData.put("bar_code_status", arr_getLocalPro.get(i).getBarcodeStatus());
                ja_NewProductFromLocal.put(jsonObjectData);
            }
            final JSONObject jo_finalProduct = new JSONObject();
            jo_finalProduct.put("user_id", user_id);
            jo_finalProduct.put("max_id", max_id);
            jo_finalProduct.put("pro_data", ja_NewProductFromLocal);
            Log.e("callingProductAPI ", "jo_finalProduct " + jo_finalProduct.toString() + "");

            RequestBody body = RequestBody.create(MediaType.parse("text/plain"), jo_finalProduct.toString());

            RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
            Call<APIModel> modelCall = retrofitInterface.sendAndGetProducts(body);

            modelCall.enqueue(new Callback<APIModel>() {
                @Override
                public void onResponse(Call<APIModel> call, Response<APIModel> response) {

                    if (response.isSuccessful()) {
                        // CHANGE SYNC STATUS OF PRODUCTS
                        Log.e("callingProductAPI ", "ja_NewProductFromLocal length:" + ja_NewProductFromLocal.length() + "");
                        if (ja_NewProductFromLocal.length() > 0) {
                            try {
                                dbHelper.updateSyncStatus(ja_NewProductFromLocal, Product_Table_Name, "sync_status", "Pro_id");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                        // SYNCING NEW PRODUCTS FROM REMOTE TO LOCAL ( INSERTING NEW PRODUCTS TO LOCAL)
                        Log.e("callingProductAPI", "New Pro from remote: " + response.body().getNewProduct().size() + "");
                        Log.e("callingProductAPI", "New Pro from remote: " + response.body().getNewProduct().get(0).getPro_id() + " " + response.body().getNewProduct().get(0).getQty() + " " + response.body().getNewProduct().get(0).getBarcodeStatus() + " " + response.body().getNewProduct().get(0).getPrice() + " " + response.body().getNewProduct().get(0).getPro_name());
                        size = response.body().getNewProduct().size();
                        ProductModel productModel = null;
                        for (int i = 0; i < size; i++) {
                            productModel = response.body().getNewProduct().get(i);
                            dbHelper.insertProduct(productModel);
                        }


//                        if (size > 0) {
//                            CategoryModel categoryModel;
//                            for (int i = 0; i < size; i++) {
//                                categoryModel = new CategoryModel(response.body().getCat_Data().get(i).getCategoryName(), 1);
//                                Log.e("status ",categoryModel.getSyncStatus()+"");
//                                dbHelper.insertCategory(categoryModel);
//                            }
//                            size = 0;
//                        }
                    } else {
                        Log.e("callingProductAPI ", "not successfull");

                    }

                }

                @Override
                public void onFailure(Call<APIModel> call, Throwable t) {

                }
            });

        }
    }


    public void getAllProducts() {
        try {
            homeInfo.clear();
            homeInfo.add(dbHelper.getTotalNumofRow(Order_Table_Name));
            homeInfo.add(dbHelper.getTotalNumofRow(Product_Table_Name));
            homeInfo.add(dbHelper.getTotalNumofRow(Purchased_Table_Name));
            homeInfo.add(dbHelper.getTotalNumofRow(Order_Table_Name));

            Log.e("sdfdsf",homeInfo.get(0)+"" );
            bk_listText.get(0).setText(String.valueOf(homeInfo.get(0)));
            bk_listText.get(1).setText(String.valueOf(homeInfo.get(1)));
            bk_listText.get(2).setText(UseSingleton.getInstance().decimalFormat(dbHelper.noOfProductsSold()));
            Log.e("noOfProductsSold ",dbHelper.noOfProductsSold()+"");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.img_logout)
    protected void Click(View view) {
        logout();
    }
}
