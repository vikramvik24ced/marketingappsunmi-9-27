package com.sunmi.printerhelper.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Adapter.SettingAdapter;
import com.sunmi.printerhelper.Adapter.TaxAdapter;
import com.sunmi.printerhelper.Fragments.ChangePassFragment;
import com.sunmi.printerhelper.Fragments.SettingTaxesFrag;
import com.sunmi.printerhelper.Fragments.UpdateUserInfoFragment;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Model.SettingModel;
import com.sunmi.printerhelper.Model.TaxModel;
import com.sunmi.printerhelper.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SettingActivity extends AppCompatActivity  {

    // setting recyclerview
    @BindViews({ R.id.rv_Settings})
    List<RecyclerView> bk_RV_list;
    // image view
    SettingAdapter settingAdapter;
    ArrayList<SettingModel> settingList = new ArrayList<>();




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_setting);
        if (MakeView.isTablet(this))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        settingRecyclerView();
    }

    public void settingRecyclerView() {
        data();
        settingAdapter = new SettingAdapter(settingList,SettingActivity.this);
        if (MakeView.isTablet(this)) {
            bk_RV_list.get(0).setLayoutManager(new GridLayoutManager(this, 3));
        }else
            bk_RV_list.get(0).setLayoutManager(new LinearLayoutManager(this));
        bk_RV_list.get(0).setAdapter(settingAdapter);
    }


    public void data()
    {
        if (MakeView.isTablet(this))
        {
            settingList.add(new SettingModel("Change Password"));
            settingList.add(new SettingModel("Manage Taxes"));
        }
        else
        {
            settingList.add(new SettingModel("Change Password"));
            settingList.add(new SettingModel("Update Information"));
            settingList.add(new SettingModel("Manage Taxes"));
        }
    }


    @OnClick({R.id.img_BackPress,R.id.img_home,R.id.img_inventery,R.id.img_purchase,R.id.img_receipt,R.id.img_billing})
    public void ClickMe(View view)
    {
     Intent intent = null;
        switch (view.getId())
        {
            case R.id.img_BackPress:
                onBackPressed();
                break;

            case R.id.img_home:
                intent = new Intent(SettingActivity.this,HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.img_inventery:
                intent = new Intent(SettingActivity.this,InventoryActivity.class);
                startActivity(intent);
                break;
            case R.id.img_purchase:
                intent = new Intent(SettingActivity.this,PurchaseNewActivity.class);
                startActivity(intent);
                break;
            case R.id.img_receipt:
                intent = new Intent(SettingActivity.this,ReceiptsActivity.class);
                startActivity(intent);
                break;
            case R.id.img_billing:
                intent = new Intent(SettingActivity.this,BillingActivity.class);
                startActivity(intent);
        }
    }

}