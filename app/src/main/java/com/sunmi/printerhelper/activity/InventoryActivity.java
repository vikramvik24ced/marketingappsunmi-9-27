package com.sunmi.printerhelper.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.barcodescanner.CaptureActivity;
import com.sunmi.barcodescanner.DecoratedBarcodeView;
import com.sunmi.printerhelper.Adapter.ProductAdapter;
import com.sunmi.printerhelper.Adapter.CategoryAdapter;
import com.sunmi.printerhelper.Adapter.SoldByAdapter;
import com.sunmi.printerhelper.Fragments.ProductListFragment;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.CategoryModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Product_Table_Name;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Category_Table_Name;
import static com.sunmi.printerhelper.Model.BarCodeModel.setStatus;
import static com.sunmi.printerhelper.activity.AddProductActivity.status;


public class InventoryActivity extends CaptureActivity implements View.OnClickListener {

    //----------------------------------------------recyclerview---------------------------------------------------//
    @BindViews({ R.id.recycleCategoryList, R.id.rv_Products, R.id.rv_slodBY})
    public List<RecyclerView> recyclerViewList;
    //------------------------------------------------views----------------------------------------------------//
    @BindViews({R.id.edit_ProductName, R.id.edit_Quantity, R.id.edit_price, R.id.edtBarCode, R.id.edit_Search})
    public List<EditText> editTextList;
    @BindView(R.id.spinner)
    Spinner spinner_CategoryName;
    @BindViews({R.id.txt_NoProdcutAvailble, R.id.txtLabelBarcode, R.id.txtDate, R.id.txtTime})
    public List<TextView> textViewList;
    @BindViews({R.id.llBarCodeGen, R.id.ll_button, R.id.ll_search_inv})
    public List<LinearLayout> linearLayoutList;
    @BindView(R.id.btn_AddProduct)
    public Button btn_AddProduct;
    View alertView;
    EditText edtCategoryDialog;
    //-------------------------------------------------Varaible--------------------------------------------------//
    public int category_Id, sold_Id = 0, check = 1, categoryPostion, checkingProduct, product_id;
    public String TAG = "AddProductActivity:", productName, quantity, barcode, header, categoryName, Category_Name, soldName;
    //-------------------------------------------------Classes--------------------------------------------------//
    Fragment fragment;
    DBHelper dbHelper;
    CategoryAdapter categoryAdapter;
    SoldByAdapter soldBy;
    ProductAdapter productAdapter;
    ArrayAdapter<String> mAdapter;
    //-------------------------------------------------Arraylist-----------------------------------------------//
    ArrayList<CategoryModel> listCategory = new ArrayList<>();
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<ProductModel> productModelArrayList, tlist;
    //-----------------------------------------------Static variableF------------------------------------------//
    public static DecoratedBarcodeView inventoryBarcodeScanner;
    public static Switch barCodeScanner;

    @Override
    protected DecoratedBarcodeView initializeContent() {
        setContentView(R.layout.activity_inventory);
        return (DecoratedBarcodeView) findViewById(R.id.zxing_barcode_scanner);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            ButterKnife.bind(this);
        if (MakeView.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            textViewList.get(3).setText(MakeView.showCurrentTime());
            textViewList.get(2).setText(MakeView.showCuurentDate());
        }
        dbHelper = new DBHelper(this);
        init();
        // setSideMenu();
        spinnerCategory();

        barCodeScanner = findViewById(R.id.switch_BarCode);
        barCodeScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (barCodeScanner.isChecked()) {
                    setStatus(3);
                    inventoryBarcodeScanner.setVisibility(View.VISIBLE);
                    inventoryBarcodeScanner.getBarcodeView().resume();
                } else {
                    inventoryBarcodeScanner.setVisibility(View.GONE);
                }
            }
        });
    }


    public void clickSideMenu(View view)
    {

    }

    //---------------------------------------Add new Category into Local DataBase--------------------------------------//
    private void AddCategoryList() {
        CategoryModel categoryModel = new CategoryModel(categoryName, 0);
        dbHelper.insertCategory(categoryModel);
    }

    //---------------------------------------Category Spinner--------------------------------------//
    public void spinnerCategory() {
        checkingProduct = 0;
        arrayList.add("Category");
        listCategory = dbHelper.getCategoryData();
        spinner_CategoryName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category_Id = (int) parent.getItemIdAtPosition(position);
                Log.e(TAG + " cat_id", category_Id + "");
                categoryPostion = position - 1;
                Log.e(TAG + " Postion", categoryPostion + "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (listCategory.size() == 0) {
            mAdapter = new ArrayAdapter<String>(this, R.layout.row_spinner_list_item, arrayList);
            mAdapter.setDropDownViewResource(R.layout.row_spinner_list_item);
            spinner_CategoryName.setAdapter(mAdapter);
        } else {
            for (int i = 0; i < listCategory.size(); i++) {
                arrayList.add(listCategory.get(i).getCategegoryname());
                mAdapter = new ArrayAdapter<String>(this, R.layout.row_spinner_list_item, arrayList);
                Log.e(TAG + " adapter", mAdapter.getItem(i));
                mAdapter.setDropDownViewResource(R.layout.row_spinner_list_item);
                spinner_CategoryName.setAdapter(mAdapter);
            }
        }
    }

    public void init() {
        barCodeScanner = findViewById(R.id.switch_BarCode);
        inventoryBarcodeScanner = findViewById(R.id.zxing_barcode_scanner);
    }

    //-----------------------------------recyclerView of showing category---------------------------------------//
    private void settingRecyclerViewCategory(int frag, int categoryPostion) {
        Collections.sort(listCategory, CategoryModel.BY_TOTAL_QUANTITY);
        if (MakeView.isTablet(this)) {
            categoryAdapter = new CategoryAdapter(this, null, InventoryActivity.this, dbHelper, dbHelper.getCategoryData(), frag, categoryPostion);
            recyclerViewList.get(0).setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        } else {
            categoryAdapter = new CategoryAdapter(dbHelper.getCategoryData(), this, dbHelper, 1);
            recyclerViewList.get(0).setLayoutManager(new LinearLayoutManager(this));
        }
        recyclerViewList.get(0).setAdapter(categoryAdapter);
    }

    //---------------------------------------recyclerView of showing Products------------------------------------------//
    public void settingRecyclerViewProduct(ArrayList<ProductModel> product_list, int frag) {
        productAdapter = new ProductAdapter(this, null, InventoryActivity.this, product_list, dbHelper, frag);
        recyclerViewList.get(1).setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewList.get(1).setAdapter(productAdapter);
    }

    //-------------------------------------recyclerViewofSoldBy---------------------------------------------//
    public void settingRecyclerViewSold() {
        recyclerViewList.get(2).setLayoutManager(new GridLayoutManager(this, 2));
        soldBy = new SoldByAdapter(this, dbHelper.getSoldById(), InventoryActivity.this, sold_Id, soldName, check);
        recyclerViewList.get(2).setAdapter(soldBy);
    }


    //-----------------------------------Add New Product into DataBase ---------------------------------------/
    public void ProductData() {
        int selected_soldby_id = soldBy.lastSelectedPosition + 1;
        productName = editTextList.get(0).getText().toString();
        double price = Double.valueOf(editTextList.get(1).getText().toString());
        quantity = editTextList.get(2).getText().toString();
        barcode = editTextList.get(3).getText().toString();
        ProductModel productModel = new ProductModel(productName, Double.valueOf(quantity), price, barcode, status, category_Id, selected_soldby_id, 0);
        boolean result = dbHelper.insertProduct(productModel);
        if (result == true) {
            editTextList.get(0).setText("");
            editTextList.get(1).setText("");
            editTextList.get(2).setText("");
            editTextList.get(3).setText("");
            settingRecyclerViewCategory(1, categoryPostion);
            Toast.makeText(this, "Your data is successfully saved", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Your data is not saved", Toast.LENGTH_SHORT).show();
        }

    }



    //------------------------------------Edit Existing Product -------------------------------------------//
    public void updateProductDetails(ProductModel productModel, int checkingProduct) {
        this.checkingProduct = checkingProduct;

        if (checkingProduct == 1)
        {
            category_Id = productModel.getCategory_id();
            product_id = productModel.getPro_id();
            editTextList.get(4).setEnabled(false);
            linearLayoutList.get(1).setVisibility(View.VISIBLE);
            linearLayoutList.get(0).setVisibility(View.GONE);
            btn_AddProduct.setVisibility(View.GONE);

            // fill edit text of selected product
            editTextList.get(0).setText(productModel.getPro_name());
            editTextList.get(1).setText(UseSingleton.getInstance().decimalFormat(productModel.getQty()));
            editTextList.get(2).setText(UseSingleton.getInstance().decimalFormat(productModel.getPrice()));
            editTextList.get(3).setText(productModel.getBarcode());
            textViewList.get(1).setVisibility(View.VISIBLE);
            // adding category to spinner
            Category_Name = dbHelper.getName(Category_Table_Name, "Category_id", "Category_Name", productModel.getCategory_id());
            Log.e(TAG + " Category Name", Category_Name);
            if (Category_Name != null) {
                int spinnerPosition = mAdapter.getPosition(Category_Name + "");
                Log.e(TAG + " spinnerPosition", spinnerPosition + "");
                spinner_CategoryName.setSelection(spinnerPosition);
            }

            check = 0;
            sold_Id = productModel.getSoldbyId();
            settingRecyclerViewSold();
        }

        else
        {
          no_Delete_update();
        }

    }

    //---------------------------------------Click listener--------------------------------------//
    @OnClick({R.id.fb_AddProduct, R.id.btnDelete, R.id.img_BackArrow, R.id.ll_search_inv, R.id.rl_create, R.id.btn_AddProduct,
            R.id.llBarCodeGen, R.id.btnUpdate, R.id.img_home,R.id.img_billing,R.id.img_purchase,R.id.img_receipt})
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fb_AddProduct:
                Intent intent = new Intent(this, AddProductActivity.class);
                intent.putExtra("Check", 0);
                startActivity(intent);
                break;

            case R.id.img_BackArrow:
                onBackPressed();
                break;

            case R.id.ll_search_inv:
                fragment = new ProductListFragment();
                fragment.setEnterTransition(new Fade());
                Bundle bundle = new Bundle();
                bundle.putString("Search_By", "viewSearch");
                fragment.setArguments(bundle);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.rl_inventory, fragment).commit();
//                txt_home.setText("Products List");
                ft.addToBackStack("");
                break;

            case R.id.rl_create:
                final AlertDialog dialog = new AlertDialog.Builder(this, R.style.alertDialog).create();
                LayoutInflater inflater = LayoutInflater.from(this);
                alertView = inflater.inflate(R.layout.dialog, null);
                edtCategoryDialog = alertView.findViewById(R.id.edtCategoryDialog);
                Button btnAddCategory = alertView.findViewById(R.id.btnAddCategory);
                btnAddCategory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        categoryName = edtCategoryDialog.getText().toString();
                        arrayList.add(categoryName);
                        spinner_CategoryName.setSelection(arrayList.size() - 1);
                        categoryPostion = arrayList.size() - 1;
                        AddCategoryList();
                        settingRecyclerViewCategory(1, 0);
                        dialog.dismiss();
                    }
                });
                dialog.setView(alertView);
                dialog.show();
                break;
            case R.id.btn_AddProduct:
                if (editTextList.get(0).getText().toString().isEmpty() && editTextList.get(1).getText().toString().isEmpty()
                        && editTextList.get(2).getText().toString().isEmpty() && editTextList.get(3).getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please Fill Details", Toast.LENGTH_SHORT).show();
                } else if (spinner_CategoryName.getSelectedItemId() == 0) {
                    Toast.makeText(this, "Please Selete Category", Toast.LENGTH_SHORT).show();
                } else if (editTextList.get(1).getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please Enter Quantity", Toast.LENGTH_SHORT).show();
                } else if (editTextList.get(2).getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please Enter Price", Toast.LENGTH_SHORT).show();
                } else if (editTextList.get(3).getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please Generate Barcode", Toast.LENGTH_SHORT).show();
                } else {
                    if (checkingProduct == 0) {
                        ProductData();
                    }
                }
                break;

            case R.id.llBarCodeGen:
                try {
                    String pr = editTextList.get(0).getText().toString();
                    int prod = Integer.parseInt(Integer.toBinaryString(pr.charAt(1)));
                    prod = prod + 73 * 3;
                    Random random = new Random();
                    String genBarcode = random.nextInt(10) + String.valueOf(prod);
//                            + dbHelper.getLastRowId(Product_Table_Name, "Pro_id");
                    Log.e(TAG + " genBarcode", genBarcode);
                    editTextList.get(3).setText(genBarcode);
                    status = 0;
                } catch (Exception e) {
                    Toast.makeText(this, "Please Enter Product Name", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btnDelete:
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, R.style.alertDialogLarger);
                alertDialogBuilder.setMessage("Do you want delete this Product?");
                alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
//                     dbHelper.change_Status(Product_Table_Name, "delete_status", "Pro_id", product_id, 1);
                        dbHelper.delete_Cust_From_Table(product_id,Product_Table_Name,"Pro_id");
                            Toast.makeText(InventoryActivity.this, "Customer Deleted", Toast.LENGTH_LONG).show();
                            no_Delete_update();
                            settingRecyclerViewCategory(1,categoryPostion);
                    }
                });
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override public void onClick(DialogInterface dialog, int which) { }});
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
                textView.setTextSize(30);
                Button button2 = alertDialog.findViewById(android.R.id.button2);
                button2.setTextSize(30);
                Button button1 = alertDialog.findViewById(android.R.id.button1);
                button1.setTextSize(30);
                break;

                // need to make changes
            case R.id.btnUpdate:
                if (checkingProduct == 1)
                {
                    int result = 0;
                    // update product in product table
                    try {
                        result = dbHelper.updateProductDetails(product_id,  editTextList.get(0).getText().toString()
                                , editTextList.get(1).getText().toString(), editTextList.get(2).getText().toString(),
                                String.valueOf(category_Id), String.valueOf(soldBy.lastSelectedPosition+1));
                    } catch (JSONException e) { e.printStackTrace(); }

                    if (result == 1) {
                        sold_Id = 0; check = 1;
                        settingRecyclerViewCategory(1, categoryPostion);
                        no_Delete_update();
                    }
                }
                break;


            case R.id.img_home:
                intent = new Intent(InventoryActivity.this,HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.img_billing:
                intent = new Intent(InventoryActivity.this,BillingActivity.class);
                startActivity(intent);
                break;
            case R.id.img_purchase:
                intent = new Intent(InventoryActivity.this,PurchaseNewActivity.class);
                startActivity(intent);
                break;
            case R.id.img_receipt:
                intent = new Intent(InventoryActivity.this,ReceiptsActivity.class);
                startActivity(intent);
                break;

        }
    }

    public void no_Delete_update()
    {
        clearEditText();
        editTextList.get(4).setEnabled(true);
        linearLayoutList.get(1).setVisibility(View.GONE);
        linearLayoutList.get(0).setVisibility(View.VISIBLE);
        btn_AddProduct.setVisibility(View.VISIBLE);
        textViewList.get(1).setVisibility(View.GONE);
        this.checkingProduct = 0;
        // soldby recyclerview

     //   soldBy.notifyDataSetChanged();
    }

    public void clearEditText() {
        editTextList.get(4).setText("");
        editTextList.get(0).setText("");
        editTextList.get(1).setText("");
        editTextList.get(2).setText("");
        editTextList.get(3).setText("");
    }

    //---------------------------------------On Start Method--------------------------------------//
    @Override
    public void onStart() {
        super.onStart();
        settingRecyclerViewSold();
        settingRecyclerViewCategory(1, categoryPostion);
        productModelArrayList = dbHelper.getProdcutSearched();
        tlist = new ArrayList<>();
    }

    //-------------------------------------------Searching---------------------------------------------//
    @OnTextChanged(value = R.id.edit_Search, callback = OnTextChanged.Callback.TEXT_CHANGED)
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        onStart();
        if (count > 0) {
            recyclerViewList.get(0).setVisibility(View.GONE);
        } else {
            recyclerViewList.get(0).setVisibility(View.VISIBLE);
            settingRecyclerViewCategory(1, 0);
        }

        String productName = s.toString();
        if (!productName.equals("")) {
            for (int i = 0; i < productModelArrayList.size(); ++i) {
                if (productModelArrayList.get(i).getPro_name().toLowerCase().contains(productName.toLowerCase())) {
                    tlist.add(productModelArrayList.get(i));
                }
            }
            if (tlist.size() == 0) {
                tlist.clear();
                productAdapter.updateListInventory(tlist);
                textViewList.get(0).setVisibility(View.VISIBLE);
            } else {
                textViewList.get(0).setVisibility(View.GONE);
                productAdapter.updateListInventory(tlist);
                tlist.clear();
            }
        } else if (count == 0) {
            textViewList.get(0).setVisibility(View.GONE);
//            settingRecyclerView(productList, dbHelper.getCategoryData());
        }

    }

    //---------------------------------------onBackPressed--------------------------------------//
    @Override
    public void onBackPressed() {
        if (MakeView.isTablet(this)) {
            Intent intent = new Intent(InventoryActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            if (barCodeScanner.isChecked()) {
                barCodeScanner.setChecked(false);
                inventoryBarcodeScanner.setVisibility(View.GONE);
                super.onBackPressed();
            } else {
                super.onBackPressed();
            }
        }
    }
}
