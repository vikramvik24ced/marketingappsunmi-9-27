package com.sunmi.printerhelper.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.R;

import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileActivity extends AppCompatActivity {

    //Views
    @BindViews({R.id.edtUserName, R.id.edtMobileNo, R.id.edtGender, R.id.edtAddress, R.id.edtCompanyName, R.id.edtRegsNo, R.id.edtBusinessType})
    List<EditText> editTextList;
    @BindViews({R.id.txtEmail_id, R.id.txtSubType,R.id.txtTime,R.id.txtDate})
    List<TextView> textViewList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        if(MakeView.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            textViewList.get(2).setText(MakeView.showCurrentTime());
            textViewList.get(3).setText(MakeView.showCuurentDate());
        }
        textViewList.get(0).setText(SharedPref.getInstance(this).getLoginDetails().get(7));
        textViewList.get(1).setText(SharedPref.getInstance(this).getLoginDetails().get(4));
    }


    @OnClick({R.id.btnUpdate,R.id.img_home,R.id.img_inventery,R.id.img_purchase,R.id.img_receipt,R.id.img_billing})
    protected void clickMe(View view) {

        Intent intent = null;
        switch(view.getId())
        {
            case R.id.btnUpdate:
                if (editTextList.get(0).getText().toString().isEmpty() || editTextList.get(1).getText().toString().isEmpty() || editTextList.get(2).getText().toString().isEmpty() ||
                        editTextList.get(3).getText().toString().isEmpty() || editTextList.get(4).getText().toString().isEmpty() || editTextList.get(5).getText().toString().isEmpty() ||
                        editTextList.get(6).getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please Fill all the Details", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Not Empty", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.img_home:
                intent = new Intent(ProfileActivity.this,HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.img_inventery:
                intent = new Intent(ProfileActivity.this,InventoryActivity.class);
                startActivity(intent);
                break;
            case R.id.img_purchase:
                intent = new Intent(ProfileActivity.this,PurchaseActivity.class);
                startActivity(intent);
                break;
            case R.id.img_receipt:
                intent = new Intent(ProfileActivity.this,ReceiptsActivity.class);
                startActivity(intent);
                break;
            case R.id.img_billing:
                intent = new Intent(ProfileActivity.this,BillingActivity.class);
                startActivity(intent);

        }



    }
}
