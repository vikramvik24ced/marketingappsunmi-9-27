package com.sunmi.printerhelper.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicatorPurchase;
import com.sunmi.Interfaces.ComunicatorPurchaseOrder;
import com.sunmi.printerhelper.Adapter.PurchaseAdapter;
import com.sunmi.printerhelper.Fragments.AddPurchaseOrder;
import com.sunmi.printerhelper.Fragments.CustomerFrag;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Model.PurchaseModel;
import com.sunmi.printerhelper.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PurchaseNewActivity extends AppCompatActivity implements ComunicatorPurchaseOrder{

    //Views
    View v;
    TextView txtNoCustomer;
    public EditText searchDetails;
    PurchaseAdapter purchaseAdapter;
    RecyclerView purchasingList;
    @BindViews({R.id.txtTime,R.id.txtDate})
    List<TextView> bk_listText;

    public ArrayList<PurchaseModel> purchaseModelArrayList, tlist;
    //variables
    int delete = 0, come_back = 0;
    //Database
    DBHelper dbHelper;
    // fragments
    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_new);
        ButterKnife.bind(this);
        if (MakeView.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            bk_listText.get(0).setText(MakeView.showCurrentTime());
            bk_listText.get(1).setText(MakeView.showCuurentDate());   }
        init();
        dbHelper = new DBHelper(this);
        purchaseModelArrayList = dbHelper.getPurchaseDtailsFromDB();
        tlist = new ArrayList<>();

        if (MakeView.isTablet(this))
        { calling_fragments(); }
        searching();
    }


    private void init() {
        purchasingList = findViewById(R.id.purchaseRecycler);
        searchDetails = findViewById(R.id.searchh);
        txtNoCustomer = findViewById(R.id.no_customer);
    }

    // tablet
    private void calling_fragments() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        fragment = new AddPurchaseOrder();
        ft.replace(R.id.rl_AddOrder, fragment,"PurchaseOrderFrag" );
        ft.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        settingRecyclerView();
    }


    private void settingRecyclerView() {
        if (!MakeView.isTablet(this)) {
            purchasingList.setLayoutManager(new LinearLayoutManager(PurchaseNewActivity.this, LinearLayoutManager.VERTICAL, false));
            purchaseAdapter = new PurchaseAdapter(this,this, dbHelper.getPurchaseDtailsFromDB(), dbHelper, null);

        } else {
            purchasingList.setLayoutManager(new GridLayoutManager(this, 2));
            purchaseAdapter = new PurchaseAdapter(this,this, dbHelper.getPurchaseDtailsFromDB(), dbHelper, (AddPurchaseOrder) fragment);
        }

        purchasingList.setAdapter(purchaseAdapter);
    }

    public void delete_order(int delete, int pos)
    {
        purchaseModelArrayList.remove(pos);
        this.delete = delete;
    }

    private void searching() {

        searchDetails.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (delete == 1)
                {
                    purchaseModelArrayList = dbHelper.getPurchaseDtailsFromDB();
                    delete = 0;
                }
                String name = s.toString();
                String vendorName;
                String[] amount = {null};
                if (!name.equals("")) {
                    for (int i = 0; i < purchaseModelArrayList.size(); i++) {
                        amount[0] = String.valueOf(purchaseModelArrayList.get(i).getPurchaseAmount());
                        vendorName = purchaseModelArrayList.get(i).getVendorName();
                        if (vendorName.toLowerCase().contains(name.toLowerCase()) || amount[0].toLowerCase().contains(name.toLowerCase())) {
                            tlist.add(purchaseModelArrayList.get(i));
                        }
                    }
                    if (tlist.size() == 0) {
                        tlist.clear();
                        purchaseAdapter.search(tlist);
                        txtNoCustomer.setVisibility(View.VISIBLE);
                    } else {
                        txtNoCustomer.setVisibility(View.GONE);
                        purchaseAdapter.search(tlist);
                        tlist.clear();
                    }
                } else if (count == 0) {
                    txtNoCustomer.setVisibility(View.GONE);
                    settingRecyclerView();
                }
            }

            @Override public void afterTextChanged(Editable s) {}});
    }


    @OnClick({R.id.imgBillingBack,R.id.fb_AddPurchased,R.id.img_home,
              R.id.img_inventery,R.id.img_receipt,R.id.img_billing})
    public void clickMe(View v) {

        Intent intent = null;
        switch (v.getId()) {
            case R.id.imgBillingBack:
                onBackPressed();
                break;

            case R.id.fb_AddPurchased:
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                fragment = new AddPurchaseOrder();
                ft.replace(R.id.rl_purchaseContainer, fragment,"PurchaseOrderFrag" );
                ft.addToBackStack("null");
                ft.commit();
                break;

            case R.id.img_home:
                intent = new Intent(PurchaseNewActivity.this,HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.img_inventery:
                intent = new Intent(PurchaseNewActivity.this,InventoryActivity.class);
                startActivity(intent);
                break;
            case R.id.img_receipt:
                intent = new Intent(PurchaseNewActivity.this,ReceiptsActivity.class);
                startActivity(intent);
                break;
            case R.id.img_billing:
                intent = new Intent(PurchaseNewActivity.this,BillingActivity.class);
                startActivity(intent);
        }
    }

    @Override
    public void order(PurchaseModel purchaseModel, int comeBack) {

        come_back = comeBack;
        // adding new Order
        if (purchaseModel != null && comeBack == 0)
        {
            purchaseModelArrayList.add(purchaseModel);
            purchaseAdapter.updateList(purchaseModel);
        }
        else if( purchaseModel == null && comeBack == 0)// when order is updated
        {
            onResume();
            searchDetails.setEnabled(true);
            searchDetails.getText().clear();
        }
    }

    @Override
    public void onBackPressed() {

        if (!MakeView.isTablet(this)) {
            switch (come_back) {
                case 1:
                    getSupportFragmentManager().popBackStack();
                    come_back = 0;
                    break;

                case 0:
                    finish();
                    break;
            }
        }
        else super.onBackPressed();
    }
}
