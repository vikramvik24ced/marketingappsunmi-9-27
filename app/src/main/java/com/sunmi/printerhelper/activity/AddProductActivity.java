package com.sunmi.printerhelper.activity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.DataBaseSunmi.TablesAndColumns;
import com.sunmi.barcodescanner.BarcodeResult;
import com.sunmi.barcodescanner.CameraPreview;
import com.sunmi.barcodescanner.CaptureActivity;
import com.sunmi.barcodescanner.DecoratedBarcodeView;
import com.sunmi.barcodescanner.camera.CameraInstance;
import com.sunmi.printerhelper.Adapter.SoldByAdapter;
import com.sunmi.printerhelper.Model.BarCodeModel;
import com.sunmi.printerhelper.Model.CategoryModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Category_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Product_Table_Name;
import static com.sunmi.printerhelper.Model.BarCodeModel.setStatus;

public class AddProductActivity extends CaptureActivity {

    //--------------------------------------------------Recyclerview---------------------------------------------------------//
    @BindView(R.id.rv_slodBY)
    RecyclerView rv_slodBY;
    //-----------------------------------------------------views--------------------------------------------------------//
    @BindViews({R.id.edit_ProductName, R.id.edit_price, R.id.edit_Quantity})
    List<EditText> editTextList;
    @BindViews({R.id.imgBarCodeGen, R.id.ll_barcode})
    List<LinearLayout> linearLayoutList;
    @BindView(R.id.txt_home)
    TextView txt_home;
    @BindView(R.id.spinner)
    Spinner spinner_CategoryName;
    Button btnAddCategory;
    EditText edtCategoryDialog;
    View alertView;
    //--------------------------------------------------------Variables---------------------------------------------------------//
    int category_Id, sold_Id = 0, check;
    public static int status = 0;
    public static String taxes;
    String categoryName, Category_Name, soldName, productName, quantity, barcode, header = "AddProductActivity", TAG = "AddProductActivity:";
    //-----------------------------------------------------------ArrayList------------------------------------------------------//
    ArrayList<CategoryModel> categoryModelArrayList = new ArrayList<>();
    ArrayList<String> arrayList = new ArrayList<>();
    //---------------------------------------------------------static Variable--------------------------------------------------//
    public static Switch barCode;
    public static EditText edtBarCode;
    public static DecoratedBarcodeView addProductbarcodeScanner;
    public static TextView txtLabelBarcode;
    //------------------------------------------------------------Classes-------------------------------------------------------//
    ProductModel productModel;
    BarcodeResult barcodeResult;
    CameraPreview cameraPreview;
    CameraInstance cameraInstance;
    BarCodeModel barCodeModel;
    SoldByAdapter soldBy;
    ArrayAdapter<String> mAdapter;
    DBHelper dbHelper;

    @Override
    protected DecoratedBarcodeView initializeContent() {
        setContentView(R.layout.activity_add_product);
        return (DecoratedBarcodeView) findViewById(R.id.zxing_barcode_scanner);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        classesInit();
        init();
        dbHelper = new DBHelper(this);
        arrayList.add("Category");
        check = 1;
        categoryModelArrayList = dbHelper.getCategoryData();

        spinner_CategoryName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category_Id = (int) parent.getItemIdAtPosition(position);
                Log.e(TAG + " cat id", category_Id + "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        for (int i = 0; i < categoryModelArrayList.size(); i++) {
            arrayList.add(categoryModelArrayList.get(i).getCategegoryname());
            mAdapter = new ArrayAdapter<String>(this, R.layout.row_spinner_list_item, arrayList);
            Log.e(TAG + " adapter", mAdapter.getItem(i));
            mAdapter.setDropDownViewResource(R.layout.row_spinner_list_item);
            spinner_CategoryName.setAdapter(mAdapter);
        }

        barCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (barCode.isChecked()) {
                    setStatus(1);
                    addProductbarcodeScanner.setVisibility(View.VISIBLE);
                    addProductbarcodeScanner.getBarcodeView().resume();
                } else {
                    addProductbarcodeScanner.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateProductDetails();
        settingRecyclerViewSold();
    }

    //-----------------------------------------------------Classes Himanshu------------------------------------------------------//
    private void classesInit() {
        barCodeModel = new BarCodeModel(this);
        barcodeResult = new BarcodeResult();
        cameraInstance = new CameraInstance(this);
        cameraPreview = new CameraPreview(this);
        dbHelper = new DBHelper(this);
    }

    public void init() {
        addProductbarcodeScanner = findViewById(R.id.zxing_barcode_scanner);
        txtLabelBarcode = findViewById(R.id.txtLabelBarcode);
        edtBarCode = findViewById(R.id.edtBarCode);
        barCode = findViewById(R.id.switch_BarCode);
    }

    //-----------------------------------------------Product Update Himanshu----------------------------------------------------//
    private void updateProductDetails() {
        productModel = (ProductModel) getIntent().getSerializableExtra("Bundle");
        if (productModel != null) {
            Log.e(TAG + " size", productModel.getPro_id() + "");
            editTextList.get(0).setText(productModel.getPro_name());
            editTextList.get(1).setText(String.valueOf(productModel.getPrice()));
            editTextList.get(2).setText(productModel.getQty() + "");
            edtBarCode.setText(productModel.getBarcode());
            Log.e(TAG + " Category id", productModel.getCategory_id() + "");
            Category_Name = dbHelper.getName(Category_Table_Name, "Category_id", "Category_Name", productModel.getCategory_id());
            Log.e(TAG + " Category Name", Category_Name);
            if (Category_Name != null) {
                int spinnerPosition = mAdapter.getPosition(Category_Name + "");
                Log.e(TAG + " spinnerPosition", spinnerPosition + "");
                spinner_CategoryName.setSelection(spinnerPosition);
            }
            soldName = dbHelper.getSoldByType(productModel.getSoldbyId());
            sold_Id = productModel.getSoldbyId();
            Log.e(TAG + " sold", soldName + " " + sold_Id);
            check = 0;
            header = getIntent().getExtras().getString("Header");
//            if (!header.equals("Update Product Info")) {
            txt_home.setText(header);
            linearLayoutList.get(0).setVisibility(View.GONE);
            linearLayoutList.get(1).setVisibility(View.GONE);
            txtLabelBarcode.setVisibility(View.VISIBLE);
//            }
        }
    }

    public String validationSoldBY() {
        String ValueBarCode = edtBarCode.getText().toString();
        if (!ValueBarCode.isEmpty()) {
        }
        return ValueBarCode;
    }

    //-------------------------------------------------recyclerViewofSoldBy----------------------------------------------------//
    public void settingRecyclerViewSold() {
        rv_slodBY.setLayoutManager(new GridLayoutManager(this, 2));
        Log.e("Size", dbHelper.getSoldById() + "");
        soldBy = new SoldByAdapter(this, dbHelper.getSoldById(), this, sold_Id, soldName, check);
        rv_slodBY.setAdapter(soldBy);
    }

    //-----------------------------------------------Add new Category into Local DataBase---------------------------------------//
    private void AddCategoryList() {
        CategoryModel categoryModel = new CategoryModel(categoryName, 0);
        dbHelper.insertCategory(categoryModel);
    }

    //--------------------------------------------Add new Product written by Himanshu-------------------------------------------------//
    public void ProductData() {
        int selected_soldby_id = soldBy.lastSelectedPosition + 1;
        Log.e(TAG + " SoldBy", selected_soldby_id + "");
        Log.e(TAG + " total categories: ", dbHelper.getTotalNumofRow(TablesAndColumns.Category_Table_Name) + " ");
        Log.e(TAG + " category id ", category_Id + "");
        productName = editTextList.get(0).getText().toString();
        quantity = editTextList.get(2).getText().toString();
        double price = Double.valueOf(editTextList.get(1).getText().toString());
        barcode = edtBarCode.getText().toString();
        productModel = new ProductModel(productName, Double.valueOf(quantity), price, barcode, status, category_Id, selected_soldby_id, 0);
        boolean result = dbHelper.insertProduct(productModel);
        if (result == true) {
            editTextList.get(0).setText("");
            editTextList.get(2).setText("");
            editTextList.get(1).setText("");
            edtBarCode.setText("");
//            Toast.makeText(this, "Your data is successfully saved", Toast.LENGTH_SHORT).show();
            onBackPressed();
        } else if (dbHelper.searchBarCodeProduct(productModel.getBarcode())) {
            Log.e("product", productModel.getPro_name());
            final AlertDialog dialog = new AlertDialog.Builder(AddProductActivity.this, R.style.alertDialog).create();
            View view = LayoutInflater.from(this).inflate(R.layout.dialog_error, null);
            TextView txtError = view.findViewById(R.id.txtError);
            txtError.setSingleLine(false);
            txtError.setText(Html.fromHtml("This BarCode is Already Exist " + "(" + dbHelper.getProductFromBraCode().get(0).getPro_name() + ")" + " Please Enter Unique Barcode"));
            dialog.setTitle("Error");
            dialog.setView(view);
            dialog.show();
            Button btnOk = view.findViewById(R.id.btnOk);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        } else {
            Toast.makeText(this, "Your data is not saved", Toast.LENGTH_SHORT).show();
        }
    }

    //vicky
//    public void InsertDataInSoldBy() {
//        String[] saleTypeArray = {"Piece", "Kilogram", "Litre", "Metre"};
//        for (int i = 0; i < saleTypeArray.length; i++) {
//            saleTypeModel = new SaleTypeModel(saleTypeArray[i]);
//            boolean result = dbHelper.insertDataInSoldBy(saleTypeModel);
//            if (result == true)
//                Toast.makeText(this, "Your data is successfully saved", Toast.LENGTH_SHORT).show();
//            else Toast.makeText(this, "Your data is not saved", Toast.LENGTH_SHORT).show();
//        }
//    }
//    private void insertData() {
//        ArrayList<ProductModel> arrayList = new ArrayList<>();
//        arrayList = dbHelper.getProductList();
//        JsonArray jsonArray = new JsonArray();
//
//        JsonObject jsonObject = new JsonObject();
//        for (int i=0; i<arrayList.size(); i++) {
//            jsonObject.addProperty("user_id", 6);
//            jsonObject.addProperty("pro_id", 5);
//            jsonObject.addProperty("pro_name", arrayList.get(i).getPro_name());
//            jsonObject.addProperty("quantity", 24);
//            jsonObject.addProperty("price", 142.5);
//            jsonObject.addProperty("category_id", 2);
//            jsonObject.addProperty("soldby_id", 2);
////            jsonObject.addProperty("barcodes", arrayList.get(i).getBarcode());
////            jsonObject.addProperty("barcode_status", arrayList.get(i).getBarcodeStatus());
//            jsonArray.add(jsonObject);
//        }
//        Log.d("Response", jsonArray+"");
//        Log.e("StartTime", "StartTime");
//        RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
//        Call<APIModel> apiModelCall = retrofitInterface.sendProduct(jsonArray);
//
//        apiModelCall.enqueue(new Callback<APIModel>() {
//            @Override
//            public void onResponse(Call<APIModel> call, Response<APIModel> response) {
//                if (response.isSuccessful()) {
//                    Log.d("Response", response.body().getSuccess() + "");
//                    Log.d("Response", response.body().getMessage() + "");
//                    if (response.body().getSuccess()) {
//////                        List<GetPro> d = response.body().getDatas();
//////                        for (int i = 0; i < d.size(); i++) {
//////                            ProductModel productModel = new ProductModel(d.get(i).getProName(), d.get(i).getQuantity(), Double.valueOf(d.get(i).getPrice()),
//////                                    null, 0, Integer.valueOf(d.get(i).getCategoryId()), Integer.valueOf(d.get(i).getSoldbyId()));
//////                            dbHelper.insertProduct(productModel);
//////                        }
//                        Toast.makeText(AddProductFrag.this, "Your data is successfully saved", Toast.LENGTH_SHORT).show();
//                        Log.d("Response", response.body().getDatas() + "");
//                        Log.e("EndTime", "EndTime");
//                    } else {
//                        Log.e("Response", response.body() + "");
//                        Toast.makeText(AddProductFrag.this, "Wrong Credential", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<APIModel> call, Throwable t) {
//                Log.e("Response", t.getMessage());
//
//            }
//        });
//    }

    //--------------------------------------------Click Listener written by Himanshu-------------------------------------------------//
    @OnClick({R.id.img_BackArrow, R.id.btn_AddProduct, R.id.edtBarCode, R.id.imgBarCodeGen, R.id.rl_create})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_AddProduct:
                if (editTextList.get(0).getText().toString().isEmpty() && editTextList.get(2).getText().toString().isEmpty()
                        && editTextList.get(1).getText().toString().isEmpty() && edtBarCode.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please Fill Details", Toast.LENGTH_SHORT).show();
                } else if (spinner_CategoryName.getSelectedItemId() == 0) {
                    Toast.makeText(this, "Please Selete Category", Toast.LENGTH_SHORT).show();
                } else if (editTextList.get(2).getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please Enter Quantity", Toast.LENGTH_SHORT).show();
                } else if (editTextList.get(1).getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please Enter Price", Toast.LENGTH_SHORT).show();
                } else if (edtBarCode.getText().toString().isEmpty()) {
                    Toast.makeText(this, "Please Generate Barcode", Toast.LENGTH_SHORT).show();
                } else {
                    if (header.equals("Update Product Info")) {
                        try {
                            int selected_soldby_id = soldBy.lastSelectedPosition + 1;
                            productName = editTextList.get(0).getText().toString();
                            String qty = editTextList.get(2).getText().toString();
                            String prices = editTextList.get(1).getText().toString();
                            barcode = edtBarCode.getText().toString();
                            dbHelper.updateProductDetails(productModel.getPro_id(), productName, qty, prices,
                                    String.valueOf(category_Id), String.valueOf(selected_soldby_id));
                            Toast.makeText(this, "Successfully Update Details", Toast.LENGTH_SHORT).show();
                            onBackPressed();

                        } catch (Exception e) {
                            Toast.makeText(this, "Not Updated", Toast.LENGTH_SHORT).show();
                        }
                    } else if (header.equals("AddProductActivity")) {
                        ProductData();
                    }
                }
//                insertData();
                break;

            case R.id.imgBarCodeGen:
                try {
                    if (barCode.isChecked()) {
                        barCode.setChecked(false);
                        addProductbarcodeScanner.setVisibility(View.GONE);
                    }
                    String pr = editTextList.get(0).getText().toString();
                    int prod = Integer.parseInt(Integer.toBinaryString(pr.charAt(1)));
                    prod = prod + 73 * 3;
                    Random random = new Random();
                    String genBarcode = random.nextInt(10) + String.valueOf(prod) + dbHelper.getLastRowId(Product_Table_Name, "Pro_id");
                    Log.e(TAG + " genBarcode", genBarcode);
                    edtBarCode.setText(genBarcode);
                    status = 0;
                    linearLayoutList.get(0).setVisibility(View.GONE);
                    linearLayoutList.get(1).setVisibility(View.GONE);
                    txtLabelBarcode.setVisibility(View.VISIBLE);
//                    recreate();

                } catch (Exception e) {
                    Toast.makeText(this, "Please Enter Product Name", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.img_BackArrow:
                onBackPressed();
                break;
            case R.id.rl_create:
                final AlertDialog dialog = new AlertDialog.Builder(AddProductActivity.this, R.style.alertDialog).create();
                LayoutInflater inflater = LayoutInflater.from(AddProductActivity.this);
                alertView = inflater.inflate(R.layout.dialog, null);
                edtCategoryDialog = alertView.findViewById(R.id.edtCategoryDialog);
                btnAddCategory = alertView.findViewById(R.id.btnAddCategory);
                btnAddCategory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        categoryName = edtCategoryDialog.getText().toString();
                        arrayList.add(categoryName);
                        spinner_CategoryName.setSelection(arrayList.size() - 1);
                        AddCategoryList();
                        dialog.dismiss();
                    }
                });
                dialog.setTitle("Add Category");
                dialog.setView(alertView);
                dialog.show();
                break;

            case R.id.edtBarCode:
                addProductbarcodeScanner.setVisibility(View.VISIBLE);
                barCode.setChecked(true);
                break;
        }
    }
}



