package com.sunmi.printerhelper.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicaterInterface;
import com.sunmi.Interfaces.CommunicatorMoveBack;
import com.sunmi.barcodescanner.BarcodeResult;
import com.sunmi.barcodescanner.CameraPreview;
import com.sunmi.barcodescanner.camera.CameraInstance;
import com.sunmi.printerhelper.Adapter.CategoryAdapter;
import com.sunmi.printerhelper.Fragments.AddToCartFrag;
import com.sunmi.printerhelper.Fragments.CartAltemsFrag;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.Model.BarCodeModel;
import com.sunmi.printerhelper.Model.CategoryModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;

import java.util.ArrayList;
import java.util.Collections;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.AddToCart_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Temp_Table_Name;
import static com.sunmi.printerhelper.Model.BarCodeModel.setStatus;

public class SmallCaptureActivity extends BaseActivity implements View.OnClickListener, CommunicaterInterface, CommunicatorMoveBack {

    @SuppressLint("StaticFieldLeak")
    public static Switch barcodebtn;
    @SuppressLint("StaticFieldLeak")
//    public static DecoratedBarcodeView billingBarcodeScanner;
            //View
            Fragment fragment;
    ImageView imgBillingBack;
    TextView txt_NoOfItems, txt_NoProductAvailable, txtItems, edit_Search;
    public static RelativeLayout ll_ItemsClick;
    LinearLayout ll_clearCart;
    //Recyclerview Setting
    RecyclerView recycleCategoryList;
    //classes
    CategoryAdapter categoryAdapter;
    CameraInstance cameraInstance;
    BarcodeResult barcodeResult;
    CameraPreview cameraPreview;
    BarCodeModel barCodeModel;
    DBHelper dbHelper;
    //Array
    ArrayList<ProductModel> ItemsSelectedByUser = new ArrayList<>();
    ArrayList<CategoryModel> listCategory = new ArrayList<>();
    ArrayList<ProductModel> productModelArrayList = new ArrayList<>();
    int countItems = 0;
    String totalCount = "";
    int goBack = 0;
    double discount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.capture_small);
        init();
        classesInit();
        //search_Via_EditText();
        settingRecyclerView(dbHelper);
        disableClearCart();
    }

    public void disableClearCart() {
        if (ItemsSelectedByUser.isEmpty()) {
            ll_clearCart.setBackgroundColor(Color.parseColor("#7fffffff"));
            ll_clearCart.setEnabled(false);
        }
    }

    public void enableClearCart() {
        ll_clearCart.setBackgroundColor(Color.parseColor("#ffffffff"));
        ll_clearCart.setEnabled(true);
    }

    //init the classes
    private void classesInit() {
        barCodeModel = new BarCodeModel(this);
        cameraInstance = new CameraInstance(this);
        barcodeResult = new BarcodeResult();
        cameraPreview = new CameraPreview(this);
    }

    public void init() {
        dbHelper = new DBHelper(this);
        dbHelper.truncate_AddToCart(AddToCart_Table_Name);
        dbHelper.truncate_AddToCart(Temp_Table_Name);
        barcodebtn = findViewById(R.id.barcodeScreen);
        recycleCategoryList = findViewById(R.id.recycleCategoryList);
        imgBillingBack = findViewById(R.id.imgBillingBack);
        ll_clearCart = findViewById(R.id.ll_clearCart);
        txt_NoProductAvailable = findViewById(R.id.txt_NoProductAvailable);
        txt_NoOfItems = findViewById(R.id.txt_NoOfItems);
        txtItems = findViewById(R.id.txtItems);
        edit_Search = findViewById(R.id.edit_Search);
        ll_ItemsClick = findViewById(R.id.ll_ItemsClick);
        ll_ItemsClick.setOnClickListener(this);
        imgBillingBack.setOnClickListener(this);
        ll_clearCart.setOnClickListener(this);
        imgBillingBack.setOnClickListener(this);
        barcodebtn.setOnClickListener(this);
        edit_Search.setOnClickListener(this);

    }

    public void settingRecyclerView(DBHelper dbHelper) {
        if (newData() == 0) {

        } else {
            Collections.sort(listCategory, CategoryModel.BY_TOTAL_QUANTITY);
            categoryAdapter = new CategoryAdapter(listCategory, this, dbHelper, 0);
            recycleCategoryList.setLayoutManager(new LinearLayoutManager(this));
            recycleCategoryList.setAdapter(categoryAdapter);
        }
    }


    public int newData() {
        ArrayList<CategoryModel> arrayList = dbHelper.getCategoryData();
        for (int i = 0; i < arrayList.size(); i++) {
            listCategory.add(new CategoryModel(arrayList.get(i).getId(), arrayList.get(i).getCategegoryname(), arrayList.get(i).getQuantityInEach_Cat()));
//            listCategory.add(new CategoryModel(arrayList.get(i).getId(), arrayList.get(i).getCategegoryname(),arrayList.get(i).getQuantityInEach_Cat()));
        }
        int count = listCategory.size();
        return count;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (SharedPref.getInstance(this).getBarCodeScannerActivity() == true) {
            Fragment f = getSupportFragmentManager().findFragmentByTag("SmallCaptureActivity");
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            if (f != null) {
                ft.remove(f);
                ft.commit();
                SharedPref.getInstance(this).setBarCodeScannerActivity(false);
                goBack = 0;
            }
        }
        countItems = dbHelper.getTotalRow(Temp_Table_Name);
        if (countItems == 0) {
            disableClearCart();
            txt_NoOfItems.setText("");
            txt_NoOfItems.setVisibility(View.GONE);
            txtItems.setVisibility(View.VISIBLE);
        } else {
            enableClearCart();
            totalCount = "Cart(" + countItems + ")";
            txt_NoOfItems.setText(totalCount);
            txt_NoOfItems.setVisibility(View.VISIBLE);

            txtItems.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.imgBillingBack:
                onBackPressed();
                break;

            case R.id.ll_clearCart:
                clearCart();
                break;

            case R.id.ll_ItemsClick:
                if (countItems == 0) {
                    Toast.makeText(this, "Please select items", Toast.LENGTH_SHORT).show();
                } else {
                    Intent intent = new Intent(SmallCaptureActivity.this, CartAltemsFrag.class);
                    startActivityForResult(intent, 100);

                }
                break;


            case R.id.barcodeScreen:
                if (barcodebtn.isChecked()) {
                    setStatus(2);
                    Intent intent = new Intent(SmallCaptureActivity.this, BarCodeScannerActivity.class);
                    startActivity(intent);
                } else if (!barcodebtn.isChecked()) {
//                    billingBarcodeScanner.setVisibility(View.GONE);
                }
                break;

            case R.id.edit_Search:
                fragment = new AddToCartFrag();
                Bundle bundle = new Bundle();
                bundle.putString("Search_By", "viewSearch");
                fragment.setEnterTransition(new Fade());
                fragment.setArguments(bundle);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fl_frame, fragment, "SmallCaptureActivity").commit();
                ft.addToBackStack("");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            int a = data.getExtras().getInt("SmallCaptureToken");
            if (a == 1) {
                Fragment f = getSupportFragmentManager().findFragmentByTag("SmallCaptureActivity");
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                if (f != null) {
                    countItems = dbHelper.getTotalRow(Temp_Table_Name);
                    if (countItems == 0) {
                        disableClearCart();
                        txt_NoOfItems.setText("");
                        txt_NoOfItems.setVisibility(View.GONE);
                        txtItems.setVisibility(View.VISIBLE);
                    } else {
                        totalCount = "Cart(" + countItems + ")";
                        txt_NoOfItems.setText(totalCount);
                        txt_NoOfItems.setVisibility(View.VISIBLE);
                        txtItems.setVisibility(View.GONE);
                    }
                    goBack = data.getExtras().getInt("goBack");
                    ft.remove(f);
                    ft.commit();
                }
            }
        }
    }


    public void clearCart() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Are you sure, You wanted to make decision");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        //  Vacant_All_Items_Selected_For_billing();
                        dbHelper.truncate_AddToCart(Temp_Table_Name);
                        finish();
                    }
                });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    // interface called when user clicks on plus minus icons
    public void data(String plus_minus) {

        if (plus_minus.equals("p")) {
            enableClearCart();
            countItems = 1 + countItems;
            totalCount = "Cart(" + countItems + ")";
            txt_NoOfItems.setText(totalCount);
            txt_NoOfItems.setVisibility(View.VISIBLE);
        } else if (plus_minus.equals("m")) {
            countItems = countItems - 1;
            totalCount = "Cart(" + countItems + ")";
            txt_NoOfItems.setText(totalCount);
            txt_NoOfItems.setVisibility(View.VISIBLE);
        }

        if (countItems == 0) {
            disableClearCart();
            txt_NoOfItems.setVisibility(View.GONE);
            txtItems.setVisibility(View.VISIBLE);
        }

    }


    public ArrayList<ProductModel> getList() {
        return productModelArrayList;
    }

    @Override
    public void onBackPressed() {

        Log.e("goBack ", goBack + "");

        // '0' represents billing activity, '1' represent AddToCart fragment, '2' represents CartAlItems fragment
        // when items are selected and you are in SmallCapture acti
        if (countItems > 0 && goBack == 0) {
            clearCart();
        } // AddToCart frag
        else if (goBack == 1) {
            //clearCart();
            super.onBackPressed();
            goBack = 0;
        }
        // it calls when no items are selected
        else {
            super.onBackPressed();
        }
    }


    @Override
    public void goBack(int keyyy) {
        goBack = keyyy;
        Log.e("keyyy ", keyyy + "");
    }

}

