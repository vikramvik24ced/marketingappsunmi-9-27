package com.sunmi.printerhelper.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.FontsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicaterReceiptBack;
import com.sunmi.printerhelper.Adapter.ReceiptAdapter;
import com.sunmi.printerhelper.Fragments.ViewBillFrag;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.Model.OrderModel;
import com.sunmi.printerhelper.R;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Optional;

public class ReceiptsActivity extends BaseActivity implements View.OnClickListener, CommunicaterReceiptBack {

    //-----------------------------------------------------RecyclerView---------------------------------------------//
    @BindViews({R.id.receiptRecyclerView})
    List<RecyclerView> recyclerViewList;
    //-----------------------------------------------------Views---------------------------------------------//
    @BindViews({R.id.no_Receipt})
    List<TextView> textViewList;
    private Button btnFilter, btnClearFilter;
    TextView txtFromDate, txtToDate;

    @BindViews({R.id.txtTime,R.id.txtDate})
    List<TextView> bk_listText;
    //-----------------------------------------------------Classes---------------------------------------------//
    DatePickerDialog datePickerDialog;
    ReceiptAdapter receiptAdapter;
    Calendar calendar;
    DBHelper dbHelper;
    //-----------------------------------------------------Variable---------------------------------------------//
    private int yy, mm, dd;
    String fromDate, toDate;
    int from_ViewBill = 0;
    //-----------------------------------------------------ArrayList---------------------------------------------//
    ArrayList<OrderModel> list_OrderDetails = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipts);
        ButterKnife.bind(this);
        if (MakeView.isTablet(this)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            bk_listText.get(0).setText(MakeView.showCurrentTime());
            bk_listText.get(1).setText(MakeView.showCuurentDate());   }
        dbHelper = new DBHelper(this);
        list_OrderDetails = dbHelper.getOrderDetails();
        settingRecycle(dbHelper.getOrderDetails());
        calendar = Calendar.getInstance();
        yy = calendar.get(Calendar.YEAR);
        mm = calendar.get(Calendar.MONTH);
        dd = calendar.get(Calendar.DAY_OF_MONTH);



    }


    private void settingRecycle(ArrayList<OrderModel> orderModelArrayList) {
        receiptAdapter = new ReceiptAdapter(this, orderModelArrayList, dbHelper, this);
        recyclerViewList.get(0).setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerViewList.get(0).setAdapter(receiptAdapter);
    }


    //----------------------------------------------------filter Dialog--------------------------------------------------------//
    private void dialog() {
        final AlertDialog dialog = new AlertDialog.Builder(this, R.style.alertDialog).create();
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_filter, null);
        txtFromDate = view.findViewById(R.id.txtFromDate);
        txtToDate = view.findViewById(R.id.txtToDate);
        btnFilter = view.findViewById(R.id.btnFilter);
        txtFromDate.setOnClickListener(this);
        txtToDate.setOnClickListener(this);
        btnClearFilter = view.findViewById(R.id.btnClearFilter);
        dialog.setTitle("Filter");
        dialog.setView(view);
        dialog.show();
        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDate = txtFromDate.getText().toString();
                toDate = txtToDate.getText().toString();
                if (!fromDate.isEmpty() || !toDate.isEmpty()) {
                    if (getMilliSecofDate(fromDate) <= getMilliSecofDate(toDate)) {
                        settingRecycle(dbHelper.getFilterOrderDetails(getMilliSecofDate(fromDate), getMilliSecofDate(toDate)));
                        dialog.dismiss();
                    }
                } else {
                    Toast.makeText(baseApp, "Please Select Dates", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnClearFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtFromDate.setText("");
                txtToDate.setText("");
                settingRecycle(dbHelper.getOrderDetails());
                dialog.dismiss();
                fromDate = " ";
                toDate = " ";
            }
        });

    }

    //----------------------------------------------------get current date  ----------------------------------------------------//
    private void showCurrentDate(int year, int month, int day) {
        String months = new DateFormatSymbols().getMonths()[month];
        Log.e("months", months);
        txtFromDate.setText(new StringBuilder().append(day).append(" ").append(months).append(" ").append(year));
        txtToDate.setText(new StringBuilder().append(day).append(" ").append(months).append(" ").append(year));
    }

    //----------------------------------------------------get filter date  ----------------------------------------------------//
    private void showFromDate(int year, int month, int day) {
        String months = new DateFormatSymbols().getMonths()[month];
        Log.e("months", months);
        txtFromDate.setText(new StringBuilder().append(day).append(" ").append(months).append(" ").append(year));
        txtToDate.setText(new StringBuilder().append(day).append(" ").append(months).append(" ").append(year));
    }

    //----------------------------------------------------get filter date  --------------------------------------------------//
    private void showToDate(int year, int month, int day) {
        String months = new DateFormatSymbols().getMonths()[month];
        Log.e("months", months);
        txtToDate.setText(new StringBuilder().append(day).append(" ").append(months).append(" ").append(year));
    }

    //----------------------------------------------------From DatePicker  --------------------------------------------------//
    public void filterFromPicker() {
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if (year > yy) {
                    showFromDate(yy, mm, dd);
                } else if (monthOfYear > mm && year == yy) {
                    showFromDate(yy, mm, dd);
                } else if (dayOfMonth > dd && monthOfYear == mm && year == yy) {
                    showFromDate(yy, mm, dd);

                } else {
                    showFromDate(year, monthOfYear, dayOfMonth);
                }
            }
        }, yy, mm, dd);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    //------------------------------------------------------to DatePicker  --------------------------------------------------//
    private void filterToDate() {
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if (year > yy) {
                    showToDate(yy, mm, dd);
                } else if (monthOfYear > mm && year == yy) {
                    showToDate(yy, mm, dd);
                } else if (dayOfMonth > dd && monthOfYear == mm && year == yy) {
                    showToDate(yy, mm, dd);
                } else {
                    showToDate(year, monthOfYear, dayOfMonth);
                }
            }
        }, yy, mm, dd);
        datePickerDialog.getDatePicker().setMinDate(getMilliSecofDate(txtFromDate.getText().toString()));
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    //-----------------------------------------------string date convert into millisecond  -----------------------------------//
    private long getMilliSecofDate(String dates) {
        long milliSeconds = 0;
        Date date;
        try {
            date = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH).parse(dates);
            milliSeconds = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return milliSeconds;
    }

    //----------------------------------------------------Seraching  --------------------------------------------------------//
    @OnTextChanged(value = R.id.edit_Search, callback = OnTextChanged.Callback.TEXT_CHANGED)
    public void onSearching(CharSequence s, int start, int before, int count) {
        String[] order_id = {null};
        ArrayList<OrderModel> new_OrderList = new ArrayList<>();
        String str = s.toString();
        String cust_name = "null";
        int cust_id = 0;
        Log.e("Receipt Activity ", "str: " + str);
        Log.e("Receipt Activity ", "receiptList: " + list_OrderDetails.size());

        for (int i = 0; i < list_OrderDetails.size(); i++) {

            cust_id = list_OrderDetails.get(i).getCus_Id();
            if (cust_id > 0) {
                cust_name = dbHelper.getCustomer(cust_id).getCustomerName();
                Log.e("cust_name", cust_name);
            } else if (cust_id == 0) {
                cust_name = list_OrderDetails.get(i).getPayment_mode();
                Log.e("cust_name", cust_name);
            }

            order_id[0] = String.valueOf(list_OrderDetails.get(i).getOrder_id());
            // date is coming with space somewhere
            if (order_id[0].toLowerCase().contains(str.toLowerCase()) || cust_name.toLowerCase().contains(str.toLowerCase())) {
                new_OrderList.add(list_OrderDetails.get(i));
            }
        }
        Log.e("Customer Activity ", "size of new customer list: " + new_OrderList.size() + "");
        //  customerAdapter.searchCustomer(newCustomerlist);

        if (new_OrderList.size() == 0) {
            new_OrderList.clear();
            receiptAdapter.searchReceipt(new_OrderList);
            textViewList.get(0).setVisibility(View.VISIBLE);
        } else {
            textViewList.get(0).setVisibility(View.GONE);
            receiptAdapter.searchReceipt(new_OrderList);
        }
        if (count == 0) {
            textViewList.get(0).setVisibility(View.GONE);
            receiptAdapter.searchReceipt(new_OrderList);
        }
    }

    //----------------------------------------------------Click Listener  --------------------------------------------------------//
    @Optional
    @OnClick({R.id.img_BackPress, R.id.rl_filter, R.id.txtFromDate, R.id.txtToDate, R.id.img_home,R.id.img_inventery,R.id.img_purchase,R.id.img_receipt,R.id.img_billing})
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.img_BackPress:
                onBackPressed();
                break;

            case R.id.rl_filter:
                dialog();
                try {
                    if (!fromDate.isEmpty() && !toDate.isEmpty()) {
                        txtFromDate.setText(fromDate);
                        txtToDate.setText(toDate);
                    } else {
                    }
                } catch (Exception e) {
                }
                break;

            case R.id.txtFromDate:
                filterFromPicker();
                break;

            case R.id.txtToDate:
                filterToDate();
                break;

            case R.id.img_home:
                intent = new Intent(ReceiptsActivity.this,HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.img_inventery:
                intent = new Intent(ReceiptsActivity.this,InventoryActivity.class);
                startActivity(intent);
                break;
            case R.id.img_purchase:
                intent = new Intent(ReceiptsActivity.this,PurchaseActivity.class);
                startActivity(intent);
                break;

            case R.id.img_billing:
                intent = new Intent(ReceiptsActivity.this,BillingActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {

        // smart phone
        if (!MakeView.isTablet(this))
        {
            if (from_ViewBill == 1)
            {
                getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("ViewBillFrag")).commit();
                settingRecycle(dbHelper.getOrderDetails());
                from_ViewBill = 0;
            }

            else
            {
                finish();
            }
        }
        // tablet
        else
        {
            finish();
        }
    }

    // smart phone
    @Override
    public void backPressReceipt(int goo) {

        if (MakeView.isTablet(this))
        {
            settingRecycle(dbHelper.getOrderDetails());
        }
        else from_ViewBill = goo;
    }

}





