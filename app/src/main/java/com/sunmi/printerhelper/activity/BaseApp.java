package com.sunmi.printerhelper.activity;

import android.app.Application;

import com.sunmi.printerhelper.utils.AidlUtil;

/**
 * Created by Administrator on 2017/4/27.
 */

public class BaseApp extends Application {

    private static BaseApp mInstance;
    private boolean isAidl;
    public boolean isAidl() {
        return isAidl;
    }

    public void setAidl(boolean aidl) {
        isAidl = aidl;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        isAidl = true;
        AidlUtil.getInstance().connectPrinterService(this);
    }

    public static synchronized BaseApp getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }















}
