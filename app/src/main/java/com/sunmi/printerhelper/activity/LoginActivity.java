package com.sunmi.printerhelper.activity;


import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.RetrofitDetails.APIClient;
import com.sunmi.RetrofitDetails.RetrofitInterface;
import com.sunmi.printerhelper.Fragments.ForgotPassword;
import com.sunmi.printerhelper.Fragments.TermsConditionsFragment;
import com.sunmi.printerhelper.Helper.DialogBox;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.Model.APIModel;
import com.sunmi.printerhelper.Model.CatDatum;
import com.sunmi.printerhelper.Model.CategoryModel;
import com.sunmi.printerhelper.Model.GetPro;
import com.sunmi.printerhelper.Model.LoginModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.Model.SaleTypeModel;
import com.sunmi.printerhelper.Model.TaxModel;
import com.sunmi.printerhelper.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Category_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Product_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.SoldBy_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Tax_Table_Name;

public class LoginActivity extends AppCompatActivity {

    //---------------------------------------------------views----------------------------------------------------//
    @BindViews({R.id.edtUsername, R.id.edtPassword})
    List<EditText> editTextList;
    //---------------------------------------------------Classes----------------------------------------------------//
    SaleTypeModel saleTypeModel;
    DBHelper dbHelper;
    private Dialog myDialog;
    //---------------------------------------------------Variables----------------------------------------------------//
    int user_id = 0, max_id = 0, size = 0;
    String email, pass, device_id;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (MakeView.isTablet(this))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        dbHelper = new DBHelper(this);
        myDialog = new Dialog(this);
    }

    //---------------------------------------------------Login API----------------------------------------------------//
    private void loginAPI(String email, String pass, String device_id) {
        RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
        Log.e("LoginAPI", device_id + "");
        Call<APIModel> apiModelCall = retrofitInterface.getLoginDetails(email, pass, "92fcf4b70f94e7d0");
        DialogBox.ShowProgressDialog(LoginActivity.this, 0, 0);

        apiModelCall.enqueue(new Callback<APIModel>() {
            @Override
            public void onResponse(Call<APIModel> call, Response<APIModel> response) {
                if (response.isSuccessful()) {

                    if (response.body().getSuccess()) {
                        Log.d("Response", response.body().getSuccess() + "");
                        Log.d("Response", response.body().getMessage() + "");
                        List<LoginModel> details = response.body().getData();
                        SharedPref.getInstance(LoginActivity.this).setUserID(details.get(0).getUserId());
                        SharedPref.getInstance(LoginActivity.this).setLoginDetails(LoginActivity.this, details.get(0).getUserId(), details.get(0).getCompanyName(), details.get(0).getRegNumber(),
                                details.get(0).getBusinessType(), details.get(0).getSubscriptionType(), details.get(0).getName(),
                                details.get(0).getContact(), details.get(0).getEmail(), details.get(0).getGender(), details.get(0).getAddress(), details.get(0).getLoginTime(), details.get(0).getExpireLogin());

                        //  insertData();
                        if (dbHelper.getTotalNumofRow(SoldBy_Table_Name) == 0 && dbHelper.getTotalNumofRow(Tax_Table_Name) == 0) {
                            InsertDataInSoldBy();
                            insertDefaultTaxInTaxTable();
                            insertDefaultCategory();
                        }

                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Log.e("Response else", response.body() + "");
                        Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                    DialogBox.DismissProgressDialog();
                }
            }

            @Override
            public void onFailure(Call<APIModel> call, Throwable t) {
                Log.e("Response", t.getMessage());
                DialogBox.DismissProgressDialog();

            }
        });
    }

    // written by vicky
    public void InsertDataInSoldBy() {
        String[] saleTypeArray = {"Piece", "Kilogram", "Litre", "Metre"};
        for (int i = 0; i < saleTypeArray.length; i++) {
            saleTypeModel = new SaleTypeModel(saleTypeArray[i]);
            if (dbHelper.insertDataInSoldBy(saleTypeModel)) {
             //   Toast.makeText(this, "Your data is successfully saved", Toast.LENGTH_SHORT).show();

            }
//            if (result == true)
//                Toast.makeText(this, "Your data is successfully saved", Toast.LENGTH_SHORT).show();
//            else Toast.makeText(this, "Your data is not saved", Toast.LENGTH_SHORT).show();
        }
    }

    // written by vicky
    public void insertDefaultTaxInTaxTable() {
        TaxModel taxModel = new TaxModel("VAT", 5.0, 1);
        dbHelper.insertTax(taxModel);
        Log.e("Tax ", "SettingActivityDefault tax inserted in tax table");
    }

    //     written by himanshu
    private void insertDefaultCategory() {
        CategoryModel categoryModel = new CategoryModel("Main", 0);
        dbHelper.insertCategory(categoryModel);
        Log.e("Tax ", "SettingActivityDefault tax inserted in tax table");
    }

    private void insertData() {
        RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
        Call<APIModel> apiModelCall = retrofitInterface.getProduct();
        apiModelCall.enqueue(new Callback<APIModel>() {
            @Override
            public void onResponse(Call<APIModel> call, Response<APIModel> response) {
                if (response.isSuccessful()) {
                    Log.d("Response", response.body().getSuccess() + "");
                    Log.d("Response", response.body().getMessage() + "");
                    Log.e("InsertStartTime", "StartTime");

                    if (response.body().getSuccess()) {
                        List<GetPro> d = response.body().getDatas();
                        for (int i = 0; i < d.size(); i++) {
                            Log.e("Barcode", d.get(i).getBarcode());
                            ProductModel productModel = new ProductModel(d.get(i).getProName(), Double.valueOf(d.get(i).getQuantity()), Double.valueOf(d.get(i).getPrice()),
                                    d.get(i).getBarcode(), 0, Integer.valueOf(d.get(i).getCategoryId()), Integer.valueOf(d.get(i).getSoldbyId()));
                            dbHelper.insertProduct(productModel);
                            Log.e("Barcode", d.get(i).getBarcode());

                        }
//                        Toast.makeText(LoginActivity.this, "Your data is successfully saved1", Toast.LENGTH_SHORT).show();
                        Log.e("InsertEndTime", "EndTime");

                    } else {
                        Log.e("Response", response.body() + "");
                        Toast.makeText(LoginActivity.this, "Wrong Credential", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<APIModel> call, Throwable t) {
                Log.e("Response", t.getMessage());

            }
        });
    }

    private void sendtData() {
        ArrayList<ProductModel> arrayList = new ArrayList<>();
        arrayList = dbHelper.getProductList();
        JsonArray jsonArray = new JsonArray();

        JsonObject jsonObject = new JsonObject();
        for (int i = 0; i < arrayList.size(); i++) {
            jsonObject.addProperty("user_id", 6);
            jsonObject.addProperty("pro_id", 5);
            jsonObject.addProperty("pro_name", arrayList.get(i).getPro_name());
            jsonObject.addProperty("quantity", 24);
            jsonObject.addProperty("price", 142.5);
            jsonObject.addProperty("category_id", 2);
            jsonObject.addProperty("soldby_id", 2);
//            jsonObject.addProperty("barcodes", arrayList.get(i).getBarcode());
//            jsonObject.addProperty("barcode_status", arrayList.get(i).getBarcodeStatus());
            jsonArray.add(jsonObject);
        }
        Log.d("Response", jsonArray + "");
        Log.e("SendStartTime", "StartTime");
        RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
        Call<APIModel> apiModelCall = retrofitInterface.sendProduct(jsonArray);

        apiModelCall.enqueue(new Callback<APIModel>() {
            @Override
            public void onResponse(Call<APIModel> call, Response<APIModel> response) {
                if (response.isSuccessful()) {
                    Log.d("Response", response.body().getSuccess() + "");
                    Log.d("Response", response.body().getMessage() + "");
                    if (response.body().getSuccess()) {
                     //   Toast.makeText(LoginActivity.this, "Your data is successfully saved", Toast.LENGTH_SHORT).show();
                        Log.d("Response", response.body().getDatas() + "");
                        Log.e("SendEndTime", "EndTime");
                    } else {
                        Log.e("Response", response.body() + "");
                        Toast.makeText(LoginActivity.this, "Wrong Credential", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<APIModel> call, Throwable t) {
                Log.e("Response", t.getMessage());

            }
        });
    }

    private void gettingCategoryFromServer() {
        JSONObject jsonFinalCatgry = new JSONObject();
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), jsonFinalCatgry.toString());
        RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
        Call<APIModel> modelCall = retrofitInterface.sendAndGetCategory(body);
        modelCall.enqueue(new Callback<APIModel>() {
            @Override
            public void onResponse(Call<APIModel> call, Response<APIModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        Log.e("Response", response.body() + "");
                        List<CatDatum> cat = response.body().getCat_Data();
                        for (int i = 0; i < cat.size(); i++) {
                            CategoryModel categoryModel = new CategoryModel(cat.get(i).getCategory_id(),
                                    cat.get(i).getCategoryName(), 0);
                            dbHelper.insertCategory(categoryModel);
                        }
                    } else {
                        Log.e("Response", response.body() + "");
                        Toast.makeText(LoginActivity.this, "Wrong Credential", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<APIModel> call, Throwable t) {
            }
        });
    }


//    public void callingAppCategoryAPI() {
//        int user_id = 0, max_id = 0;
//        ArrayList<CategoryModel> arrayListCategory = new ArrayList<>();
//        // get userid and maxid from category table
//        user_id = Integer.valueOf(SharedPref.getInstance(this).getLoginDetails().get(0));
//        max_id = dbHelper.getLastRowId(Category_Table_Name, "Category_id");
//        Log.e("categoryAPI SYNC ", "user_id: " + user_id + "  max_id: " + max_id + "");
//        // GETTING ALL CATEGORY DATA WITH SYNC STATUS 0
//        arrayListCategory = dbHelper.search_SyncStatusCate();
//
//        JSONObject jsonFinalCatgry = new JSONObject();
//        JSONArray jsonArrayData = new JSONArray();
//        for (int i = 0; i < arrayListCategory.size(); i++) {
//            JSONObject jsonObjectData = new JSONObject();
//            try {
//                jsonObjectData.put("category_id", arrayListCategory.get(i).getId());
//                jsonObjectData.put("category_name", arrayListCategory.get(i).getCategegoryname());
//                jsonArrayData.put(jsonObjectData);
//                Log.e("category added ", jsonArrayData.toString());
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//
//
//        try {
//            jsonFinalCatgry.put("user_id", user_id);
//            jsonFinalCatgry.put("max_id", 1);
//            jsonFinalCatgry.put("data", jsonArrayData);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        Log.e("categoryAPI SYNC ", "Json RequestData " + jsonFinalCatgry.toString());
//        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), jsonFinalCatgry.toString());
//        RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
//        Call<APIModel> modelCall = retrofitInterface.sendAndGetCategory(body);
//        modelCall.enqueue(new Callback<APIModel>() {
//            @Override
//            public void onResponse(Call<APIModel> call, Response<APIModel> response) {
//                if (response.isSuccessful()) {
//                    if (response.body().getSuccess()) {
//                        Log.e("Response", response.body() + "");
//                        List<CatDatum> cat = response.body().getCat_Data();
//                        for (int i = 0; i < cat.size(); i++) {
//                            CategoryModel categoryModel = new CategoryModel(cat.get(i).getCategory_id(),
//                                    cat.get(i).getCategoryName(), 0);
//                            dbHelper.insertCategory(categoryModel);
//                        }
//                    } else {
//                        Log.e("Response", response.body() + "");
//                        Toast.makeText(LoginActivity.this, "Wrong Credential", Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<APIModel> call, Throwable t) {
//            }
//        });
//
//    }


    public void callingAppCategoryAPI() throws JSONException {

        ArrayList<CategoryModel> arrayListCategory = new ArrayList<>();
        // get userid and maxid from category table
        user_id = Integer.valueOf(SharedPref.getInstance(this).getLoginDetails().get(0));
        max_id = dbHelper.getLastRowId(Category_Table_Name, "Category_id");
        Log.e("categoryAPI SYNC ", "user_id: " + user_id + "  max_id: " + max_id + "");
        // GETTING ALL CATEGORY DATA WITH SYNC STATUS 0
        arrayListCategory = dbHelper.search_SyncStatusCate();

        JSONObject jsonFinalCatgry = new JSONObject();
        final JSONArray jsonArrayData = new JSONArray();
        for (int i = 0; i < arrayListCategory.size(); i++) {
            JSONObject jsonObjectData = new JSONObject();
            jsonObjectData.put("Category_id", arrayListCategory.get(i).getId());
            jsonObjectData.put("category_name", arrayListCategory.get(i).getCategegoryname());
            jsonArrayData.put(jsonObjectData);
            Log.e("category added ", jsonArrayData.toString());
        }

        jsonFinalCatgry.put("user_id", user_id);
        jsonFinalCatgry.put("max_id", max_id);
        jsonFinalCatgry.put("data", jsonArrayData);
        Log.e("categoryAPI SYNC ", "Json RequestData " + jsonFinalCatgry.toString());
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), jsonFinalCatgry.toString());

        RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
        Call<APIModel> modelCall = retrofitInterface.sendAndGetCategory(body);
        modelCall.enqueue(new Callback<APIModel>() {
            @Override
            public void onResponse(Call<APIModel> call, Response<APIModel> response) {

//                Log.e("categoryAPI ", "response: " + response.body().getMessage());

                if (response.isSuccessful()) {

                    // CHANGE SYNC STATUS OF CATEGORY
                    Log.e("lengthh json array ", jsonArrayData.length() + "");
                    if (jsonArrayData.length() > 0) {
                        try {
                            dbHelper.updateSyncStatus(jsonArrayData, Category_Table_Name, "sync_status", "Category_id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    // SYNCING NEW CATEGORY FROM REMOTE TO LOCAL ( INSERTING NEW CATEGORIES TO LOCAL )
                    Log.e("categoryAPI ", "No.Of New Categories: " + response.body().getCat_Data().size() + "");
                    size = response.body().getCat_Data().size();
                    if (size > 0) {
                        CategoryModel categoryModel;
                        for (int i = 0; i < size; i++) {
                            categoryModel = new CategoryModel(response.body().getCat_Data().get(i).getCategoryName(), 1);
                            Log.e("status ", categoryModel.getSyncStatus() + "");
                            dbHelper.insertCategory(categoryModel);
                        }
                        size = 0;
                    }

                    // SYNCING UPDATED CATEGORY FROM REMOTE TO LOCAL ( UPDATE CATEGORIES TO LOCAL)
                    Log.e("categoryAPI ", "No.Of UPDATED Categories: " + response.body().getCat_update_data().size() + "");
                    size = response.body().getCat_update_data().size();
                    if (size > 0) {
                        for (int i = 0; i < size; i++) {
                            try {
                                dbHelper.updateCategoryName(response.body().getCat_update_data().get(i).getCategory_id(), response.body().getCat_update_data().get(i).getCategoryName());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        size = 0;
                    }

                    // SYNCING DELETED CATEGORY FROM REMOTE TO LOCAL ( DELETED CATEGORIES TO LOCAL)
                    Log.e("categoryAPI ", "No.Of Categories to be DELETED : " + response.body().getCat_delete_data().size() + "");
                    size = response.body().getCat_delete_data().size();
                    if (size > 0) {
                        for (int i = 0; i < size; i++) {
                            Log.e("category id ", response.body().getCat_delete_data().get(i).getCategory_id() + "");
                            dbHelper.delete_category(response.body().getCat_delete_data().get(i).getCategory_id());
                        }
                    }
                }


            }

            @Override
            public void onFailure(Call<APIModel> call, Throwable t) {
            }
        });

    }


    public void callingProductAPI(boolean isConnected) throws JSONException {
        if (isConnected) {
            Log.e("callingProductAPI ", "Product API");

            // GETTING ALL CATEGORY DATA WITH SYNC STATUS 0
            final ArrayList<ProductModel> arr_getLocalPro = dbHelper.search_SyncStatus();
            Log.e("callingProductAPI ", arr_getLocalPro.size() + "");
            user_id = Integer.valueOf(SharedPref.getInstance(this).getLoginDetails().get(0));
            max_id = dbHelper.getLastRowId(Product_Table_Name, "Pro_id");
            Log.e("callingProductAPI ", "user_id: " + user_id + "  max_id: " + max_id + "");

            final JSONArray ja_NewProductFromLocal = new JSONArray();
            for (int i = 0; i < arr_getLocalPro.size(); i++) {
                JSONObject jsonObjectData = new JSONObject();
                jsonObjectData.put("Pro_id", arr_getLocalPro.get(i).getPro_id());
                jsonObjectData.put("category_id", arr_getLocalPro.get(i).getCategory_id());
                jsonObjectData.put("pro_name", arr_getLocalPro.get(i).getPro_name());
                jsonObjectData.put("quantity", arr_getLocalPro.get(i).getQty());
                jsonObjectData.put("price", arr_getLocalPro.get(i).getPrice());
                jsonObjectData.put("soldby_id", arr_getLocalPro.get(i).getSoldbyId());
                jsonObjectData.put("bar_code", arr_getLocalPro.get(i).getBarcode());
                jsonObjectData.put("bar_code_status", arr_getLocalPro.get(i).getBarcodeStatus());
                ja_NewProductFromLocal.put(jsonObjectData);
            }
            final JSONObject jo_finalProduct = new JSONObject();
            jo_finalProduct.put("user_id", user_id);
            jo_finalProduct.put("max_id", max_id);
            jo_finalProduct.put("pro_data", ja_NewProductFromLocal);
            Log.e("callingProductAPI ", "jo_finalProduct " + jo_finalProduct.toString() + "");

            RequestBody body = RequestBody.create(MediaType.parse("text/plain"), jo_finalProduct.toString());

            RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
            Call<APIModel> modelCall = retrofitInterface.sendAndGetProducts(body);

            modelCall.enqueue(new Callback<APIModel>() {
                @Override
                public void onResponse(Call<APIModel> call, Response<APIModel> response) {

                    if (response.isSuccessful()) {
                        // CHANGE SYNC STATUS OF PRODUCTS
                        Log.e("callingProductAPI ", "ja_NewProductFromLocal length:" + ja_NewProductFromLocal.length() + "");
                        if (ja_NewProductFromLocal.length() > 0) {
                            try {
                                dbHelper.updateSyncStatus(ja_NewProductFromLocal, Product_Table_Name, "sync_status", "Pro_id");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                        // SYNCING NEW PRODUCTS FROM REMOTE TO LOCAL ( INSERTING NEW PRODUCTS TO LOCAL)
                        Log.e("callingProductAPI", "New Pro from remote: " + response.body().getNewProduct().size() + "");
                        Log.e("callingProductAPI", "New Pro from remote: " + response.body().getNewProduct().get(0).getPro_id() + " " + response.body().getNewProduct().get(0).getQty() + " " + response.body().getNewProduct().get(0).getBarcodeStatus() + " " + response.body().getNewProduct().get(0).getPrice() + " " + response.body().getNewProduct().get(0).getPro_name());
                        size = response.body().getNewProduct().size();
                        ProductModel productModel = null;
                        for (int i = 0; i < size; i++) {
                            productModel = response.body().getNewProduct().get(i);
                            dbHelper.insertProduct(productModel);
                        }


//                        if (size > 0) {
//                            CategoryModel categoryModel;
//                            for (int i = 0; i < size; i++) {
//                                categoryModel = new CategoryModel(response.body().getCat_Data().get(i).getCategoryName(), 1);
//                                Log.e("status ",categoryModel.getSyncStatus()+"");
//                                dbHelper.insertCategory(categoryModel);
//                            }
//                            size = 0;
//                        }
                    } else {
                        Log.e("callingProductAPI ", "not successfull");

                    }

                }

                @Override
                public void onFailure(Call<APIModel> call, Throwable t) {

                }
            });

        }
    }

    //---------------------------------------------------Click Listener----------------------------------------------------//
    @Optional
    @OnClick({R.id.btnLogin, R.id.txtForgotPass, R.id.txtConditions})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                boolean status = ConnectivityReceiver.isConnected();
                if (status) {
                    email = editTextList.get(0).getText().toString();
                    pass = editTextList.get(1).getText().toString();
                    device_id = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                    Log.e("device Id", device_id);
                    loginAPI(email, pass, device_id);
//                    InsertDataInSoldBy();
//                    insertDefaultTaxInTaxTable();
//                    insertDefaultCategory();
//                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
//                    startActivity(intent);
//                    finish();
                } else {
                    myDialog.setContentView(R.layout.no_internet);
                    myDialog.show();
                }
                break;

            case R.id.txtForgotPass:
                Fragment fragment = new ForgotPassword();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.rl_container, fragment);
                ft.commit();
                ft.addToBackStack("");
                break;

            case R.id.txtConditions:
                Fragment fragments = new TermsConditionsFragment();
                FragmentTransaction fts = getSupportFragmentManager().beginTransaction();
                fts.replace(R.id.rl_container, fragments);
                fts.commit();
                fts.addToBackStack("");
                break;
        }

    }
}

