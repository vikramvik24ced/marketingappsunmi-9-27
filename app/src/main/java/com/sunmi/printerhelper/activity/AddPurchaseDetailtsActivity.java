package com.sunmi.printerhelper.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.JsonObject;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.RetrofitDetails.APIClient;
import com.sunmi.RetrofitDetails.RetrofitInterface;
import com.sunmi.printerhelper.Adapter.ImageAdapter;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.APIModel;
import com.sunmi.printerhelper.Model.PurchaseModel;
import com.sunmi.printerhelper.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Purchased_Table_Name;

public class AddPurchaseDetailtsActivity extends BaseActivity implements View.OnClickListener {

    //View
    Dialog dialog;
    Button btnSave;
    TextView txt_home;
    public RecyclerView imageRecyclerview;
    LinearLayout ll_Purchasedate;
    DatePickerDialog datePickerDialog;
    ImageView img_BackPress, img_addImage;
    private EditText edtVendorName, edtPurchasingDated, edtPurchasedAmt;
    //Variable
    String TAG = "AddPurchaseDetailsActivity", vendorName, purchasingDated, header = "AddPurchaseDetails";
    private int yy, mm, dd, purchasedAmt;
    //database
    DBHelper dbHelper;
    //class variable
    private PurchaseModel purchaseModel;
    Calendar calendar;
    ImageAdapter imageAdapter;
    //ArrayList
    ArrayList<String> arrayList = new ArrayList<>();
    ArrayList<String> arrayList1 = new ArrayList<>();

    int GALLERY_REQUEST_CODE = 200;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_purchase_details);
        dbHelper = new DBHelper(this);
        init();
        ActivityCompat.requestPermissions(this, new String[]
                {Manifest.permission.CAMERA}, 0);
        ActivityCompat.requestPermissions(this, new String[]
                {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
        updatePurchaseDetails();
        calendar = Calendar.getInstance();
        yy = calendar.get(Calendar.YEAR);
        mm = calendar.get(Calendar.MONTH);
        dd = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(yy, mm, dd);
    }

    // written by himanshu
    private void init() {
        edtVendorName = findViewById(R.id.edtVendorName);
        edtPurchasingDated = findViewById(R.id.edtPurchasedt);
        ll_Purchasedate = findViewById(R.id.ll_Purchasedate);
        edtPurchasedAmt = findViewById(R.id.edtBillAmt);
        img_addImage = findViewById(R.id.img_addImage);
        img_BackPress = findViewById(R.id.img_BackPress);
        imageRecyclerview = findViewById(R.id.rv_imgContainer);
        btnSave = findViewById(R.id.btnSave);
        txt_home = findViewById(R.id.txt_home);
        btnSave.setOnClickListener(this);
        img_addImage.setOnClickListener(this);
        img_BackPress.setOnClickListener(this);
    }

    //DatePicker written by himanshu
    public void datepicker(View v) {
        datePickerDialog = new DatePickerDialog(AddPurchaseDetailtsActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if (year > yy) {
                    showDate(yy, mm, dd);
                } else if (monthOfYear > mm && year == yy) {
                    showDate(yy, mm, dd);
                } else if (dayOfMonth > dd && monthOfYear == mm && year == yy) {
                    showDate(yy, mm, dd);

                } else {
                    showDate(year, monthOfYear, dayOfMonth);
                }
            }
        }, yy, mm, dd);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    private void settingRecycle(ArrayList<String> imagePath) {
//        imageRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//        imageAdapter = new ImageAdapter(this, imagePath, this, dbHelper);
//        imageRecyclerview.setAdapter(imageAdapter);
    }

    // written by himanshu
    //get current date
    private void showDate(int year, int month, int day) {
        String months = new DateFormatSymbols().getMonths()[month];
        Log.e("months", months);
        edtPurchasingDated.setText(new StringBuilder().append(day).append(" ")
                .append(months).append(" ").append(year));
    }

    //Edit purchased Detials
    private void updatePurchaseDetails() {
        purchaseModel = (PurchaseModel) getIntent().getSerializableExtra("purhaseDetails");
        arrayList1 = (ArrayList<String>) getIntent().getSerializableExtra("purchasedImage");

        Log.e(TAG + " ImageList", arrayList1 + "");

        if (purchaseModel != null) {
            Log.e("size", purchaseModel.getPurchasedId() + "");
            edtVendorName.setText(purchaseModel.getVendorName());
            edtPurchasingDated.setText(purchaseModel.getPurchased_Date());
            edtPurchasedAmt.setText(UseSingleton.getInstance().decimalFormat(purchaseModel.getPurchaseAmount()));
            try {
                header = getIntent().getExtras().getString("header");
                if (!header.isEmpty()) {
                    txt_home.setText(header);
                    settingRecycle(arrayList1);
                    imageRecyclerview.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
            }
        }
    }

    // written by himanshu
    // upload image
    private void uploadBIll() {


//        dialog = new Dialog(this);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
//        dialog.setContentView(R.layout.imagesdialog);
//        dialog.show();
//        ImageView cameradialog = dialog.findViewById(R.id.cameradialog);
//        cameradialog.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//        Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        imagePath = getOutputMediaFile();
//        startActivityForResult(camera, 0);
//                dialog.cancel();
//    }
//        });
//        ImageView gallerydialog = dialog.findViewById(R.id.gallerydialog);
//        gallerydialog.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {



        Intent intent = new Intent(AddPurchaseDetailtsActivity.this, CustomPhotoGalleryActivity.class);
        startActivityForResult(intent, 1);
    }

    private void purchaseData() {

        JSONObject jsonObjectPurchase = new JSONObject();
        JSONArray jsonArrayPurchase = new JSONArray();
        JsonObject jsonObject = new JsonObject();
        ArrayList<PurchaseModel> arrayListPurchase = new ArrayList<>();
        arrayListPurchase = dbHelper.getPurchaseDtailsFromDB();
        try {
            for (int i = 0; i < arrayListPurchase.size(); i++) {
                JSONObject jsonObjectPurchaseInside = new JSONObject();
                jsonObjectPurchaseInside.put("purchase_id", arrayListPurchase.get(i).getPurchasedId());
                jsonObjectPurchaseInside.put("vendor_name", arrayListPurchase.get(i).getVendorName());
                jsonObjectPurchaseInside.put("purchase_date", arrayListPurchase.get(i).getPurchased_Date());
                jsonObjectPurchaseInside.put("purchase_amount", arrayListPurchase.get(i).getPurchaseAmount());
//                jsonObject.addProperty("image", "/storage/emulated/0/Pictures/Screenshots/Screenshot_2018-05-14-12-50-29.png");
                jsonObjectPurchaseInside.put("image", arrayList + "");
                jsonArrayPurchase.put(jsonObjectPurchaseInside);
                Log.e("purchase added ", jsonArrayPurchase.toString());
            }
            jsonObjectPurchase.put("user_id", SharedPref.getInstance(this).getLoginDetails().get(0));
            jsonObjectPurchase.put("max_id", dbHelper.getLastRowId(Purchased_Table_Name, "purchase_id"));
            jsonObjectPurchase.put("data", jsonArrayPurchase);
            Log.e("fianlData", jsonObjectPurchase.toString());
        } catch (Exception e) {
        }


        RequestBody purchaseBody = RequestBody.create(MediaType.parse("text/plain"), jsonObjectPurchase.toString());
        RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
        Call<APIModel> apiModelCall = retrofitInterface.sendAndGetPurchase(purchaseBody);

        apiModelCall.enqueue(new Callback<APIModel>() {
            @Override
            public void onResponse(Call<APIModel> call, Response<APIModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        Log.d("Response", response.body().getSuccess() + "");
                        Log.d("Response", response.body().getMessage() + "");
                        List<PurchaseModel> details = response.body().getPurchaseData();
                        Log.d("Response Data", details + "");
//                        dbHelper.insertPurchaseDetails(details, );
//                        Toast.makeText(AddPurchaseDetailtsActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    } else {
                        Log.e("Response else", response.body() + "");
                        Toast.makeText(AddPurchaseDetailtsActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<APIModel> call, Throwable t) {
                Log.e("Response", t.getMessage());
                Toast.makeText(AddPurchaseDetailtsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


    // written by himanshu
//    // Create folder for store capture image
//    private File getOutputMediaFile() {
//        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "Slipkode");
//        if (!f.exists()) {
//            f.mkdirs();
//        }
//        String fusername = "/IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".png";
//        File file = new File(f.getPath() + fusername);
//
//
//        return file;
//    }

    // written by himanshu
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            //camera Case

//            case 0:
//                if (resultCode == RESULT_OK) {

//                   imageRecyclerview.setVisibility(View.GONE);
//                    try {
//                        //Convert Bitmap To Base64 String
////                        imagePath = data.getData();
//                    Log.e("CameraImagePath", imagePath.getAbsolutePath());
//                          imagesPathList.add(imagePath.getAbsolutePath());

//                    Bitmap bitmap = (Bitmap) data.getExtras().get("data");
//                    imagesPathList.add(bitmap);
//                    Log.e("CameraImagePath", imagesPathList + "");

//                        Uri filetouri = data.get
//                     Log.e("CameraImagePath", imagePath.getAbsolutePath());Data();
//                        Log.e("CameraImageUri", filetouri+"");

//                        InputStream inputStream = getContentResolver().openInputStream(filetouri);
//                        Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
//                        byte[] image = new byte[fileInputStream.available()];
//                        fileInputStream.read(image);
//                        Bitmap bitmap = BitmapFactory.decodeByteArray(image, 0 ,image.length);
//                        Log.e("CameraImageBitmap", bitmap+"");
//                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
//                        byte[] byteArray = byteArrayOutputStream.toByteArray();
//                        final String imagePath = Base64.encodeToString(byteArray, Base64.DEFAULT);
//                        rl_CameraView.setVisibility(View.VISIBLE);
//                        imgCameraView.setImageBitmap(bitmap);
//                        imgCameraView.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                DialogBox.ShowProgressDialog(AddPurchaseDetailtsActivity.this, imagePath+"");
//                            }
//                        });
//                    settingRecycle(imagesPathList);
//                        purchaseModel = new PurchaseModel(0, vendorName, purchasingDated, purchasedAmt);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//                break;
            // gallery case
            case 1:
                if (resultCode == RESULT_OK) {
                    arrayList1 = new ArrayList<>();
                    arrayList = data.getStringArrayListExtra("data");
                    Log.e(TAG + " SeletedImagePath", arrayList + "");
                    try {
                        arrayList1 = dbHelper.getImage(purchaseModel.getPurchasedId());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    for (int i = 0; i < arrayList.size(); i++) {
                        Log.e(TAG + " SeletedImagePath", arrayList.get(i) + "");
                        arrayList1.add(arrayList.get(i));
                        Log.e(TAG + " SeletedImagePath", arrayList1 + "");
                    }
                    settingRecycle(arrayList1);
                    imageRecyclerview.setVisibility(View.VISIBLE);
                    break;
                }
        }
    }

    //validate
    private boolean validation() {
        boolean check;
        try {
            vendorName = edtVendorName.getText().toString();
            purchasingDated = edtPurchasingDated.getText().toString();
            purchasedAmt = Integer.parseInt(edtPurchasedAmt.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (vendorName.isEmpty() || purchasingDated.isEmpty() || purchasedAmt == 0) {
            Toast.makeText(this, "Please Fill All Details", Toast.LENGTH_SHORT).show();
            check = false;
        } else {
            check = true;
        }

        return check;
    }

    public void openGallery() { uploadBIll(); }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        {
            if (requestCode==GALLERY_REQUEST_CODE)
            {
                if(grantResults[0]==PackageManager.PERMISSION_GRANTED)
                {
                    openGallery();
                }
                else
                {
                    Toast.makeText(this, "permission not grated", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_addImage:
                if (validation()) {
                    try {



                        int MyVersion = Build.VERSION.SDK_INT;
                        if (MyVersion < Build.VERSION_CODES.M)
                        {
                            openGallery();
                        }
                        else
                        {
                            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED
                                    && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED)
                            {
                                openGallery();
                            }

                            else
                            {
                                String[]permissionRequest={Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};
                                requestPermissions(permissionRequest,GALLERY_REQUEST_CODE);
                            }
                        }

                    } catch (Exception e) {
                        Toast.makeText(baseApp, "Sorry U Can't Access This feature into this device", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case R.id.img_BackPress:
                onBackPressed();
                break;

            case R.id.btnSave:

                if (validation()) {
                    if (header.equals("Update Purchase Info")) {
                        try {
                            if (vendorName.isEmpty() || purchasingDated.isEmpty() || purchasedAmt == 0) {
                                Toast.makeText(this, "Please Fill All Details", Toast.LENGTH_SHORT).show();
                            } else {
                                if(dbHelper.updatePurchaseDetails(purchaseModel.getPurchasedId(), vendorName, purchasingDated, purchasedAmt, null)) {
                                    if (dbHelper.insertImage(arrayList1, purchaseModel.getPurchasedId())) {
                                        Toast.makeText(this, "Successfully Update Details", Toast.LENGTH_SHORT).show();
                                        onBackPressed();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.e("Exception", e.getMessage());
                        }
                    } else if (header.equals("AddPurchaseDetails")) {

                        if (vendorName.isEmpty() || purchasingDated.isEmpty() || purchasedAmt == 0) {
                            Toast.makeText(this, "Please Fill All Details", Toast.LENGTH_SHORT).show();
                        } else {
                            purchaseModel = new PurchaseModel(0, vendorName, purchasingDated, purchasedAmt);
                            if (dbHelper.insertPurchaseDetails(purchaseModel, arrayList)>0) {
                                Toast.makeText(this, "Order Added Successfully", Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }
                        }

                        if (ConnectivityReceiver.isConnected()) {
                            purchaseData();
                        }
                    }
                }

                break;
        }
    }
}
