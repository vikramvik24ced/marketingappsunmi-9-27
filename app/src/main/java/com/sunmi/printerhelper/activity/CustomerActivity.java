package com.sunmi.printerhelper.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicatorCustomer;
import com.sunmi.printerhelper.Adapter.CustomerAdapter;
import com.sunmi.printerhelper.Fragments.AddCustomerInfoFrag;
import com.sunmi.printerhelper.Fragments.CustomerFrag;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomerActivity extends BaseActivity implements CommunicatorCustomer {
    //Views
    View v;

    // text view
    @BindViews({R.id.no_customer,R.id.txtTime,R.id.txtDate})
    List<TextView> bk_listText;
    // edit text
    @BindView(R.id.searchh)
    EditText search;
    //  setting recycler view
    @BindViews({R.id.recycler_customerList})
    List<RecyclerView> bk_listRecycler;
    CustomerAdapter customerAdapter;
    ArrayList<CustomerDetailsModel> customerlist = new ArrayList<>();
    //Database
    DBHelper dbHelper;
    // variable, fragment, classes
    String object = null;
    // fragments for tablet
    Fragment fragment;
    FragmentManager fm = getSupportFragmentManager();
    FragmentTransaction ft = fm.beginTransaction();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_customer);
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        if (MakeView.isTablet(this))
        {   setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            calling_fragments(); }
        else {
            dbHelper = new DBHelper(this);
            settingRecyclerCustomerView();
            search();}
    }

    // (VICKY LONGADGE)tablet
    private void calling_fragments() {
        // customer fragment
        fragment = new CustomerFrag();
        ft.replace(R.id.fl_CustomerLayout, fragment);
        ft.commit();
    }


    @OnClick({R.id.imgCustomerBack,R.id.fb_AddCustomer, R.id.img_home,R.id.img_inventery,R.id.img_purchase,R.id.img_receipt,R.id.img_billing})
    public void clickMe(View view)
    {
        Intent intent = null;
        switch (view.getId())
        {
            case R.id.imgCustomerBack:
                onBackPressed();
                break;

            case R.id.fb_AddCustomer:
                Fragment fragment = new AddCustomerInfoFrag();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction().replace(R.id.rl_container, fragment);
                ft.commit();
                ft.addToBackStack("");
                break;

            case R.id.img_home:
                intent = new Intent(CustomerActivity.this,HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.img_inventery:
                intent = new Intent(CustomerActivity.this,InventoryActivity.class);
                startActivity(intent);
                break;
            case R.id.img_purchase:
                intent = new Intent(CustomerActivity.this,PurchaseActivity.class);
                startActivity(intent);
                break;
            case R.id.img_receipt:
                intent = new Intent(CustomerActivity.this,ReceiptsActivity.class);
                startActivity(intent);
                break;
            case R.id.img_billing:
                intent = new Intent(CustomerActivity.this,BillingActivity.class);
                startActivity(intent);
        }
    }


    private void settingRecyclerCustomerView() {

        if (getIntent().getExtras() != null) {
            if (getIntent().hasExtra("object")) {
                object = getIntent().getStringExtra("object");
                Log.e("object ", object);
            }
        } else {
            object = "CustomerActivity";
        }
        customerlist = dbHelper.getCustomerListFromDataBase();
        customerAdapter = new CustomerAdapter(customerlist, CustomerActivity.this, dbHelper, object);
        bk_listRecycler.get(0).setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        bk_listRecycler.get(0).setAdapter(customerAdapter);
        customerAdapter.notifyDataSetChanged();
    }

    public void search()
    {
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<CustomerDetailsModel> newCustomerlist = new ArrayList<>();
                String str = s.toString();
                Log.e("Customer Activity ","str: "+str);
                Log.e("Customer Activity ","customerlist: "+customerlist.size());

                for (int i = 0; i<customerlist.size(); i++)
                {
                    if (customerlist.get(i).getCustomerName().toLowerCase().contains(str.toLowerCase()) || customerlist.get(i).getCustomerMobileNumber().toString().toLowerCase().contains(str.toLowerCase()))
                    {
                        newCustomerlist.add(customerlist.get(i));
                    }
                }
                Log.e("Customer Activity ","size of new customer list: "+newCustomerlist.size()+"");
                //  customerAdapter.searchCustomer(newCustomerlist);

                if (newCustomerlist.size() == 0)
                {
                    newCustomerlist.clear();
                    customerAdapter.searchCustomer(newCustomerlist);
                    bk_listText.get(0).setVisibility(View.VISIBLE);
                }

                else{ bk_listText.get(0).setVisibility(View.GONE);customerAdapter.searchCustomer(newCustomerlist); }
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    @Override
    public void getCustomer(int i) {
        if (i == 1) {
            clearData();
            settingRecyclerCustomerView();
        }
    }

    // clear data of recyclerview
    public void clearData() {
        customerlist.clear(); //clear list
        customerAdapter.notifyDataSetChanged(); //let your adapter know about the changes and reload view.
    }


//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.imgCustomerBack:
//                onBackPressed();
//                break;
//
//            case R.id.fb_AddCustomer:
//                Fragment fragment = new AddCustomerInfoFrag();
//                FragmentTransaction ft = getSupportFragmentManager().beginTransaction().replace(R.id.rl_container, fragment);
//                ft.commit();
//                ft.addToBackStack("");
//                break;
//        }
//    }

}
