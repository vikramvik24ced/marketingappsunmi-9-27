package com.sunmi.printerhelper.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Adapter.ViewBillReceiptTaxesAdapter;
import com.sunmi.printerhelper.Adapter.ViewBillReceiptProductAdapter;
import com.sunmi.printerhelper.Helper.UsePrinter;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.BillingModel;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.Model.FinalBill;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.utils.AidlUtil;

import java.util.ArrayList;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Order_Table_Name;

public class ViewBillActivity extends BaseActivity implements View.OnClickListener {

    //View
    TextView txtCustomerName, txtInvoiceNumber, txtDate, txtTotalAmt, txtLabelDiscount, txtDiscountPerc, txtDiscountAmt, txtTaxAmount, txtNetAmtPay, txtSubTotal;
    ImageView img_BackPress, imgStatus;
    Button btn_Refund, btn_cancel, btn_Reprint, btn_Disable_Refund, btn_Disable_Cancel, btn_Reprintstate;
    LinearLayout ll_Refund_without, ll_cancel_without, ll_btnBottom, ll_btnBottom_without;
    //RecyclerView
    RecyclerView rv_viewBill, rv_Taxes;
    //Database Class
    DBHelper dbHelper;
    //Adapter Class
    ViewBillReceiptProductAdapter viewBillReceiptProductAdapter;
    ViewBillReceiptTaxesAdapter viewBillReceiptTaxesAdapter;
    //Variable
    String TAG = "ViewBillActivity";
    int check = 0;
    FinalBill finalBill;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_bill);
        dbHelper = new DBHelper(this);
        init();
        if (AidlUtil.getInstance().initPrinter() == false) {
            ll_btnBottom.setVisibility(View.GONE);
            ll_btnBottom_without.setVisibility(View.VISIBLE);
        }
        getInvoiceData();

    }

    //init
    private void init() {
        img_BackPress = findViewById(R.id.img_BackPress);
        imgStatus = findViewById(R.id.imgStatus);
        rv_viewBill = findViewById(R.id.rv_viewBill);
        rv_viewBill.setNestedScrollingEnabled(false);
        rv_Taxes = findViewById(R.id.rv_Taxes);
        rv_Taxes.setNestedScrollingEnabled(false);
        txtCustomerName = findViewById(R.id.txtCustomerName);
        txtInvoiceNumber = findViewById(R.id.txtInvoiceNumber);
        txtDate = findViewById(R.id.txtDate);
        txtTotalAmt = findViewById(R.id.txtTotalAmt);
        txtLabelDiscount = findViewById(R.id.txtLabelDiscount);
        txtDiscountPerc = findViewById(R.id.txtDiscountPerc);
        txtDiscountAmt = findViewById(R.id.txtDiscountAmt);
        txtTaxAmount = findViewById(R.id.txtTaxAmount);
        txtNetAmtPay = findViewById(R.id.txtNetAmtPay);
        btn_Reprintstate = findViewById(R.id.btn_Reprintstate);
        btn_Reprint = findViewById(R.id.btn_Reprint);
        btn_cancel = findViewById(R.id.btn_cancel);
        btn_Refund = findViewById(R.id.btn_Refund);
        btn_Disable_Refund = findViewById(R.id.btn_Disable_Refund);
        btn_Disable_Cancel = findViewById(R.id.btn_Disable_Cancel);
        ll_btnBottom = findViewById(R.id.ll_btnBottom);
        txtSubTotal = findViewById(R.id.txtSubTotal);

        //withoutPrinter
        ll_cancel_without = findViewById(R.id.btn_cancel_without);
        ll_Refund_without = findViewById(R.id.btn_Refund_without);
        ll_btnBottom_without = findViewById(R.id.ll_btnBottom_without);

        btn_Reprintstate.setOnClickListener(this);
        btn_Refund.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        btn_Reprint.setOnClickListener(this);
        img_BackPress.setOnClickListener(this);

        //withoutPrinter
        ll_cancel_without.setOnClickListener(this);
        ll_Refund_without.setOnClickListener(this);
    }

    private void getInvoiceData() {

        finalBill = (FinalBill) getIntent().getSerializableExtra("finalBill");
        check = getIntent().getIntExtra("check", 0);
        if (finalBill != null) {
            String customer_name = null;
            if (finalBill.getOrderModel().getCus_Id() > 0) {
                CustomerDetailsModel customerDetailsModel = dbHelper.getCustomer(finalBill.getOrderModel().getCus_Id());
                customer_name = customerDetailsModel.getCustomerName();
            } else if (finalBill.getOrderModel().getCus_Id() == 0) {
                customer_name = finalBill.getOrderModel().getPayment_mode();
            }

            txtCustomerName.setText(customer_name);
            txtInvoiceNumber.setText(finalBill.getOrderModel().getOrder_id() + "");
            txtDate.setText(finalBill.getOrderModel().getBillDate());
            txtTotalAmt.setText(UseSingleton.getInstance().decimalFormat(finalBill.getOrderModel().getAmtBeforeDiscount()));
            txtNetAmtPay.setText(finalBill.getOrderModel().getAmtAfterTax());
            settingRecyclerView(finalBill.getItemsSelectedByUser());
            txtSubTotal.setText("Sub Total (" + finalBill.getItemsSelectedByUser().size() + ")");
            if (finalBill.getOrderModel().getDiscountAmt() == 0.0) {
                txtDiscountAmt.setVisibility(View.GONE);
                txtDiscountPerc.setVisibility(View.GONE);
                txtLabelDiscount.setVisibility(View.GONE);
            } else {
                if (finalBill.getOrderModel().getDiscountPerc() == 0.0) {
                    Log.e("disV ", finalBill.getOrderModel().getDiscountPerc() + " " + finalBill.getOrderModel().getDiscountAmt());
                    txtDiscountPerc.setVisibility(View.INVISIBLE);
                    //  txtDiscountPerc.setText(UseSingleton.getInstance().decimalFormat(finalBill.getOrderModel().getDiscountPerc()) + "%");
                    txtDiscountAmt.setText(UseSingleton.getInstance().decimalFormat(finalBill.getOrderModel().getDiscountAmt()));
                } else if (finalBill.getOrderModel().getDiscountPerc() > 0.0) {
                    Log.e("disV ", finalBill.getOrderModel().getDiscountPerc() + " " + finalBill.getOrderModel().getDiscountAmt());
                    txtDiscountPerc.setVisibility(View.VISIBLE);
                    txtDiscountPerc.setText(UseSingleton.getInstance().decimalFormat(finalBill.getOrderModel().getDiscountPerc()) + "%");
                    txtDiscountAmt.setText(UseSingleton.getInstance().decimalFormat(finalBill.getOrderModel().getDiscountAmt()));
                }

            }
        }
        if (finalBill.getOrderModel().getTaxType() == 2) {
            String[] taxId = finalBill.getOrderModel().getTaxID().split(",");
            String[] taxAmount = finalBill.getOrderModel().getAmtOfEach_Tax().split(",");
            settingRecyclerViewTaxes(taxId, taxAmount);
        }

        if (finalBill.getOrderModel().getBillStatus() == 1) {

            imgStatus.setImageResource(R.drawable.cancelled);
            imgStatus.setVisibility(View.VISIBLE);
            if (AidlUtil.getInstance().initPrinter() == false) {
                ll_btnBottom_without.setVisibility(View.GONE);
            } else {
                ll_btnBottom.setVisibility(View.GONE);
                btn_Reprintstate.setVisibility(View.VISIBLE);
            }
        } else if (finalBill.getOrderModel().getBillStatus() == 2) {
            imgStatus.setImageResource(R.drawable.refunded);
            imgStatus.setVisibility(View.VISIBLE);
            if (AidlUtil.getInstance().initPrinter() == false) {
                ll_btnBottom_without.setVisibility(View.GONE);
            } else {
                ll_btnBottom.setVisibility(View.GONE);
                btn_Reprintstate.setVisibility(View.VISIBLE);
            }
        }
    }


    //recyclerView of Products written by Himanshu
    public void settingRecyclerView(ArrayList<ProductModel> ItemsSelectedByUser) {
        rv_viewBill.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        viewBillReceiptProductAdapter = new ViewBillReceiptProductAdapter(this, ItemsSelectedByUser, dbHelper);
        rv_viewBill.setAdapter(viewBillReceiptProductAdapter);
    }

    //recyclerView of Taxes written by Himanshu
    public void settingRecyclerViewTaxes(String[] taxId, String[] taxAmount) {
        rv_Taxes.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        viewBillReceiptTaxesAdapter = new ViewBillReceiptTaxesAdapter(this, taxId, taxAmount, dbHelper);
        rv_Taxes.setAdapter(viewBillReceiptTaxesAdapter);
    }

    //cancel OR Refund Method written by Himanshu
    public void cancelRefund(String message, final int status, final int order_id) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (updateQuantity(status, order_id)) {
                            if (status == 1) {
                                imgStatus.setImageResource(R.drawable.cancelled);
                                imgStatus.setVisibility(View.VISIBLE);
                                if (AidlUtil.getInstance().initPrinter() == false) {
                                    ll_btnBottom_without.setVisibility(View.GONE);
                                } else {
                                    ll_btnBottom.setVisibility(View.GONE);
                                    btn_Reprintstate.setVisibility(View.VISIBLE);
                                }
                            } else if (status == 2) {
                                imgStatus.setImageResource(R.drawable.refunded);
                                imgStatus.setVisibility(View.VISIBLE);
                                if (AidlUtil.getInstance().initPrinter() == false) {
                                    ll_btnBottom_without.setVisibility(View.GONE);
                                } else {
                                    ll_btnBottom.setVisibility(View.GONE);
                                    btn_Reprintstate.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }
                });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_BackPress:
                onBackPressed();
                break;
            case R.id.btn_cancel:
                cancelRefund("Are you sure, You wanted to Cancel this Bill", 1, finalBill.getOrderModel().getOrder_id());
                break;

            case R.id.btn_Refund:
                cancelRefund("Are you sure, You wanted to Refund this Bill Amount", 2, finalBill.getOrderModel().getOrder_id());
                break;

            case R.id.btn_Reprint:
                Log.e("totalamt ", finalBill.getOrderModel().getAmtAfterTax() + "  "+finalBill.getOrderModel().getPayment_mode()+"  "+finalBill.getOrderModel().getBillStatus());
                try {
                    if (!finalBill.equals(null)) {
                        try {
                            Log.e("ViewBillActivity ", finalBill.getItemsSelectedByUser().size() + "");
                            UsePrinter usePrinter = new UsePrinter(this, finalBill);
                            usePrinter.printReceipt();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                }
                break;

            case R.id.btn_Reprintstate:
                if (!finalBill.equals(null)) {
                    try {
                        Log.e("ViewBillActivity ", finalBill.getItemsSelectedByUser().size() + " "+finalBill.getOrderModel().getPayment_mode()+" "+finalBill.getOrderModel().getBillStatus());
                        UsePrinter usePrinter = new UsePrinter(this, finalBill);
                        usePrinter.printReceipt();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.btn_cancel_without:
                btn_cancel.performClick();
                break;

            case R.id.btn_Refund_without:
                btn_Refund.performClick();
                break;
        }
    }

    //update Quantity when bill REFUND OR CANCEL
    private boolean updateQuantity(int status, int order_id) {
        boolean check = false;
        ArrayList<BillingModel> billingModelArrayList = dbHelper.getBillingId(order_id);
        for (int i = 0; i < billingModelArrayList.size(); i++) {
            if (dbHelper.updateProductQuantity(billingModelArrayList.get(i).getProduct_id(), billingModelArrayList.get(i).getTotal_quantity())) {
                if (dbHelper.change_Status(Order_Table_Name, "bill_status", "orderr_id", finalBill.getOrderModel().getOrder_id(), status)) {
                    check = true;
                } else
                    check = false;
            }
        }
        return check;
    }

    @Override
    public void onBackPressed() {
        if (check == 1) {
            Intent i = new Intent();
            i.putExtra("data", 1);
            setResult(Activity.RESULT_OK, i);
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
