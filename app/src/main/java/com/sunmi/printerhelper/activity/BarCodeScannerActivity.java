package com.sunmi.printerhelper.activity;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.DataBaseSunmi.TablesAndColumns;
import com.sunmi.barcodescanner.CaptureActivity;
import com.sunmi.barcodescanner.DecoratedBarcodeView;
import com.sunmi.printerhelper.Fragments.CartAltemsFrag;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.R;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Temp_Table_Name;
import static com.sunmi.printerhelper.activity.SmallCaptureActivity.barcodebtn;
import static com.sunmi.printerhelper.activity.SmallCaptureActivity.ll_ItemsClick;

/**
 * Custom Scannner Activity extending from Activity to display a custom layout form scanner view.
 */
public class BarCodeScannerActivity extends CaptureActivity {

    TextView txtNoOfItems, txtItems;
    DBHelper dbHelper;
    String TAG = "CustomScannerActivity";
    int count = 0;

    @Override
    protected DecoratedBarcodeView initializeContent() {
        setContentView(R.layout.activity_custom_scanner);
        return (DecoratedBarcodeView) findViewById(R.id.zxing_barcode_scanner);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new DBHelper(this);
        txtNoOfItems = findViewById(R.id.txtNoOfItems);
        txtItems = findViewById(R.id.txtItems);

        findViewById(R.id.ll_ItemsClick).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count == 0) {
                    Toast.makeText(BarCodeScannerActivity.this, "Please select items", Toast.LENGTH_SHORT).show();
                } else if (count > 0) {
                    Intent intent = new Intent(BarCodeScannerActivity.this, CartAltemsFrag.class);
                    startActivityForResult(intent, 100);
                    barcodebtn.setChecked(false);
                    finish();
                }
            }
        });
        findViewById(R.id.ll_viewItems).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        count = dbHelper.getTotalRow(Temp_Table_Name);
        Log.e(TAG + " count", count + "");
        if (count > 0) {
            txtNoOfItems.setText("Cart("+count+")");
            txtNoOfItems.setVisibility(View.VISIBLE);
            txtItems.setVisibility(View.GONE);
        }
        else {

            txtNoOfItems.setVisibility(View.GONE);
            txtItems.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        barcodebtn.setChecked(false);
        SharedPref.getInstance(this).setBarCodeScannerActivity(true);
        finish();
    }
}
