package com.sunmi.printerhelper.activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicaterMoveBackPayment;
import com.sunmi.Interfaces.CommunicatorCustomer_Tablet;
import com.sunmi.printerhelper.Adapter.BillingAdapter;
import com.sunmi.printerhelper.Adapter.CategoryAdapter_Tablet;
import com.sunmi.printerhelper.Adapter.ProductAdapter_Tablet;
import com.sunmi.printerhelper.Adapter.TaxAdapterTab;
import com.sunmi.printerhelper.Fragments.CashAndCardFrag;
import com.sunmi.printerhelper.Fragments.CustomerFrag;
import com.sunmi.printerhelper.Fragments.EmptyFrag1;
import com.sunmi.printerhelper.Fragments.EmptyFrag2;
import com.sunmi.printerhelper.Fragments.EmptyFragment;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.Model.OrderModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Temp_Tab_Table_Name;

public class BillingActivity extends AppCompatActivity implements CommunicatorCustomer_Tablet, CommunicaterMoveBackPayment {

    // recyclerview
    @BindViews({R.id.rv_Category, R.id.rv_BilingProoduct, R.id.rv_CartAlItems, R.id.rv_taxes})
    public List<RecyclerView> bk_RV_list;
    // linear layout
    @BindView(R.id.ll_discountClick)
    LinearLayout ll_discountClick;
    // textview
    @BindViews({R.id.txt_NoProdcutAvailble, R.id.searchh, R.id.txt_FinalAmt_PAY, R.id.txt_CustName, R.id.txt_ItemsAmount
            , R.id.txt_TotalItems, R.id.txt_DiscountPerc, R.id.txt_Discount, R.id.txtTime, R.id.txtDate})
    public List<TextView> bk_listTextView;
    // Linear layout
    @BindViews({R.id.ll_add_customer,R.id.ll_selected_customer,R.id.ll_upArrow, R.id.ll_Hide_Unhide})
    List<LinearLayout> bk_listLiner;
    @BindViews({R.id.ll_Discount})
    List<RelativeLayout> bk_listRelative;
    // image view
    @BindViews({R.id.img_UpDown_Arrow,R.id.img_deleteDiscunt})
    List<ImageView> bk_listImage;
    // relative layout
    @BindView(R.id.rl_PayNow)
    RelativeLayout rl_PayNow;
    // adapter class
    CategoryAdapter_Tablet categoryAdapter_tablet;
    ProductAdapter_Tablet productAdapter_tablet;
    BillingAdapter billingAdapter;
    TaxAdapterTab taxAdapter;
    // classes
    DBHelper dbHelper;
    ProductModel productModel;
    ArrayList<ProductModel> allProducts = new ArrayList<>();
    // variables, arrays, classes
    int come_Back = 0;
    String TAG = "BillingFragment", dis_editext = null;
    public int category_id = 0, search_Is_Active = 0, discount_check = 0, total_Items = 0, up_down = 0, cust_id = 0;
    double sub_Total = 0, dis_amt = 0, dis_perc = 0, amt_afterDiscount = 0;
    OrderModel orderModel;
    // fragment
    Fragment fragment;
    FragmentManager fm =getSupportFragmentManager();
    FragmentTransaction ft = fm.beginTransaction();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);
        ButterKnife.bind(this);
        if (MakeView.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            bk_listTextView.get(8).setText(MakeView.showCurrentTime());
            bk_listTextView.get(9).setText(MakeView.showCuurentDate());
        }
        dbHelper = new DBHelper(this);
        dbHelper.truncate_Table(Temp_Tab_Table_Name);
        allProducts = dbHelper.getProdcutSearched();
        Log.e("height", bk_RV_list.get(2).getMeasuredHeight()+"");
        settingRecyclerViewCategory();
        searchProducts();
        disable();
    }


    public void searchProducts()
    {
        bk_listTextView.get(1).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // category list invisible
                bk_RV_list.get(0).setVisibility(View.GONE);
                String searchedString = s.toString();
                Log.e(TAG, "onTextChanged: "+searchedString );
                ArrayList<ProductModel> newList = new ArrayList<>();

                if (!searchedString.equals("")) {
                    for (int i = 0; i < allProducts.size(); i++) {
                        if (allProducts.get(i).getPro_name().toLowerCase().contains(searchedString.toLowerCase())) {
                            newList.add(allProducts.get(i));
                        }
                    }
                    Log.e(TAG, "tttttt: " + newList.size() + "");
                    productAdapter_tablet.updateList(newList);
                }
                else
                {
                    Log.e("category_id ", category_id+"");
                    if (bk_RV_list.get(0).getVisibility() == GONE)
                    {
                        search_Is_Active = 1;
                        bk_RV_list.get(0).setVisibility(View.VISIBLE);
                        settingRecyclerViewCategory();
                    }
                } }

            @Override
            public void afterTextChanged(Editable s) { }}); }


    // category recyclerview
    private void settingRecyclerViewCategory() {
        categoryAdapter_tablet = new CategoryAdapter_Tablet(this,BillingActivity.this, dbHelper, dbHelper.getCategoryData(),0);
        bk_RV_list.get(0).setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        bk_RV_list.get(0).setAdapter(categoryAdapter_tablet); }

    // product recyclerview
    public void settingRecyclerViewProduct(ArrayList<ProductModel> product_list) {
        if (product_list.size() != 0){
            bk_listTextView.get(0).setVisibility(View.GONE);
            bk_RV_list.get(1).setVisibility(View.VISIBLE);
            productAdapter_tablet = new ProductAdapter_Tablet(this, BillingActivity.this, product_list, dbHelper);
            bk_RV_list.get(1).setLayoutManager(new GridLayoutManager(this, 2));
            bk_RV_list.get(1).setAdapter(productAdapter_tablet);  }
        else
        {
            bk_listTextView.get(0).setVisibility(View.VISIBLE);
            bk_RV_list.get(1).setVisibility(View.GONE);
        }}

    // billing recyclerview
    public void settingRecyclerViewBilling(ArrayList<ProductModel> list_selectedProdctForBiling) {
        billingAdapter = new BillingAdapter(BillingActivity.this,dbHelper,list_selectedProdctForBiling);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        bk_RV_list.get(2).setLayoutManager(mLayoutManager);
        bk_RV_list.get(2).setAdapter(billingAdapter);
        additional_Info(dbHelper.qty_subtotal_Temp());
    }



    // tax recyclerview
    public void settingRecyclerView_Tax(double amtAfterDiscount) {
        taxAdapter = new TaxAdapterTab(dbHelper.getSelectedTax(),BillingActivity.this,null,null,dbHelper,amtAfterDiscount,1);
        bk_RV_list.get(3).setLayoutManager(new LinearLayoutManager(this));
        bk_RV_list.get(3).setAdapter(taxAdapter);
        taxAdapter.notifyDataSetChanged();
    }


    // (VICKY LONGADGE)delete products selected for billing
    public void getItemDeleted(int pro_id) {
        dbHelper.delete_Pro_From_Table(pro_id,Temp_Tab_Table_Name, "pro_idd");
        SharedPref.getInstance(this).setDeletePro_ID(pro_id);
        productAdapter_tablet.notifyDataSetChanged();
        additional_Info(dbHelper.qty_subtotal_Temp());
    }

    // (VICKY LONGADGE)update selected qty and total amt of selected product
    public void getItemUpdate() {
        additional_Info( dbHelper.qty_subtotal_Temp());
    }

    public void additional_Info(ProductModel productModel)
    {
        total_Items = (int)productModel.getQty();
        sub_Total = productModel.getPrice();
        bk_listTextView.get(4).setText(UseSingleton.getInstance().decimalFormat(sub_Total));
        bk_listTextView.get(5).setText("("+UseSingleton.getInstance().decimalFormat(productModel.getQty())+")");

        // calculate discount
        discount(sub_Total,dis_editext,discount_check);
    }


    public void discount(double amt_before_Discount, String dis_editext, int discount_check)
    {
        if (total_Items == 0)
        {
            dis_editext = null;
            dis_amt = 0;
            dis_perc = 0;
            bk_listTextView.get(2).setText("0 SAR");
            disable();
            bk_listLiner.get(0).setVisibility(View.VISIBLE);
            up_down = 0;
            bk_listImage.get(0).setImageResource(R.drawable.up_arrow);
            bk_listLiner.get(3).setVisibility(View.GONE);
        }

        else
        {
            bk_listLiner.get(0).setEnabled(true);
            ll_discountClick.setEnabled(true);
            rl_PayNow.setEnabled(true);
            up_down = 1;
            bk_listImage.get(0).setImageResource(R.drawable.down_arrow);
            bk_listLiner.get(3).setVisibility(View.VISIBLE);
        }



        if (dis_editext == null)
        {
            bk_listRelative.get(0).setVisibility(View.GONE);
        }

        else {

            up_down = 1;
            bk_listLiner.get(3).setVisibility(View.VISIBLE);
            bk_listImage.get(0).setImageResource(R.drawable.down_arrow);


            bk_listRelative.get(0).setVisibility(View.VISIBLE);
            if (discount_check == 0)
            {
                dis_perc = Double.valueOf(dis_editext.toString());
                dis_amt = amt_before_Discount * dis_perc/100;
                bk_listTextView.get(6).setVisibility(View.VISIBLE);
            }

            else if (discount_check == 1)
            {
                dis_perc = 0;
                dis_amt = Double.valueOf(dis_editext.toString());
                bk_listTextView.get(6).setVisibility(View.INVISIBLE);
            }
            bk_listTextView.get(6).setText(UseSingleton.getInstance().decimalFormat(dis_perc)+"%");
            bk_listTextView.get(7).setText(UseSingleton.getInstance().decimalFormat(dis_amt));
        }

        calculateTax(sub_Total-dis_amt);

    }


    // calculate tax
    public void calculateTax(double amt_afterDiscount)
    {
        this.amt_afterDiscount = amt_afterDiscount;

        int[] taxCount = new int[3]; // [0] means total number of taxes, [1] means total number of InclusiveTaxes, [2] means total number of ExclusiveTaxes
        taxCount = dbHelper.searchInclusiveExclusive();
        // inclusive
        if (taxCount[0] == taxCount[1]) {
            if (discount_check == 0)
            {

                orderModel = new OrderModel(0,1,sub_Total,dis_perc,dis_amt,amt_afterDiscount,null,0.0,roundOff(amt_afterDiscount),null);
                Log.e("bill ",orderModel+"");
            }

            else if (discount_check == 1)
            {
                orderModel = new OrderModel(0,1,sub_Total,0,dis_amt,amt_afterDiscount,null,0.0,roundOff(amt_afterDiscount),null);
                Log.e("bill ",orderModel+"");

            }

            bk_listTextView.get(2).setText(roundOff(amt_afterDiscount)+" SAR");

        }
        // exclusive
        else if (taxCount[0] == taxCount[2]) {
            settingRecyclerView_Tax(amt_afterDiscount);

        }
    }

    // total tax amt in case of EXCLUSIVE
    public void total_Tax(String tax_id, String amt_EachTax,double tax_Amt, double total_TaxAmt)
    {
        double amt_afterAtx = amt_afterDiscount + total_TaxAmt;
        tax_id = tax_id.substring(5);
        amt_EachTax = amt_EachTax.substring(5);
        Log.e(TAG, "Total_Tax "+total_TaxAmt + "   tax id: "+tax_id + "   amt_EachTax: "+amt_EachTax);
        if (discount_check == 0)
        {
            orderModel = new OrderModel(0,2,sub_Total,dis_perc,dis_amt,amt_afterDiscount,tax_id,tax_Amt,roundOff(amt_afterAtx),amt_EachTax);
            Log.e("bill ",orderModel+"");
        }

        else if (discount_check == 1)
        { orderModel = new OrderModel(0,2,sub_Total,0,dis_amt,amt_afterDiscount,tax_id,tax_Amt,roundOff(amt_afterAtx),amt_EachTax);
            Log.e("billvv ",discount_check+""); }

        // total amount to pay
        bk_listTextView.get(2).setText(roundOff(amt_afterAtx)+" SAR");
    }


    @OnClick({R.id.ll_discountClick, R.id.ll_upArrow, R.id.ll_add_customer, R.id.ll_DeleteCustomer,
            R.id.ll_selected_customer, R.id.img_deleteDiscunt, R.id.rl_PayNow,R.id.img_home,R.id.img_inventery,R.id.img_purchase,R.id.img_receipt,R.id.img_billing})
    public void clickMe(View view)
    {
        Intent intent = null;
        switch (view.getId())
        {
            case R.id.ll_discountClick:
                discount_AlertDialog();
                break;

            case R.id.ll_upArrow:
                if (up_down == 0) {
                    up_down = 1;
                    bk_listLiner.get(3).setVisibility(View.VISIBLE);
                    bk_listImage.get(0).setImageResource(R.drawable.down_arrow);
                    break;
                }
                else if (up_down == 1){
                    up_down = 0;
                    bk_listLiner.get(3).setVisibility(View.GONE);
                    bk_listImage.get(0).setImageResource(R.drawable.up_arrow);
                    break;}
                break;

            case R.id.ll_add_customer:
                calling_CustomerFrag();
                break;

            case R.id.ll_DeleteCustomer:
                bk_listLiner.get(0).setVisibility(View.VISIBLE);
                bk_listLiner.get(1).setVisibility(View.GONE);
                cust_id = 0;
                break;

            case R.id.ll_selected_customer:
                calling_CustomerFrag();
                break;

            case R.id.img_deleteDiscunt:
                dis_editext = null;
                dis_perc = 0; dis_amt = 0; discount_check = 0;
                discount(sub_Total,dis_editext,discount_check);
                break;

            case R.id.rl_PayNow:
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                // cash and card fragment
                Bundle bundle = new Bundle();
                if (!orderModel.equals(""))
                    Log.e("rl_PayNow ",orderModel.getAmtAfterTax()+"");
                fragment = new CashAndCardFrag();
                bundle.putInt("cust_id",cust_id);
                bundle.putSerializable("orderModel",orderModel);
                fragment.setArguments(bundle);
                ft.replace(R.id.rl_billing, fragment, "CashCardFrag");
                ft.addToBackStack("null");
                // empty fragment
                fragment = new EmptyFragment();
                ft.replace(R.id.rl_parent1, fragment, "EmptyFrag");
                ft.addToBackStack("null");

                fragment = new EmptyFrag1();
                ft.replace(R.id.rl_Actionbar, fragment, "EmptyFrag1");
                ft.addToBackStack("null");
                fragment = new EmptyFrag2();
                ft.replace(R.id.ll_side_menu, fragment, "EmptyFrag2");
                ft.addToBackStack("null");
                ft.commit();
                break;

            case R.id.img_home:
                intent = new Intent(BillingActivity.this,HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            case R.id.img_inventery:
                intent = new Intent(BillingActivity.this,InventoryActivity.class);
                startActivity(intent);
                break;
            case R.id.img_purchase:
                intent = new Intent(BillingActivity.this,PurchaseNewActivity.class);
                startActivity(intent);
                break;
            case R.id.img_receipt:
                intent = new Intent(BillingActivity.this,ReceiptsActivity.class);
                startActivity(intent);
                break;
        }
    }


    public void calling_CustomerFrag()
    {
        Bundle bundle = null;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        bundle = new Bundle();
        bundle.putInt("goingTo",1);
        fragment = new CustomerFrag();
        fragment.setArguments(bundle);
        ft.replace(R.id.ll_billingParentLayout, fragment);
        ft.addToBackStack("null");
        ft.commit();
    }

    // (VICKY LONGADGE)get selected customer for billing
    @Override
    public void selected_Cust(CustomerDetailsModel customerDetailsModel) {
        if (customerDetailsModel != null){
            setCustomer(customerDetailsModel); }
    }

    public void setCustomer(CustomerDetailsModel customer)
    {
        bk_listLiner.get(0).setVisibility(View.GONE);
        bk_listLiner.get(1).setVisibility(View.VISIBLE);
        bk_listTextView.get(3).setText(customer.getCustomerName());
        cust_id = customer.getCustomerId();
    }

    public void discount_AlertDialog()
    {
        final AlertDialog dialog = new AlertDialog.Builder(this, R.style.AlertDialog).create();
        LayoutInflater inflater = LayoutInflater.from(this);
        View alertView = inflater.inflate(R.layout.dialog_discount, null);
        final EditText edtEnterQuantity = alertView.findViewById(R.id.edtEnterQuantity);
        edtEnterQuantity.setSelection(edtEnterQuantity.getText().length());
        final TextView txt_percent = alertView.findViewById(R.id.txt_percent);
        final TextView txt_sar = alertView.findViewById(R.id.txt_sar);
        TextView btnAddToCart = alertView.findViewById(R.id.btnAddToCart);
        TextView btnCancle = alertView.findViewById(R.id.btnCancle);

        if (dis_editext == null)
        {
            dis_perc = 0; dis_amt = 0; discount_check = 0;
        }

        if (dis_editext!= null)
        {
            edtEnterQuantity.setSelection(edtEnterQuantity.getText().length());
            edtEnterQuantity.setText(dis_editext);
            if (discount_check == 0)
            {
                txt_percent.setBackgroundResource(R.color.BABABA);
                txt_sar.setBackgroundResource(R.color.white);
            }
            else if (discount_check == 1)
            {
                txt_percent.setBackgroundResource(R.color.white);
                txt_sar.setBackgroundResource(R.color.BABABA);
            }
        }

        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtEnterQuantity.getText().toString().isEmpty()) {
                    Toast.makeText(BillingActivity.this, "Enter amount for discount", Toast.LENGTH_SHORT).show();
                }
                else if (Double.valueOf(edtEnterQuantity.getText().toString()) <= 0)
                {
                    Toast.makeText(BillingActivity.this, "Enter a valid discount", Toast.LENGTH_SHORT).show();
                }
                else {
                    dis_editext = edtEnterQuantity.getText().toString();
                    // discount in percent
                    if (discount_check == 0)
                    {
                        dis_perc = Double.valueOf(edtEnterQuantity.getText().toString());
                        discount(sub_Total,dis_editext,0);
                    }
                    // discount in amount
                    else if (discount_check == 1)
                    {
                        dis_perc = 0;
                        discount(sub_Total,dis_editext,1);
                    }
                    bk_listLiner.get(3).setVisibility(View.VISIBLE);
                    up_down = 1;
                    bk_listImage.get(0).setImageResource(R.drawable.down_arrow);
                    dialog.dismiss();
                } }
        });
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txt_percent.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                discount_check = 0;
                txt_percent.setBackgroundResource(R.color.BABABA);
                txt_sar.setBackgroundResource(R.color.white);
            }
        });
        txt_sar.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                discount_check = 1;
                txt_percent.setBackgroundResource(R.color.white);
                txt_sar.setBackgroundResource(R.color.BABABA);
            }
        });
        dialog.setView(alertView);
        dialog.show();
    }



    public String roundOff(double d)
    {
        return String.valueOf((int)Math.round(d));
    }

    public void disable()
    {
        bk_listLiner.get(0).setEnabled(false);
        rl_PayNow.setEnabled(false);
        ll_discountClick.setEnabled(false);
    }

    @Override
    public void onBackPressed() {

        // from confirmation screen
        if (come_Back == 2)
        {
//            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("EmptyFrag")).commit();
//            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("CashCardFrag")).commit();
//            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("ConfirmationFrag")).commit();
//            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("EmptyFrag1")).commit();
//            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("EmptyFrag2")).commit();
//            recreate();
//            finish();
            Intent intent = new Intent(BillingActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }

        // from viewBill frag
        else if (come_Back == 3)
        {
            getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("ViewBillFrag")).commit();
        }
        
        else if (come_Back == 1)
        {
            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("EmptyFrag")).commit();
            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("CashCardFrag")).commit();
            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("EmptyFrag1")).commit();
            getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentByTag("EmptyFrag2")).commit();

        }


        else {
            Intent intent = new Intent(BillingActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public void moveBack(int back) {
        come_Back = back;
    }
}
