package com.sunmi.printerhelper.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

import com.sunmi.printerhelper.Adapter.FullImageAdapter;
import com.sunmi.printerhelper.R;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;


public class Fullmage extends BaseActivity {

    ViewPager viewPager;
    private int currentPage = 0;
    private ArrayList<String> XMENArray = new ArrayList<String>();
    ArrayList<String> datumList;
//    int [] image;
    int pos;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_fullmage);
        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        init();
    }

    private void init() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            datumList = bundle.getStringArrayList("image_path");
        }
        for (int i = 0; i < datumList.size(); i++) {
            XMENArray.add(datumList.get(i));
        }
        Log.e("imagepathhhhh",XMENArray.size()+"");
        viewPager = findViewById(R.id.viewpager);

        viewPager.setAdapter(new FullImageAdapter(this, XMENArray));

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == datumList.size()) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 100000, 100000);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


    }
}
