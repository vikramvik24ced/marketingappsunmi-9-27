package com.sunmi.printerhelper.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Adapter.PurchaseAdapter;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.PurchaseModel;
import com.sunmi.printerhelper.R;


import org.json.JSONException;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class PurchaseActivity extends AppCompatActivity {

    @BindViews({R.id.no_product, R.id.txtDate, R.id.txtTime})
    List<TextView> textViewList;
    @BindView(R.id.purchaseRecycler)
    RecyclerView purchaseRecycler;
    @BindView(R.id.btnSavePurchase)
    Button btnSavePurchase;
    @BindViews({R.id.edtVendorName, R.id.edtPurchasedt, R.id.edtBillAmt, R.id.edit_Search})
    public List<EditText> editTextList;
    //Adapter
    PurchaseAdapter purchaseAdapter;
    //Model Classes
    PurchaseModel purchaseModel;
    Calendar calendar;
    //Database
    DBHelper dbHelper;
    //Variable
    String TAG = "AddPurchaseDetailsActivity", vendorName, purchasingDated;
    int purchasedAmt, yy, mm, dd, check = 1, purchaseIdforUpdate;
    //ArrayList
    ArrayList<String> arrayList, arrayList1;
    ArrayList<PurchaseModel> purchaseModelArrayList, tlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase);
        ButterKnife.bind(this);
        dbHelper = new DBHelper(this);
        textViewList.get(1).setText(MakeView.showCuurentDate());
        textViewList.get(2).setText(MakeView.showCurrentTime());
        purchaseModelArrayList = dbHelper.getPurchaseDtailsFromDB();
        calendar = Calendar.getInstance();
        yy = calendar.get(Calendar.YEAR);
        mm = calendar.get(Calendar.MONTH);
        dd = calendar.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    protected void onStart() {
        super.onStart();
        arrayList = new ArrayList<>();
        tlist = new ArrayList<>();
        settingRecyclerView();
        showDate(yy, mm, dd);
    }

    public void settingRecyclerView() {
//        purchaseAdapter = new PurchaseAdapter(this, dbHelper.getPurchaseDtailsFromDB(), dbHelper, PurchaseActivity.this);
//        purchaseRecycler.setLayoutManager(new GridLayoutManager(this, 2));
//        purchaseRecycler.setAdapter(purchaseAdapter);
    }

    //validate
    private boolean validation() {
        boolean check;
        try {
            vendorName = editTextList.get(0).getText().toString();
            purchasingDated = editTextList.get(1).getText().toString();
            purchasedAmt = Integer.parseInt(editTextList.get(2).getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (vendorName.isEmpty() || purchasingDated.isEmpty() || purchasedAmt == 0) {
            Toast.makeText(this, "Please Fill All Details", Toast.LENGTH_SHORT).show();
            check = false;
        } else {
            check = true;
        }

        return check;
    }

    //DatePicker written by himanshu
    public void datepicker(View v) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if (year > yy) {
                    showDate(yy, mm, dd);
                } else if (monthOfYear > mm && year == yy) {
                    showDate(yy, mm, dd);
                } else if (dayOfMonth > dd && monthOfYear == mm && year == yy) {
                    showDate(yy, mm, dd);

                } else {
                    showDate(year, monthOfYear, dayOfMonth);
                }
            }
        }, yy, mm, dd);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    //get current date written by himanshu
    private void showDate(int year, int month, int day) {
        String months = new DateFormatSymbols().getMonths()[month];
        Log.e("months", months);
        editTextList.get(1).setText(new StringBuilder().append(day).append(" ")
                .append(months).append(" ").append(year));
    }

    @OnClick({R.id.btnSavePurchase,R.id.img_home,R.id.img_inventery,R.id.img_purchase,R.id.img_receipt,R.id.img_billing})
    public void saveDetails(View view) {
        Intent intent = null;
        switch (view.getId())
        {
            case R.id.btnSavePurchase:
                if (validation()) {
                    if (check == 1) {
                        purchaseModel = new PurchaseModel(0, vendorName, purchasingDated, purchasedAmt);
                        int status = dbHelper.insertPurchaseDetails(purchaseModel, arrayList);
                        if (status>0) {
                            editTextList.get(0).setText("");
                            editTextList.get(2).setText("");
                            Toast.makeText(this, "Order Added Successfully", Toast.LENGTH_SHORT).show();
                            onStart();
                        } else {
                            Toast.makeText(this, "Your Order Not Added Successfully", Toast.LENGTH_SHORT).show();
                        }
                    }
                    //Update Purchase Info
                    else if (check == 2) {
                        try {
                            if (dbHelper.updatePurchaseDetails(purchaseIdforUpdate, vendorName, purchasingDated, purchasedAmt, null)) {
                                onStart();
                                Toast.makeText(this, "Successfully Update Details", Toast.LENGTH_SHORT).show();
                                editTextList.get(0).setText("");
                                editTextList.get(2).setText("");
                                editTextList.get(3).setText("");
                                check = 1;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

            case R.id.img_home:
                intent = new Intent(PurchaseActivity.this,HomeActivity.class);
                startActivity(intent);
                break;

            case R.id.img_billing:
                intent = new Intent(PurchaseActivity.this,BillingActivity.class);
                startActivity(intent);

            case R.id.img_inventery:
                intent = new Intent(PurchaseActivity.this,InventoryActivity.class);
                startActivity(intent);
                break;
            case R.id.img_purchase:
                break;
            case R.id.img_receipt:
                intent = new Intent(PurchaseActivity.this,ReceiptsActivity.class);
                startActivity(intent);
                break;

                
        }
       
        
        
        
        
    }

    //Update purchase Details
    public void editPurchaseDetails(PurchaseModel purchaseModel) {
        editTextList.get(0).setText(purchaseModel.getVendorName());
        editTextList.get(1).setText(purchaseModel.getPurchased_Date());
        editTextList.get(2).setText(UseSingleton.getInstance().decimalFormat(purchaseModel.getPurchaseAmount()));
        purchaseIdforUpdate = purchaseModel.getPurchasedId();
        check = 2;
    }

    // written by himanshu
    @OnTextChanged(value = R.id.edit_Search, callback = OnTextChanged.Callback.TEXT_CHANGED)
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        onStart();
        String name = s.toString();
        String vendorName;
        String[] amount = {null};
        if (!name.equals("")) {
            for (int i = 0; i < purchaseModelArrayList.size(); i++) {
                amount[0] = String.valueOf(purchaseModelArrayList.get(i).getPurchaseAmount());
                vendorName = purchaseModelArrayList.get(i).getVendorName();
                if (vendorName.toLowerCase().contains(name.toLowerCase()) || amount[0].toLowerCase().contains(name.toLowerCase())) {
                    tlist.add(purchaseModelArrayList.get(i));
                }
            }
            if (tlist.size() == 0) {
                tlist.clear();
                purchaseAdapter.search(tlist);
                textViewList.get(0).setVisibility(View.VISIBLE);
            } else {
                textViewList.get(0).setVisibility(View.GONE);
                purchaseAdapter.search(tlist);
                tlist.clear();
            }
        } else if (count == 0) {
            textViewList.get(0).setVisibility(View.GONE);
            settingRecyclerView();
        }
    }

    @Override
    public void onBackPressed() {
        try {
            vendorName = editTextList.get(0).getText().toString();
            purchasingDated = editTextList.get(2).getText().toString();//this is for amount purchasingDated is use for purchaseAmt
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (vendorName.isEmpty() && purchasingDated.isEmpty()) {
            Intent intent = new Intent(PurchaseActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Please Save your Modify Data, otherwise Your Data will be Lost", Toast.LENGTH_SHORT).show();
        }
    }
}
