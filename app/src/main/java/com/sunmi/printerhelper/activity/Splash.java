package com.sunmi.printerhelper.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.R;

public class Splash extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (MakeView.isTablet(this))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        setContentView(R.layout.activity_splash);
        RelativeLayout rl_getStart = findViewById(R.id.rl_getStart);
        rl_getStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_getStart:
                if (SharedPref.getInstance(this).getUserID() == null)
                {
                    startActivity(new Intent(this, LoginActivity.class)); }
                else {
                    startActivity(new Intent(this, HomeActivity.class));
                }
                finish();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
