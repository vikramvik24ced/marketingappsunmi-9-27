package com.sunmi.printerhelper.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.DataBaseSunmi.TablesAndColumns;
import com.sunmi.printerhelper.Fragments.TaxFrag;
import com.sunmi.printerhelper.Fragments.ViewBillFrag;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.Model.FinalBill;
import com.sunmi.printerhelper.Model.OrderModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.ReceiptsActivity;
import com.sunmi.printerhelper.activity.ViewBillActivity;

import java.util.ArrayList;
import java.util.Collections;


// ViewBillActivity


public class ReceiptAdapter extends RecyclerView.Adapter<ReceiptAdapter.MyViewHolder> {
    // database
    Context context;
    ArrayList<OrderModel> orderModelArrayList, mainList;
    FinalBill finalBill = null;
    DBHelper dbHelper;
    ReceiptsActivity receiptsActivity;
    String customer_name;
    int selected = 0;
    Fragment  fragment = null;

    public ReceiptAdapter(Context context, ArrayList<OrderModel> orderModelArrayList, DBHelper dbHelper, ReceiptsActivity receiptsActivity) {
        this.context = context;
        this.orderModelArrayList = orderModelArrayList;
        Collections.reverse(orderModelArrayList);
        this.dbHelper = dbHelper;
        this.receiptsActivity = receiptsActivity;
        mainList = dbHelper.getOrderDetails();

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_receipt, parent, false);
        return new MyViewHolder(v);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtFirstLetter, txtCustomerName, txtDate, txtBillNo, txtBillAmt, txtStatus;
        LinearLayout ll_container;

        MyViewHolder(View v) {
            super(v);
            txtFirstLetter = v.findViewById(R.id.txt_First_Letter);
            txtCustomerName = v.findViewById(R.id.txtCustomerName);
            txtDate = v.findViewById(R.id.txtBillingDate);
            txtBillNo = v.findViewById(R.id.txtBillNo);
            txtBillAmt = v.findViewById(R.id.txtAmount);
            txtStatus = v.findViewById(R.id.txtStatus);
            ll_container = v.findViewById(R.id.ll_container);
            ll_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (MakeView.isTablet(receiptsActivity))
                    {
                        selected = getAdapterPosition();
                        notifyDataSetChanged();
                    }
                    else
                    {
                        finalBill = dbHelper.getDataForBilling(orderModelArrayList.get(getAdapterPosition()).getOrder_id());
                        FragmentManager  fm = receiptsActivity.getSupportFragmentManager();
                        FragmentTransaction   ft = fm.beginTransaction();
                        fragment = new ViewBillFrag();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("finalBill",finalBill);
                        bundle.putInt("check",1);
                        fragment.setArguments(bundle);
                        ft.replace(R.id.fl_receiptParent, fragment, "ViewBillFrag");
                        ft.addToBackStack("");
                        ft.commit();
                    }

                }
            });
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try {
            CustomerDetailsModel customerDetailsModel = dbHelper.getCustomer(orderModelArrayList.get(position).getCus_Id());
            if (orderModelArrayList.get(position).getCus_Id() > 0) {
                customer_name = customerDetailsModel.getCustomerName();
            } else if (orderModelArrayList.get(position).getCus_Id() == 0) {
                customer_name = orderModelArrayList.get(position).getPayment_mode();
            }

            holder.txtCustomerName.setText(customer_name);
            String date = orderModelArrayList.get(position).getBillDate().substring(0, orderModelArrayList.get(position).getBillDate().length() - 11);
            holder.txtDate.setText(date);
            holder.txtBillAmt.setText(String.valueOf(orderModelArrayList.get(position).getAmtAfterTax())+" SAR");
            holder.txtBillNo.setText(String.valueOf(orderModelArrayList.get(position).getOrder_id()));
            holder.txtFirstLetter.setText(customer_name.substring(0, 1));
            Log.e(" statusAdpater", orderModelArrayList.get(position).getBillStatus() + "");
            if (orderModelArrayList.get(position).getBillStatus() > 0) {
                if (orderModelArrayList.get(position).getBillStatus() == 1) {
                    holder.txtStatus.setText(R.string.cancelled);
                    holder.txtStatus.setVisibility(View.VISIBLE);
                } else if (orderModelArrayList.get(position).getBillStatus() == 2) {
                    holder.txtStatus.setText(R.string.refunded);
                    holder.txtStatus.setVisibility(View.VISIBLE);
                    holder.txtStatus.setTextColor(context.getResources().getColor(R.color.refunded));
                }
            }

            // draw outline on selected receipt and display receipt
            if (MakeView.isTablet(receiptsActivity))
            {
                if (selected == position) {
                    holder.ll_container.setBackground(context.getResources().getDrawable(R.drawable.green_outline_tablet));
                    finalBill = dbHelper.getDataForBilling(orderModelArrayList.get(selected).getOrder_id());
                    FragmentManager  fm = receiptsActivity.getSupportFragmentManager();
                    FragmentTransaction   ft = fm.beginTransaction();
                    fragment = new ViewBillFrag();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("finalBill",finalBill);
                    bundle.putInt("check",1);
                    fragment.setArguments(bundle);
                    ft.replace(R.id.fl_receipt_tablet, fragment, "ViewBillFrag");
                    ft.commit();
                }
                else holder.ll_container.setBackground(context.getResources().getDrawable(R.drawable.white_outline));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void callingFragment()
    {

    }


    public void searchReceipt( ArrayList<OrderModel> new_orderModelArrayList)
    {
        orderModelArrayList = new ArrayList<>();
        orderModelArrayList.addAll(new_orderModelArrayList);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return orderModelArrayList.size();
    }
}
