package com.sunmi.printerhelper.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicatorCustomer_Tablet;
import com.sunmi.printerhelper.Fragments.CustomerFrag;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.R;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CustomerAdapter_Tablet extends RecyclerView.Adapter<CustomerAdapter_Tablet.MyViewHolderr> {


    ArrayList<CustomerDetailsModel> list_customer;
    CustomerDetailsModel customerDetailsModel;
    // variables, classes, fragment
    String TAG = "CustomerAdapter";
    public int comes_From, state = -1;
    CustomerFrag customerFrag;
    // interface
    CommunicatorCustomer_Tablet communicatorCustomer;
    // database
    DBHelper dbHelper;

    public CustomerAdapter_Tablet(CustomerFrag customerFrag, ArrayList<CustomerDetailsModel> list_customer, DBHelper dbHelper, int comes_From)
    {
        this.list_customer = list_customer;
        this.customerFrag = customerFrag;
        this.comes_From = comes_From;
        this.dbHelper = dbHelper;
    }

    @NonNull
    @Override
    public MyViewHolderr onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_customer_list, parent, false);
        return new MyViewHolderr(itemView); }

    public class MyViewHolderr extends RecyclerView.ViewHolder {

        // text view
        @BindViews({R.id.txtCustomerName,R.id.txtCustomerMobileNo,R.id.txtCustomerAddress,R.id.txt_firstLetter,
                R.id.txt_firstLetter_outline,R.id.txtCustomerName_outline,R.id.txtCustomerMobileNo_outline,R.id.txtCustomerAddress_outline})
        List<TextView> bk_listTextView;
        // card view
        @BindViews({R.id.cv_Customer,R.id.cv_Customer_outline})
        List<CardView> bk_listCardView;
        public MyViewHolderr(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick({R.id.cv_Customer,R.id.cv_Customer_outline})
        public void clickMe(View view)  {
            switch (view.getId())
            {
                case R.id.cv_Customer:
                    // customer frag
                    if (comes_From == 0)
                    {
                        try {
                            customerFrag.update_Customer(list_customer.get(getAdapterPosition()), getAdapterPosition()); } catch (JSONException e) { e.printStackTrace(); }
                        state = getAdapterPosition();
                        notifyDataSetChanged();
                    }
                    // billing
                    else if (comes_From == 1)
                    {
                        customerFrag.comes_From = 0;
                        communicatorCustomer = (CommunicatorCustomer_Tablet) customerFrag.getContext();
                        communicatorCustomer.selected_Cust(list_customer.get(getAdapterPosition()));
                        customerFrag.getFragmentManager().popBackStack();
                    }
                    break;

                // user unselect the selected customer
                case R.id.cv_Customer_outline:
                    try {
                        customerFrag.update_Customer(null, -1); } catch (JSONException e) { e.printStackTrace(); }
                    state = -1;
                    clearSharedPref();
                    notifyDataSetChanged();

                    break;
            }
        }
    }


    // =================================== VICKY LONGADGE ==========================================
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolderr holder, final int position) {

        customerDetailsModel = list_customer.get(position);
        // when user tries to update customer info
        if (SharedPref.getInstance(customerFrag.getContext()).getCustID() == customerDetailsModel.getCustomerId())
        {

            // when user clicks on save button then customer details will be updated in list
            if(customerFrag.save_clicked == 1) {
                holder.bk_listTextView.get(3).setText(String.valueOf(SharedPref.getInstance(customerFrag.getContext()).getCust_Name().charAt(0)).toUpperCase());
                holder.bk_listTextView.get(0).setText(SharedPref.getInstance(customerFrag.getContext()).getCust_Name());
                holder.bk_listTextView.get(1).setText(String.valueOf(SharedPref.getInstance(customerFrag.getContext()).getCust_Mobile()));
                holder.bk_listTextView.get(2).setText(SharedPref.getInstance(customerFrag.getContext()).getCust_Address());
                customerFrag.save_clicked = 0;
                try {
                    // update customer info in customer table
                    dbHelper.updateCustomer(SharedPref.getInstance(customerFrag.getContext()).getCustID(),
                            SharedPref.getInstance(customerFrag.getContext()).getCust_Name(), SharedPref.getInstance(customerFrag.getContext()).getCust_Address(),
                            SharedPref.getInstance(customerFrag.getContext()).getCust_Mobile(), SharedPref.getInstance(customerFrag.getContext()).getCust_EmailID()); } catch (JSONException e) { e.printStackTrace(); }
                // update list
                list_customer.set(position,new CustomerDetailsModel(SharedPref.getInstance(customerFrag.getContext()).getCustID(),
                        SharedPref.getInstance(customerFrag.getContext()).getCust_Name(), SharedPref.getInstance(customerFrag.getContext()).getCust_Address(),
                        SharedPref.getInstance(customerFrag.getContext()).getCust_Mobile(), SharedPref.getInstance(customerFrag.getContext()).getCust_EmailID()));
                // clearing  shared preferences
                clearSharedPref();
            }
        }
        else {
            holder.bk_listTextView.get(3).setText(String.valueOf(customerDetailsModel.getCustomerName().charAt(0)).toUpperCase());
            holder.bk_listTextView.get(0).setText(customerDetailsModel.getCustomerName());
            holder.bk_listTextView.get(1).setText(String.valueOf(customerDetailsModel.getCustomerMobileNumber()));
            holder.bk_listTextView.get(2).setText(customerDetailsModel.getCustomerAddress());
        }

        if (state != position) {
            holder.bk_listCardView.get(0).setVisibility(View.VISIBLE);
            holder.bk_listCardView.get(1).setVisibility(View.GONE);
        }

        // when user clicks on any customer in list
        else if (state == position)
        {
            holder.bk_listTextView.get(4).setText(String.valueOf(customerDetailsModel.getCustomerName().charAt(0)).toUpperCase());
            holder.bk_listTextView.get(5).setText(customerDetailsModel.getCustomerName());
            holder.bk_listTextView.get(6).setText(String.valueOf(customerDetailsModel.getCustomerMobileNumber()));
            holder.bk_listTextView.get(7).setText(customerDetailsModel.getCustomerAddress());
            holder.bk_listCardView.get(0).setVisibility(View.GONE);
            holder.bk_listCardView.get(1).setVisibility(View.VISIBLE);
        }
    }

    // (VICKY LONGADGE) search using edit text
    public void updateList(ArrayList<CustomerDetailsModel> newCustomerList) {
        list_customer.clear();
        Log.e(TAG,"new customer list size: "+newCustomerList.size()+"   ");
        if (newCustomerList.size() != 0) {
            customerFrag.bk_listText.get(2).setVisibility(View.GONE);
            list_customer = new ArrayList<>();
            list_customer.addAll(newCustomerList);
        }
        else
        {
            list_customer.clear();
            customerFrag.bk_listText.get(2).setVisibility(View.VISIBLE);
        }
        notifyDataSetChanged();
    }


    public void clearSharedPref()
    {
        SharedPref.getInstance(customerFrag.getContext()).setCustID(0);
        SharedPref.getInstance(customerFrag.getContext()).setCust_Name("");
        SharedPref.getInstance(customerFrag.getContext()).setCust_Mobile(0);
        SharedPref.getInstance(customerFrag.getContext()).setCust_EmailID("");
        SharedPref.getInstance(customerFrag.getContext()).setCust_Address("");
    }

    @Override
    public int getItemCount() {
        return list_customer.size();
    }

}
