package com.sunmi.printerhelper.Adapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Fragments.AddCustomerInfoFrag;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.R;

import java.util.ArrayList;

import com.sunmi.printerhelper.activity.CustomerActivity;

import java.util.Collections;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.MyViewHolderr> {
    // database
    DBHelper dbHelper;
    ArrayList<CustomerDetailsModel> customerDetailsModelArrayList, mainList;
    CustomerDetailsModel customerDetailsModel;
    CustomerActivity customerActivity;
    String object = null;


    public CustomerAdapter(ArrayList<CustomerDetailsModel> customerDetailsModelArrayList, CustomerActivity customerActivity, DBHelper dbHelper, String object) {
        this.customerDetailsModelArrayList = customerDetailsModelArrayList;
        this.customerActivity = customerActivity;
        Collections.reverse(customerDetailsModelArrayList);
        mainList = dbHelper.getCustomerListFromDataBase();
        this.object = object;
    }

    @NonNull
    @Override
    public MyViewHolderr onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_customer_list, parent, false);
        return new MyViewHolderr(itemView); }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolderr holder, final int position) {
        final int pos = position;
        dbHelper = new DBHelper(customerActivity);
        // init interface
        dbHelper = new DBHelper(customerActivity);
        customerDetailsModel = customerDetailsModelArrayList.get(position);
        holder.txtfirstLetter.setText(customerDetailsModel.getCustomerName().substring(0, 1));
        holder.txtCustomerName.setText(customerDetailsModel.getCustomerName());
        holder.txtCustomerMobileNo.setText(customerDetailsModel.getCustomerMobileNumber().toString());
        holder.txtCustomerAddress.setText(customerDetailsModel.getCustomerAddress());

        if (SharedPref.getInstance(customerActivity).getCustomerSelected() == customerDetailsModel.getCustomerId()) {
            holder.cv_CustomerInfo.setBackgroundResource(R.color.lightGreen);
        }
        else
        {
            holder.cv_CustomerInfo.setBackgroundResource(R.color.white);
        }
        if (object.equals("CartAltemsFrag")) {
            holder.cv_CustomerInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customerDetailsModel = customerDetailsModelArrayList.get(pos);
                    SharedPref.getInstance(customerActivity).setCustomerSelected(customerDetailsModel.getCustomerId());
                    // sending data back to CartAlItem Fragment
                    SharedPref.getInstance(customerActivity).setCustID(customerDetailsModel.getCustomerId());
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra("cust_ID", customerDetailsModel.getCustomerId());
                    customerActivity.setResult(100, resultIntent);
                    customerActivity.finish();

                }
            });
        } else if (object.equals("CustomerActivity")) {
            holder.cv_CustomerInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customerDetailsModel = customerDetailsModelArrayList.get(pos);
                    Fragment fragment = new AddCustomerInfoFrag();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("customerInfo", customerDetailsModel);
                    fragment.setArguments(bundle);
                    FragmentTransaction ft = customerActivity.getSupportFragmentManager().beginTransaction().replace(R.id.rl_container, fragment);
                    ft.commit();
                    ft.addToBackStack("");
                }
            });
        }
    }

    public class MyViewHolderr extends RecyclerView.ViewHolder {
        TextView txtCustomerName, txtCustomerMobileNo, txtCustomerAddress, txtfirstLetter;
        CardView cv_CustomerInfo;

        public MyViewHolderr(View itemView) {
            super(itemView);
            txtfirstLetter = itemView.findViewById(R.id.txt_firstLetter);
            txtCustomerName = itemView.findViewById(R.id.txtCustomerName);
            txtCustomerMobileNo = itemView.findViewById(R.id.txtCustomerMobileNo);
            txtCustomerAddress = itemView.findViewById(R.id.txtCustomerAddress);
            cv_CustomerInfo = itemView.findViewById(R.id.cv_CustomerInfo);
        }
    }

    public void searchCustomer( ArrayList<CustomerDetailsModel> newCustomerList)
    {
        customerDetailsModelArrayList = new ArrayList<>();
        customerDetailsModelArrayList.addAll(newCustomerList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return customerDetailsModelArrayList.size();
    }


}
