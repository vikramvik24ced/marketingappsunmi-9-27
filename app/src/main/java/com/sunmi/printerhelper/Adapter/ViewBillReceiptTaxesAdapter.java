package com.sunmi.printerhelper.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.TaxModel;
import com.sunmi.printerhelper.R;

public class ViewBillReceiptTaxesAdapter extends RecyclerView.Adapter<ViewBillReceiptTaxesAdapter.MyViewHolderr> {

    Context context;
    String[] taxAmount;
    String[] taxId;
    DBHelper dbHelper;

    public ViewBillReceiptTaxesAdapter(Context context, String[] taxId, String[] taxAmount, DBHelper dbHelper) {
        this.context = context;
        this.taxAmount = taxAmount;
        this.taxId = taxId;
        this.dbHelper = dbHelper;
    }

    public class MyViewHolderr extends RecyclerView.ViewHolder {
        TextView txtTaxName, txtTaxPerc, txtTaxAmount;

        public MyViewHolderr(View itemView) {
            super(itemView);
            txtTaxName = itemView.findViewById(R.id.txtTaxName);
            txtTaxPerc = itemView.findViewById(R.id.txtTaxPerc);
            txtTaxAmount = itemView.findViewById(R.id.txtTaxAmount);
        }
    }

    @NonNull
    @Override
    public MyViewHolderr onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tax_adapter, parent, false);
        return new ViewBillReceiptTaxesAdapter.MyViewHolderr(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolderr holder, int position) {
        int id = Integer.parseInt(taxId[position]);
        TaxModel taxModel = dbHelper.getTax_DB(id);
        holder.txtTaxName.setText(taxModel.getTaxname());
        holder.txtTaxPerc.setText(UseSingleton.getInstance().decimalFormat(taxModel.getTaxPercent())+"%");
        holder.txtTaxAmount.setText(taxDecimalFormat(Double.valueOf(taxAmount[position])));

    }


    public String taxDecimalFormat(double d)
    {
        int index = UseSingleton.getInstance().decimalFormat(d).indexOf(".");
        String s = null;
        if (index>0)
        {
            Log.e("p ","point found");
            s = String.format("%.2f",Double.valueOf(UseSingleton.getInstance().decimalFormat(d)));
        }

        else
        {
            Log.e("p ","point not found");
            s = UseSingleton.getInstance().decimalFormat(d);
        }

        return s;
    }

    @Override
    public int getItemCount() {
        return taxAmount.length;
    }
}
