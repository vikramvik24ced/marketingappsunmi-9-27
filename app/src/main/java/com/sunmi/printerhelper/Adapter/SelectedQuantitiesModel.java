package com.sunmi.printerhelper.Adapter;

public class SelectedQuantitiesModel {

    int SelectedQuantity;

    public int getSelectedQuantity() {
        return SelectedQuantity;
    }

    public void setSelectedQuantity(int selectedQuantity) {
        SelectedQuantity = selectedQuantity;
    }
}
