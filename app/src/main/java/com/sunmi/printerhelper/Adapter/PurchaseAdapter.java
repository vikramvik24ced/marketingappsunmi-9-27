package com.sunmi.printerhelper.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicatorPurchase;
import com.sunmi.printerhelper.Fragments.AddPurchaseOrder;
import com.sunmi.printerhelper.Fragments.PurchasedFragment;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.Model.OrderModel;
import com.sunmi.printerhelper.Model.PurchaseModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.AddPurchaseDetailtsActivity;
import com.sunmi.printerhelper.activity.Fullmage;
import com.sunmi.printerhelper.activity.PurchaseActivity;
import com.sunmi.printerhelper.activity.PurchaseNewActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Purchased_Table_Name;


/**
 * Created by DesignOWeb on 5/15/2018.
 */
public class PurchaseAdapter extends RecyclerView.Adapter<PurchaseAdapter.PurchaseHolder> {

    private PurchaseNewActivity purchaseNewActivity;
    private Context context;
    public ArrayList<PurchaseModel> purchaseModelArrayList;
    DBHelper dbHelper;
    ArrayList<String> imagePath = new ArrayList<>();
    AddPurchaseOrder addPurchaseOrder;

    // how to maintain the state of previously selected order.
    int state = -1, previous_state = -2;// when you click on any order to update it

    public PurchaseAdapter(PurchaseNewActivity purchaseNewActivity,Context context, ArrayList<PurchaseModel> purchaseModelArrayList, DBHelper dbHelper, AddPurchaseOrder addPurchaseOrder) {
        this.purchaseNewActivity = purchaseNewActivity;
        this.context = context;
        this.purchaseModelArrayList = purchaseModelArrayList;
        Collections.reverse(purchaseModelArrayList);
        this.dbHelper = dbHelper;
        this.addPurchaseOrder = addPurchaseOrder;
    }

    @NonNull
    @Override
    public PurchaseHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_purchaser, parent, false);
        return new PurchaseHolder(v);
    }


    class PurchaseHolder extends RecyclerView.ViewHolder {

        @BindViews({R.id.vendorName, R.id.purchaseDated, R.id.purchaseAmt, R.id.txtCount, R.id.txtCount_radius,R.id.vendorName_radius,
                R.id.purchaseAmt_radius,R.id.purchaseDated_radius})
        List<TextView> textViewList;
        @BindView(R.id.imgBill)
        ImageView imgBill;
        @BindViews({R.id.cv_PurchaseRow,R.id.cv_PurchaseRow_Radius})
        List<CardView> bk_listcardView ;
        @BindViews({R.id.rl_imgBill})
        List<RelativeLayout> bk_listRelative;

        PurchaseHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
        //---------------------------------------------------------------OnClick Method ---------------------------------------------------------------//
        @OnClick({R.id.rl_PurchaseDelete, R.id.rl_imgBill, R.id.ll_PurchaseRow, R.id.ll_PurchaseRow_Radius})
        protected void onClick(View view) {
            Intent intent;
            switch (view.getId()) {
                case R.id.rl_PurchaseDelete:
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.alertDialogLarger);
                    alertDialogBuilder.setMessage("Do you want delete this Orders Details ?");
                    alertDialogBuilder.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    int purchase_id = purchaseModelArrayList.get(getAdapterPosition()).getPurchasedId();
                                    dbHelper.delete_Pro_From_Table(purchase_id,"Purchase", "purchase_id");
                                    dbHelper.delete_Pro_From_Table(purchase_id,"Purchase_Image", "image_id");
                                    purchaseModelArrayList.remove(getAdapterPosition());
                                    purchaseNewActivity.delete_order(1, getAdapterPosition());
                                    notifyItemRemoved(getAdapterPosition());
                                }});
                    alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override public void onClick(DialogInterface dialog, int which) { }});
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                    TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);
                    Button button2 = (Button)alertDialog.findViewById(android.R.id.button2);
                    Button button1 = (Button)alertDialog.findViewById(android.R.id.button1);
                    if (!MakeView.isTablet(purchaseNewActivity))
                    {
                        textView.setTextSize(15);
                        button2.setTextSize(15);
                        button1.setTextSize(15);
                    }
                    else
                    {
                        textView.setTextSize(25);
                        button2.setTextSize(20);
                        button1.setTextSize(20);
                    }
                    break;

                case R.id.rl_imgBill:
                    intent = new Intent(context, Fullmage.class);
                    intent.putStringArrayListExtra("image_path", dbHelper.getImage(purchaseModelArrayList.get(getAdapterPosition()).getPurchasedId()));
                    context.startActivity(intent);
                    break;

                case R.id.ll_PurchaseRow:
                    // ==============  TABLET  =============== //
                    if (MakeView.isTablet(context)) {

                        if (state != -1)
                        {
                            previous_state = state;
                            state = getAdapterPosition();
                            bk_listRelative.get(0).setEnabled(false);
                        }
                        else if (state == -1)
                        {
                            state = getAdapterPosition();
                        }

                        notifyDataSetChanged();

                    }     // ==============  SMART PHONE  =============== //
                    else {
                        PurchaseModel purchaseModel = dbHelper.getPurchaseRow(purchaseModelArrayList.get(getAdapterPosition()).getPurchasedId());
                        FragmentManager fm = purchaseNewActivity.getSupportFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();
                        AddPurchaseOrder addPurchaseOrder = new AddPurchaseOrder();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("edit_order",purchaseModel);
                        addPurchaseOrder.setArguments(bundle);
                        ft.replace(R.id.rl_purchaseContainer, addPurchaseOrder,"PurchaseOrderFrag");
                        ft.addToBackStack("null");
                        ft.commit();
                    }
                    break;

                case R.id.ll_PurchaseRow_Radius:
                    state = -1;
                    previous_state = -2;
                    bk_listcardView.get(0).setVisibility(View.VISIBLE);
                    bk_listcardView.get(1).setVisibility(View.GONE);
                    purchaseNewActivity.searchDetails.setEnabled(true);
                    bk_listRelative.get(0).setEnabled(true);
                    addPurchaseOrder.clearFields();
                    break;

            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final PurchaseHolder holder, final int position) {

        holder.textViewList.get(0).setText(purchaseModelArrayList.get(position).getVendorName());
        holder.textViewList.get(5).setText(purchaseModelArrayList.get(position).getVendorName());
        holder.textViewList.get(1).setText(purchaseModelArrayList.get(position).getPurchased_Date());
        holder.textViewList.get(7).setText(purchaseModelArrayList.get(position).getPurchased_Date());
        holder.textViewList.get(2).setText(UseSingleton.getInstance().decimalFormat(purchaseModelArrayList.get(position).getPurchaseAmount()) + " SAR");
        holder.textViewList.get(6).setText(UseSingleton.getInstance().decimalFormat(purchaseModelArrayList.get(position).getPurchaseAmount()) + " SAR");
        // image path
        imagePath =  dbHelper.getImage(purchaseModelArrayList.get(position).getPurchasedId());
        holder.textViewList.get(3).setText("+"+imagePath.size());
        holder.textViewList.get(4).setText("+"+imagePath.size());


        if (MakeView.isTablet(purchaseNewActivity))
        {
            if (state == position) // highlight the order which is selected
            {
                holder.bk_listcardView.get(0).setVisibility(View.GONE);
                holder.bk_listcardView.get(1).setVisibility(View.VISIBLE);
                holder.bk_listRelative.get(0).setEnabled(false);
                purchaseNewActivity.searchDetails.setEnabled(false);
                addPurchaseOrder.edit_Order(purchaseModelArrayList.get(state));
            }
            if (previous_state == position) // unselect previously selected order
            {
                holder.bk_listcardView.get(0).setVisibility(View.VISIBLE);
                holder.bk_listcardView.get(1).setVisibility(View.GONE);
                holder.bk_listRelative.get(0).setEnabled(true);
            }

        }


    }





    //---------------------------------------------------------------Searching Purchase ORder---------------------------------------------------------------//
    public void search(ArrayList<PurchaseModel> newPurchaseModelArrayList) {
        purchaseModelArrayList = new ArrayList<>();
        purchaseModelArrayList.addAll(newPurchaseModelArrayList);
        //  Collections.reverse(purchaseModelArrayList);
        notifyDataSetChanged();
    }


    public void updateList(PurchaseModel purchaseModel)
    {
        purchaseModelArrayList.add(purchaseModel);
        Collections.reverse(purchaseModelArrayList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return purchaseModelArrayList.size();
    }
}
