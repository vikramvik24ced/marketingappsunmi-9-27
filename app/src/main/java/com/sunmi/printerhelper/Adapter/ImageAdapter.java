package com.sunmi.printerhelper.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Fragments.AddPurchaseOrder;
import com.sunmi.printerhelper.Helper.FullImageDialog;

import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.AddPurchaseDetailtsActivity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageHolder> {

    AddPurchaseOrder addPurchaseOrder;
    ArrayList<String> list_imgPath;
    DBHelper dbHelper;

    public ImageAdapter(AddPurchaseOrder addPurchaseOrder, ArrayList<String> list_imgPath, DBHelper dbHelper) {
        this.addPurchaseOrder = addPurchaseOrder;
        this.list_imgPath = list_imgPath;
        this.dbHelper = dbHelper;
    }

    @NonNull
    @Override
    public ImageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_image, parent, false);
        return new ImageHolder(v);
    }

    class ImageHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView img_bill;

        ImageHolder(View view) {
            super(view);
            img_bill = view.findViewById(R.id.img_bill);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {


            }
        }
    }



    @Override
    public void onBindViewHolder(@NonNull ImageHolder holder, final int position) {

        Glide.with(addPurchaseOrder)
                .load(list_imgPath.get(position))
                .into(holder.img_bill);
    }


    public Bitmap uri_into_bitmap(Uri uri)
    {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(addPurchaseOrder.getActivity().getContentResolver(), uri);
        } catch (IOException e) { e.printStackTrace(); }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        // reducing the size of bit map image
        bitmap.compress(Bitmap.CompressFormat.JPEG, 10, stream);
        byte[] byteArray = stream.toByteArray();
        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

       return compressedBitmap ;
    }


    @Override
    public int getItemCount() {
        return list_imgPath.size();
    }
}