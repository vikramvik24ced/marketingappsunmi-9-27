package com.sunmi.printerhelper.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.printerhelper.Model.SaleTypeModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.AddProductActivity;
import com.sunmi.printerhelper.activity.InventoryActivity;

import java.util.ArrayList;

import static com.sunmi.printerhelper.activity.AddProductActivity.edtBarCode;


public class SoldByAdapter extends RecyclerView.Adapter<SoldByAdapter.MyViewHolder> {

    Context context;
    private ArrayList<SaleTypeModel> saleTypeModelArrayList;
    public int lastSelectedPosition = -1, sold_Id, check;
    AddProductActivity addProductActivity;
    InventoryActivity inventoryActivity;
    String soldName;


    // smart phone
    public SoldByAdapter(Context context, ArrayList<SaleTypeModel> saleTypeModelArrayList, AddProductActivity addProductActivity, int sold_Id, String soldName, int check) {
        this.context = context;
        this.saleTypeModelArrayList = saleTypeModelArrayList;
        this.addProductActivity = addProductActivity;
        this.sold_Id = sold_Id;
        this.soldName = soldName;
        this.check = check;
    }

    // tablet
    public SoldByAdapter(Context context, ArrayList<SaleTypeModel> saleTypeModelArrayList, InventoryActivity inventoryActivity, int sold_Id, String soldName, int check) {
        this.context = context;
        this.saleTypeModelArrayList = saleTypeModelArrayList;
        this.inventoryActivity = inventoryActivity;
        this.sold_Id = sold_Id;
        this.soldName = soldName;
        this.check = check;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout ll_soldBy;
        RadioButton rb_soldBy;
        TextView txtSoldby;

        public MyViewHolder(View itemView) {
            super(itemView);
            ll_soldBy = itemView.findViewById(R.id.ll_soldBy);
            rb_soldBy = itemView.findViewById(R.id.rb_soldBy);
            txtSoldby = itemView.findViewById(R.id.txtSoldby);
//            if (edtBarCode.getText().length() == 0) {
//                rb_soldBy.setEnabled(false);
//            } else {
            lastSelectedPosition = 0;
            rb_soldBy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    Log.e("Index", lastSelectedPosition + "");
                    notifyDataSetChanged();
                }
            });
//            }
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewInflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_soldby, parent, false);
        return new MyViewHolder(viewInflate);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        int pos = position + 1;
        if (check == 0) {
            if (sold_Id == pos) {
                holder.rb_soldBy.setChecked(true);
                holder.txtSoldby.setText(saleTypeModelArrayList.get(position).getSaleType());
            } else {
                holder.txtSoldby.setText(saleTypeModelArrayList.get(position).getSaleType());
            }
            if (saleTypeModelArrayList.size() - 1 == position) {
                check = 1;
            }
        } else if (check == 1) {
            holder.rb_soldBy.setChecked(lastSelectedPosition == position);
            holder.txtSoldby.setText(saleTypeModelArrayList.get(position).getSaleType());
        }
    }

    @Override
    public int getItemCount() {
        return saleTypeModelArrayList.size();
    }
}
