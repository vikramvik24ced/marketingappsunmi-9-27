package com.sunmi.printerhelper.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.BillingActivity;
import com.sunmi.printerhelper.activity.HomeActivity;
import com.sunmi.printerhelper.activity.InventoryActivity;
import com.sunmi.printerhelper.activity.PurchaseActivity;
import com.sunmi.printerhelper.activity.ReceiptsActivity;
import com.sunmi.printerhelper.activity.ReportActivity;
import com.sunmi.printerhelper.activity.SmallCaptureActivity;

public class SideMenuAdapter extends RecyclerView.Adapter<SideMenuAdapter.MyViewHolder> {

    private int[] images;
    private Context context;

    public SideMenuAdapter(int[] images, Context context) {
        this.images = images;
        this.context = context;
    }

    @NonNull
    @Override
    public SideMenuAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(null, parent, false);
        return new SideMenuAdapter.MyViewHolder(view);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgSideNav;

        public MyViewHolder(View itemView) {
            super(itemView);
//            imgSideNav = itemView.findViewById(R.id.imgSideNav);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull SideMenuAdapter.MyViewHolder holder, int position) {
        final int pos = position;
        holder.imgSideNav.setImageResource(images[position]);
        holder.imgSideNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                switch (pos) {
                    case 0:
                        intent = new Intent(context, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        break;

                    case 1:
                        Intent intent2=new Intent(context, BillingActivity.class);
                        intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        context.startActivity(intent2);
                       break;

                    case 2:
                        intent = new Intent(context, InventoryActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        context.startActivity(intent);
                        ((Activity) context).finish();
                        break;
                    case 3:
//                        intent = new Intent(context, PurchaseActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                        context.startActivity(intent);
//                        ((Activity) context).finish();
                        break;
                    case 4:
//                        intent = new Intent(context, ReceiptsActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                        context.startActivity(intent);
//                        ((Activity) context).finish();
                        break;
                    case 5:
//                        intent = new Intent(context, ReportActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                        context.startActivity(intent);
//                        ((Activity) context).finish();
                        break;
                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return images.length;
    }
}
