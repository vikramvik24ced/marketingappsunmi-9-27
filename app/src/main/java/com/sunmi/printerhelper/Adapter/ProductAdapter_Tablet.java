package com.sunmi.printerhelper.Adapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.BillingActivity;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindViews;
import butterknife.ButterKnife;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Temp_Tab_Table_Name;

public class ProductAdapter_Tablet extends RecyclerView.Adapter<ProductAdapter_Tablet.MyViewHolder> {

    private Context context;
    //array
    ArrayList<ProductModel> product_list;
    ArrayList<ProductModel> productFrom_TempTable = new ArrayList<>();
    // vairables
    String TAG = "ProductAdapter";
    int check = 0, frag;
    // classes
    DBHelper dbHelper;
    ProductModel productModel;
    BillingActivity billingActivity;

    public ProductAdapter_Tablet(Context context, BillingActivity billingActivity, ArrayList<ProductModel> product_list, DBHelper dbHelper) {
        this.context = context;
        this.product_list = product_list;
        this.billingActivity = billingActivity;
        this.dbHelper = dbHelper;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewInflate = null;
       if (MakeView.isTablet(context))
        viewInflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product, parent, false);
        return new MyViewHolder(viewInflate);
    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardView cd_CardView_noRadius, cd_CardView_Radius;
        @BindViews({R.id.txtProductName, R.id.txtProductName_Radius,R.id.txtAvailableQuantity, R.id.txtAvailableQuantity_Radius
                , R.id.txtRupees,R.id.txtPrice_Radius,R.id.txt_firstLetter,R.id.txt_firstLetter_Radius, R.id.txtCategoryName, R.id.txt_category_name_radius})
        List<TextView> bk_listTextView;
        MyViewHolder(View itemView) {
            super(itemView);
            cd_CardView_noRadius = itemView.findViewById(R.id.cd_CardView_noRadius);
            cd_CardView_Radius = itemView.findViewById(R.id.cd_CardView_Radius);
            cd_CardView_noRadius.setOnClickListener(this);
            cd_CardView_Radius.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.cd_CardView_noRadius:
                        cd_CardView_noRadius.setVisibility(View.GONE);
                        cd_CardView_Radius.setVisibility(View.VISIBLE);
                        temp_table(product_list.get(getAdapterPosition()), 0, getAdapterPosition());
                    break;

                case R.id.cd_CardView_Radius:

                        cd_CardView_noRadius.setVisibility(View.VISIBLE);
                        cd_CardView_Radius.setVisibility(View.GONE);
                        temp_table(product_list.get(getAdapterPosition()), 1, getAdapterPosition());
                     break; }
        }
    }

    public void temp_table(ProductModel selected_product, int add_remove, int pos)
    {
        if (add_remove == 0) {
            dbHelper.insertIntoTemp_TabletVicky(selected_product.getPro_id(), selected_product.getPro_name(), 1, selected_product.getPrice(),selected_product.getPrice());
            billingActivity.settingRecyclerViewBilling(dbHelper.getFromTemp_TabletVicky());
        }
        else if (add_remove == 1)
        {
            dbHelper.delete_Pro_From_Table(selected_product.getPro_id(),Temp_Tab_Table_Name, "pro_idd");
            billingActivity.settingRecyclerViewBilling(dbHelper.getFromTemp_TabletVicky());
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        productModel = product_list.get(position);

        // FIRST LETTER of Product name
        holder.bk_listTextView.get(6).setText(String.valueOf(productModel.getPro_name().charAt(0)).toUpperCase());
        holder.bk_listTextView.get(7).setText(String.valueOf(productModel.getPro_name().charAt(0)).toUpperCase());
        // product name
        holder.bk_listTextView.get(0).setText(productModel.getPro_name());
        holder.bk_listTextView.get(1).setText(productModel.getPro_name());
        // product quantity
        if (String.valueOf(productModel.getQty()).indexOf("-") >= 0) {
            holder.bk_listTextView.get(2).setTextColor(context.getResources().getColor(android.R.color.holo_red_light));
            holder.bk_listTextView.get(3).setTextColor(context.getResources().getColor(android.R.color.holo_red_light));
        }
        holder.bk_listTextView.get(2).setText(UseSingleton.getInstance().decimalFormat(Double.valueOf(productModel.getQty())));
        holder.bk_listTextView.get(3).setText(UseSingleton.getInstance().decimalFormat(Double.valueOf(productModel.getQty())));
        // product price
        holder.bk_listTextView.get(4).setText(UseSingleton.getInstance().decimalFormat(productModel.getPrice()));
        holder.bk_listTextView.get(5).setText(UseSingleton.getInstance().decimalFormat(productModel.getPrice()));
        // category name

        holder.bk_listTextView.get(8).setText(dbHelper.getCategoryName(productModel.getCategory_id()));
        holder.bk_listTextView.get(9).setText(dbHelper.getCategoryName(productModel.getCategory_id()));

        // Billing
        if (frag == 0) {
            // highlight only those products which are available in temp table
            if (dbHelper.count_Table(Temp_Tab_Table_Name) > 0) {
                productFrom_TempTable = dbHelper.getFromTemp_TabletVicky();
                for (int i = 0; i < productFrom_TempTable.size(); i++) {
                    if (productFrom_TempTable.get(i).getPro_id() == productModel.getPro_id()) {
                        holder.cd_CardView_noRadius.setVisibility(View.GONE);
                        holder.cd_CardView_Radius.setVisibility(View.VISIBLE);
                        break;
                    } else {
                        holder.cd_CardView_noRadius.setVisibility(View.VISIBLE);
                        holder.cd_CardView_Radius.setVisibility(View.GONE);
                    }
                }
            } else {
                holder.cd_CardView_noRadius.setVisibility(View.VISIBLE);
                holder.cd_CardView_Radius.setVisibility(View.GONE);
            }
        }
    }


    // (VICKY LONGADGE) search using edit text
    public void updateList(ArrayList<ProductModel> newProductList) {
        Log.e(TAG,"new product list size: "+newProductList.size()+"   ");

        if (newProductList.size() == 0)
        {
            billingActivity.bk_listTextView.get(0).setVisibility(View.VISIBLE);
            billingActivity.bk_RV_list.get(1).setVisibility(View.GONE);
            product_list.clear();
        }
        else {
            billingActivity.bk_listTextView.get(0).setVisibility(View.GONE);
            billingActivity.bk_RV_list.get(1).setVisibility(View.VISIBLE);
            product_list = new ArrayList<>();
            product_list.addAll(newProductList);
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return product_list.size();
    }

}

