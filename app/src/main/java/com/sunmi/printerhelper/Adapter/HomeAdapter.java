package com.sunmi.printerhelper.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.activity.BillingActivity;
import com.sunmi.printerhelper.activity.CustomerActivity;
import com.sunmi.printerhelper.activity.ProfileActivity;
import com.sunmi.printerhelper.activity.PurchaseActivity;
import com.sunmi.printerhelper.activity.PurchaseNewActivity;
import com.sunmi.printerhelper.activity.ReceiptsActivity;
import com.sunmi.printerhelper.Fragments.ReportFargment;
import com.sunmi.printerhelper.Fragments.PurchasedFragment;

import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.ReportActivity;
import com.sunmi.printerhelper.activity.SettingActivity;
import com.sunmi.printerhelper.activity.SmallCaptureActivity;
import com.sunmi.printerhelper.activity.InventoryActivity;

import java.util.ArrayList;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    ArrayList<Integer> homeInfo = new ArrayList<>();
    Context context;
    Fragment fragment;
    int[] img;
    String[] title;

    public HomeAdapter(Context context, int[] img, String[] title, ArrayList<Integer> homeInfo) {
        this.context = context;
        this.img = img;
        this.title = title;
        this.homeInfo = homeInfo;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtTitle, txt_record;
        CardView cv_CardView;
        ImageView img_home;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.txt_Tittle);
            txt_record = itemView.findViewById(R.id.txt_record);
            cv_CardView = itemView.findViewById(R.id.cv_CardView);
            txt_record = itemView.findViewById(R.id.txt_record);
            img_home = itemView.findViewById(R.id.img_h);
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_rv, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final int pos = position;
        holder.img_home.setImageResource(img[position]);
        holder.txtTitle.setText(title[position]);

        if (position >= 4) holder.txt_record.setVisibility(View.GONE);

        try {
            switch (position) {
                case 0:
                    holder.txt_record.setText(homeInfo.get(position) + " Bills");
                    break;
                case 1:
                    holder.txt_record.setText(homeInfo.get(position) + " Products");
                    break;
                case 2:
                    holder.txt_record.setText(homeInfo.get(position) + " Orders");
                    break;
                case 3:
                    holder.txt_record.setText(homeInfo.get(position) + " Receipts");
                    break;
                case 4:
                    break;

                case 5:
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        //click event
        holder.cv_CardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                FragmentManager fm = activity.getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                switch (pos) {
                    case 0:
                        if (MakeView.isTablet(context))
                        {
                            context.startActivity(new Intent(context, BillingActivity.class));
                        }
                        else {
                        context.startActivity(new Intent(context, SmallCaptureActivity.class));
                        SharedPref.getInstance(context).setCustID(0);
                        SharedPref.getInstance(context).setDiscount(0);
                        SharedPref.getInstance(context).setDiscountType(0);
                        SharedPref.getInstance(context).setCustomerSelected(0);   }
                        break;

                    case 1:
                        context.startActivity(new Intent(context, InventoryActivity.class));
                        break;

                    case 2:
                        if (MakeView.isTablet(context)) {
                            context.startActivity(new Intent(context, PurchaseNewActivity.class));
                        } else {

                            context.startActivity(new Intent(context, PurchaseNewActivity.class));
//                            fragment = new PurchasedFragment();
//                            ft.replace(R.id.rl_frame, fragment);
//                            ft.addToBackStack("null");
//                            ft.commit();
                        }
                        break;

                    case 3:
                        context.startActivity(new Intent(context, ReceiptsActivity.class));
                        break;

                    case 4:
                        if (MakeView.isTablet(context)) {
                            context.startActivity(new Intent(context, ReportActivity.class));
                        } else {
                            fragment = new ReportFargment();
                            ft.replace(R.id.rl_frame, fragment);
                            ft.addToBackStack("");
                            ft.commit();
                        }
                        break;

                    case 5:
                        if (MakeView.isTablet(context)) {
                            context.startActivity(new Intent(context, CustomerActivity.class));
                        } else {
                            context.startActivity(new Intent(context, SettingActivity.class));
                        }
                        break;
                    case 6:
                        context.startActivity(new Intent(context, ProfileActivity.class));
                        break;
                    case 7:
                        context.startActivity(new Intent(context, SettingActivity.class));
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return title.length;
    }
}

