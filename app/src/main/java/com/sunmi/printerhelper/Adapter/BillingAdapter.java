package com.sunmi.printerhelper.Adapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.BillingActivity;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BillingAdapter extends RecyclerView.Adapter<BillingAdapter.MyViewHolder> {

    ArrayList<ProductModel> product_list;
    ProductModel productModel;

    // variable, classes, fragments
    BillingActivity billingActivity;
    String TAG = "BillingAdapter";
    // database
    DBHelper dbHelper;

    public BillingAdapter(BillingActivity billingActivity, DBHelper dbHelper, ArrayList<ProductModel> product_list)
    {
        this.product_list = product_list;
        this.billingActivity = billingActivity;
        this.dbHelper = dbHelper;
    }


    @NonNull
    @Override
    public BillingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewInflate = null;
        if (MakeView.isTablet(billingActivity)) {
            viewInflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_selected_items, parent, false);
        }return new MyViewHolder(viewInflate);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindViews({R.id.txt_ProductName, R.id.txt_TotalPriceOfItem})
        List<TextView> bf_listTextView;
        @BindView(R.id.edit_Quantity) EditText edit_Quantity;

        @BindViews({R.id.img_add,R.id.img_minus})
        List<ImageView> bk_listImageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

            go();
        }


        public void go()
        {
            edit_Quantity.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                    String str = s.toString(), total_amt = null;
                    int pro_id = product_list.get(getAdapterPosition()).getPro_id();
                    Double price =  product_list.get(getAdapterPosition()).getPrice();
                    if (!str.isEmpty())
                    {
                    Double qty_updated = Double.valueOf(s.toString());

                        total_amt  = UseSingleton.getInstance().decimalFormat(qty_updated * price);

                    // update qty in temp table
                    dbHelper.updateTemp_TabTable(pro_id,
                            qty_updated,Double.valueOf(total_amt));

                    // update data in billing activity
                    billingActivity.getItemUpdate();

                    bf_listTextView.get(1).setText(total_amt);
                    Log.e("billing ","onTextChanged "+getAdapterPosition()+ "qty_updated " +qty_updated);
                }
                

                else if (str.isEmpty())
                    {
                        edit_Quantity.setText(1+"");
                        edit_Quantity.clearFocus();
                        // update qty in temp table
                        total_amt  = UseSingleton.getInstance().decimalFormat(1 * price);
                        dbHelper.updateTemp_TabTable(pro_id,
                                1,Double.valueOf(total_amt));

                        // update data in billing activity
                        billingActivity.getItemUpdate();
                    }
                }

                @Override public void afterTextChanged(Editable s) { }});
        }

        @OnClick({R.id.img_Delete,R.id.img_minus,R.id.img_add})
        public void clickMe(View view)
        {double updatedQuantity =0;
            int pro_id = product_list.get(getAdapterPosition()).getPro_id();
            switch (view.getId())
            {
                case R.id.img_Delete:
                    Log.e("BillingAdapter ", "delete product "+product_list.get(getAdapterPosition()).getPro_name());
                    billingActivity.getItemDeleted(pro_id);
                    product_list.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    break;

                case R.id.img_add:

                    edit_Quantity.setEnabled(true);
                    bk_listImageView.get(1).setEnabled(true);
                    Log.e(TAG, "clickMe: qty "+edit_Quantity.getText().toString() );
                    updatedQuantity = Double.valueOf(edit_Quantity.getText().toString()) + 1;
                    update_product(getAdapterPosition(),pro_id,updatedQuantity);
                    Log.e(TAG, "add pro ID & NewQty "+pro_id+" "+updatedQuantity);
                    edit_Quantity.clearFocus();
                    break;

                case R.id.img_minus:
                    Log.e(TAG, "clickMe: qty "+edit_Quantity.getText().toString() );
                    if (Double.valueOf(edit_Quantity.getText().toString())  == 1)
                    {
                        edit_Quantity.setEnabled(false);
                        bk_listImageView.get(1).setEnabled(false);
                    }
                    else {
                        edit_Quantity.setEnabled(true);
                        bk_listImageView.get(1).setEnabled(true);
                        updatedQuantity = Double.valueOf(edit_Quantity.getText().toString()) - 1;
                        update_product(getAdapterPosition(), pro_id, updatedQuantity);
                        Log.e(TAG, "minus pro ID & NewQty " + pro_id + " " + updatedQuantity);
                    }     edit_Quantity.clearFocus();
                    break; }
        }



        public void update_product(int pos, int pro_id, double updated_Qty)
        {
            double total_amt = 0, price = product_list.get(pos).getPrice();
            total_amt = updated_Qty * price;
            // update billing product list
            productModel = new ProductModel(pro_id,product_list.get(pos).getPro_name(),updated_Qty,price,total_amt);
            product_list.set(pos,productModel);
            // update temp table
            dbHelper.updateTemp_TabTable(pro_id,updated_Qty,total_amt);
            // moving data to billing_fragment
            billingActivity.getItemUpdate();
            // update values in fields
            bf_listTextView.get(1).setText(UseSingleton.getInstance().decimalFormat(updated_Qty * price));
            edit_Quantity.setText(UseSingleton.getInstance().decimalFormat(updated_Qty));
        }

    }


    @Override
    public void onBindViewHolder(@NonNull BillingAdapter.MyViewHolder holder, int position) {
        // product name
        holder.bf_listTextView.get(0).setText(product_list.get(position).getPro_name());
        // total amount of each product
        Log.e("total_amt ",product_list.get(position).getTotalAmt()+"");
        holder.bf_listTextView.get(1).setText(UseSingleton.getInstance().decimalFormat(product_list.get(position).getTotalAmt()));
        // selected quantity
        holder.edit_Quantity.setText(UseSingleton.getInstance().decimalFormat(product_list.get(position).getQty()));
    }





    @Override
    public int getItemCount() {
        return product_list.size();
    }
}
