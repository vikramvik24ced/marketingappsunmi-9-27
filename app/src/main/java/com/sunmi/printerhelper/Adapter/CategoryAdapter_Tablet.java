package com.sunmi.printerhelper.Adapter;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Model.CategoryModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.BillingActivity;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryAdapter_Tablet extends RecyclerView.Adapter<CategoryAdapter_Tablet.MyViewHolder> {

    Context context;
    CategoryModel categoryModel;
    ArrayList<CategoryModel> categoryModelArrayList;
    BillingActivity billingActivity;
    DBHelper dbHelper;
    int  checked_category = 0;

    public CategoryAdapter_Tablet(Context context, BillingActivity billingActivity, DBHelper dbHelper,
                                  ArrayList<CategoryModel> categoryModelArrayList, int categoryPostion) {
        this.context = context;
        this.categoryModelArrayList = categoryModelArrayList;
        this.billingActivity = billingActivity;
        this.dbHelper = dbHelper;
        checked_category = categoryPostion;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.txt_CategoryName)
        TextView txt_CategoryName;

        LinearLayout ll_container;

        public MyViewHolder(View itemView) {
            super(itemView);
            ll_container = itemView.findViewById(R.id.ll_container);
            ll_container.setOnClickListener(this);
            ButterKnife.bind(this, itemView);
        }


        @Override
        public void onClick(View v) {
                checked_category = getAdapterPosition();
                if (billingActivity.bk_RV_list.get(0).getVisibility() == View.VISIBLE) {
                    billingActivity.category_id = categoryModelArrayList.get(getAdapterPosition()).getId();
                }
                notifyDataSetChanged(); }}


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewInflate = null;
        if (MakeView.isTablet(context)) {
            viewInflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_category_rv, parent, false);
        }
        return new CategoryAdapter_Tablet.MyViewHolder(viewInflate);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        categoryModel = categoryModelArrayList.get(position);
        holder.txt_CategoryName.setText(categoryModel.getCategegoryname());
        Log.e("pos " ,position+"" );
        // billing

            Log.e("mmsearch_Is_Active ", billingActivity.search_Is_Active + "");
            // if condition will only run in case of search
            // when search edit text is empty then previusly selected category is displayed
            if (billingActivity.search_Is_Active == 1) {
                Log.e("mmcategory_id ", billingActivity.category_id + "");
                if (billingActivity.category_id == categoryModel.getId()) {
                    holder.ll_container.setBackgroundColor(context.getResources().getColor(R.color.selected_state));
                    billingActivity.settingRecyclerViewProduct(dbHelper.getProduct_Tablet(billingActivity.category_id));
                }
                if (categoryModelArrayList.size() - 1 == position) {
                    billingActivity.search_Is_Active = 0;
                }

            } else {
                if (checked_category == position) {
                    int cat_id = categoryModelArrayList.get(checked_category).getId();
                    billingActivity.category_id = cat_id;
                    billingActivity.settingRecyclerViewProduct(dbHelper.getProduct_Tablet(categoryModelArrayList.get(checked_category).getId()));
                    holder.ll_container.setBackgroundColor(context.getResources().getColor(R.color.selected_state));
                } else {
                    holder.ll_container.setBackgroundColor(context.getResources().getColor(R.color.zxing_transparent));
                }
            }
    }

    @Override
    public int getItemCount() {
        return categoryModelArrayList.size();
    }
}
