package com.sunmi.printerhelper.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicaterInterface;
import com.sunmi.printerhelper.Fragments.AddToCartFrag;
import com.sunmi.printerhelper.Fragments.ProductListFragment;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.CategoryModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.AddProductActivity;
import com.sunmi.printerhelper.activity.InventoryActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Product_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Purchased_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Temp_Table_Name;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {


    private Context context, c;
    ArrayList<ProductModel> listProduct, mainList;
    ProductModel productModel;
    // interface
    CommunicaterInterface communicaterInterface;
    ArrayList<Integer> selectedQuantityList = new ArrayList<>();
    // database, variables, fragments
    DBHelper dbHelper;
    int frag, pos, initialPos, qty;
    String category, TAG = "BillingProductAdapter:";
    ArrayList<CategoryModel> CategoryName;
    //    Classes
    ProductListFragment productListFragment;
    Fragment billing_fragment;
    InventoryActivity inventoryActivity;

    public ProductAdapter(ArrayList<ProductModel> listProduct, Context context, CommunicaterInterface communicaterInterface,
                          DBHelper dbHelper, ArrayList<Integer> selectedQuantityList, int frag, ArrayList<CategoryModel> CategoryName, ProductListFragment productListFragment) {
        this.listProduct = listProduct;
        this.communicaterInterface = communicaterInterface;
        this.context = context;
        this.dbHelper = dbHelper;
        this.selectedQuantityList = selectedQuantityList;
        this.frag = frag;
        this.productListFragment = productListFragment;
        this.CategoryName = CategoryName;
        mainList = dbHelper.getProdcutSearched();
    }


    public ProductAdapter(Context context, Fragment billing_fragment, InventoryActivity inventoryActivity, ArrayList<ProductModel> listProduct, DBHelper dbHelper, int frag) {
        this.context = context;
        this.listProduct = listProduct;
        this.billing_fragment = billing_fragment;
        this.inventoryActivity = inventoryActivity;
        this.dbHelper = dbHelper;
        this.frag = frag;
        initialPos = 0;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewInflate = null;
        if (frag == 0) {
            viewInflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.billingproductrow, parent, false);
//            c = parent.getContext();
        } else if (frag == 1) {
            viewInflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product, parent, false);
        }
        return new MyViewHolder(viewInflate);

    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        pos = position;
        productModel = listProduct.get(position);
        category = dbHelper.getCategoryName(listProduct.get(position).getCategory_id());
        // it is an interface which is sending data to SmallCaptureActivity
        if (frag == 0) {
            communicaterInterface = (CommunicaterInterface) context;
            holder.txtPrice.setText(UseSingleton.getInstance().decimalFormat(productModel.getPrice()));
            holder.txtProductname.setText(Html.fromHtml(productModel.getPro_name()));
            holder.txtSoldByy.setText(dbHelper.getSoldByType(productModel.getSoldbyId()));
            if (String.valueOf(productModel.getQty()).indexOf("-") >= 0)
                holder.txtQuantity.setTextColor(context.getResources().getColor(android.R.color.holo_red_light));
            holder.txtQuantity.setText(UseSingleton.getInstance().decimalFormat(Double.valueOf(productModel.getQty())));
            holder.txt_selected_quantity.setText(String.valueOf(selectedQuantityList.get(position)));
        } else if (frag == 1) {
            //-------------------------------------------Inventory----------------------------------------------//
            holder.textViewList.get(0).setText(Html.fromHtml(productModel.getPro_name()));
            holder.textViewList.get(1).setText(category);
            holder.textViewList.get(4).setText(productModel.getPro_name().substring(0,1).toUpperCase());
            if (String.valueOf(productModel.getQty()).indexOf("-") >= 0)
                holder.textViewList.get(2).setTextColor(context.getResources().getColor(android.R.color.holo_red_light));
            holder.textViewList.get(2).setText(": " + UseSingleton.getInstance().decimalFormat(Double.valueOf(productModel.getQty())));
            holder.textViewList.get(3).setText(UseSingleton.getInstance().decimalFormat(productModel.getPrice()));

            holder.cd_CardView_noRadius.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (MakeView.isTablet(context)) {
                        if (listProduct.get(position).isSelected()) {
                            holder.img_tick.setVisibility(View.GONE);
                            holder.cd_CardView_noRadius.setBackground(context.getDrawable(R.drawable.white_outline));
                            listProduct.get(position).setSelected(false);
                            inventoryActivity.updateProductDetails(null, 0);

                            notifyDataSetChanged();
                        } else {
                            if (inventoryActivity.editTextList.get(0).getText().toString().isEmpty() || inventoryActivity.editTextList.get(1).getText().toString().isEmpty() ||
                                    inventoryActivity.editTextList.get(2).getText().toString().isEmpty() || inventoryActivity.editTextList.get(3).getText().toString().isEmpty()) {
                                holder.img_tick.setVisibility(View.VISIBLE);

                                listProduct.get(position).setSelected(true);
                                if (initialPos != position) {
                                    listProduct.get(initialPos).setSelected(false);
                                    notifyItemChanged(initialPos);
                                }
                                initialPos = position;
                                inventoryActivity.updateProductDetails(dbHelper.getProductRow(listProduct.get(position).getPro_id()), 1);
                                notifyDataSetChanged();
                            } else {
                                Toast.makeText(context, "Please Save your Modify Data, otherwise Your Data will be Lost", Toast.LENGTH_LONG).show();
                            }
                        }
                    } else {
                        editProduct(pos);
                    }
                }
            });

            if (MakeView.isTablet(context)) {
                if (listProduct.get(position).isSelected()) {
                    holder.img_tick.setVisibility(View.VISIBLE);
//                    holder.cd_CardView_noRadius.setBackground(context.getDrawable(R.drawable.green_outline));
//                    inventoryActivity.editTextList.get(4).setEnabled(false);
                } else {
                    holder.img_tick.setVisibility(View.GONE);
                    holder.cd_CardView_noRadius.setBackground(context.getDrawable(R.drawable.white_outline));
//                    inventoryActivity.editTextList.get(4).setEnabled(true);
                }
            }
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtProductname, txtQuantity, txtPrice, txt_selected_quantity, txtSoldByy;
        LinearLayout ll_productlist;
        ImageView img_add, img_minus;
        //Inventory
        @BindViews({R.id.txtProductName, R.id.txtCategoryName, R.id.txtAvailableQuantity, R.id.txtRupees, R.id.txt_firstLetter})
        List<TextView> textViewList;
        CardView cd_CardView_noRadius;
        @BindView(R.id.img_tick)
        ImageView img_tick;

        MyViewHolder(View itemView) {
            super(itemView);
            if (frag == 0) {
                txtProductname = itemView.findViewById(R.id.txtProductName);
                txtQuantity = itemView.findViewById(R.id.txtAvailableQuantity);
                txtPrice = itemView.findViewById(R.id.txtPrice);
                txt_selected_quantity = itemView.findViewById(R.id.txt_selected_quantity);
                txtSoldByy = itemView.findViewById(R.id.txtSoldByy);
                ll_productlist = itemView.findViewById(R.id.ll_productlist);
                img_add = itemView.findViewById(R.id.img_add);
                img_minus = itemView.findViewById(R.id.img_minus);
                img_add.setOnClickListener(this);
                img_minus.setOnClickListener(this);
            } else if (frag == 1) {
                //Inventory
                ButterKnife.bind(this, itemView);
                cd_CardView_noRadius = itemView.findViewById(R.id.cd_CardView_noRadius);
            }
        }

        @OnClick({R.id.txtProductName, R.id.txtCategoryName, R.id.rl_deleteProduct, R.id.cd_CardViewProd})
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.img_add:
                    // totalAmtOfProduct = 0;
                    qty = 0;
                    qty = selectedQuantityList.get(getAdapterPosition()) + 1;
                    if (qty != 0) {
                        int pro_id = selectedProduct(getAdapterPosition()).getPro_id();

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
                        //                                             NEW
                        // checking product_id is availbale in Temp table or not. If available then we update the quantity else insert a new row
                        Log.e("pro_id ", pro_id + "");
                        if (dbHelper.Pro_Available_To_AddToCart(pro_id, Temp_Table_Name, "pro_idd") == 1) {
                            Log.e(TAG + " P ", "product available" + "    position" + getAdapterPosition());

                            dbHelper.updateTempTable(pro_id, qty, getPrice(getAdapterPosition()) * qty);
                        }

                        // if product id is not available then insert that product
                        else if (dbHelper.Pro_Available_To_AddToCart(pro_id, Temp_Table_Name, "pro_idd") == 0) {
                            Log.e(TAG + " P ", "product not available" + "    position" + getAdapterPosition());
                            dbHelper.insertIntoTemp_vicky(pro_id, qty, getPrice(getAdapterPosition()) * qty);
                        }

                    }
                    // dbHelper.count_AddToCart();
                    selectedQuantityList.set(getAdapterPosition(), qty);
                    txt_selected_quantity.setText(String.valueOf(selectedQuantityList.get(getAdapterPosition())));
                    //  price = getPrice(getAdapterPosition());
                    // totalAmtOfProduct = qty * price;
                    if (qty < 2 && qty >= 0)
                        communicaterInterface.data("p");
                    break;
// --------------------------------------------------------------------------------------------------------------------------------------------------------------------

                case R.id.img_minus:
                    // price = 0;
                    // totalAmtOfProduct = 0;
                    qty = 0;
                    qty = selectedQuantityList.get(getAdapterPosition()) - 1;
                    int pro_id = selectedProduct(getAdapterPosition()).getPro_id();
                    if (qty == 0 || qty < 0) {

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------
                        //                                         NEW
                        selectedQuantityList.set(getAdapterPosition(), 0);
                        dbHelper.delete_Pro_From_AddToCart(pro_id, Temp_Table_Name, "pro_idd");

                    } else if (qty > 0) {
                        // checking product_id is availbale in Add_To_Cart table or not. If available then we update the quantity else insert a new row
                        //if prodcut id is available then update the quantity
                        Log.e("pro_id ", pro_id + "");
                        if (dbHelper.Pro_Available_To_AddToCart(pro_id, Temp_Table_Name, "pro_idd") == 1) {
                            Log.e(TAG + " P ", "product available" + "    position" + getAdapterPosition());
                            dbHelper.updateTempTable(pro_id, qty, getPrice(getAdapterPosition()) * qty);
                        }

                        // if product id is not available then insert that product
                        else if (dbHelper.Pro_Available_To_AddToCart(pro_id, Temp_Table_Name, "pro_idd") == 0) {
                            Log.e(TAG + " P ", "product not available" + "    position" + getAdapterPosition());
                            dbHelper.insertIntoTemp(pro_id, qty, getPrice(getAdapterPosition()) * qty);
                        }
                        selectedQuantityList.set(getAdapterPosition(), qty);
                    }
                    txt_selected_quantity.setText(String.valueOf(selectedQuantityList.get(getAdapterPosition())));
                    // price = getPrice(getAdapterPosition());
                    //  totalAmtOfProduct = qty * price;
                    if (qty == 0) communicaterInterface.data("m");
                    // else if (qty == 0) communicaterInterface.data(0,"minus");
                    break;
                //-----------------------------------------------------------------------------------------------------------------------------------------------------

                case R.id.rl_deleteProduct:
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage("Do you want delete this Product?");
                    alertDialogBuilder.setPositiveButton("yes",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    Log.e(TAG + " Delete_Pro_id", listProduct.get(getAdapterPosition()).getPro_id() + "");
                                    boolean status = dbHelper.change_Status(Product_Table_Name, "delete_status", "Pro_id", listProduct.get(getAdapterPosition()).getPro_id(), 1);
                                    Log.e("Delete_Status", status + "");
                                    if (status) {
                                        Log.e(TAG + " pos", getAdapterPosition() + "");
                                        notifyItemRemoved(getAdapterPosition());
                                        listProduct.remove(getAdapterPosition() + 1);
                                    }
                                }
                            });
                    alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                    break;

                case R.id.txtProductName:
                    if (!MakeView.isTablet(context))
                        editProduct(getAdapterPosition());
                    break;

                case R.id.txtCategoryName:
                    if (!MakeView.isTablet(context))
                        editProduct(getAdapterPosition());
                    break;
            }
        }
    }

    //-------------------------------------------------------------------edit product--------------------------------------------------------------//
    private void editProduct(int pos) {
        Log.e(TAG + " Edit_Pro_id", listProduct.get(pos).getPro_id() + "");
        ProductModel productModel = (ProductModel) dbHelper.getProductRow(listProduct.get(pos).getPro_id());
        Intent intent = new Intent(context, AddProductActivity.class);
        intent.putExtra("Bundle", productModel);
        intent.putExtra("Header", "Update Product Info");
        context.startActivity(intent);
    }

    //----------------------------------------------------------------price of each product--------------------------------------------------------//
    public double getPrice(int position) {
        double price = listProduct.get(position).getPrice();
        return price;
    }

    //-------------------------------------------------------------------one  product---------------------------------------------------------------//
    public ProductModel selectedProduct(int position) {
        ProductModel productModel1 = listProduct.get(position);
        return productModel1;
    }

    //-------------------------------------------------------search using edit text(billing products)------------------------------------------------//
    public void updateList(ArrayList<ProductModel> newProductList, ArrayList<Integer> selectedQuantity) {
        // products
        Log.e("productttt ", newProductList.size() + "   " + selectedQuantity.size());
        if (newProductList.size() == 0 && selectedQuantity.size() == 0) {
            AddToCartFrag.txt_NoProductAvailable.setVisibility(View.VISIBLE);
            listProduct.clear();
            selectedQuantityList.clear();
        } else {
            listProduct = new ArrayList<>();
            listProduct.addAll(newProductList);
            // selected quantities
            selectedQuantityList = new ArrayList<>();
            selectedQuantityList.addAll(selectedQuantity);
            AddToCartFrag.txt_NoProductAvailable.setVisibility(View.GONE);
        }
        notifyDataSetChanged();

    }

    //-------------------------------------------------------------search inventory products--------------------------------------------------------//
    public void updateListInventory(ArrayList<ProductModel> newProductList) {
        listProduct = new ArrayList<>();
        listProduct.addAll(newProductList);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return listProduct.size();
    }
}

