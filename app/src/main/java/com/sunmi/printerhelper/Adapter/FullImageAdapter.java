package com.sunmi.printerhelper.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.sunmi.printerhelper.R;

import java.util.ArrayList;

public class FullImageAdapter extends PagerAdapter {

    private ArrayList<String> images;
    private LayoutInflater inflater;
    private Context context;

    public FullImageAdapter(Context context, ArrayList<String> images) {
        this.context = context;
        this.images=images;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.galleryfull, view, false);
        ImageView myImage = myImageLayout.findViewById(R.id.img_banner);
        final RelativeLayout close = myImageLayout.findViewById(R.id.close);

        Glide.with(context)
                .load(images.get(position))
                .into(myImage);

//         .load(images.get(position))
//                .crossFade(5)
//                .fitCenter()
//                .placeholder(R.drawable.image_placeholder)
//                .error(R.drawable.image_placeholder)
//                .into(myImage);


        view.addView(myImageLayout, 0);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity)context).onBackPressed();
            }
        });

        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}