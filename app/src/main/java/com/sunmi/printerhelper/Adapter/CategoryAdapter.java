package com.sunmi.printerhelper.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Fragments.AddToCartFrag;
import com.sunmi.printerhelper.Fragments.ProductListFragment;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Model.CategoryModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.InventoryActivity;

import java.util.ArrayList;
import java.util.Collections;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {


    Context context;
    ArrayList<CategoryModel> categoryList;
    CategoryModel categoryModel;
    // database class
    DBHelper dbHelper;
    int frag, checked_category = 0;
    InventoryActivity inventoryActivity;
    Fragment billing_fragment;

    // tablet
    public CategoryAdapter(ArrayList<CategoryModel> categoryList, Context context, DBHelper dbHelper, int frag) {
        this.categoryList = categoryList;
        this.context = context;
        this.dbHelper = dbHelper;
        this.frag = frag;
    }

    public CategoryAdapter(Context context, Fragment billing_fragment, InventoryActivity inventoryActivity, DBHelper dbHelper,
                           ArrayList<CategoryModel> categoryModelArrayList, int frag, int categoryPostion) {
        this.context = context;
        categoryList = categoryModelArrayList;
        this.inventoryActivity = inventoryActivity;
        this.billing_fragment = billing_fragment;
        this.dbHelper = dbHelper;
        this.frag = frag;
        checked_category = categoryPostion;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_CategoryName, txt_firstLetter;
        LinearLayout ll_container;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_CategoryName = itemView.findViewById(R.id.txt_CategoryName);
            txt_firstLetter = itemView.findViewById(R.id.txt_firstLetter);
            ll_container = itemView.findViewById(R.id.ll_container);
            ll_container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewInflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_category_rv, parent, false);
        return new MyViewHolder(viewInflate);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final int pos = position;
        categoryModel = categoryList.get(position);
        holder.txt_CategoryName.setText(categoryModel.getCategegoryname());
        holder.txt_firstLetter.setText(categoryModel.getCategegoryname().substring(0, 1));
        holder.txt_CategoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.ll_container.performClick();
            }
        });

        holder.ll_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // billing
                if (frag == 0) {
                    categoryModel = categoryList.get(pos);
                    ArrayList<ProductModel> productList = dbHelper.getProductArrayList(categoryModel.getId());
                    Bundle bundle = new Bundle();
//                    Collections.sort(productList, ProductModel.BY_QUANTITY_SOLD);
                    Collections.sort(productList, ProductModel.BY_QUANTITY_SOLD);
                    bundle.putSerializable("productList", productList);
                    bundle.putString("Search_By", "Clicking_Category");
                    Fragment fragment = new AddToCartFrag();
                    fragment.setEnterTransition(new Fade());
                    fragment.setArguments(bundle);
                    FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.fl_frame, fragment, "SmallCaptureActivity").commit();
                    ft.addToBackStack("");
                    //-----------------------------------------Inventroy----------------------------------------//
                } else if (frag == 1) {
                    if (MakeView.isTablet(context)) {
                        //-----------------------------------------TABLET -------------------------------------------//
                        if (inventoryActivity.editTextList.get(0).getText().toString().isEmpty() || inventoryActivity.editTextList.get(1).getText().toString().isEmpty() ||
                                inventoryActivity.editTextList.get(2).getText().toString().isEmpty() || inventoryActivity.editTextList.get(3).getText().toString().isEmpty()) {
                            checked_category = pos;
                            notifyDataSetChanged();
                            if (dbHelper.getProductArrayList(categoryList.get(checked_category).getId()).size() == 0) {
                                inventoryActivity.textViewList.get(0).setVisibility(View.VISIBLE);
                                inventoryActivity.recyclerViewList.get(2).setVisibility(View.GONE);
                            } else {
                                inventoryActivity.textViewList.get(0).setVisibility(View.GONE);
                                inventoryActivity.recyclerViewList.get(2).setVisibility(View.VISIBLE);
                            }
                        } else {
                            Toast.makeText(context, "Please Save your Modify Data, otherwise Your Data will be Lost", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        //-----------------------------------------SMARTPHONE-----------------------------------------------//
                        categoryModel = categoryList.get(pos);
                        int id = categoryModel.getId();
//                        ArrayList<ProductModel> productList = dbHelper.getProductArrayList(categoryModel.getId());
//                        Log.e("categoryyyy ", categoryModel.getId() + " " + productList.size());
                        Bundle bundle = new Bundle();
                        // Collections.sort(productList,ProductModel.BY_QUANTITY_SOLD);
                        bundle.putInt("productList", id);
                        bundle.putSerializable("CategoryName", dbHelper.getCategoryData());
                        bundle.putString("Search_By", "Clicking_Category");
//                    bundle.putString("CategoryName", categoryModel.getCategegoryname());
                        Fragment fragment = new ProductListFragment();
                        fragment.setEnterTransition(new Fade());
                        fragment.setArguments(bundle);
                        FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.rl_inventory, fragment).commit();
//                    ft.replace(R.id.ll_search_containerr, fragment).commit();
                        ft.addToBackStack("ProductListFragment");
                    }
                }
            }
        });

        if (frag == 1) {
            if (MakeView.isTablet(context)) {
                // inventary
                if (checked_category == position) {
                    inventoryActivity.settingRecyclerViewProduct(dbHelper.getProductArrayList(categoryList.get(checked_category).getId()), frag);
                    holder.ll_container.setBackgroundColor(context.getResources().getColor(R.color.selected_state));
                } else {
                    holder.ll_container.setBackgroundColor(context.getResources().getColor(R.color.zxing_transparent));
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}
