package com.sunmi.printerhelper.Adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Fragments.TaxFrag;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.TaxModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.BillingActivity;

import java.util.ArrayList;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Tax_Table_Name;

public class TaxAdapterTab extends RecyclerView.Adapter<TaxAdapterTab.MyViewHolderr> {

    DBHelper dbHelper;
    ArrayList<Boolean> taxBoolean = new ArrayList<>();
    ArrayList<TaxModel> listHome;
    TaxModel taxModel;
    View itemView;
    TaxFrag taxFrag;
    BillingActivity billingActivity;
    int activity;
    double amtAfterDiscount, total_AmtOfTax = 0, tax_Amt = 0;
    String tax_id = null, amt_EachTax = null;
    public TaxAdapterTab(ArrayList<TaxModel> listHome,BillingActivity billingActivity,  TaxFrag taxFrag, ArrayList<Boolean> taxBoolean, DBHelper dbHelper, double amtAfterDiscount, int activity)
    {
        this.listHome=listHome;
        this.taxFrag = taxFrag;
        this.billingActivity = billingActivity;
        this.taxBoolean = taxBoolean;
        this.activity = activity;
        this.amtAfterDiscount = amtAfterDiscount;
        this.dbHelper = dbHelper;
    }

    @NonNull
    @Override
    public TaxAdapterTab.MyViewHolderr onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (activity == 0) { itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_tax_rv, parent, false); }
        else if (activity == 1) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_taxes_onselected_products, parent, false); }
        return new TaxAdapterTab.MyViewHolderr(itemView);
    }

    public class MyViewHolderr extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_TaxName, txt_TaxPrice, txtTaxPerc, txtTaxName, txtTaxAmount;
        ImageView img_delete_tax;
        CardView cv_Tax;
        CheckBox chkBox_Tax;
        public MyViewHolderr(View itemView) {
            super(itemView);
            if (activity == 0) {
                txt_TaxName = itemView.findViewById(R.id.txt_TaxName);
                txt_TaxPrice = itemView.findViewById(R.id.txt_TaxPrice);
                cv_Tax = itemView.findViewById(R.id.cv_Tax);
                chkBox_Tax = itemView.findViewById(R.id.chkBox_Tax);
                img_delete_tax = itemView.findViewById(R.id.img_delete_tax);
                chkBox_Tax.setOnClickListener(this);
                img_delete_tax.setOnClickListener(this);}

            else if (activity == 1)
            {
                txtTaxPerc = itemView.findViewById(R.id.txtTaxPerc);
                txtTaxName = itemView.findViewById(R.id.txtTaxName);
                txtTaxAmount = itemView.findViewById(R.id.txtTaxAmount);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.chkBox_Tax:
                {
                    if (chkBox_Tax.isChecked())
                    {
                        TaxFrag.changes = true;
                        taxFrag.getUpdatedTax(getAdapterPosition(), true);
                    }
                    else if (!chkBox_Tax.isChecked())
                    {
                        TaxFrag.changes = true;
                        taxFrag.getUpdatedTax(getAdapterPosition(), false);
                    }
                    break; }

                case R.id.img_delete_tax:

                    Log.e("TaxAdapter ",getAdapterPosition()+"    "+"id: "+listHome.get(getAdapterPosition()).getId()+ " " +listHome.get(getAdapterPosition()).getTaxname());
                    // delete tax from tax table
                    dbHelper.delete_Pro_From_AddToCart(listHome.get(getAdapterPosition()).getId(),Tax_Table_Name,"Tax_id");
                    listHome.remove(getAdapterPosition());
                    taxBoolean.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
            } }
    }

    @Override
    public void onBindViewHolder(@NonNull TaxAdapterTab.MyViewHolderr holder, int position) {

        if (activity == 0) {
            if (position == 0) holder.img_delete_tax.setVisibility(View.INVISIBLE);
            taxModel = listHome.get(position);
            holder.txt_TaxName.setText(taxModel.getTaxname());
            holder.txt_TaxPrice.setText(taxDecimalFormat(taxModel.getTaxPercent()));
            if (listHome.get(position).getSelected() == 0) {
                holder.chkBox_Tax.setChecked(false);
            } else if (listHome.get(position).getSelected() == 1) {
                holder.chkBox_Tax.setChecked(true);
            }
            Log.e("taxv adapter ", taxBoolean.get(position) + "");
        }
        else if (activity == 1)
        {
            double amtOfTax = 0;
            taxModel = listHome.get(position);
            holder.txtTaxName.setText(taxModel.getTaxname());
            holder.txtTaxPerc.setText(UseSingleton.getInstance().decimalFormat(taxModel.getTaxPercent())+"%");
            amtOfTax = (amtAfterDiscount * taxModel.getTaxPercent()) / 100;
            holder.txtTaxAmount.setText(UseSingleton.getInstance().decimalFormat(amtOfTax));
            total_AmtOfTax = total_AmtOfTax + amtOfTax ;
            tax_id = tax_id + "," + taxModel.getId();
            amt_EachTax = amt_EachTax + "," + amtOfTax;
            tax_Amt = tax_Amt + amtOfTax;
            if (position == listHome.size()-1)
            {
                billingActivity.total_Tax(tax_id,amt_EachTax,tax_Amt,total_AmtOfTax);
            }

        }
    }




    public String taxDecimalFormat(double d)
    {
        int index = UseSingleton.getInstance().decimalFormat(d).indexOf(".");
        String s = null;
        if (index>0)
        {
            Log.e("p ","point found");
            s = String.format("%.2f",Double.valueOf(UseSingleton.getInstance().decimalFormat(d)));
        }

        else
        {
            Log.e("p ","point not found");
            s = UseSingleton.getInstance().decimalFormat(d);
        }
        return s+"%";
    }

    @Override
    public int getItemCount() { return listHome.size(); }
}