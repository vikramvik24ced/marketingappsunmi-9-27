package com.sunmi.printerhelper.Adapter;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.sunmi.printerhelper.Fragments.ChangePassFragment;
import com.sunmi.printerhelper.Fragments.SettingTaxesFrag;
import com.sunmi.printerhelper.Fragments.TaxFrag;
import com.sunmi.printerhelper.Fragments.UpdateUserInfoFragment;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Model.SettingModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.SettingActivity;
import java.util.ArrayList;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingAdapter extends RecyclerView.Adapter<SettingAdapter.MyViewHolder>  {

    SettingActivity settingActivity;
    ArrayList<SettingModel> list_Setting;
    SettingModel settingModel;
    int state = -1;
    public SettingAdapter(ArrayList<SettingModel> list_Setting, SettingActivity settingActivity)
    {
        this.list_Setting = list_Setting;
        this.settingActivity = settingActivity;
    }

    @NonNull
    @Override
    public SettingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_settings, parent, false);
        return new SettingAdapter.MyViewHolder(view); }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_tittle)
        TextView txt_tittle;
        @BindView(R.id.cv_CardView)
        CardView cv_CardView;
        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick(R.id.ll_row_parent)
        public void clickMe() {
            FragmentManager fm = settingActivity.getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            Fragment fragment;
            if (MakeView.isTablet(settingActivity)) {

                if (getAdapterPosition() == 0)
                {
                    fragment = new ChangePassFragment();
                    ft.replace(R.id.fl_Setting, fragment, "ChangePassFrag");
                    ft.commit();
                    state = getAdapterPosition();
                }
                else if (getAdapterPosition() == 1)
                {
                    fragment = new TaxFrag();
                    ft.replace(R.id.fl_Setting, fragment, "TaxFrag");
                    ft.commit();
                    state = 2;
                    state = 1;
                }

                else if (getAdapterPosition() == 2)
                {
//                    fragment = new TaxFrag();
//                    ft.replace(R.id.fl_Setting, fragment, "TaxFrag");
//                    ft.commit();
//                    state = 2;
                }
                notifyDataSetChanged();
            }
            else {
                if (getAdapterPosition() == 0) {
                    fragment = new ChangePassFragment();
                    ft.replace(R.id.ll_SettingParent, fragment, "ChangePassFrag");
                    ft.addToBackStack("");
                    ft.commit();
                } else if (getAdapterPosition() == 1) {
                    fragment = new UpdateUserInfoFragment();
                    ft.replace(R.id.ll_SettingParent, fragment, "UpdateUserInfoFrag");
                    ft.addToBackStack("");
                    ft.commit();
                } else if (getAdapterPosition() == 2) {
                    settingActivity.startActivity(new Intent(settingActivity, SettingTaxesFrag.class));
                }
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull SettingAdapter.MyViewHolder holder, int position) {
        settingModel = list_Setting.get(position);
        holder.txt_tittle.setText(settingModel.getTittle());

        if (MakeView.isTablet(settingActivity)) {
            if (state != position) {
                holder.cv_CardView.setBackgroundResource(R.drawable.white_outline);
            } else if (state == position) {
                holder.cv_CardView.setBackgroundResource(R.drawable.green_outline_tablet);
            }
        }
    }

    @Override
    public int getItemCount() {
        return list_Setting.size();
    }
}
