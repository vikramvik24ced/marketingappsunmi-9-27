package com.sunmi.printerhelper.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;

import java.util.ArrayList;

public class ViewBillReceiptProductAdapter extends RecyclerView.Adapter<ViewBillReceiptProductAdapter.MyViewHolder> {

    Context context;
    ArrayList<ProductModel> ItemsSelectedByUser;
    DBHelper dbHelper;

    public ViewBillReceiptProductAdapter(Context context, ArrayList<ProductModel> ItemsSelectedByUser, DBHelper dbHelper) {
        this.context = context;
        this.ItemsSelectedByUser = ItemsSelectedByUser;
        this.dbHelper = dbHelper;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_billreceipt, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try {
            holder.txtProductName.setText(ItemsSelectedByUser.get(position).getPro_name());
            holder.txtQuantity.setText(UseSingleton.getInstance().decimalFormat(ItemsSelectedByUser.get(position).getQty()));
            holder.txtAmount.setText(UseSingleton.getInstance().decimalFormat(ItemsSelectedByUser.get(position).getPrice()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return ItemsSelectedByUser.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtProductName, txtQuantity, txtAmount;
        MyViewHolder(View v) {
            super(v);
            txtProductName = v.findViewById(R.id.txtProductName);
            txtQuantity = v.findViewById(R.id.txtQuantity);
            txtAmount = v.findViewById(R.id.txtAmount);
        }
    }
}
