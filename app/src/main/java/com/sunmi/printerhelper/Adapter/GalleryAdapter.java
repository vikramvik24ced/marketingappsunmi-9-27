//package com.sunmi.printerhelper.Adapter;
//
//import android.content.Context;
//import android.support.v7.widget.RecyclerView;
//import android.util.Log;
//import android.util.SparseBooleanArray;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.ImageView;
//
//import com.bumptech.glide.Glide;
//import com.sunmi.printerhelper.Model.ImageModel;
//import com.sunmi.printerhelper.R;
//
//import java.util.ArrayList;
//
///**
// * @author Paresh Mayani (@pareshmayani)
// */
//public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.MyViewHolder> {
//
//    private ArrayList<ImageModel> mImagesList;
//    private Context mContext;
//    private SparseBooleanArray mSparseBooleanArray;
//
//    public GalleryAdapter(Context context, ArrayList<ImageModel> imageList) {
//        mContext = context;
//        mSparseBooleanArray = new SparseBooleanArray();
//        mImagesList = new ArrayList<ImageModel>();
//        this.mImagesList = imageList;
//
//    }
//
//    public ArrayList<ImageModel> getCheckedItems() {
//        ArrayList<ImageModel> mTempArry = new ArrayList<>();
//
//        for(int i=0;i<mImagesList.size();i++) {
//            if(mSparseBooleanArray.get(i)) {
//                mTempArry.add(mImagesList.get(i));
//                Log.e("hi", mTempArry+"");
//            }
//        }
//
//        return mTempArry;
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    CompoundButton.OnCheckedChangeListener mCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
//
//        @Override
//        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//            mSparseBooleanArray.put((Integer) buttonView.getTag(), isChecked);
//        }
//    };
//
//    @Override
//    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.row_customegallery, parent, false);
//
//        return new MyViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(MyViewHolder holder, int position) {
//
//        String imageUrl = mImagesList.get(position).getUri();
//
//        Glide.with(mContext)
//                .load("file://"+imageUrl)
//                .centerCrop()
//                .placeholder(R.drawable.placeholder)
//                .error(R.drawable.placeholder)
//                .into(holder.imageView);
//
//        holder.checkBox.setTag(position);
//        holder.checkBox.setChecked(mSparseBooleanArray.get(position));
//        holder.checkBox.setOnCheckedChangeListener(mCheckedChangeListener);
//    }
//
//    @Override
//    public int getItemCount() {
//        return mImagesList.size();
//    }
//
//    public class MyViewHolder extends RecyclerView.ViewHolder {
//
//        public CheckBox checkBox;
//        public ImageView imageView;
//
//        public MyViewHolder(View view) {
//            super(view);
//
//            checkBox = (CheckBox) view.findViewById(R.id.checkBox1);
//            imageView = (ImageView) view.findViewById(R.id.imageView1);
//        }
//    }
//
//}
