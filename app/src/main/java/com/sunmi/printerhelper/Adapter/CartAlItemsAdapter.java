package com.sunmi.printerhelper.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Fragments.CartAltemsFrag;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;

import java.util.ArrayList;

public class CartAlItemsAdapter extends RecyclerView.Adapter<CartAlItemsAdapter.MyViewHolder>{

    ArrayList<ProductModel> ItemsSelectedByUserList;
    ArrayList<ProductModel> quantityAndTotalAmttList;
    ProductModel productModel;
    // classes
    Context context;
    CartAltemsFrag cartAltemsFrag;
    private boolean check = false;
    // database
    DBHelper dbHelper;
    double qty;
    boolean plus_minus = false;

    public CartAlItemsAdapter(Context context, CartAltemsFrag cartAltemsFrag, ArrayList<ProductModel> ItemsSelectedByUserList, ArrayList<ProductModel> quantityAndTotalAmttList, DBHelper dbHelper)
    {
        this.ItemsSelectedByUserList = ItemsSelectedByUserList;
        this.quantityAndTotalAmttList = quantityAndTotalAmttList;
        this.context = context;
        this.cartAltemsFrag = cartAltemsFrag;
        this.dbHelper = dbHelper;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView txt_ProductName, txt_TotalPriceOfItem, txt_Soldby, txt_selected_quantity;
        RelativeLayout ll_plus_minus;
        EditText edit_Quantity;
        ImageView img_Delete, img_add, img_minus;
        public MyViewHolder(View itemView) {
            super(itemView);
            txt_ProductName = itemView.findViewById(R.id.txt_ProductName);
            txt_TotalPriceOfItem = itemView.findViewById(R.id.txt_TotalPriceOfItem);
            edit_Quantity = itemView.findViewById(R.id.edit_Quantity);
            txt_selected_quantity = itemView.findViewById(R.id.txt_selected_quantity);
            ll_plus_minus = itemView.findViewById(R.id.ll_plus_minus);
            img_add = itemView.findViewById(R.id.img_add);
            img_minus = itemView.findViewById(R.id.img_minus);
            img_Delete = itemView.findViewById(R.id.img_Delete);
            txt_Soldby = itemView.findViewById(R.id.txt_Soldby);
            img_Delete.setOnClickListener(this);
            img_add.setOnClickListener(this);
            img_minus.setOnClickListener(this);
            edit_Quantity.setOnClickListener(this);
            txt_selected_quantity.setOnClickListener(this);

            edit_Quantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String str_qty = s.toString();
                    if (str_qty.isEmpty())
                    {
                        Log.e("text","i a empty");
                        double updatedAmountofItem = Double.valueOf(cartAltemsFrag.getItemUpdate(getAdapterPosition(), Double.valueOf(0)));
                        txt_TotalPriceOfItem.setText(UseSingleton.getInstance().decimalFormat(updatedAmountofItem));
                        edit_Quantity.setText(String.valueOf(0));
                    }
                    else
                    {
                        if (plus_minus == false) {
                            double updatedAmountofItem = Double.valueOf(cartAltemsFrag.getItemUpdate(getAdapterPosition(), Double.valueOf(str_qty)));
                            txt_TotalPriceOfItem.setText(UseSingleton.getInstance().decimalFormat(updatedAmountofItem));
                        }
                        plus_minus = false;
                    }
                }
                @Override public void afterTextChanged(Editable s) { }}


                );}

        @Override
        public void onClick(View v) {

            switch(v.getId()) {
                case R.id.img_Delete:
                    Log.e("before:", getAdapterPosition() + "");
                    cartAltemsFrag.getItemDeleted(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    break;

                case R.id.img_add:
                   // edit_Quantity.setCursorVisible(false);
                    if (edit_Quantity.getText().toString().isEmpty()) {
                        qty = 1;
                        double updatedAmountofItem = Double.valueOf(cartAltemsFrag.getItemUpdate(getAdapterPosition(), qty));
                        edit_Quantity.setText(UseSingleton.getInstance().decimalFormat(qty));
                        txt_TotalPriceOfItem.setText(UseSingleton.getInstance().decimalFormat(updatedAmountofItem));
                    } else {
                        qty = Double.valueOf(edit_Quantity.getText().toString()) + 1;
                        //  double updatedAmountofItem =  Double.valueOf(cartAltemsFrag.getItemUpdate(getAdapterPosition(),Double.valueOf(txt_selected_quantity.getText().toString())));
                        double updatedAmountofItem = Double.valueOf(cartAltemsFrag.getItemUpdate(getAdapterPosition(), qty));
                        edit_Quantity.setText(UseSingleton.getInstance().decimalFormat(qty));
                        txt_TotalPriceOfItem.setText(UseSingleton.getInstance().decimalFormat(updatedAmountofItem));
                       }     edit_Quantity.setSelection(edit_Quantity.getText().length());
                       img_minus.setEnabled(true);
                    break;

                case R.id.img_minus:
                  //  edit_Quantity.setCursorVisible(false);
                    if (edit_Quantity.getText().toString().isEmpty()) img_minus.setEnabled(false);
                    else {
                        plus_minus = true;
                        qty = Double.valueOf(edit_Quantity.getText().toString()) - 1;
                        if (qty == 0.0) img_minus.setEnabled(false);
                        double updatedAmountofItem = Double.valueOf(cartAltemsFrag.getItemUpdate(getAdapterPosition(), qty));
                        edit_Quantity.setText(UseSingleton.getInstance().decimalFormat(qty));
                        txt_TotalPriceOfItem.setText(UseSingleton.getInstance().decimalFormat(updatedAmountofItem));
                        edit_Quantity.setSelection(edit_Quantity.getText().length());}
                    break;

                case R.id.edit_Quantity:
                    edit_Quantity.setEnabled(true);
                    edit_Quantity.requestFocus();
                   edit_Quantity.setCursorVisible(true);


//                   edit_Quantity.setVisibility(View.VISIBLE);
//                   txt_selected_quantity.setVisibility(View.GONE);
//                   edit_Quantity.setCursorVisible(true);
//                   edit_Quantity.setFocusable(true);
                    break;
            }
        }
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewInflate =  LayoutInflater.from(parent.getContext()).inflate(R.layout.row_cart_all_items_rv, parent,false);
        return new MyViewHolder(viewInflate);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        check = false;
        productModel = ItemsSelectedByUserList.get(position);
        Log.e("ssssssssssssss ",quantityAndTotalAmttList.get(position).getQty()+"");
        holder.txt_ProductName.setText(productModel.getPro_name());
        Log.e("soid",quantityAndTotalAmttList.get(position).getQty()+"");
        holder.txt_ProductName.setText(productModel.getPro_name());
        if (quantityAndTotalAmttList.get(position).getQty() == 0.0)
            holder.edit_Quantity.setText("");
        else
            holder.edit_Quantity.setText(UseSingleton.getInstance().decimalFormat(quantityAndTotalAmttList.get(position).getQty()));
        holder.edit_Quantity.setSelection(  holder.edit_Quantity.getText().length());
       // holder.edit_Quantity.setCursorVisible(true);
        holder.txt_TotalPriceOfItem.setText(UseSingleton.getInstance().decimalFormat(quantityAndTotalAmttList.get(position).getPrice()));
        // soldbyType
        holder.txt_Soldby.setText( dbHelper.getSoldByType(productModel.getSoldbyId()));
        holder.txt_Soldby.setText( dbHelper.getSoldByType(productModel.getSoldbyId()));
//
    }


    @Override
    public int getItemCount() {
        return ItemsSelectedByUserList.size();
    }}
