package com.sunmi.printerhelper.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.sunmi.printerhelper.R;

public class TermsConditionsFragment extends Fragment {

    WebView webViewTermsCondition;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_terms_conditions, container, false);
        webViewTermsCondition = view.findViewById(R.id.webViewTermsCondition);
        webViewTermsCondition.loadUrl("http://slipkode.com/terms_app.html");
        return view;
    }
}
