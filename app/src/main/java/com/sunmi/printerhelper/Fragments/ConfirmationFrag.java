package com.sunmi.printerhelper.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompatSideChannelService;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunmi.Interfaces.CommunicaterMoveBackPayment;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.UsePrinter;
import com.sunmi.printerhelper.Model.FinalBill;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Model.OrderModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.SmallCaptureActivity;
import com.sunmi.printerhelper.activity.ViewBillActivity;
import com.sunmi.printerhelper.utils.AidlUtil;

import java.util.IllegalFormatCodePointException;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmationFrag extends Fragment {

    // button
    @BindView(R.id.btn_Reprint)
    Button btn_Reprint;
    // text view
    @BindViews({R.id.txtTotalAmount, R.id.txtInoviceNo})
    List<TextView> bk_listTextView;
    // Image View
    @BindView(R.id.imgBillingBack)
    ImageView imgBillingBack;

    // variables, classes, fragments
    OrderModel orderModel;
    int order_id;
    FinalBill finalBill;
    DBHelper dbHelper;
    // interface
    CommunicaterMoveBackPayment communicaterMoveBackPayment;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_confirmation, container, false);
        ButterKnife.bind(this,view);

        // ============= TABLET
        if (MakeView.isTablet(getContext()))
        {
            getDataOfBilling();
            interfaces();
        }
        // ============= SMART PHONES
        else {
            if (AidlUtil.getInstance().initPrinter() == false) {
                Log.e("aaaa", AidlUtil.getInstance().initPrinter() + "");
                btn_Reprint.setVisibility(View.GONE); }
            interfaces();
            getPrintedDataFrom_PaymentFrag();
        }
        dbHelper = new DBHelper(getContext());
        return view;
    }


    public void interfaces() {
        communicaterMoveBackPayment = (CommunicaterMoveBackPayment) getContext();
        // ============= TABLET
        if (MakeView.isTablet(getContext()))
        { communicaterMoveBackPayment.moveBack(2); }
        // ============= SMART PHONES
        else
        { communicaterMoveBackPayment.moveBack(1); }
    }


    // ============= SMART PHONES
    public void getPrintedDataFrom_PaymentFrag() {
        finalBill = (FinalBill) getArguments().getSerializable("finalBill");
        if (finalBill.getOrderModel().getAmtAfterTax().indexOf(".") >= 0)
            bk_listTextView.get(0).setText(finalBill.getOrderModel().getAmtAfterTax().substring(0, finalBill.getOrderModel().getAmtAfterTax().indexOf(".")) + " SAR");
        else bk_listTextView.get(0).setText(finalBill.getOrderModel().getAmtAfterTax() + " SAR");
        bk_listTextView.get(1).setText("Invoice No." + finalBill.getOrderModel().getOrder_id());
    }

    // ============= TABLET
    public void getDataOfBilling()
    {
        orderModel = (OrderModel) getArguments().getSerializable("orderModell");
        order_id = getArguments().getInt("order_id");
        bk_listTextView.get(0).setText(orderModel.getAmtAfterTax()+" SAR");
        bk_listTextView.get(1).setText("Invoice No. "+order_id);
    }



    @OnClick({R.id.btn_Reprint, R.id.btn_NewSale, R.id.btnViewBill, R.id.imgBillingBack})
    public void clickMe(View view)
    { Intent intent = null;
        switch (view.getId())
        {
            case R.id.btn_Reprint:
                try {
                    if (!finalBill.equals(null)) {
                        try {
                            UsePrinter usePrinter = new UsePrinter(getActivity(), finalBill);
                            usePrinter.printReceipt();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                }
                break;

            case R.id.btn_NewSale:
                // ============= TABLET
                if (MakeView.isTablet(getContext()))
                {
                    getActivity().finish();
                }
                // ============= SMART PHONES
                else {
                    intent = new Intent(getContext(), SmallCaptureActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
                break;

            case R.id.btnViewBill:
                // ============= TABLET
                if (MakeView.isTablet(getContext()))
                {
                    finalBill = dbHelper.getDataForBilling(order_id);
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    Fragment fragment = new ViewBillFrag();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("finalBill",finalBill);
                    bundle.putInt("comeFrom",1);
                    bundle.putInt("check",1);
                    fragment.setArguments(bundle);
                    ft.replace(R.id.rl_billing, fragment, "ViewBillFrag");
                    ft.addToBackStack("");
                    ft.commit();



                }
                // ============= SMART PHONES
                else {
                    intent = new Intent(getContext(), ViewBillActivity.class);
                    FinalBill finalBil = dbHelper.getDataForBilling(finalBill.getOrderModel().getOrder_id());
                    intent.putExtra("finalBill", finalBil);
                    startActivity(intent);
                }
                break;
            case R.id.imgBillingBack:
                getActivity().onBackPressed();
                break;
        }
    }




}
