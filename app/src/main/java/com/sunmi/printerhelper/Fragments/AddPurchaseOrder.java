package com.sunmi.printerhelper.Fragments;
import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicatorMoveBack;
import com.sunmi.Interfaces.ComunicatorPurchaseOrder;
import com.sunmi.printerhelper.Adapter.ImageAdapter;
import com.sunmi.printerhelper.Helper.Constants;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.Image;
import com.sunmi.printerhelper.Model.PurchaseModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.AlbumSelectActivity;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;

import static android.app.Activity.RESULT_OK;


public class AddPurchaseOrder extends Fragment implements View.OnClickListener {

    //View
    View view = null;
    Button btnSave;
    TextView txt_home;
    public RecyclerView imageRecyclerview;
    LinearLayout ll_Purchasedate;
    DatePickerDialog datePickerDialog;
    ImageView img_BackPress, img_addImage, img_edit;
    private EditText edtVendorName, edtPurchasingDated, edtPurchasedAmt;

    //database
    DBHelper dbHelper;
    // setting recycelrview
    ImageAdapter imageAdapter;
    ArrayList<String> list_imagePath = new ArrayList<>();
    PurchaseModel purchaseModel;
    //class variable
    Calendar calendar;
    //Variable
    String vendorName, purchasingDated, basePath = "file://";
    private int yy, mm, dd, action = 0, purchase_id;
    double purchasedAmt;
    // interface
    ComunicatorPurchaseOrder comunicatorPurchaseOrder;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_add_purchase_order, container, false);
        init();
        dbHelper = new DBHelper(getContext());
        if (!MakeView.isTablet(getContext())) interfacess(null,1);
        edit_order();
        calendar = Calendar.getInstance();
        yy = calendar.get(Calendar.YEAR);
        mm = calendar.get(Calendar.MONTH);
        dd = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(yy, mm, dd);
        return view;
    }


    // smart phone
    public void edit_order()
    {
        Bundle bundle=getArguments();
        if (bundle != null) {
            if (bundle.containsKey("edit_order"))
            {
                txt_home.setText("Update Purchased Details");
                purchaseModel = (PurchaseModel) bundle.getSerializable("edit_order");
                edit_Order(purchaseModel);
//                action = 1;  purchase_id = purchaseModel.getPurchasedId(); vendorName = purchaseModel.getVendorName();
//                purchasingDated = purchaseModel.getPurchased_Date();
//                purchasedAmt = purchaseModel.getPurchaseAmount();
//                edtVendorName.setText(vendorName);
//                edtPurchasingDated.setText(purchasingDated);
//                edtPurchasedAmt.setText(UseSingleton.getInstance().decimalFormat(purchasedAmt));
//                list_imagePath = dbHelper.getImage(purchase_id);
//                settingRecycle();
            } }
    }

    // smart phone and tablet
    public void edit_Order(PurchaseModel purchaseModel)
    {
        action = 1;  purchase_id = purchaseModel.getPurchasedId(); vendorName = purchaseModel.getVendorName();
        purchasingDated = purchaseModel.getPurchased_Date();
        purchasedAmt = purchaseModel.getPurchaseAmount();
        edtVendorName.setText(vendorName);
        edtPurchasingDated.setText(purchasingDated);
        edtPurchasedAmt.setText(UseSingleton.getInstance().decimalFormat(purchasedAmt));
        list_imagePath = dbHelper.getImage(purchase_id);
        settingRecycle();
    }


    private void init() {
        edtVendorName = view.findViewById(R.id.edtVendorName);
        edtPurchasingDated = view.findViewById(R.id.edtPurchasedt);
        ll_Purchasedate = view.findViewById(R.id.ll_Purchasedate);
        edtPurchasedAmt = view.findViewById(R.id.edtBillAmt);
        img_addImage = view.findViewById(R.id.img_addImage);
        img_BackPress = view.findViewById(R.id.img_BackPress);
        imageRecyclerview = view.findViewById(R.id.rv_imgContainer);
        btnSave = view.findViewById(R.id.btnSave);
        txt_home = view.findViewById(R.id.txt_home);
        img_edit = view.findViewById(R.id.img_edit);
        ll_Purchasedate.setOnClickListener(this);
        edtPurchasingDated.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        img_addImage.setOnClickListener(this);
        img_edit.setOnClickListener(this);
        img_BackPress.setOnClickListener(this);
    }


    //get current date
    private void showDate(int year, int month, int day) {
        String months = new DateFormatSymbols().getMonths()[month];
        Log.e("months", months);
        edtPurchasingDated.setText(new StringBuilder().append(day).append(" ")
                .append(months).append(" ").append(year));
    }


    private void settingRecycle() {

        if (list_imagePath.size() == 0)
        {
            img_addImage.setVisibility(View.VISIBLE);
            imageRecyclerview.setVisibility(View.GONE);
            img_edit.setVisibility(View.GONE);
        }
        else
        {
            img_addImage.setVisibility(View.GONE);
            imageRecyclerview.setVisibility(View.VISIBLE);
            img_edit.setVisibility(View.VISIBLE);
            imageRecyclerview.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            imageAdapter = new ImageAdapter(AddPurchaseOrder.this,  list_imagePath, dbHelper);
            imageRecyclerview.setAdapter(imageAdapter);
        }
    }


    //validate
    private boolean validation() {
        boolean check;
        try {
            vendorName = edtVendorName.getText().toString();
            purchasingDated = edtPurchasingDated.getText().toString();
            purchasedAmt = Double.valueOf(edtPurchasedAmt.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (vendorName.isEmpty() || purchasingDated.isEmpty() || purchasedAmt == 0) {
            Toast.makeText(getContext(), "Please Fill All Details", Toast.LENGTH_SHORT).show();
            check = false;
        } else {
            check = true;
        }

        return check;
    }


//    public void openGallery()
//    {
//
//
//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent,"Select Picture"), PICK_IMAGE_MULTIPLE);
//
//    }
//
//    public void checking_Permission()
//    {
//        int MyVersion = Build.VERSION.SDK_INT;
//        if (MyVersion < Build.VERSION_CODES.M)
//        {
//            openGallery();
//        }
//        else
//        {
//            if (getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED
//                    && getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)== PackageManager.PERMISSION_GRANTED)
//            {
//                openGallery();
//            }
//
//            else
//            {
//                String[]permissionRequest={Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE};
//                requestPermissions(permissionRequest,GALLERY_REQUEST_CODE);
//            }
//        }
//    }
//
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        {
//            if (requestCode==GALLERY_REQUEST_CODE)
//            {
//                if(grantResults[0]==PackageManager.PERMISSION_GRANTED)
//                {
//                    openGallery();
//                }
//                else
//                {
//                    Toast.makeText(getContext(), "permission not grated", Toast.LENGTH_SHORT).show();
//                }
//            }
//        }
//    }
//


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        try {
//            // When an Image is picked
//            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK && null != data) {
//
//                    if (data.getClipData() != null) {
//                        ClipData mClipData = data.getClipData();
//
//                        for (int i = 0; i < mClipData.getItemCount(); i++) {
//
//                            ClipData.Item item = mClipData.getItemAt(i);
//                            Uri uri = item.getUri();
//                            mArrayUri.add(uri);
//                            Log.v("LOG_TAG", "Selected Images" + mArrayUri.get(i));
//
//                        }
//                        Log.v("LOG_TAG", "Selected Images" + mArrayUri.size());
//                        Bitmap bitmap = null;
//                        try {
//                            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mArrayUri.get(1));
//                        } catch (IOException e) { e.printStackTrace(); }
//
//                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//                        // reducing the size of bit map image
//                        bitmap.compress(Bitmap.CompressFormat.JPEG, 10, stream);
//                        byte[] byteArray = stream.toByteArray();
//                        Bitmap compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
////                        img_camera.setImageBitmap(compressedBitmap);
//
//                        settingRecycle();
//
//                    }
//
//            } else {
//                Toast.makeText(getContext(), "You haven't picked Image",
//                        Toast.LENGTH_LONG).show();
//            }
//        } catch (Exception e) {
//            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_LONG)
//                    .show();
//        }
//
//        super.onActivityResult(requestCode, resultCode, data);
//    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);

            for (int i = 0; i < images.size(); i++)
            { list_imagePath.add(images.get(i).path); }
            settingRecycle();
            Log.e("no_of_images ",list_imagePath.size()+"" );
        } }


    public void call_ImageClass()
    {
        Intent intent = new Intent(getContext(), AlbumSelectActivity.class);
        intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 8);
        startActivityForResult(intent, Constants.REQUEST_CODE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_addImage:
                if (validation()) {
                    call_ImageClass();
                }
                break;

            case R.id.img_edit:
                list_imagePath.clear();
                call_ImageClass();
                break;

            case R.id.btnSave:

                if (validation()) {
                    File file = creating_Directory();
                    if (file != null)
                    {
                        try {
                            storing_ImageToFolder(file);
                        } catch (JSONException e) { e.printStackTrace();
                        }
                    }
                }
                break;

            case R.id.img_BackPress:
                getActivity().onBackPressed();
                break;

            case R.id.ll_Purchasedate:
            case R.id.edtPurchasedt:
                datepickerr();
                break;
        }
    }


    public File creating_Directory()
    {
        String folder_main = "SlipKode";
        File sd = Environment.getExternalStorageDirectory();

        File directory = new File(sd.getAbsolutePath() + "/" + folder_main);
        //create directory if not exist
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        return directory;
    }

    public void storing_ImageToFolder(File directory) throws JSONException {
        File image_name = null, file = null;
        Uri uri = null;
        Bitmap bitmap = null;
        ArrayList<String> local_ImagePath = new ArrayList<>();
        // for adding orders
        if (action == 0)
        {
            for (int i = 0; i < list_imagePath.size(); i++)
            {

                try {
                    // ====================== CONVERTING URI INTO BITMAP =============================== //
                    uri = Uri.parse(basePath+list_imagePath.get(i));
                    image_name = new File(uri.getPath());
                    Log.e("filename ",image_name.getName()+"");
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(),uri);
                    Log.e("image ", uri+"");
                    // ====================== CONVERTING URI INTO BITMAP =============================== //

                    // ====================== SAVING IMAGE IN FOLDER ================================//
                    file = new File(directory, image_name.getName());
                    OutputStream output = new FileOutputStream(file);
                    // Compress into png format image from 0% - 100%
                    bitmap.compress(Bitmap.CompressFormat.PNG, 10, output);
                    output.flush();
                    output.close();
                    // local path of saved image
                    local_ImagePath.add(basePath+directory+"/"+image_name.getName());
                    // ====================== SAVING IMAGE IN FOLDER ================================//
                }

                catch (Exception e) { e.printStackTrace();

                    Log.v("file exception ",e+"");


                }
            }}
        // for edit order
        else if (action == 1)
        {

            for (int i = 0; i < list_imagePath.size(); i++)
            {
                try {
                    uri = Uri.parse(list_imagePath.get(i));
                    image_name = new File(uri.getPath());
                    Log.e("filename ",image_name.getName()+"");
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    Log.e("image ", uri + "");
                    // ====================== SAVING IMAGE IN FOLDER ================================//
                    file = new File(directory, image_name.getName());
                    OutputStream output = new FileOutputStream(file);
                    // Compress into png format image from 0% - 100%
                    bitmap.compress(Bitmap.CompressFormat.PNG, 10, output);
                    output.flush();
                    output.close();

                }
                catch (Exception e) { e.printStackTrace();
                    Log.v("file exception ",e+"");
                }

            }}

        if (action == 0) {
            // storing new purchase dta in database
            int purchaseID = dbHelper.insertPurchaseDetails(new PurchaseModel(0, vendorName, purchasingDated, purchasedAmt), local_ImagePath);
            if (purchaseID > 0) {
                // sending new purchase data to purchaseNewActivity
                Log.e("purchaseID ", purchaseID + "");
                purchaseModel = new PurchaseModel(purchaseID, vendorName, purchasingDated, purchasedAmt);
                interfacess(purchaseModel, 0);
                getActivity().getSupportFragmentManager().popBackStack();
            }
        }
        else if (action == 1)
        {
            if (dbHelper.updatePurchaseDetails(purchase_id,vendorName, purchasingDated, purchasedAmt, list_imagePath))
            {
                interfacess(null, 0);
//                Toast.makeText(getActivity(), "updated", Toast.LENGTH_SHORT).show();
                getActivity().getSupportFragmentManager().popBackStack();
            }
            else  Toast.makeText(getContext(), "not updated", Toast.LENGTH_SHORT).show();
            action = 0;
        }

        if (MakeView.isTablet(getActivity()))  clearFields();

    }


    public void interfacess(PurchaseModel purchaseModel, int go_back)
    {
        comunicatorPurchaseOrder = (ComunicatorPurchaseOrder) getContext();
        comunicatorPurchaseOrder.order(purchaseModel, go_back);
    }

    // tablet
    public void clearFields()
    {
        if (MakeView.isTablet(getContext())) {
            edtVendorName.getText().clear();
            edtPurchasedAmt.getText().clear();
            showDate(yy, mm, dd);
            list_imagePath.clear();
            imageAdapter.notifyDataSetChanged();
            imageRecyclerview.setVisibility(View.GONE);
            img_addImage.setVisibility(View.VISIBLE);
            img_edit.setVisibility(View.GONE);
        }
    }






    public void datepickerr() {
        datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                if (year > yy) {
                    showDate(yy, mm, dd);
                } else if (monthOfYear > mm && year == yy) {
                    showDate(yy, mm, dd);
                } else if (dayOfMonth > dd && monthOfYear == mm && year == yy) {
                    showDate(yy, mm, dd);

                } else {
                    showDate(year, monthOfYear, dayOfMonth);
                }
            }
        }, yy, mm, dd);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }






//    //Edit purchased Detials
//    private void updatePurchaseDetails() {
//        purchaseModel = (PurchaseModel) getIntent().getSerializableExtra("purhaseDetails");
//        arrayList1 = (ArrayList<String>) getIntent().getSerializableExtra("purchasedImage");
//
//        Log.e(TAG + " ImageList", arrayList1 + "");
//
//        if (purchaseModel != null) {
//            Log.e("size", purchaseModel.getPurchasedId() + "");
//            edtVendorName.setText(purchaseModel.getVendorName());
//            edtPurchasingDated.setText(purchaseModel.getPurchased_Date());
//            edtPurchasedAmt.setText(UseSingleton.getInstance().decimalFormat(purchaseModel.getPurchaseAmount()));
//            try {
//                header = getIntent().getExtras().getString("header");
//                if (!header.isEmpty()) {
//                    txt_home.setText(header);
//                    settingRecycle(arrayList1);
//                    imageRecyclerview.setVisibility(View.VISIBLE);
//                }
//            } catch (Exception e) {
//            }
//        }
//    }





//    private void purchaseData() {
//
//        JSONObject jsonObjectPurchase = new JSONObject();
//        JSONArray jsonArrayPurchase = new JSONArray();
//        JsonObject jsonObject = new JsonObject();
//        ArrayList<PurchaseModel> arrayListPurchase = new ArrayList<>();
//        arrayListPurchase = dbHelper.getPurchaseDtailsFromDB();
//        try {
//            for (int i = 0; i < arrayListPurchase.size(); i++) {
//                JSONObject jsonObjectPurchaseInside = new JSONObject();
//                jsonObjectPurchaseInside.put("purchase_id", arrayListPurchase.get(i).getPurchasedId());
//                jsonObjectPurchaseInside.put("vendor_name", arrayListPurchase.get(i).getVendorName());
//                jsonObjectPurchaseInside.put("purchase_date", arrayListPurchase.get(i).getPurchased_Date());
//                jsonObjectPurchaseInside.put("purchase_amount", arrayListPurchase.get(i).getPurchaseAmount());
////                jsonObject.addProperty("image", "/storage/emulated/0/Pictures/Screenshots/Screenshot_2018-05-14-12-50-29.png");
//                jsonObjectPurchaseInside.put("image", arrayList + "");
//                jsonArrayPurchase.put(jsonObjectPurchaseInside);
//                Log.e("purchase added ", jsonArrayPurchase.toString());
//            }
//            jsonObjectPurchase.put("user_id", SharedPref.getInstance(this).getLoginDetails().get(0));
//            jsonObjectPurchase.put("max_id", dbHelper.getLastRowId(Purchased_Table_Name, "purchase_id"));
//            jsonObjectPurchase.put("data", jsonArrayPurchase);
//            Log.e("fianlData", jsonObjectPurchase.toString());
//        } catch (Exception e) {
//        }
//
//
//        RequestBody purchaseBody = RequestBody.create(MediaType.parse("text/plain"), jsonObjectPurchase.toString());
//        RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
//        Call<APIModel> apiModelCall = retrofitInterface.sendAndGetPurchase(purchaseBody);
//
//        apiModelCall.enqueue(new Callback<APIModel>() {
//            @Override
//            public void onResponse(Call<APIModel> call, Response<APIModel> response) {
//                if (response.isSuccessful()) {
//                    if (response.body().getSuccess()) {
//                        Log.d("Response", response.body().getSuccess() + "");
//                        Log.d("Response", response.body().getMessage() + "");
//                        List<PurchaseModel> details = response.body().getPurchaseData();
//                        Log.d("Response Data", details + "");
////                        dbHelper.insertPurchaseDetails(details, );
////                        Toast.makeText(AddPurchaseDetailtsActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//
//                    } else {
//                        Log.e("Response else", response.body() + "");
//                        Toast.makeText(AddPurchaseDetailtsActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<APIModel> call, Throwable t) {
//                Log.e("Response", t.getMessage());
//                Toast.makeText(AddPurchaseDetailtsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
//
//            }
//        });
//    }




}
