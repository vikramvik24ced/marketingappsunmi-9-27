package com.sunmi.printerhelper.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Adapter.CartAlItemsAdapter;
import com.sunmi.printerhelper.Adapter.TaxAdapter;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.Model.OrderModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.Model.TaxModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.CustomerActivity;


import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.AddToCart_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Temp_Table_Name;

public class CartAltemsFrag extends AppCompatActivity {
    //Views
    View view;
    ImageView imgSettings, img_UpDown_Arrow;

    // no use (txt_AfterDiscountt, txt_DefaultTaxName, txt_taxPerc, txt_taxAmount)
    TextView txt_TotalItems, txt_ItemsAmount, txt_Discount, txt_FinalAmt_PAY, txt_NoProdcutAvail, txt_DiscountPerc, txt_CustName;
    LinearLayout  ll_Hide_Unhide;
    @BindView(R.id.ll_add_customer) LinearLayout ll_add_customer;
    @BindView(R.id.ll_selected_customer) LinearLayout ll_selected_customer;
    @BindView(R.id.ll_customer_parent) LinearLayout ll_customer_parent;
    @BindView(R.id.cv_recyclerview) CardView cv_recyclerview;
    @BindView(R.id.ll_discountClick) LinearLayout ll_discountClick;
    @BindView(R.id.ll_upArrow) LinearLayout ll_upArrow;
    @BindView(R.id.rl_PayNow) RelativeLayout rl_PayNow;
    RelativeLayout  rootView,ll_Discount;
    //Recyclerview Setting
    RecyclerView rv_CartAlItems, rv_taxes;
    CartAlItemsAdapter cartAlItemsAdapter;
    TaxAdapter taxAdapter;
    ArrayList<ProductModel> ItemsSelectedByUser = new ArrayList<>();
    ArrayList<OrderModel> orderArrayList = new ArrayList<>();
    ArrayList<ProductModel> quantityAndTotalAmtt = new ArrayList<>();
    RecyclerView.LayoutManager layoutManager;
    //  variables, arrays, fragments, classes;
    Fragment fragment;
    private double discountAmt = 0, discount_provided=0;
    int pro_id, deleted =0, discount_check = 0, up_down = 0;
    int discunt_check = 0;
    double totalAmtBeforeDis = 0;
    // database
    DBHelper dbHelper;
    // interface
    CustomerDetailsModel customerDetailsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing);
        dbHelper = new DBHelper(this);
        ButterKnife.bind(this);
        init();
        getDataFromTempTable();
        settingRecyclerView();
        getCustomer();
        softKeypad();
    }

    public void init() {
        rv_CartAlItems = findViewById(R.id.rv_CartAlItems);
        rv_taxes = findViewById(R.id.rv_taxes);
        img_UpDown_Arrow = findViewById(R.id.img_UpDown_Arrow);
        txt_TotalItems = findViewById(R.id.txt_TotalItems);
        txt_ItemsAmount = findViewById(R.id.txt_ItemsAmount);
        txt_Discount = findViewById(R.id.txt_Discount);
        txt_FinalAmt_PAY = findViewById(R.id.txt_FinalAmt_PAY);
        txt_DiscountPerc = findViewById(R.id.txt_DiscountPerc);
        imgSettings = findViewById(R.id.imgSettings);
        txt_NoProdcutAvail = findViewById(R.id.txt_NoProdcutAvail);
        txt_CustName = findViewById(R.id.txt_CustName);
        ll_Hide_Unhide = findViewById(R.id.ll_Hide_Unhide);
        ll_Discount = findViewById(R.id.ll_Discount);
        rootView = findViewById(R.id.rootView);
    }

    public void getDataFromTempTable() {
        // get data from temp table
        ArrayList<ProductModel> dataFromTempTable = dbHelper.getDataFrom_TempTable();
        for (int i = 0; i < dataFromTempTable.size(); i++)
        {
            quantityAndTotalAmtt.add(new ProductModel(dataFromTempTable.get(i).getQty(), dataFromTempTable.get(i).getPrice()));
            ItemsSelectedByUser.add(dbHelper.getProductRow(dataFromTempTable.get(i).getPro_id()));
        }
        Log.e("CartAltemsFrag","size: "+quantityAndTotalAmtt.size()+ " "+ItemsSelectedByUser.size());

        if (SharedPref.getInstance(this).getDiscount() != 0)
        {
            discount_provided = SharedPref.getInstance(this).getDiscount();
            discount_check = SharedPref.getInstance(this).getDiscountType();
            txt_DiscountPerc.setText(UseSingleton.getInstance().decimalFormat(discount_provided)+"%");
            if (SharedPref.getInstance(this).getDiscountType() == 0) {
                txt_DiscountPerc.setVisibility(View.VISIBLE);
            }
            else if (SharedPref.getInstance(this).getDiscountType() ==  1)
            {
                txt_DiscountPerc.setVisibility(View.INVISIBLE);
            }
        }
    }

    public void settingRecyclerView() {
        Log.e("agya:(CartAltemsFrag) ", quantityAndTotalAmtt.toString());
        cartAlItemsAdapter = new CartAlItemsAdapter(this, CartAltemsFrag.this, ItemsSelectedByUser, quantityAndTotalAmtt, dbHelper);
        layoutManager = new LinearLayoutManager(this);
        rv_CartAlItems.setLayoutManager(layoutManager);
        rv_CartAlItems.setAdapter(cartAlItemsAdapter);
        calculateTotalbillAmountBeforeDis(quantityAndTotalAmtt);
    }

    public void settingRecyclerView_Tax(double amtAfterDiscount) {
        taxAdapter = new TaxAdapter(dbHelper.getSelectedTax(),this,null,null,amtAfterDiscount,1);
        rv_taxes.setLayoutManager(new LinearLayoutManager(this));
        rv_taxes.setAdapter(taxAdapter);
    }


    public void getItemDeleted(int pos) {

        // sending position of deleted item to 'SmallCaptureActivity' to decrease the item count
        deleted = 1;
        pro_id = ItemsSelectedByUser.get(pos).getPro_id();
        dbHelper.delete_Pro_From_AddToCart(pro_id,AddToCart_Table_Name,"pro_idd");
        // delete data from temp table
        dbHelper.delete_Pro_From_AddToCart(pro_id,Temp_Table_Name,"pro_idd");
        // sending position of deleted item to 'SmallCaptureActivity' to decrease the item count
        Log.e("deleted: ", pos + "");
        Log.e("deleted item: ", ItemsSelectedByUser.get(pos).getPro_name() + " ");
        Log.e("deleted quantityandAmt ", quantityAndTotalAmtt.get(pos) + " ");
        Log.e("del totalItems: ", cartAlItemsAdapter.getItemCount() + "");
        ItemsSelectedByUser.remove(pos);
        quantityAndTotalAmtt.remove(pos);
        if (ItemsSelectedByUser.isEmpty() && quantityAndTotalAmtt.isEmpty()) {
            ll_customer_parent.setVisibility(View.GONE);
            cv_recyclerview.setVisibility(View.GONE);
            txt_NoProdcutAvail.setVisibility(View.VISIBLE);
            ll_discountClick.setEnabled(false); ll_upArrow.setEnabled(false); rl_PayNow.setEnabled(false);
            txt_FinalAmt_PAY.setText("0 SAR");
        } else {
            calculateTotalbillAmountBeforeDis(quantityAndTotalAmtt);
        }
    }

    public String getItemUpdate(int pos, Double updatedQuantity) {
        double u_itemTotalAmt = 0;
        Log.e("update ", pos + "");
        Log.e("update edittext ", updatedQuantity+"");
        u_itemTotalAmt = updatedQuantity * ItemsSelectedByUser.get(pos).getPrice();
        quantityAndTotalAmtt.set(pos, new ProductModel(updatedQuantity, u_itemTotalAmt));
        calculateTotalbillAmountBeforeDis(quantityAndTotalAmtt);
        pro_id = ItemsSelectedByUser.get(pos).getPro_id();
        Log.e("CartAltemsFrag", "product " + ItemsSelectedByUser.get(pos).getPro_name() + " with updated QTY " + quantityAndTotalAmtt.get(pos).getQty());
        // checking whether product is available im Temp table or not, if '1' means available
        if (dbHelper.Pro_Available_To_AddToCart(pro_id,Temp_Table_Name,"pro_idd") == 1)
        {
            dbHelper.updateTempTable(pro_id,updatedQuantity,u_itemTotalAmt);
        }
        return String.valueOf(u_itemTotalAmt);
    }


    //Calculate Amount before dicount
    public void calculateTotalbillAmountBeforeDis(ArrayList<ProductModel> quantityAndTotalAmt) {
        double totalBillAmount = 0;
        for (int i = 0; i < quantityAndTotalAmt.size(); i++) {
            totalBillAmount = totalBillAmount + quantityAndTotalAmt.get(i).getPrice();
        }
        Log.e("totalBillAmount ", totalBillAmount + "");
        txt_TotalItems.setText("("+ UseSingleton.getInstance().decimalFormat(cartAlItemsAdapter.getItemCount())+")");
        discount(totalBillAmount, discount_provided, discount_check);
    }

    //  getting discount
    public void discount(double totalAmountbeforeDis, double discountInPercentt, int discount_check) {
        calculateTax(totalAmountbeforeDis, discountInPercentt, discount_check);
    }

    // calculating tax and final amount and discount
    public void calculateTax(double totalAmountbeforeDis, double discount_provided, int discount_check) {
        boolean check = false;
        String taxType = "", taxID = null, taxOnEachItem = null;
        double totalTaxx = 0, amountOfTax = 0, amtAftertax = 0,totalAdditionalTax_new = 0, taxPercent = 0, totalAmountInEx = 0, amtAfterDiscount = 0, totalAdditionalTax = 0;
        ArrayList<TaxModel> listTaxPercentt = new ArrayList<>();
        orderArrayList.clear();
        int[] taxCount = new int[3]; // [0] means total number of taxes, [1] means total number of InclusiveTaxes, [2] means total number of ExclusiveTaxes
        taxCount = dbHelper.searchInclusiveExclusive();
        // inclusive
        if (taxCount[0] == taxCount[1]) {
            Log.e("inclusive ", " NoOfTotalTax: " + taxCount[0] + "     NoOfInclusiveTax: " + taxCount[1]);
            listTaxPercentt = dbHelper.getTaxPercentColumn(1);
            check = true;
            taxType = "in";
        }
        // exclusive
        else if (taxCount[0] == taxCount[2]) {
            Log.e("exclusive ", " NoOfTotalTax: " + taxCount[0] + "     NoOfExclusiveTax: " + taxCount[2]);
            listTaxPercentt = dbHelper.getTaxPercentColumn(2);
            check = true;
            taxType = "ex";
        }

        if (check == true) {
            // default tax
            TaxModel taxModel = dbHelper.getDefaultTax();
            totalTaxx = 0;
            taxPercent = 0;
            // in case of inclusive
            if (taxType.equals("in")) {
                Log.e("inclusive ", taxType);
                taxType = "";
                totalAmountInEx = totalAmountbeforeDis;
                txt_ItemsAmount.setText(UseSingleton.getInstance().decimalFormat(totalAmountbeforeDis));
                if (discount_check == 0) discountAmt = totalAmountInEx * discount_provided / 100;
                else discountAmt = discount_provided;


//                ll_amtAfterDis.setVisibility(View.GONE);
                amtAfterDiscount = totalAmountbeforeDis - discountAmt;
//                totalAdditionalTax = additionaltaxes(amtAfterDiscount);
                Log.e("vickyyy ", totalAmountbeforeDis - discountAmt + "");
                txt_FinalAmt_PAY.setText(roundOff(totalAmountInEx - discountAmt)+" SAR");
                Log.e("before", totalAmountbeforeDis+"");
                if(discount_check == 0) {
                    orderArrayList.add(new OrderModel(0, 1, totalAmountbeforeDis, discount_provided, discountAmt, amtAfterDiscount, null, 0.0, roundOff(totalAmountInEx - discountAmt), null));
                }
                else if(discount_check == 1) {
                    orderArrayList.add(new OrderModel(0, 1, totalAmountbeforeDis, 0, discountAmt, amtAfterDiscount, null, 0.0, roundOff(totalAmountInEx - discountAmt), null));
                    }

            }       // in case of exclusive
            else if (taxType.equals("ex")) {
                Log.e("exclusive ", taxType);

                totalAmountInEx = amtAftertax;
                txt_ItemsAmount.setText(UseSingleton.getInstance().decimalFormat(totalAmountbeforeDis));
                if (discount_check == 0) discountAmt = totalAmountbeforeDis * discount_provided / 100;
                else discountAmt = discount_provided;
                amtAfterDiscount = totalAmountbeforeDis - discountAmt;
                // calculating additional tax
                if (dbHelper.getSelectedTax().size()>0) {
                    settingRecyclerView_Tax(amtAfterDiscount);
                    for (int i = 0; i < dbHelper.getSelectedTax().size(); i++) {
                        taxID = taxID + "," + dbHelper.getSelectedTax().get(i).getId();
                        taxOnEachItem = taxOnEachItem + "," + UseSingleton.getInstance().decimalFormat(amtAfterDiscount * dbHelper.getSelectedTax().get(i).getTaxPercent() / 100);
                        totalAdditionalTax_new = totalAdditionalTax_new + amtAfterDiscount * dbHelper.getSelectedTax().get(i).getTaxPercent() / 100;
                    }

                    taxID = taxID.substring(5); taxOnEachItem = taxOnEachItem.substring(5);
                }
                Log.e("CartAltemsFrag ", "taxID: "+taxID+"  taxOnEachItem: "+taxOnEachItem+"  TotalTaxAmt: "+UseSingleton.getInstance().decimalFormat(totalAdditionalTax_new));
                amountOfTax = (totalAmountbeforeDis - discountAmt) * totalTaxx / 100;
                amountOfTax = amountOfTax + totalAdditionalTax_new;
                txt_FinalAmt_PAY.setText((roundOff((totalAmountbeforeDis - discountAmt)+ amountOfTax))+" SAR");
                Log.e("before", totalAmountbeforeDis + "  discount Amt"+discountAmt);

                if(discount_check == 0) {
                    orderArrayList.add(new OrderModel(0, 2, totalAmountbeforeDis, discount_provided, discountAmt, totalAmountbeforeDis - discountAmt, taxID, amountOfTax, (roundOff((totalAmountbeforeDis - discountAmt) + amountOfTax)), taxOnEachItem));
                    Log.e("before", totalAmountbeforeDis + "  discount Amt"+discountAmt);

                }
                else if(discount_check == 1)
                {
                    orderArrayList.add(new OrderModel(0, 2, totalAmountbeforeDis, 0, discountAmt, totalAmountbeforeDis - discountAmt, taxID, amountOfTax, (roundOff((totalAmountbeforeDis - discountAmt) + amountOfTax)), taxOnEachItem));

                }
                Log.e("ex ", orderArrayList.get(0).getAmtBeforeDiscount() + " " + orderArrayList.get(0).getDiscountPerc() + " " + orderArrayList.get(0).getDiscountAmt() + " " + orderArrayList.get(0).getAmtBeforeTax() + " " + orderArrayList.get(0).getTaxID() + " " + orderArrayList.get(0).getTaxAmt() + " " + orderArrayList.get(0).getAmtAfterTax());
                totalAmountbeforeDis = 0;
            }

            if (discount_provided > 0) {
                ll_Discount.setVisibility(View.VISIBLE);
                txt_DiscountPerc.setText(UseSingleton.getInstance().decimalFormat(discount_provided) + "%");
                txt_Discount.setText(UseSingleton.getInstance().decimalFormat(discountAmt));
                if (discount_check == 1) txt_DiscountPerc.setVisibility(View.INVISIBLE);
                else txt_DiscountPerc.setVisibility(View.VISIBLE);
            }
            check = false;
        }
    }

    public String roundOff(double d)
    {
        return String.valueOf((int)Math.round(d));
    }

    @OnClick({R.id.ll_upArrow, R.id.ll_discountClick,R.id.imgBillingBack,R.id.ll_add_customer,R.id.ll_DeleteCustomer,R.id.rl_PayNow,R.id.ll_selected_customer,
            R.id.img_deleteDiscunt})
    public void up_down_arrow(View view)
    {
        Intent intent = null;
        Bundle bundle = new Bundle();
        switch (view.getId())
        {
            case R.id.ll_upArrow:
                if (up_down == 0) {
                    up_down = 1;
                    ll_Hide_Unhide.setVisibility(View.VISIBLE);
                    img_UpDown_Arrow.setImageResource(R.drawable.down_arrow);
                    break;
                }
                else if (up_down == 1){
                    up_down = 0;
                    ll_Hide_Unhide.setVisibility(View.GONE);
                    img_UpDown_Arrow.setImageResource(R.drawable.up_arrow);
                    break;}
                break;

            case R.id.ll_discountClick:
                discount_AlertDialog();
                break;

            case R.id.imgBillingBack:
                onBackPressed(); break;

            case R.id.ll_add_customer:
                intent = new Intent(CartAltemsFrag.this, CustomerActivity.class);
                intent.putExtra("object","CartAltemsFrag");
                startActivityForResult(intent, 10);
                break;

            case R.id.ll_DeleteCustomer:
                SharedPref.getInstance(this).setCustID(0);
                ll_selected_customer.setVisibility(View.GONE);
                ll_add_customer.setVisibility(View.VISIBLE);
                customerDetailsModel = null;
                break;

            case R.id.ll_selected_customer:
                intent = new Intent(CartAltemsFrag.this, CustomerActivity.class);
                intent.putExtra("object","CartAltemsFrag");
                startActivityForResult(intent, 10);
                break;

            case R.id.rl_PayNow:
                int cust_id = 0;
                if (customerDetailsModel != (null)) cust_id = customerDetailsModel.getCustomerId();
                intent = new Intent(this,PaymentFrag.class);
                intent.putExtra("cust_id",cust_id);
                intent.putExtra("orderArrayList",orderArrayList);
                intent.putExtra("ItemsSelectedByUser",ItemsSelectedByUser);
                intent.putExtra("quantityAndTotalAmtt",quantityAndTotalAmtt);
                startActivity(intent);
                break;

            case R.id.img_deleteDiscunt:
                Toast.makeText(this, "delete discount", Toast.LENGTH_SHORT).show();
                if (discount_provided > 0)
                { up_down = 1;

                    Log.e(" pooppp ",totalAmtBeforeDis+"    "+discount_provided+"     "+discount_check);
                    discount_provided = 0; discount_check = 0;
                    discount(totalAmtBeforeDis, discount_provided, discunt_check);
                    img_UpDown_Arrow.setImageResource(R.drawable.down_arrow);
                    ll_Discount.setVisibility(View.GONE);
                    SharedPref.getInstance(CartAltemsFrag.this).setDiscountType(0);
                    SharedPref.getInstance(CartAltemsFrag.this).setDiscount(0);
                    discount(totalAmtBeforeDis, discount_provided, discunt_check);
                }
                break;
        }
    }


    public void discount_AlertDialog()
    {
        final AlertDialog dialog = new AlertDialog.Builder(CartAltemsFrag.this, R.style.alertDialog).create();
        LayoutInflater inflater = LayoutInflater.from(CartAltemsFrag.this);
        View alertView = inflater.inflate(R.layout.dialog_discount, null);
        final EditText edtEnterQuantity = alertView.findViewById(R.id.edtEnterQuantity);
        final TextView txt_percent = alertView.findViewById(R.id.txt_percent);
        final TextView txt_sar = alertView.findViewById(R.id.txt_sar);
        TextView btnAddToCart = alertView.findViewById(R.id.btnAddToCart);
        TextView btnCancle = alertView.findViewById(R.id.btnCancle);

        if (SharedPref.getInstance(this).getDiscount() > 0)
        {
            edtEnterQuantity.setText(UseSingleton.getInstance().decimalFormat(SharedPref.getInstance(this).getDiscount()));
            edtEnterQuantity.setSelection( edtEnterQuantity.getText().length());
            if (SharedPref.getInstance(this).getDiscountType() == 0)
            {
                txt_percent.setBackgroundResource(R.color.BABABA);
                txt_percent.setTextColor(getResources().getColor(R.color.black));
            }
            else if(SharedPref.getInstance(this).getDiscountType() == 1)
            {
                txt_percent.setBackgroundResource(R.color.white);
                txt_sar.setBackgroundResource(R.color.BABABA);
            }
        }


        btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtEnterQuantity.getText().toString().isEmpty()) {
                    Toast.makeText(CartAltemsFrag.this, "Enter amount for discount", Toast.LENGTH_SHORT).show();
                }
                else if (Double.valueOf(edtEnterQuantity.getText().toString()) <= 0)
                {
                    Toast.makeText(CartAltemsFrag.this, "Enter a valid discount", Toast.LENGTH_SHORT).show();
                }
                else {

                    discount_provided = Double.valueOf(edtEnterQuantity.getText().toString());
                    Log.e("amt ", txt_ItemsAmount.getText() + "");
                    // calculating total amount
                    for (int i = 0; i < quantityAndTotalAmtt.size(); i++) {
                        totalAmtBeforeDis = totalAmtBeforeDis + quantityAndTotalAmtt.get(i).getPrice();
                    }
                    Log.e("amt vicky ", totalAmtBeforeDis + "  " + discount_provided);
                    if (discunt_check == 0){
                        discount(totalAmtBeforeDis, discount_provided, discunt_check);
                    }
                    else if(discunt_check == 1){
                        discount(totalAmtBeforeDis, discount_provided, discunt_check);
                    }
                    SharedPref.getInstance(CartAltemsFrag.this).setDiscount((float) discount_provided);
                    SharedPref.getInstance(CartAltemsFrag.this).setDiscountType(discunt_check);
                    dialog.dismiss();
                    up_down = 1;
                    ll_Hide_Unhide.setVisibility(View.VISIBLE);
                    img_UpDown_Arrow.setImageResource(R.drawable.down_arrow);



                }
            }
        });
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        txt_percent.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                discunt_check = 0;
                txt_percent.setBackgroundResource(R.color.BABABA);
                txt_sar.setBackgroundResource(R.color.white);
            }
        });
        txt_sar.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                discunt_check = 1;
                txt_percent.setBackgroundResource(R.color.white);
                txt_sar.setBackgroundResource(R.color.BABABA);
            }
        });
        dialog.setView(alertView);
        dialog.show();
    }


    @Override
    public void onBackPressed() {
        Intent resultIntent = new Intent();
        resultIntent.putExtra("SmallCaptureToken",1);
        resultIntent.putExtra("goBack",0);
        SharedPref.getInstance(this).setBarCodeScannerActivity(true);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }


    // getting data from Customer Activity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 10) { // Please, use a final int instead of hardcoded int value
            if (resultCode == 100) {
                if (data != null) {
                    customerDetailsModel =  getCustomerFromDB(data.getExtras().getInt("cust_ID"));
                    txt_CustName.setText(customerDetailsModel.getCustomerName());
                    ll_add_customer.setVisibility(View.GONE);
                    ll_selected_customer.setVisibility(View.VISIBLE);

                }
            }}}

    public void getCustomer()
    {
        if (SharedPref.getInstance(this).getCustID() == 0)
        {
            ll_add_customer.setVisibility(View.VISIBLE);
            ll_selected_customer.setVisibility(View.GONE);
        }
        else // if customer is available in database
        {
            customerDetailsModel = getCustomerFromDB(SharedPref.getInstance(this).getCustID());
            txt_CustName.setText(customerDetailsModel.getCustomerName());
            ll_add_customer.setVisibility(View.GONE);
            ll_selected_customer.setVisibility(View.VISIBLE);
        }
    }

    public CustomerDetailsModel getCustomerFromDB(int cusID)
    {
        Log.e("getCustomerFromDB ", cusID+"");
        customerDetailsModel =  dbHelper.getCustomer(cusID);
        return customerDetailsModel;
    }

    public void softKeypad()
    {
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                //r will be populated with the coordinates of your view that area still visible.
                rootView.getWindowVisibleDisplayFrame(r);


                Log.e("root view ",rootView.getRootView().getHeight()+"");
                Log.e("root second ",r.bottom - r.top+"");

                int heightDiff = rootView.getRootView().getHeight() - (r.bottom - r.top);
                Log.e("diff", heightDiff+"");
                if (heightDiff > 100) { // if more than 100 pixels, its probably a keyboard...
                    Log.e("hiii", "opend");
                    // llllll.setVisibility(View.VISIBLE);
                }
                else
                {
                    Log.e("hiii", "closed");
                    //  llllll.setVisibility(View.GONE);
                }
            }
        });
    }


}
