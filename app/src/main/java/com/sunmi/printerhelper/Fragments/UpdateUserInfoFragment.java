package com.sunmi.printerhelper.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.sunmi.RetrofitDetails.APIClient;

import com.sunmi.RetrofitDetails.RetrofitInterface;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.Model.APIModel;
import com.sunmi.printerhelper.Model.LoginModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.BaseApp;
import com.sunmi.printerhelper.activity.ConnectivityReceiver;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateUserInfoFragment extends Fragment implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    TextView txt_WifiOnOff;
    ImageView img_WifiOnOff, img_BackPress;
    Button btnUpdate;
    private Dialog myDialog;


    EditText edtUserName, edtAddress, edtCompName, edtMobile, edtRegNo, edtBusinessType, edtGender;
    String username, address, compname, mobile, regno, businessType, gender;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_update_user_info, container, false);
        init(v);
        getUserInfo();
        myDialog = new Dialog(getContext());
        txt_WifiOnOff = v.findViewById(R.id.txt_WifiOnOff);
        img_WifiOnOff = v.findViewById(R.id.img_WifiOnOff);
        checkConnection();
        return v;
    }

    private void init(View v) {
        edtUserName = v.findViewById(R.id.edtUserName);
        edtAddress = v.findViewById(R.id.edtAddress);
        edtCompName = v.findViewById(R.id.edtCompName);
        edtMobile = v.findViewById(R.id.edtMobileNo);
        edtRegNo = v.findViewById(R.id.edtResNo);
        edtBusinessType = v.findViewById(R.id.edtBusinessType);
        edtGender = v.findViewById(R.id.edtGender);
        btnUpdate = v.findViewById(R.id.btnUpdate);
        img_BackPress = v.findViewById(R.id.img_BackPress);
        btnUpdate.setOnClickListener(this);
        img_BackPress.setOnClickListener(this);

    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        wifiStatus(isConnected);
    }

    private void wifiStatus(boolean isConnected) {
        if (isConnected) {
            txt_WifiOnOff.setText("Online");
            img_WifiOnOff.setImageResource(R.drawable.wifi_on_white);
        } else {
            txt_WifiOnOff.setText("Offline");
            img_WifiOnOff.setImageResource(R.drawable.wifi_off_white);
        }
    }

//    @Override
//    public void onNetworkConnectionChanged(boolean isConnected) {
//        wifiStatus(isConnected);
//    }

    @Override
    public void onResume() {
        super.onResume();
        BaseApp.getInstance().setConnectivityListener(this);
    }

    private void getUserInfo() {
        edtUserName.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(5));
        edtAddress.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(9));
        edtMobile.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(6));
        edtCompName.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(1));
        edtRegNo.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(2));
        edtBusinessType.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(3));
        edtGender.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(8));
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnUpdate:
                username = edtUserName.getText().toString();
                address = edtAddress.getText().toString();
                compname = edtCompName.getText().toString();
                mobile = edtMobile.getText().toString();
                regno = edtRegNo.getText().toString();
                businessType = edtBusinessType.getText().toString();
                gender = edtGender.getText().toString();
                if (ConnectivityReceiver.isConnected()) {
                    updateUserDetails(username, address, compname, mobile, regno, businessType, gender);
                } else {
                    myDialog.setContentView(R.layout.no_internet);
                    myDialog.show();
                }
                break;

            case R.id.img_BackPress:
                getActivity().onBackPressed();
                break;
        }
    }

    private void updateUserDetails(String username, String address, String compname, String mobile,
                                   String regno, String businessType, String gender) {

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("user_id", SharedPref.getInstance(getContext()).getLoginDetails().get(0));
        jsonObject.addProperty("name", username);
        jsonObject.addProperty("address", address);
        jsonObject.addProperty("company_name", compname);
        jsonObject.addProperty("contact", mobile);
        jsonObject.addProperty("reg_number", regno);
        jsonObject.addProperty("business_type", businessType);
        jsonObject.addProperty("gender", gender);
        Toast.makeText(getContext(), "Information updated successfully", Toast.LENGTH_SHORT).show();
        RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
        Call<APIModel> apiModelCall = retrofitInterface.updateUser(jsonObject);
        apiModelCall.enqueue(new Callback<APIModel>() {
            @Override
            public void onResponse(Call<APIModel> call, Response<APIModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        List<LoginModel> details = response.body().getData();
                        SharedPref.getInstance(getActivity()).setLoginDetails(getActivity(), details.get(0).getUserId(), details.get(0).getCompanyName(), details.get(0).getRegNumber(),
                                details.get(0).getBusinessType(), details.get(0).getSubscriptionType(), details.get(0).getName(),
                                details.get(0).getContact(), details.get(0).getEmail(), details.get(0).getGender(), details.get(0).getAddress(), details.get(0).getLoginTime(), details.get(0).getExpireLogin());
                        getActivity().onBackPressed();
                    } else {
                        Log.e("Response", response.body() + "");
                        Toast.makeText(getContext(), "Wrong", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<APIModel> call, Throwable t) {
                Log.e("Error", t.getMessage());

            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {


        Toast.makeText(getContext(), "update customer " + isConnected, Toast.LENGTH_SHORT).show();
    }

}
