package com.sunmi.printerhelper.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Adapter.PurchaseAdapter;
import com.sunmi.printerhelper.Model.PurchaseModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.AddPurchaseDetailtsActivity;

import java.util.ArrayList;


public class PurchasedFragment extends Fragment implements View.OnClickListener {

    //Views
    View v;
    Context context;
    ImageView img_back, fb_AddPurchased;
    TextView txtNoCustomer;
    EditText searchDetails;
    PurchaseAdapter purchaseAdapter;
    RecyclerView purchasingList;
    ArrayList<PurchaseModel> purchaseModelArrayList, tlist;
    //String
    String TAG = "PurchasedFrag :";
    //Database
    DBHelper dbHelper;
    // interface

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(v == null) {
            v = inflater.inflate(R.layout.fragment_purchase, container, false);
            dbHelper = new DBHelper(getContext());
            purchaseModelArrayList = dbHelper.getPurchaseDtailsFromDB();
            init(v);
            tlist = new ArrayList<>();
            searching();
        }
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        settingRecyclerView();
    }


    // written by himanshu
    private void init(View view) {
        fb_AddPurchased = view.findViewById(R.id.fb_AddPurchased);
        img_back = view.findViewById(R.id.imgBillingBack);
        purchasingList = view.findViewById(R.id.purchaseRecycler);
        searchDetails = view.findViewById(R.id.searchh);
        txtNoCustomer = view.findViewById(R.id.no_customer);
        img_back.setOnClickListener(this);
        fb_AddPurchased.setOnClickListener(this);
    }

    private void settingRecyclerView() {
        purchasingList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        purchaseAdapter = new PurchaseAdapter(null,getContext(), dbHelper.getPurchaseDtailsFromDB(), dbHelper, null);
        purchasingList.setAdapter(purchaseAdapter);
    }    private void searching() {
        searchDetails.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String name = s.toString();
                String vendorName;
                String[] amount = {null};
                if (!name.equals("")) {
                    for (int i = 0; i < purchaseModelArrayList.size(); i++) {
                        amount[0] = String.valueOf(purchaseModelArrayList.get(i).getPurchaseAmount());
                        vendorName = purchaseModelArrayList.get(i).getVendorName();
                        if (vendorName.toLowerCase().contains(name.toLowerCase()) || amount[0].toLowerCase().contains(name.toLowerCase())) {
                            tlist.add(purchaseModelArrayList.get(i));
                        }
                    }
                    if (tlist.size() == 0) {
                        tlist.clear();
                        purchaseAdapter.search(tlist);
                        txtNoCustomer.setVisibility(View.VISIBLE);
                    } else {
                        txtNoCustomer.setVisibility(View.GONE);
                        purchaseAdapter.search(tlist);
                        tlist.clear();
                    }
                } else if (count == 0) {
                    txtNoCustomer.setVisibility(View.GONE);
                    settingRecyclerView();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    // written by himanshu


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBillingBack:
                getActivity().onBackPressed();
                break;

            case R.id.fb_AddPurchased:
                getActivity().startActivity(new Intent(getContext(), AddPurchaseDetailtsActivity.class));
                break;
        }
    }
}
