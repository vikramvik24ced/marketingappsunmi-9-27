package com.sunmi.printerhelper.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Adapter.CustomerAdapter;
import com.sunmi.printerhelper.Adapter.CustomerAdapter_Tablet;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.R;
import org.json.JSONException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.IllegalFormatCodePointException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Customer_Table_Name;

public class CustomerFrag extends Fragment {

    View view;
    // recyclerview
    @BindView(R.id.rv_customerInfo)
    RecyclerView rv_customerInfo;
    // edittext
    @BindViews({R.id.edtCustomerName,R.id.edtMobileNo,R.id.edtEmail,R.id.edtAddress})
    List<EditText> bk_listEditText;
    // button
    @BindViews({R.id.btnAddCustomer})
    List<Button> bk_listButton;
    // edit text
    @BindView(R.id.searchh)
    EditText searchh;
    // text view
    @BindViews({R.id.txtTime,R.id.txtDate,R.id.txt_NoCustomerFound})
  public List<TextView> bk_listText;

    @BindView(R.id.ll_delete_update)
    LinearLayout ll_delete_update;



    // variables, classes, fragments
    DBHelper dbHelper;
    String userName, address, email, TAG = "CustomerActivity";
    long mobNumber;
    public int comes_From = 0, save_clicked = 0, position = -1;
    Bundle bundle;
    // setting recyclerview
    CustomerAdapter_Tablet customerAdapter;
    CustomerDetailsModel customerDetails;
    ArrayList<CustomerDetailsModel> customer_list = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_customer, container, false);
        ButterKnife.bind(this,view);
        bk_listText.get(0).setText(MakeView.showCurrentTime());
        bk_listText.get(1).setText(MakeView.showCuurentDate());
        dbHelper = new DBHelper(getContext());

        bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey("goingTo"))
            {
                comes_From = bundle.getInt("goingTo");
            }
        }
        settingRecyclerView();
        searchCustomer();
        return view;
    }



    public void settingRecyclerView()
    {
        customer_list = dbHelper.getCustomerListFromDataBase();
        Collections.reverse(customer_list);
        customerAdapter = new CustomerAdapter_Tablet(CustomerFrag.this,customer_list,dbHelper,comes_From);
        rv_customerInfo.setLayoutManager(new GridLayoutManager(getContext(), 2));
        rv_customerInfo.setAdapter(customerAdapter);
    }

    public void update_Customer(CustomerDetailsModel customerDetailsModel, int position) throws JSONException {
        // customer update
        if (customerDetailsModel != null) {
            bk_listEditText.get(0).setText(customerDetailsModel.getCustomerName());
            bk_listEditText.get(1).setText(String.valueOf(customerDetailsModel.getCustomerMobileNumber()));
            bk_listEditText.get(2).setText(customerDetailsModel.getCustomerEmail());
            bk_listEditText.get(3).setText(customerDetailsModel.getCustomerAddress());
            SharedPref.getInstance(getContext()).setCustID(customerDetailsModel.getCustomerId());
            SharedPref.getInstance(getContext()).setCust_Name(customerDetailsModel.getCustomerName());
            SharedPref.getInstance(getContext()).setCust_Mobile(customerDetailsModel.getCustomerMobileNumber());
            SharedPref.getInstance(getContext()).setCust_EmailID(customerDetailsModel.getCustomerEmail());
            SharedPref.getInstance(getContext()).setCust_Address(customerDetailsModel.getCustomerAddress());
            searchh.setEnabled(false);
            ll_delete_update.setVisibility(View.VISIBLE);
            bk_listButton.get(0).setVisibility(View.GONE);
        }
        // customer not update
        else {
            searchh.setEnabled(true);
            SharedPref.getInstance(getContext()).setCustID(0);
            SharedPref.getInstance(getContext()).setCust_Name("");
            SharedPref.getInstance(getContext()).setCust_Mobile(0);
            SharedPref.getInstance(getContext()).setCust_EmailID("");
            SharedPref.getInstance(getContext()).setCust_Address("");
            clearFields();
            position = -1;
            ll_delete_update.setVisibility(View.GONE);
            bk_listButton.get(0).setVisibility(View.VISIBLE);
        }
    }


    public void searchCustomer() {
        searchh.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //  search_Active = 1;
                String searchedString = s.toString();
                ArrayList<CustomerDetailsModel> newList = new ArrayList<>();

                if (!searchedString.equals("")) {
                    for (int i = 0; i < customer_list.size(); i++) {
                        if (customer_list.get(i).getCustomerName().toLowerCase().contains(searchedString.toLowerCase())) {
                            newList.add(customer_list.get(i));
                        }
                    }
                    Log.e(TAG, "newList size: " + newList.size() + "");
                    customerAdapter.updateList(newList);
                }
                else
                {
                    newList = dbHelper.getCustomerListFromDataBase();
                    Collections.reverse(newList);
                    customerAdapter.updateList(newList);
                } }

            @Override
            public void afterTextChanged(Editable s) { }}); }

    //  (VICKY LONGADGE)
    @OnClick({R.id.btnAddCustomer, R.id.btn_update,R.id.btn_delete})
    public void clickMe(View view) {
        switch (view.getId()) {
            case R.id.btnAddCustomer:
                Toast.makeText(getContext(), "smdf", Toast.LENGTH_SHORT).show();
                if (bk_listEditText.get(0).getText().toString().isEmpty())
                    Toast.makeText(getContext(), "Enter customer name", Toast.LENGTH_SHORT).show();
                else if (bk_listEditText.get(1).getText().toString().length() < 10)
                    Toast.makeText(getContext(), "Enter valid mobile number", Toast.LENGTH_SHORT).show();
                else if (bk_listEditText.get(2).getText().toString().isEmpty())
                    Toast.makeText(getContext(), "Enter email id", Toast.LENGTH_SHORT).show();
                else if (emailValidator(bk_listEditText.get(2).getText().toString()) == false)
                    Toast.makeText(getContext(), "Enter valid email id", Toast.LENGTH_SHORT).show();
                else if (bk_listEditText.get(3).getText().toString().isEmpty())
                    Toast.makeText(getContext(), "Enter address of customer", Toast.LENGTH_SHORT).show();

                else {
                    if (SharedPref.getInstance(getContext()).getCustID() == 0) {
                        if (dbHelper.searchAvailability(Customer_Table_Name, "Mobile_Number", bk_listEditText.get(1).getText().toString()) == 0) {
                            userName = bk_listEditText.get(0).getText().toString();
                            mobNumber = Long.valueOf(bk_listEditText.get(1).getText().toString());
                            email = bk_listEditText.get(2).getText().toString();
                            address = bk_listEditText.get(3).getText().toString();
                            // when new customer is inserted
                            customerDetails = new CustomerDetailsModel(0, userName, address, mobNumber, email);
                            if (dbHelper.insertCustomer(customerDetails)) {
                                Toast.makeText(getContext(), "Customer Added Successfully", Toast.LENGTH_SHORT).show();
                                clearFields();
                                searchh.getText().clear();
                                customer_list.clear();
                                settingRecyclerView(); }
                        } else {
                            bk_listEditText.get(1).getText().clear();
                            Toast.makeText(getContext(), "This mobile number is already registered", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;

            case R.id.btn_update:
                // customer updated
                 if (SharedPref.getInstance(getContext()).getCustID() > 0)
            {
                userName = bk_listEditText.get(0).getText().toString();
                mobNumber = Long.valueOf(bk_listEditText.get(1).getText().toString());
                email = bk_listEditText.get(2).getText().toString();
                address = bk_listEditText.get(3).getText().toString();
                save_clicked = 1;
                SharedPref.getInstance(getContext()).setCust_Name(userName);
                SharedPref.getInstance(getContext()).setCust_Mobile(mobNumber);
                SharedPref.getInstance(getContext()).setCust_EmailID(email);
                SharedPref.getInstance(getContext()).setCust_Address(address);
                // enabling search
                searchh.setEnabled(true);
                customerAdapter.state = -1;
                position = -1;
                customerAdapter.notifyDataSetChanged();
                ll_delete_update.setVisibility(View.GONE);
                bk_listButton.get(0).setVisibility(View.VISIBLE);
                Toast.makeText(getContext(), "update Customer", Toast.LENGTH_SHORT).show();
            }
                clearFields();
                break;


            case  R.id.btn_delete:
                dbHelper.delete_Cust_From_Table(SharedPref.getInstance(getContext()).getCustID() , Customer_Table_Name,"Customer_id");
                customer_list.clear();
                settingRecyclerView();
                Toast.makeText(getContext(), "Delete Customer", Toast.LENGTH_SHORT).show();
                customerAdapter.clearSharedPref();
                clearFields();
                position = -1;
                customerAdapter.state = -1;
                searchh.setEnabled(true);
                searchh.getText().clear();
                ll_delete_update.setVisibility(View.GONE);
                bk_listButton.get(0).setVisibility(View.VISIBLE);
                break;
        } }

    // clear all edit text of customer
    public void clearFields()
    {
        bk_listEditText.get(0).getText().clear();
        bk_listEditText.get(1).getText().clear();
        bk_listEditText.get(2).getText().clear();
        bk_listEditText.get(3).getText().clear();
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
