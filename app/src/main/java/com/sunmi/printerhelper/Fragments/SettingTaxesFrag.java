package com.sunmi.printerhelper.Fragments;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.DataBaseSunmi.TablesAndColumns;
import com.sunmi.Interfaces.CommunicaterMoveBackPayment;
import com.sunmi.printerhelper.Adapter.TaxAdapter;
import com.sunmi.printerhelper.Model.TaxModel;
import com.sunmi.printerhelper.R;
import java.util.ArrayList;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Tax_Table_Name;

public class SettingTaxesFrag extends AppCompatActivity implements View.OnClickListener {

    // Views
    View  alertView;
    EditText edit_TaxName, edit_TaxRupees;
    TextView txt_CreateTax;
    Button btn_CreateTax, btn_selectedTax;
    RadioButton radioButtonTax, rb_Inclusive, rb_Exclusive;
    RadioGroup rg_TaxType;
    ImageView img_BackPress;
    // database class
    DBHelper dbHelper;
    // recyclerview setting
    RecyclerView rv_Taxes;
    ArrayList<TaxModel> listHome = new ArrayList<>();
    ArrayList<TaxModel> arrayList;
    TaxModel taxModel;
    TaxAdapter taxAdapter;
    ArrayList<Boolean> taxBoolean = new ArrayList<>();
    // varibles, fragments
    int taxType = 2;
    private boolean taxSelected = false;
    public static boolean changes = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.fragment_setting_taxes);
        super.onCreate(savedInstanceState);
        dbHelper = new DBHelper(this);
        init();
        arrayList = dbHelper.getTaxData();
        settingRecyclerView();
        taxBoolean.clear();
        for (TaxModel u : arrayList) {
            Log.e("taxvselected oncreate ",u.getSelected()+"" );
            if (u.getSelected()==0)
            {
                taxBoolean.add(false);
            }else if (u.getSelected()==1)
            {
                taxBoolean.add(true);
            }
            listHome.add(u);
            taxAdapter.notifyDataSetChanged();
        }
        Log.e("taxv ", "arraysize - "+listHome.size()+" boolean size - "+taxBoolean.size());
        taxRadioGroup();
    }

    public void init() {
        txt_CreateTax = findViewById(R.id.txt_CreateTax);
        rv_Taxes = findViewById(R.id.rv_Taxes);
        rb_Exclusive = findViewById(R.id.rb_Exclusive);
        rb_Inclusive = findViewById(R.id.rb_Inclusive);
        rg_TaxType = findViewById(R.id.rg_TaxType);
        img_BackPress = findViewById(R.id.img_BackPress);
        btn_selectedTax = findViewById(R.id.btn_selectedTax);
        btn_selectedTax.setOnClickListener(this);
        txt_CreateTax.setOnClickListener(this);
        img_BackPress.setOnClickListener(this);
    }

    public void settingRecyclerView() {
        listHome.clear();
        taxAdapter = new TaxAdapter(listHome, this, SettingTaxesFrag.this, taxBoolean,0, 0);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rv_Taxes.setLayoutManager(layoutManager);
        rv_Taxes.setAdapter(taxAdapter);
    }


    public void taxRadioGroup() {
        int[] arr = dbHelper.searchInclusiveExclusive();
        if (arr[0] == arr[1]) rb_Inclusive.setChecked(true);
        else if (arr[0] == arr[2]) rb_Exclusive.setChecked(true);
        rg_TaxType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                radioButtonTax = (RadioButton) group.findViewById(checkedId);
                if (radioButtonTax.getText().equals("Inclusive")) {
                    taxSelected = true;
                    taxType = 1;
                    Log.e("sdfdsf", radioButtonTax.getId() + "  " + radioButtonTax.getText());
                } else if (radioButtonTax.getText().equals("Exclusive")) {
                    taxSelected = true;
                    taxType = 2;
                    Log.e("sdfdsf", radioButtonTax.getId() + "  " + radioButtonTax.getText());
                }
            }
        });
    }

    public void getTaxes(Dialog dialog) {
        if (edit_TaxName.getText().toString().isEmpty())
        {
            Toast.makeText(this, "Enter Tax Name", Toast.LENGTH_SHORT).show();
        }
        else if (edit_TaxRupees.getText().toString().isEmpty())
        {
            Toast.makeText(this, "Enter Tax ", Toast.LENGTH_SHORT).show();
        }
        else {
            taxModel = new TaxModel(edit_TaxName.getText().toString(), Double.valueOf(edit_TaxRupees.getText().toString()));
            if ( dbHelper.searchAvailability(Tax_Table_Name,"Tax_Name",edit_TaxName.getText().toString().toUpperCase()) == 1){
                Toast.makeText(this, "Tax Name you have entered is already exist.", Toast.LENGTH_SHORT).show();
                edit_TaxName.getText().clear(); edit_TaxRupees.getText().clear();  }
            else {
                dbHelper.insertData(taxModel);
                taxBoolean.add(false);
                arrayList = dbHelper.getTaxData();
                listHome.add(arrayList.get(arrayList.size() - 1));
                taxAdapter.notifyDataSetChanged();
                Toast.makeText(this, "Tax Added successfully", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                Log.e("taxv ", "size of taxBoolean (dialog): "+ taxBoolean.size()+"");
            }}}

    public void getUpdatedTax(int position, boolean check)
    {
        taxBoolean.set(position, check);
    }

    public void warningDialog()
    {
        final AlertDialog dialog = new AlertDialog.Builder(this, R.style.alertDialog).create();
        LayoutInflater inflater = LayoutInflater.from(this);
        alertView = inflater.inflate(R.layout.dialog_warning_tax, null);
        Button btn_cancel = alertView.findViewById(R.id.btn_cancel);
        Button btn_save = alertView.findViewById(R.id.btn_save);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // selection of taxes
                saveTaxes();
                // change in Inc/Exc
                if (taxType == 1)// updating tax table
                    dbHelper.updateTaxTable(taxType);
                else if (taxType == 2) {// updating tax table
                    dbHelper.updateTaxTable(taxType); }
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        dialog.setView(alertView);
        dialog.show();
    }

    public void taxDialog() {
        final AlertDialog dialog = new AlertDialog.Builder(this, R.style.alertDialog).create();
        LayoutInflater inflater = LayoutInflater.from(this);
        alertView = inflater.inflate(R.layout.dialog_create_tax, null);
        btn_CreateTax = alertView.findViewById(R.id.btn_CreateTax);
        edit_TaxName = alertView.findViewById(R.id.edit_taxnamee);
        edit_TaxRupees = alertView.findViewById(R.id.edit_taxpercentt);
        edit_TaxName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        btn_CreateTax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTaxes(dialog);
            }
        });
        dialog.setView(alertView);
        dialog.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_selectedTax:// selection of taxes
                saveTaxes();
                // change in Inc/Exc
                if (taxType == 1)// updating tax table
                    dbHelper.updateTaxTable(taxType);
                else if (taxType == 2) {// updating tax table
                    dbHelper.updateTaxTable(taxType); }
                Toast.makeText(this, "submit", Toast.LENGTH_SHORT).show();

                // this loop update data in adapter and in tax table
                for (int i = 0; i< taxBoolean.size(); i++)
                {
                    Log.e("taxv buttonclick ",taxBoolean.get(i)+"");
                    // if true then checkbox is checked
                    if (taxBoolean.get(i) == true)
                    {
                        dbHelper.updateTaxSelected(listHome.get(i).getId(),1);
                        listHome.set(i, dbHelper.getTax_DB(listHome.get(i).getId()));
                    } // if true then checkbox is not checked
                    else if (taxBoolean.get(i) == false)
                    {
                        dbHelper.updateTaxSelected(listHome.get(i).getId(),0);
                        listHome.set(i, dbHelper.getTax_DB(listHome.get(i).getId()));
                    }
                    //  listHome.set(getAdapterPosition(), dbHelper.getTax_DB(listHome.get(getAdapterPosition()).getId()));
                }
                taxAdapter.notifyDataSetChanged();
                break;
            case R.id.txt_CreateTax:
                taxDialog();
                break;

            case R.id.img_BackPress:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (changes || taxSelected) {
            warningDialog();
            changes = false;
        }
        else super.onBackPressed();
    }

    public void saveTaxes()
    {
        Toast.makeText(this, "Data saved successfully", Toast.LENGTH_SHORT).show();
        // this loop update data in adapter and in tax table
        for (int i = 0; i< taxBoolean.size(); i++)
        {
            Log.e("taxv buttonclick ",taxBoolean.get(i)+"");
            // if true then checkbox is checked
            if (taxBoolean.get(i) == true)
            {
                dbHelper.updateTaxSelected(listHome.get(i).getId(),1);
                listHome.set(i, dbHelper.getTax_DB(listHome.get(i).getId()));
            } // if true then checkbox is not checked
            else if (taxBoolean.get(i) == false)
            {
                dbHelper.updateTaxSelected(listHome.get(i).getId(),0);
                listHome.set(i, dbHelper.getTax_DB(listHome.get(i).getId()));
            }
        }
        taxAdapter.notifyDataSetChanged();
        finish();
    }

}
