package com.sunmi.printerhelper.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.OrderModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ReportFargment extends Fragment {

    //Database
    DBHelper dbHelper;
    //    view
    TextView txtGrossDailyAmt, txtGrossWeeklyAmt, CostDailyInventory, netMonthlyInventory;
    ImageView img_BackPress;
    //arraylist
    ArrayList<OrderModel> arrayList;
    ArrayList<ProductModel> productModelArrayList;
    //date
    Date c;
    Calendar calendar;
    SimpleDateFormat simpleDateFormat;
    //variable
    String currentDate, lastDate, firstDayofMonth;
    double currentAmt = 0, currentAmt1, currenttotalAmt2 = 0, weeklyAmt = 0, weeklyAmt1, weeklyTotalAmt = 0,
            monthlyAmt = 0, monthlyAmt1, monthlyTotalAmt = 0, totalinventoryAmt = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_report, container, false);
        dbHelper = new DBHelper(getContext());
        arrayList = dbHelper.getOrderDetails();
        c = Calendar.getInstance().getTime();
        calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH) - 7;
        simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy");
        currentDate = simpleDateFormat.format(c);
        init(view);
        currentDaySales();
        weeklySales(yy, mm, dd);
        netMonthlyRevenue(yy, mm);
        dailyInventory();
        return view;
    }

    //init
    private void init(View v) {
        txtGrossDailyAmt = v.findViewById(R.id.txtGrossDailyAmt);
        txtGrossWeeklyAmt = v.findViewById(R.id.txtGrossWeeklyAmt);
        CostDailyInventory = v.findViewById(R.id.CostDailyInventory);
        netMonthlyInventory = v.findViewById(R.id.netMonthlyInventory);
        img_BackPress = v.findViewById(R.id.img_BackPress);
        img_BackPress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    //get today sales
    private void currentDaySales() {
        Log.e("CurrentDate", currentDate);
        for (int i = 0; i < arrayList.size(); i++) {
            String currentDateFromDatabase = arrayList.get(i).getBillDate().substring(0, arrayList.get(i).getBillDate().length() - 11);

            if (currentDateFromDatabase.equals(currentDate)) {
                currentAmt = Double.valueOf(arrayList.get(i).getAmtAfterTax());
                currenttotalAmt2 = currentAmt1 + currentAmt;
                currentAmt1 = currenttotalAmt2;
            }
        }
        txtGrossDailyAmt.setText(UseSingleton.getInstance().decimalFormat(currenttotalAmt2) + " SAR");
    }

    //get weekly Sales
    private void weeklySales(int year, int month, int day) {
        String months = new DateFormatSymbols().getMonths()[month];
        Log.e("months", months);
        StringBuilder stringBuilder = new StringBuilder().append(day).append(" ").append(months).append(" ").append(year);
        lastDate = stringBuilder + "";
        Log.e("Lastedate", lastDate);
        try {
            for (int i = 0; i < arrayList.size(); i++) {

                String dateFromDatabase = arrayList.get(i).getBillDate().substring(0, arrayList.get(i).getBillDate().length() - 9);

                if (getMilliSecofDate(dateFromDatabase) >= getMilliSecofDate(lastDate)
                        && getMilliSecofDate(dateFromDatabase) < getMilliSecofDate(currentDate)) {
                    weeklyAmt = Double.valueOf(arrayList.get(i).getAmtAfterTax());
                    weeklyTotalAmt = weeklyAmt1 + weeklyAmt;
                    weeklyAmt1 = weeklyTotalAmt;
                }
            }
            Log.e("weeklyTotal", weeklyTotalAmt + "");
            txtGrossWeeklyAmt.setText(UseSingleton.getInstance().decimalFormat(weeklyTotalAmt) + " SAR");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //get Monthly Revenue
    private void netMonthlyRevenue(int years, int month) {
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int firstdate = calendar.get(Calendar.DAY_OF_MONTH);
        String months = new DateFormatSymbols().getMonths()[month];
        StringBuilder stringBuilder = new StringBuilder().append(firstdate).append(" ").append(months).append(" ").append(years);
        firstDayofMonth = stringBuilder + "";
        Log.e("firstDate", firstDayofMonth);

        for (int i = 0; i < arrayList.size(); i++) {
            if (getMilliSecofDate(arrayList.get(i).getBillDate()) >= getMilliSecofDate(firstDayofMonth)) {
                monthlyAmt = Double.valueOf(arrayList.get(i).getAmtAfterTax());
                monthlyTotalAmt = monthlyAmt1 + monthlyAmt;
                monthlyAmt1 = monthlyTotalAmt;
            }
        }
        Log.e("MonthlyRevenue", monthlyTotalAmt + "");
        netMonthlyInventory.setText(UseSingleton.getInstance().decimalFormat(monthlyTotalAmt) + " SAR");

    }

    //string date convert into millisecond written by himanshu
    private long getMilliSecofDate(String dates) {
        long milliSeconds = 0;
        Date date;
        try {
            date = new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH).parse(dates);
            milliSeconds = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return milliSeconds;
    }

    //get today stock price
    private void dailyInventory() {
        productModelArrayList = dbHelper.getProductList();
        for (int i = 0; i < productModelArrayList.size(); i++) {
            double quantity = productModelArrayList.get(i).getQty();
            Log.e(i + "quantity", quantity + "");
            double price = productModelArrayList.get(i).getPrice();
            Log.e("price", price + "");
            double totalprice = quantity * price;
            Log.e(i + " totalprice", totalprice + "");
            totalinventoryAmt = totalinventoryAmt + totalprice;
        }
        Log.e(" todayinventoryAmt", totalinventoryAmt + "");

        CostDailyInventory.setText(UseSingleton.getInstance().decimalFormat(totalinventoryAmt) + " SAR");
    }

}
