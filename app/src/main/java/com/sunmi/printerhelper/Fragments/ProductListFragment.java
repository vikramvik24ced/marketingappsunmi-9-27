package com.sunmi.printerhelper.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Adapter.ProductAdapter;
import com.sunmi.printerhelper.Model.CategoryModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class ProductListFragment extends Fragment {

    //--------------------------------------------------------RecyclerView------------------------------------------------------//
    @BindView(R.id.rv_ProductList)
    RecyclerView rv_ProductsList;
    //------------------------------------------------------------Views---------------------------------------------------------//
    @BindView(R.id.no_customer)
    TextView txtNoCustomer;
    //---------------------------------------------------------Variables--------------------------------------------------------//
    String search_By, TAG = "ProductListFragment";
    int qty = 0, i = 1, id;
    //---------------------------------------------------------ArrayLists-------------------------------------------------------//
    ArrayList<ProductModel> productList = new ArrayList<>();
    ArrayList<ProductModel> tlist = new ArrayList<>();
    ArrayList<CategoryModel> categoryName = new ArrayList<>();
    //----------------------------------------------------------Classes---------------------------------------------------------//
    ProductAdapter productAdapter;
    DBHelper dbHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_product_list, container, false);
        ButterKnife.bind(this, v);
        dbHelper = new DBHelper(getActivity());
        return v;
    }

    //--------------------------------------------------------onResume---------------------------------------------------------//
    @Override
    public void onResume() {
        super.onResume();
        Bundle bundle = getArguments();
        if (bundle != null) {
            search_By = bundle.getString("Search_By");
            id = bundle.getInt("productList");
            categoryName = (ArrayList<CategoryModel>) bundle.getSerializable("CategoryName");
            if (search_By.equals("viewSearch")) {
                productList.clear();
                productList = dbHelper.getProdcutSearched();
//                settingRecyclerView(productList, dbHelper.getCategoryData());
            } else if (search_By.equals("Clicking_Category")) {
                qty = 0;
                productList.clear();
                productList = dbHelper.getProductArrayList(id);
//                productList = (ArrayList<ProductModel>) getArguments().getSerializable("productList");
//                settingRecyclerView(productList, categoryName);

            } else if (search_By.equals("BarcodeSearch")) {
                productList.clear();
                productList = (ArrayList<ProductModel>) getArguments().getSerializable("productList");
                Log.e(TAG + " name", productList.get(0).getPro_name());
//            categoryName = dbHelper.getCategoryName(productList.get(0).getCategory_id());
            }
        }
        settingRecyclerView(productList, categoryName);

    }

    //--------------------------------------------------------settingRecyclerView---------------------------------------------------------//
    private void settingRecyclerView(ArrayList<ProductModel> productModelArrayList, ArrayList<CategoryModel> CategoryNames) {
        //  Collections.sort(productList,ProductModel.BY_QUANTITY_SOLD);
        productAdapter = new ProductAdapter(productModelArrayList, getContext(), null, dbHelper,
                null, 1, CategoryNames, this);
        rv_ProductsList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rv_ProductsList.setAdapter(productAdapter);
    }

    //--------------------------------------------onSearching written by Himanshu-------------------------------------------------//
    @OnTextChanged(R.id.pro_searchh)
    public void onSearching(CharSequence s, int start, int before, int count) {
        String productName = s.toString();
        if (!productName.equals("")) {
            for (int i = 0; i < productList.size(); ++i) {
                if (productList.get(i).getPro_name().toLowerCase().contains(productName.toLowerCase())) {
                    tlist.add(productList.get(i));
                }
            }
            if (tlist.size() == 0) {
                tlist.clear();
                productAdapter.updateListInventory(tlist);
                txtNoCustomer.setVisibility(View.VISIBLE);
            } else {
                txtNoCustomer.setVisibility(View.GONE);
                productAdapter.updateListInventory(tlist);
                tlist.clear();
            }
        } else if (count == 0) {
            txtNoCustomer.setVisibility(View.GONE);
            settingRecyclerView(productList, dbHelper.getCategoryData());
        }
    }

    //--------------------------------------------Click Listener written by Himanshu-------------------------------------------------//
    @OnClick(R.id.img_BackArrow)
    public void onClick() {
        getActivity().onBackPressed();
    }
}
