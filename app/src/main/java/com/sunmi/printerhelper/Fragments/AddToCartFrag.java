package com.sunmi.printerhelper.Fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicaterInterface;
import com.sunmi.Interfaces.CommunicatorMoveBack;
import com.sunmi.printerhelper.Adapter.ProductAdapter;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;
import java.util.ArrayList;
import java.util.Collections;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Temp_Table_Name;

public class AddToCartFrag extends Fragment {

    // view
    View v;
    Context context;
    public EditText searchh;
   public static TextView txt_NoProductAvailable;
    // settingRecyclerView
    RecyclerView recyclerViewProductList;
    ProductAdapter productAdapter;

    ArrayList<ProductModel> productList = new ArrayList<>();
    ArrayList<ProductModel> allProducts = new ArrayList<>();
    ArrayList<Integer> selectedQuantityList = new ArrayList<>();
    // variables
    String search_By = null, TAG = "AddToCartFrag";
    int qty = 0;
    int qty_new = 0;
    // database
    DBHelper dbHelper;
    // interface
    CommunicatorMoveBack communicatorMoveBack;
    CommunicaterInterface communicaterInterface;

    public void interfaces() {
        communicatorMoveBack = (CommunicatorMoveBack) getContext();
        communicatorMoveBack.goBack(1);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_add_to_cart, container, false);
//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        init();
        interfaces();
        allProducts = dbHelper.getProdcutSearched();
        Collections.sort(allProducts,ProductModel.BY_QUANTITY_SOLD);
        Bundle bundle = getArguments();
        if (bundle != null) {
            search_By = bundle.getString("Search_By");
            //Search product from seaching bar
            if (search_By.equals("viewSearch")) {
                productList.clear();
                selectedQuantityList.clear();
                qty = 0;
                Log.e("searchBy ", search_By);
               productList = allProducts;
                for (int i = 0; i < productList.size(); i++) {
//---------------------------------------------------------------------------------------------------------------------------------------------------------
                    //                                  N E W
                    // checking wether product is available in temp table or not
                    // if available it returns '1' and update qty in selectedQuantityList
                    if (dbHelper.Pro_Available_To_AddToCart(productList.get(i).getPro_id(),Temp_Table_Name,"pro_idd") == 1) {
                        // need to change in double
                        qty_new = (int)dbHelper.getProdcut_TempTable(productList.get(i).getPro_id()).getQty();
                        selectedQuantityList.add(qty_new);
                    }// if available it returns '0' and update 0 in selectedQuantityList
                    else if (dbHelper.Pro_Available_To_AddToCart(productList.get(i).getPro_id(),Temp_Table_Name,"pro_idd") == 0)
                        selectedQuantityList.add(0);
//---------------------------------------------------------------------------------------------------------------------------------------------------------
                }

                settingRecyclerView(allProducts);
            }
            //Search product From Click on Category
            else if (search_By.equals("Clicking_Category")) {
                qty = 0;
                productList.clear();

                productList = (ArrayList<ProductModel>) getArguments().getSerializable("productList");
                for (int i = 0; i < productList.size(); i++) {

//---------------------------------------------------------------------------------------------------------------------------------------------------------
                     //                                  N E W
                    // checking wether product is available in temp table or not
                    // if available it returns '1' and update qty in selectedQuantityList
                    if (dbHelper.Pro_Available_To_AddToCart(productList.get(i).getPro_id(),Temp_Table_Name,"pro_idd") == 1) {
                        // need to change in double
                        qty_new = (int)dbHelper.getProdcut_TempTable(productList.get(i).getPro_id()).getQty();
                        selectedQuantityList.add(qty_new);
                    }// if available it returns '0' and update 0 in selectedQuantityList
                    else if (dbHelper.Pro_Available_To_AddToCart(productList.get(i).getPro_id(),Temp_Table_Name,"pro_idd") == 0)
                        selectedQuantityList.add(0);
//---------------------------------------------------------------------------------------------------------------------------------------------------------

                }

            }
            settingRecyclerView(productList);

        }
        //Search Product From BarCode
        else if (search_By.equals("BarcodeSearch")) {
            productList.clear();
            productList = (ArrayList<ProductModel>) getArguments().getSerializable("productList");
            Log.e(TAG + " on", productList.get(0).getPro_name());
            settingRecyclerView(productList);
        }

        search();
        return v;
    }


    public void init() {
        txt_NoProductAvailable = v.findViewById(R.id.txt_NoProductAvailable);
        recyclerViewProductList = v.findViewById(R.id.recyclerViewProductList);
        dbHelper = new DBHelper(getContext());
        searchh = v.findViewById(R.id.searchh);
        searchh.requestFocus();
    }

    public void settingRecyclerView(ArrayList<ProductModel> productList) {


        Log.e("selectedQuantityList ", selectedQuantityList.size() + "");
        recyclerViewProductList.setLayoutManager(new LinearLayoutManager(context));
        productAdapter = new ProductAdapter(productList, getActivity(), communicaterInterface, dbHelper,
                selectedQuantityList, 0, null, null);
      //  Log.e("productList ",productList.get(0).getQuantity_sold())
        //Collections.sort(productList, ProductModel.BY_QUANTITY_SOLD);
        //  Log.e("productList ",productList.get(0).getQuantity_sold());
        //Collections.sort(productList, ProductModel.BY_QUANTITY_SOLD);

        recyclerViewProductList.setAdapter(productAdapter);
    }

    //Seaarching Bar
    public void search() {
        searchh.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                Log.e("sdfdfs", "sdfdsf");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String searchedProduct = s + "";
                ArrayList<ProductModel> newList = new ArrayList<>();
                newList.clear();
                qty = 0;
                selectedQuantityList.clear();

                if (!searchedProduct.equals("")) {
                    for (int i = 0; i < allProducts.size(); i++) {
                        if (allProducts.get(i).getPro_name().toLowerCase().contains(searchedProduct.toLowerCase())) {
                            newList.add(allProducts.get(i));
                        }
                    }
                    // fetching quantities of each product from Add_To_Cart
                    for (int i = 0; i < newList.size(); i++) {
                        // if no data is available in Temp table
                        Log.e("product ", newList.get(i).getPro_name());
// ---------------------------------------------------------------------------------------------------------------------------------------------------------
                          //                         NEW
                        // checking wether product is available in temp table or not
                        // if available it returns '1' and update qty in selectedQuantityList
                        if (dbHelper.Pro_Available_To_AddToCart(newList.get(i).getPro_id(),Temp_Table_Name,"pro_idd") == 1) {
                            // need to change in double
                            Log.e("new_qty ",qty_new+"");
                            qty_new = (int)dbHelper.getProdcut_TempTable(newList.get(i).getPro_id()).getQty();
                            selectedQuantityList.add(qty_new);
                        }// if available it returns '0' and update 0 in selectedQuantityList
                        else if (dbHelper.Pro_Available_To_AddToCart(newList.get(i).getPro_id(),Temp_Table_Name,"pro_idd") == 0)
                            selectedQuantityList.add(0);
                    }


                    productAdapter.updateList(newList, selectedQuantityList);
                } else {
                    if (search_By.equals("Clicking_Category")) {
                        productList = (ArrayList<ProductModel>) getArguments().getSerializable("productList");

                        for (int i = 0; i < productList.size(); i++) {
                            // checking wether product is available in temp table or not
                            // if available it returns '1' and update qty in selectedQuantityList
                            if (dbHelper.Pro_Available_To_AddToCart(productList.get(i).getPro_id(),Temp_Table_Name,"pro_idd") == 1) {
                                qty_new = (int)dbHelper.getProdcut_TempTable(productList.get(i).getPro_id()).getQty();
                                selectedQuantityList.add(qty_new);
                            }// if available it returns '0' and update 0 in selectedQuantityList
                            else if (dbHelper.Pro_Available_To_AddToCart(productList.get(i).getPro_id(),Temp_Table_Name,"pro_idd") == 0)
                                selectedQuantityList.add(0);
                        }
                        productAdapter.updateList(productList, selectedQuantityList);
                    }
                    else if (search_By.equals("viewSearch"))
                    {
                        productList = dbHelper.getProdcutSearched();
                        for (int i = 0; i < productList.size(); i++) {
                            // checking wether product is available in temp table or not
                            // if available it returns '1' and update qty in selectedQuantityList
                            if (dbHelper.Pro_Available_To_AddToCart(productList.get(i).getPro_id(),Temp_Table_Name,"pro_idd") == 1) {
                                qty_new = (int)dbHelper.getProdcut_TempTable(productList.get(i).getPro_id()).getQty();
                                selectedQuantityList.add(qty_new);
                            }// if available it returns '0' and update 0 in selectedQuantityList
                            else if (dbHelper.Pro_Available_To_AddToCart(productList.get(i).getPro_id(),Temp_Table_Name,"pro_idd") == 0)
                                selectedQuantityList.add(0);
                        }

                        productAdapter.updateList(productList, selectedQuantityList);

                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.e("ppppp", "after");
            }
        });
    }

}
