package com.sunmi.printerhelper.Fragments;

import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sunmi.RetrofitDetails.APIClient;
import com.sunmi.RetrofitDetails.RetrofitInterface;
import com.sunmi.printerhelper.Model.APIModel;
import com.sunmi.printerhelper.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgotPassword extends Fragment implements View.OnClickListener {

    EditText edtEmail;
    Button btnSend;
    String email, device_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        init(v);
        return v;
    }

    public void init(View v) {
        edtEmail = v.findViewById(R.id.edtEmail);
        btnSend = v.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);

    }


    private void setForgotPassword(String email, String device_id) {
        RetrofitInterface retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
        Call<APIModel> apiModelCall = retrofitInterface.setForgotPass(email, device_id);

        apiModelCall.enqueue(new Callback<APIModel>() {
            @Override
            public void onResponse(Call<APIModel> call, Response<APIModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess()) {
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("Response", response.body().getSuccess() + "");
                        Log.d("Response", response.body().getMessage() + "");
                    } else {
                        Log.e("Response", response.body().getMessage() + "");
                        Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<APIModel> call, Throwable t) {
                Log.e("Response", t.getMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSend:
                email = edtEmail.getText().toString();
                device_id = Settings.Secure.getString(getActivity().getContentResolver(), Settings.Secure.ANDROID_ID);
                setForgotPassword(email, device_id);
        }
    }
}
