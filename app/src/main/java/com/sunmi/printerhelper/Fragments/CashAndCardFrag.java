package com.sunmi.printerhelper.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicaterMoveBackPayment;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Model.OrderModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Temp_Tab_Table_Name;

public class CashAndCardFrag extends Fragment {

    View view;
    // radio button
    @BindViews({R.id.rb_cash, R.id.rb_card})
    List<RadioButton> bk_listRadio;
    // text view
    @BindView(R.id.txtAmount)
    TextView textView;
    // variables, classes, fragments
    int cust_id = 0;
    String payment_mode = null;
    Fragment fragment;
    OrderModel orderModel;
    ArrayList<ProductModel> list_TempTablePro = new ArrayList<>();
    // database
    DBHelper dbHelper;
    // interface
    CommunicaterMoveBackPayment communicaterMoveBackPayment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_payment, container, false);
        ButterKnife.bind(this,view);
        interfaces();
        dbHelper = new DBHelper(getContext());
        getDataOfBilling();
        defaultPaymentMode();
        currentDate();
        return view;
    }

    public void interfaces() {
        communicaterMoveBackPayment = (CommunicaterMoveBackPayment) getContext();
        // ============= TABLET
        if (MakeView.isTablet(getContext()))
        { communicaterMoveBackPayment.moveBack(1); }
    }

    public void getDataOfBilling()
    {
        cust_id = getArguments().getInt("cust_id");
        orderModel = (OrderModel) getArguments().getSerializable("orderModel");
        Log.e("data", cust_id+"   "+orderModel.getAmtAfterTax());
        textView.setText(orderModel.getAmtAfterTax()+" SAR");
    }

    // VICKY LONGADGE
    public void defaultPaymentMode() {
        if (bk_listRadio.get(0).isChecked()) {
            payment_mode = "Cash";
        }
    }

    public String currentDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy | hh:mm a");
        String formattedDate = simpleDateFormat.format(c);
        Log.e("PaymentFrag ", "Current date Time " + formattedDate);
        return formattedDate;
    }


    public int saving_BillingDataInDB()
    {
        Log.e("cust_idd ", cust_id + "");
        if (cust_id != 0) { payment_mode = null; }
        return  dbHelper.insertDataInorderTable_TAB(cust_id,payment_mode,orderModel,currentDate(),0);
    }

    @OnClick({R.id.btn_Proceed,R.id.rb_cash,R.id.rb_card,R.id.cv_cash,R.id.cv_card, R.id.btn_Back})
    public void clickMe(View view)
    {
        switch (view.getId())
        {
            case R.id.rb_cash:
                payment_mode = "Cash";
                bk_listRadio.get(1).setChecked(false);
                break;

            case R.id.rb_card:
                payment_mode = "Card";
                bk_listRadio.get(0).setChecked(false);
                break;

            case R.id.cv_cash:
                payment_mode = "Cash";
                bk_listRadio.get(0).setChecked(true);
                bk_listRadio.get(1).setChecked(false);
                break;

            case R.id.cv_card:
                payment_mode = "Card";
                bk_listRadio.get(0).setChecked(false);
                bk_listRadio.get(1).setChecked(true);
                break;

            case R.id.btn_Proceed:

                int order_id = saving_BillingDataInDB();
                if (order_id > 0)
                {

                    // update quantity in product table
                    list_TempTablePro.addAll(dbHelper.getFromTemp_TabletVicky());
                    for (int i = 0; i<list_TempTablePro.size(); i++)
                    {
                        dbHelper.update_ProductQuantity(list_TempTablePro.get(i).getPro_id(), Double.valueOf(list_TempTablePro.get(i).getQty()));
//                        dbHelper.update_ProductQuantity_Tab(list_TempTablePro.get(i).getPro_id(), Double.valueOf(list_TempTablePro.get(i).getQty()));
                    }
                    dbHelper.truncate_Table(Temp_Tab_Table_Name);
                    Bundle bundle = new Bundle();
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    fragment = new ConfirmationFrag();
                    bundle.putInt("order_id",order_id);
                    bundle.putSerializable("orderModell", orderModel);
                    fragment.setArguments(bundle);
                    ft.replace(R.id.rl_billing, fragment, "ConfirmationFrag");
                    ft.addToBackStack("");
                    ft.commit();
                }
                else
                {
                    Toast.makeText(getContext(), "Billing data Not Saved", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.btn_Back:
                getActivity().getSupportFragmentManager().beginTransaction().remove(getActivity().getSupportFragmentManager().findFragmentByTag("EmptyFrag")).commit();
                getActivity().getSupportFragmentManager().beginTransaction().remove(getActivity().getSupportFragmentManager().findFragmentByTag("CashCardFrag")).commit();
                getActivity().getSupportFragmentManager().beginTransaction().remove(getActivity().getSupportFragmentManager().findFragmentByTag("EmptyFrag1")).commit();
                getActivity().getSupportFragmentManager().beginTransaction().remove(getActivity().getSupportFragmentManager().findFragmentByTag("EmptyFrag2")).commit();
                break;
        }
    }


}

