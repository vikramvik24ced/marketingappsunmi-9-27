package com.sunmi.printerhelper.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Adapter.TaxAdapterTab;
import com.sunmi.printerhelper.Model.TaxModel;
import com.sunmi.printerhelper.R;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Tax_Table_Name;

public class TaxFrag extends Fragment {

    View view, alertView;
    // recyclerview
    @BindView(R.id.rv_Taxes)
    RecyclerView rv_Taxes;
    // button
    @BindView(R.id.btn_selectedTax)
    Button btn_selectedTax;
    // radio button
    @BindViews({R.id.rb_Inclusive,R.id.rb_Exclusive})
    List<RadioButton> bk_listRadio;
    RadioButton radioButtonTax;
    // radio group
    @BindView(R.id.rg_TaxType)
    RadioGroup rg_TaxType;

    // alert dialogv views
    Button btn_CreateTax;
    EditText edit_TaxName, edit_TaxRupees;
    TaxAdapterTab taxAdapter;
    TaxModel taxModel;
    ArrayList<Boolean> taxBoolean = new ArrayList<>();
    ArrayList<TaxModel> listHome = new ArrayList<>();
    ArrayList<TaxModel> arrayList;
    // variables, classes, fragment
    DBHelper dbHelper;
    private boolean taxSelected = false;
    public static boolean changes = false;
    int taxType = 2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_setting_taxes, container, false);
        ButterKnife.bind(this,view);
        dbHelper = new DBHelper(getContext());
        arrayList = dbHelper.getTaxData();
        settingRecyclerView();
        taxBoolean.clear();
        for (TaxModel u : arrayList) {
            Log.e("taxvselected oncreate ",u.getSelected()+"" );
            if (u.getSelected()==0)
            {
                taxBoolean.add(false);
            }else if (u.getSelected()==1)
            {
                taxBoolean.add(true);
            }
            listHome.add(u);
            taxAdapter.notifyDataSetChanged();
        }
        Log.e("taxv ", "arraysize - "+listHome.size()+" boolean size - "+taxBoolean.size());
        taxRadioGroup();
        return view; }

    public void settingRecyclerView() {
        listHome.clear();
        taxAdapter = new TaxAdapterTab(listHome, null,TaxFrag.this, taxBoolean,dbHelper,0, 0);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false);
        rv_Taxes.setLayoutManager(layoutManager);
        rv_Taxes.setAdapter(taxAdapter);
    }

    public void taxDialog() {
        final AlertDialog dialog = new AlertDialog.Builder(getContext()).create();
        LayoutInflater inflater = LayoutInflater.from(getContext());
        alertView = inflater.inflate(R.layout.dialog_create_tax, null);
        btn_CreateTax = alertView.findViewById(R.id.btn_CreateTax);
        edit_TaxName = alertView.findViewById(R.id.edit_taxnamee);
        edit_TaxRupees = alertView.findViewById(R.id.edit_taxpercentt);
        edit_TaxName.setFilters(new InputFilter[] {new InputFilter.AllCaps()});
        btn_CreateTax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTaxes(dialog);
            }
        });
        dialog.setView(alertView);
        dialog.show();
    }


    public void getTaxes(Dialog dialog) {
        if (edit_TaxName.getText().toString().isEmpty())
        {
            Toast.makeText(getContext(), "Enter Tax Name", Toast.LENGTH_SHORT).show();
        }
        else if (edit_TaxRupees.getText().toString().isEmpty())
        {
            Toast.makeText(getContext(), "Enter Tax ", Toast.LENGTH_SHORT).show();
        }
        else {
            taxModel = new TaxModel(edit_TaxName.getText().toString(), Double.valueOf(edit_TaxRupees.getText().toString()));
            if ( dbHelper.searchAvailability(Tax_Table_Name,"Tax_Name",edit_TaxName.getText().toString().toUpperCase()) == 1){
                Toast.makeText(getContext(), "Tax Name you have entered is already exist.", Toast.LENGTH_SHORT).show();
                edit_TaxName.getText().clear(); edit_TaxRupees.getText().clear();  }
            else {
                dbHelper.insertData(taxModel);
                taxBoolean.add(false);
                arrayList = dbHelper.getTaxData();
                listHome.add(arrayList.get(arrayList.size() - 1));
                taxAdapter.notifyDataSetChanged();
                Toast.makeText(getContext(), "Tax Added successfully", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                Log.e("taxv ", "size of taxBoolean (dialog): "+ taxBoolean.size()+"");
            }}}

    public void getUpdatedTax(int position, boolean check)
    {
        taxBoolean.set(position, check);
    }

    public void saveTaxes()
    {
        Toast.makeText(getContext(), "Data saved successfully", Toast.LENGTH_SHORT).show();
        // this loop update data in adapter and in tax table
        for (int i = 0; i< taxBoolean.size(); i++)
        {
            Log.e("taxv buttonclick ",taxBoolean.get(i)+"");
            // if true then checkbox is checked
            if (taxBoolean.get(i) == true)
            {
                dbHelper.updateTaxSelected(listHome.get(i).getId(),1);
                listHome.set(i, dbHelper.getTax_DB(listHome.get(i).getId()));
            } // if true then checkbox is not checked
            else if (taxBoolean.get(i) == false)
            {
                dbHelper.updateTaxSelected(listHome.get(i).getId(),0);
                listHome.set(i, dbHelper.getTax_DB(listHome.get(i).getId()));
            }
        }
        taxAdapter.notifyDataSetChanged();
        // finish();
    }

    public void taxRadioGroup() {
        int[] arr = dbHelper.searchInclusiveExclusive();
        if (arr[0] == arr[1]) bk_listRadio.get(0).setChecked(true);
        else if (arr[0] == arr[2]) bk_listRadio.get(1).setChecked(true);

        rg_TaxType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                radioButtonTax = (RadioButton) group.findViewById(checkedId);
                if (radioButtonTax.getText().equals("Inclusive")) {
                    taxSelected = true;
                    taxType = 1;
                    Log.e("sdfdsf", radioButtonTax.getId() + "  " + radioButtonTax.getText());
                } else if (radioButtonTax.getText().equals("Exclusive")) {
                    taxSelected = true;
                    taxType = 2;
                    Log.e("sdfdsf", radioButtonTax.getId() + "  " + radioButtonTax.getText());
                }
            }
        });
    }

    @OnClick({R.id.txt_CreateTax,R.id.btn_selectedTax})
    public void clickMe(View view)
    {
        switch (view.getId())
        {
            case R.id.txt_CreateTax:
                taxDialog();
                break;

            case R.id.btn_selectedTax:
                // selection of taxes
                saveTaxes();
                // change in Inc/Exc
                if (taxType == 1)// updating tax table
                    dbHelper.updateTaxTable(taxType);
                else if (taxType == 2) {// updating tax table
                    dbHelper.updateTaxTable(taxType); }
                break;

        }


    }








}
