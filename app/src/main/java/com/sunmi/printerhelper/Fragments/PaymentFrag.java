

package com.sunmi.printerhelper.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicaterMoveBackPayment;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.Helper.UsePrinter;
import com.sunmi.printerhelper.Model.FinalBill;
import com.sunmi.printerhelper.Model.OrderModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.HomeActivity;
import com.sunmi.printerhelper.utils.AidlUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Temp_Table_Name;

public class PaymentFrag extends AppCompatActivity implements View.OnClickListener, CommunicaterMoveBackPayment {
    // Views
    View view;
    ImageView img_backPress;
    TextView txtAmount;
    Button btn_Proceed, btn_Save;
    RadioButton rb_card, rb_cash;
    //    LinearLayout ll_save, ll_save_print;
    ArrayList<ProductModel> ItemsSelectedByUser = new ArrayList<>();
    ArrayList<OrderModel> orderArrayList = new ArrayList<>();
    ArrayList<ProductModel> quantityAndTotalAmtt = new ArrayList<>();
    FinalBill finalBill;
    int cust_id;
    String payment_mode = null;
    // database
    DBHelper dbHelper;
    int moveBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_payment);
        dbHelper = new DBHelper(this);
        init();
        defaultPaymentMode();
        getArraysFromCartAltemsFrag();
    }

    // VICKY LONGADGE
    public void defaultPaymentMode() {
        if (rb_cash.isChecked()) {
            payment_mode = "Cash";
        }
    }

    // VICKY LONGADGE
    public void radioClick(View view) {
        switch (view.getId()) {
            case R.id.rb_cash:
                payment_mode = "Cash";
                rb_card.setChecked(false);
                break;

            case R.id.rb_card:
                payment_mode = "Card";
                rb_cash.setChecked(false);
                break;

            case R.id.cv_cash:
                payment_mode = "Cash";
                rb_cash.setChecked(true);
                rb_card.setChecked(false);
                break;

            case R.id.cv_card:
                payment_mode = "Card";
                rb_cash.setChecked(false);
                rb_card.setChecked(true);
                break;
        }
    }

    public void init() {
        img_backPress = findViewById(R.id.img_backPress);
        btn_Proceed = findViewById(R.id.btn_Proceed);
        btn_Save = findViewById(R.id.btn_Save);
        txtAmount = findViewById(R.id.txtAmount);
        rb_card = findViewById(R.id.rb_card);
        rb_cash = findViewById(R.id.rb_cash);
        img_backPress.setOnClickListener(this);
        btn_Proceed.setOnClickListener(this);
        btn_Save.setOnClickListener(this);
        if (AidlUtil.getInstance().initPrinter() == false) {
            btn_Proceed.setVisibility(View.GONE);
        }

    }

    public void getArraysFromCartAltemsFrag() {
        Log.e("orderArrayList ", orderArrayList.size() + "");
        cust_id = getIntent().getExtras().getInt("cust_id");
        orderArrayList = (ArrayList<OrderModel>) getIntent().getSerializableExtra("orderArrayList");
        ItemsSelectedByUser = (ArrayList<ProductModel>) getIntent().getSerializableExtra("ItemsSelectedByUser");
        quantityAndTotalAmtt = (ArrayList<ProductModel>) getIntent().getSerializableExtra("quantityAndTotalAmtt");
        txtAmount.setText(orderArrayList.get(0).getAmtAfterTax() + " SAR");
    }

    public String currentDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy | hh:mm a");
        String formattedDate = simpleDateFormat.format(c);
        Log.e("PaymentFrag ", "Current date Time " + formattedDate);
        return formattedDate;
    }

    public boolean saving_BillingDataInDB() {
        boolean orderSavedInDB = false;
        // inserting data in Order and BillingProduct table
        Log.e("cust_idd ", cust_id + "");
        Log.e("sizee ", orderArrayList.size() + " " + ItemsSelectedByUser.size() + " " + quantityAndTotalAmtt.size());
        if (cust_id != 0) {
            payment_mode = null;
        }
        Log.e(" ", cust_id + "");
        long staus = dbHelper.insertDataInorderTable(cust_id, payment_mode, orderArrayList, ItemsSelectedByUser, quantityAndTotalAmtt, currentDate(), 0);
        // change quantity in product table
        if (staus > 0) {
            for (int i = 0; i < quantityAndTotalAmtt.size(); i++) {
                dbHelper.update_ProductQuantity(ItemsSelectedByUser.get(i).getPro_id(), quantityAndTotalAmtt.get(i).getQty());
            }
            dbHelper.update_ProductDB(ItemsSelectedByUser, quantityAndTotalAmtt);
            ItemsSelectedByUser.clear();
            orderArrayList.clear();
            quantityAndTotalAmtt.clear();
            Log.e("PaymentFrag ", "size of all array: " + ItemsSelectedByUser.size() + "  " + orderArrayList.size() + "  " + quantityAndTotalAmtt.size());
            orderSavedInDB = true;
        } else {
            orderSavedInDB = false;
            Log.e("PaymentFrag ", "data is not inserted in orderDetails table");
        }
        SharedPref.getInstance(this).setCustID(0);
        SharedPref.getInstance(this).setDiscount(0);
        SharedPref.getInstance(this).setDiscountType(0);
        SharedPref.getInstance(this).setCustomerSelected(0);
        return orderSavedInDB;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_backPress:
                onBackPressed();
                break;

            case R.id.btn_Proceed:
                if (saving_BillingDataInDB() == true) {
                    // GETTING ID OF LAST ORDER
                    int order_id = dbHelper.getLastRowId("Order_Details", "orderr_id");
                    finalBill = dbHelper.getDataForBilling(order_id - 1);
                    if (!finalBill.equals(null)) {
                        Log.e("PaymentFrag ", finalBill.getItemsSelectedByUser().size() + "");
                        Log.e("PaymentFrag ", finalBill.getOrderModel().getAmtAfterTax());
                        Log.e("PaymentFrag ", finalBill.getOrderModel().getTaxID() + "");
                        // orderArrayList.add(finalBill.getOrderModel());
                        try {
                            UsePrinter usePrinter = new UsePrinter(this, finalBill);
                            if (usePrinter.printReceipt() == true) {
//                            if (UsePrinter.getInstance(this, finalBill).printReceipt() == true) {
                                moveToConfirmationfrag();
                                dbHelper.truncate_AddToCart(Temp_Table_Name);
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

            case R.id.btn_Save:
                if (saving_BillingDataInDB() == true) {
                    moveToConfirmationfrag();
                    // truncate temp table
                    dbHelper.truncate_AddToCart(Temp_Table_Name);
                }
                break;

//            case R.id.ll_save:
//                if (saving_BillingDataInDB() == true) {
//                    moveToConfirmationfrag();
//                    // truncate temp table
//                    dbHelper.truncate_AddToCart(Temp_Table_Name);
//                } break;

        }
    }

    public void moveToConfirmationfrag() {
        int order_id = dbHelper.getLastRowId("Order_Details", "orderr_id");
        finalBill = dbHelper.getDataForBilling(order_id - 1);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ConfirmationFrag confirmationFrag = new ConfirmationFrag();
        Bundle bundle = new Bundle();
        bundle.putSerializable("finalBill", finalBill);
        confirmationFrag.setArguments(bundle);
        ft.replace(R.id.rl_PaymentParent, confirmationFrag);
        ft.addToBackStack("");
        ft.commit();
    }

    @Override
    public void moveBack(int back) {
        moveBack = back;

    }

    @Override
    public void onBackPressed() {
        if (moveBack == 1) {
            Intent intent = new Intent(PaymentFrag.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else super.onBackPressed();

    }
}


