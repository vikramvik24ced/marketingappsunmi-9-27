package com.sunmi.printerhelper.Fragments;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicaterMoveBackPayment;
import com.sunmi.Interfaces.CommunicaterReceiptBack;
import com.sunmi.printerhelper.Adapter.ViewBillReceiptProductAdapter;
import com.sunmi.printerhelper.Adapter.ViewBillReceiptTaxesAdapter;
import com.sunmi.printerhelper.Helper.MakeView;
import com.sunmi.printerhelper.Helper.UsePrinter;
import com.sunmi.printerhelper.Helper.UseSingleton;
import com.sunmi.printerhelper.Model.BillingModel;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.Model.FinalBill;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.utils.AidlUtil;

import java.util.ArrayList;



import static com.sunmi.DataBaseSunmi.TablesAndColumns.Order_Table_Name;


public class ViewBillFrag extends Fragment implements View.OnClickListener {

    //View
    View view;
    TextView txtCustomerName, txtInvoiceNumber, txtDate, txtTotalAmt, txtLabelDiscount, txtDiscountPerc, txtDiscountAmt, txtTaxAmount, txtNetAmtPay, txtSubTotal;
    ImageView img_BackPress, imgStatus;
    Button btn_Refund, btn_cancel, btn_Reprint, btn_Disable_Refund, btn_Disable_Cancel, btn_Reprintstate;
    LinearLayout ll_Refund_without, ll_cancel_without, ll_btnBottom, ll_btnBottom_without;
    //RecyclerView
    RecyclerView rv_viewBill, rv_Taxes;
    //Database Class
    DBHelper dbHelper;
    //Adapter Class
    ViewBillReceiptProductAdapter viewBillReceiptProductAdapter;
    ViewBillReceiptTaxesAdapter viewBillReceiptTaxesAdapter;
    //Variable
    String TAG = "ViewBillActivity";
    int check = 0, comeFrom = 0;
    FinalBill finalBill;
    // interface
    CommunicaterReceiptBack communicaterReceiptBack; // Receipt activity, TABLET
    CommunicaterMoveBackPayment communicaterMoveBackPayment; // confirmation frag, TABLET



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_bill, container, false);
        dbHelper = new DBHelper(getContext());
        init();
        if (AidlUtil.getInstance().initPrinter() == false) {
            ll_btnBottom.setVisibility(View.GONE);
            ll_btnBottom_without.setVisibility(View.VISIBLE);
        }
        getInvoiceData();

        if (!MakeView.isTablet(getContext())) iinterface_smartPhone();
       else if (MakeView.isTablet(getContext())) iinterface_Tablet();
        return view;
    }



    public void iinterface_smartPhone()
    {
        communicaterReceiptBack = (CommunicaterReceiptBack) getContext();
        communicaterReceiptBack.backPressReceipt(1);
    }

    public void iinterface_Tablet() {
        // billing activity --- confirmation frag
        comeFrom = getArguments().getInt("comeFrom");
        if (comeFrom == 1) {
            communicaterMoveBackPayment = (CommunicaterMoveBackPayment) getContext();
            communicaterMoveBackPayment.moveBack(3);
        }
    }

    //init
    private void init() {
        img_BackPress = view.findViewById(R.id.img_BackPress);
        imgStatus = view.findViewById(R.id.imgStatus);
        rv_viewBill = view.findViewById(R.id.rv_viewBill);
        rv_viewBill.setNestedScrollingEnabled(false);
        rv_Taxes = view.findViewById(R.id.rv_Taxes);
        rv_Taxes.setNestedScrollingEnabled(false);
        txtCustomerName = view.findViewById(R.id.txtCustomerName);
        txtInvoiceNumber = view.findViewById(R.id.txtInvoiceNumber);
        txtDate = view.findViewById(R.id.txtDate);
        txtTotalAmt = view.findViewById(R.id.txtTotalAmt);
        txtLabelDiscount = view.findViewById(R.id.txtLabelDiscount);
        txtDiscountPerc = view.findViewById(R.id.txtDiscountPerc);
        txtDiscountAmt = view.findViewById(R.id.txtDiscountAmt);
        txtTaxAmount = view.findViewById(R.id.txtTaxAmount);
        txtNetAmtPay = view.findViewById(R.id.txtNetAmtPay);
        btn_Reprintstate = view.findViewById(R.id.btn_Reprintstate);
        btn_Reprint = view.findViewById(R.id.btn_Reprint);
        btn_cancel = view.findViewById(R.id.btn_cancel);
        btn_Refund = view.findViewById(R.id.btn_Refund);
        btn_Disable_Refund = view.findViewById(R.id.btn_Disable_Refund);
        btn_Disable_Cancel = view.findViewById(R.id.btn_Disable_Cancel);
        ll_btnBottom = view.findViewById(R.id.ll_btnBottom);
        txtSubTotal = view.findViewById(R.id.txtSubTotal);

        //withoutPrinter
        ll_cancel_without = view.findViewById(R.id.btn_cancel_without);
        ll_Refund_without = view.findViewById(R.id.btn_Refund_without);
        ll_btnBottom_without = view.findViewById(R.id.ll_btnBottom_without);

        btn_Reprintstate.setOnClickListener(this);
        btn_Refund.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        btn_Reprint.setOnClickListener(this);
        img_BackPress.setOnClickListener(this);

        //withoutPrinter
        ll_cancel_without.setOnClickListener(this);
        ll_Refund_without.setOnClickListener(this);
    }


    private void getInvoiceData() {

        finalBill = (FinalBill)getArguments().getSerializable("finalBill");
        check = getArguments().getInt("check", 0);
        if (finalBill != null) {
            String customer_name = null;
            if (finalBill.getOrderModel().getCus_Id() > 0) {
                CustomerDetailsModel customerDetailsModel = dbHelper.getCustomer(finalBill.getOrderModel().getCus_Id());
                customer_name = customerDetailsModel.getCustomerName();
            } else if (finalBill.getOrderModel().getCus_Id() == 0) {
                customer_name = finalBill.getOrderModel().getPayment_mode();
            }

            txtCustomerName.setText(customer_name);
            txtInvoiceNumber.setText(finalBill.getOrderModel().getOrder_id() + "");
            txtDate.setText(finalBill.getOrderModel().getBillDate());
            txtTotalAmt.setText(UseSingleton.getInstance().decimalFormat(finalBill.getOrderModel().getAmtBeforeDiscount()));
            txtNetAmtPay.setText(finalBill.getOrderModel().getAmtAfterTax());
            settingRecyclerView(finalBill.getItemsSelectedByUser());
            txtSubTotal.setText("Sub Total (" + finalBill.getItemsSelectedByUser().size() + ")");
            if (finalBill.getOrderModel().getDiscountAmt() == 0.0) {
                txtDiscountAmt.setVisibility(View.GONE);
                txtDiscountPerc.setVisibility(View.GONE);
                txtLabelDiscount.setVisibility(View.GONE);
            } else {
                if (finalBill.getOrderModel().getDiscountPerc() == 0.0) {
                    Log.e("disV ", finalBill.getOrderModel().getDiscountPerc() + " " + finalBill.getOrderModel().getDiscountAmt());
                    txtDiscountPerc.setVisibility(View.INVISIBLE);
                    //  txtDiscountPerc.setText(UseSingleton.getInstance().decimalFormat(finalBill.getOrderModel().getDiscountPerc()) + "%");
                    txtDiscountAmt.setText(UseSingleton.getInstance().decimalFormat(finalBill.getOrderModel().getDiscountAmt()));
                } else if (finalBill.getOrderModel().getDiscountPerc() > 0.0) {
                    Log.e("disV ", finalBill.getOrderModel().getDiscountPerc() + " " + finalBill.getOrderModel().getDiscountAmt());
                    txtDiscountPerc.setVisibility(View.VISIBLE);
                    txtDiscountPerc.setText(UseSingleton.getInstance().decimalFormat(finalBill.getOrderModel().getDiscountPerc()) + "%");
                    txtDiscountAmt.setText(UseSingleton.getInstance().decimalFormat(finalBill.getOrderModel().getDiscountAmt()));
                }

            }
        }
        if (finalBill.getOrderModel().getTaxType() == 2) {
            String[] taxId = finalBill.getOrderModel().getTaxID().split(",");
            String[] taxAmount = finalBill.getOrderModel().getAmtOfEach_Tax().split(",");
            settingRecyclerViewTaxes(taxId, taxAmount);
        }

        if (finalBill.getOrderModel().getBillStatus() == 1) {

            imgStatus.setImageResource(R.drawable.cancelled);
            imgStatus.setVisibility(View.VISIBLE);
            if (AidlUtil.getInstance().initPrinter() == false) {
                ll_btnBottom_without.setVisibility(View.GONE);
            } else {
                ll_btnBottom.setVisibility(View.GONE);
                btn_Reprintstate.setVisibility(View.VISIBLE);
            }
        } else if (finalBill.getOrderModel().getBillStatus() == 2) {
            imgStatus.setImageResource(R.drawable.refunded);
            imgStatus.setVisibility(View.VISIBLE);
            if (AidlUtil.getInstance().initPrinter() == false) {
                ll_btnBottom_without.setVisibility(View.GONE);
            } else {
                ll_btnBottom.setVisibility(View.GONE);
                btn_Reprintstate.setVisibility(View.VISIBLE);
            }
        }
    }


    //recyclerView of Products written by Himanshu
    public void settingRecyclerView(ArrayList<ProductModel> ItemsSelectedByUser) {
        rv_viewBill.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        viewBillReceiptProductAdapter = new ViewBillReceiptProductAdapter(getContext(), ItemsSelectedByUser, dbHelper);
        rv_viewBill.setAdapter(viewBillReceiptProductAdapter);
    }

    //recyclerView of Taxes written by Himanshu
    public void settingRecyclerViewTaxes(String[] taxId, String[] taxAmount) {
        rv_Taxes.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        viewBillReceiptTaxesAdapter = new ViewBillReceiptTaxesAdapter(getContext(), taxId, taxAmount, dbHelper);
        rv_Taxes.setAdapter(viewBillReceiptTaxesAdapter);
    }


    //cancel OR Refund Method written by Himanshu
    public void cancelRefund(String message, final int status, final int order_id) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if (updateQuantity(status, order_id)) {
                            if (status == 1) {
                                imgStatus.setImageResource(R.drawable.cancelled);
                                imgStatus.setVisibility(View.VISIBLE);
                                if (AidlUtil.getInstance().initPrinter() == false) {
                                    ll_btnBottom_without.setVisibility(View.GONE);
                                } else {
                                    ll_btnBottom.setVisibility(View.GONE);
                                    btn_Reprintstate.setVisibility(View.VISIBLE);
                                }
                            } else if (status == 2) {
                                imgStatus.setImageResource(R.drawable.refunded);
                                imgStatus.setVisibility(View.VISIBLE);
                                if (AidlUtil.getInstance().initPrinter() == false) {
                                    ll_btnBottom_without.setVisibility(View.GONE);
                                } else {
                                    ll_btnBottom.setVisibility(View.GONE);
                                    btn_Reprintstate.setVisibility(View.VISIBLE);
                                }
                            }

                            iinterface_smartPhone();

                        }
                    }
                });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        TextView textView = (TextView) alertDialog.findViewById(android.R.id.message);

        Button button2 = alertDialog.findViewById(android.R.id.button2);

        Button button1 = alertDialog.findViewById(android.R.id.button1);

        if (MakeView.isTablet(getContext()))
        {
            button1.setTextSize(27); button2.setTextSize(27);    textView.setTextSize(28);
        }
    }

    //update Quantity when bill REFUND OR CANCEL
    private boolean updateQuantity(int status, int order_id) {
        boolean check = false;
        ArrayList<BillingModel> billingModelArrayList = dbHelper.getBillingId(order_id);
        for (int i = 0; i < billingModelArrayList.size(); i++) {
            if (dbHelper.updateProductQuantity(billingModelArrayList.get(i).getProduct_id(), billingModelArrayList.get(i).getTotal_quantity())) {
                if (dbHelper.change_Status(Order_Table_Name, "bill_status", "orderr_id", finalBill.getOrderModel().getOrder_id(), status)) {
                    check = true;
                } else
                    check = false;
            }
        }
        return check;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_BackPress:
                getActivity().onBackPressed();
                break;
            case R.id.btn_cancel:
                cancelRefund("Are you sure, You wanted to Cancel this Bill", 1, finalBill.getOrderModel().getOrder_id());
                break;

            case R.id.btn_Refund:
                cancelRefund("Are you sure, You wanted to Refund this Bill Amount", 2, finalBill.getOrderModel().getOrder_id());
                break;

            case R.id.btn_Reprint:
                Log.e("totalamt ", finalBill.getOrderModel().getAmtAfterTax() + "  "+finalBill.getOrderModel().getPayment_mode()+"  "+finalBill.getOrderModel().getBillStatus());
                try {
                    if (!finalBill.equals(null)) {
                        try {
                            Log.e("ViewBillActivity ", finalBill.getItemsSelectedByUser().size() + "");
                            UsePrinter usePrinter = new UsePrinter(getContext(), finalBill);
                            usePrinter.printReceipt();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                }
                break;

            case R.id.btn_Reprintstate:
                if (!finalBill.equals(null)) {
                    try {
                        Log.e("ViewBillActivity ", finalBill.getItemsSelectedByUser().size() + " "+finalBill.getOrderModel().getPayment_mode()+" "+finalBill.getOrderModel().getBillStatus());
                        UsePrinter usePrinter = new UsePrinter(getContext(), finalBill);
                        usePrinter.printReceipt();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
                break;

            case R.id.btn_cancel_without:
                btn_cancel.performClick();
                break;

            case R.id.btn_Refund_without:
                btn_Refund.performClick();
                break;
        }
    }


}
