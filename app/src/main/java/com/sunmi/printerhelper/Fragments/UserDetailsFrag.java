package com.sunmi.printerhelper.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.R;


public class UserDetailsFrag extends Fragment {

    View view;
    ImageView img_BackPress, fb_UpdateInfo;
    TextView txtUsername, txtEmail, txtMobile, txtGender, txtAddress, txtCompName, txtBusiness, txtSubType, txtRegNo;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view =  inflater.inflate(R.layout.fragment_user_details, container, false);
        init();
        getDetails();
        return view;
    }

    public void init() {
        txtUsername = view.findViewById(R.id.txtUserName);
        txtEmail = view.findViewById(R.id.txtEmail);
        txtMobile = view.findViewById(R.id.txtMobile);
        txtGender = view.findViewById(R.id.txtGender);
        txtAddress = view.findViewById(R.id.txtAddress);
        txtCompName = view.findViewById(R.id.txtCompanyName);
        txtBusiness = view.findViewById(R.id.txtBusinessType);
        txtSubType = view.findViewById(R.id.txtSubType);
        txtRegNo = view.findViewById(R.id.txtRegNo);
        img_BackPress = view.findViewById(R.id.img_BackPress);
        fb_UpdateInfo = view.findViewById(R.id.fb_UpdateInfo);
        img_BackPress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        fb_UpdateInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                Fragment fragment = new UpdateUserInfoFragment();
                ft.replace(R.id.rl_frame, fragment);
                ft.addToBackStack("null");
                ft.commit();
            }
        });
    }

    private void getDetails(){
        txtCompName.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(1));
        txtRegNo.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(2));
        txtBusiness.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(3));
        txtSubType.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(4));
        txtUsername.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(5));
        txtMobile.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(6));
        txtEmail.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(7));
        txtGender.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(8));
        txtAddress.setText(SharedPref.getInstance(getContext()).getLoginDetails().get(9));

    }


}
