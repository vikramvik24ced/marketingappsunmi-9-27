package com.sunmi.printerhelper.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.sunmi.RetrofitDetails.APIClient;
import com.sunmi.RetrofitDetails.RetrofitInterface;
import com.sunmi.printerhelper.Helper.SharedPref;
import com.sunmi.printerhelper.Model.APIModel;

import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.activity.BaseApp;
import com.sunmi.printerhelper.activity.ConnectivityReceiver;
import com.sunmi.printerhelper.activity.Splash;

import org.json.JSONException;


public class ChangePassFragment extends Fragment implements View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener {

    // View
    EditText edtNewPass, edtConfirmPass, edtOldPass;
    Button btnSendOTP;
    ImageView img_BackPress, img_WifiOnOff;
    TextView txt_WifiOnOff;
    // Api
    RetrofitInterface retrofitInterface;
    boolean internetStatus = false;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_change_pass, container, false);
        init(v);
        checkConnection();

        return v;
    }

    // written by himanshu
    private void init(View v) {
        edtNewPass = v.findViewById(R.id.edtNewPass);
        edtOldPass = v.findViewById(R.id.edtOldPass);
        edtConfirmPass = v.findViewById(R.id.edtConfirmPass);
        img_BackPress = v.findViewById(R.id.img_BackPress);
        img_WifiOnOff = v.findViewById(R.id.img_WifiOnOff);
        txt_WifiOnOff = v.findViewById(R.id.txt_WifiOnOff);
        img_BackPress.setOnClickListener(this);
        btnSendOTP = v.findViewById(R.id.btnSendOTP);
        btnSendOTP.setOnClickListener(this);
    }


    @Override
    public void onResume() {
        super.onResume();
        // register connection status listener
        BaseApp.getInstance().setConnectivityListener(this);
        checkConnection();
    }

    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        try {
            wifiStatus(isConnected);
        } catch (JSONException e) {
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        try {
            wifiStatus(isConnected);
        } catch (JSONException e) {
        }
    }

    private void wifiStatus(boolean isConnected) throws JSONException {
        if (isConnected) {
            txt_WifiOnOff.setText("Online");
            img_WifiOnOff.setImageResource(R.drawable.wifi_on_white);
          //  Toast.makeText(getContext(), "connection", Toast.LENGTH_SHORT).show();
            internetStatus = true;


        } else {
            txt_WifiOnOff.setText("Offline");
            img_WifiOnOff.setImageResource(R.drawable.wifi_off_white);
           // Toast.makeText(getContext(), "not connected", Toast.LENGTH_SHORT).show();
            // callingAppCategoryAPI(isConnected);
            internetStatus = false;

        }
    }

    // written by vicky
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSendOTP:
                if (internetStatus == true)
                changePassword();
                else Toast.makeText(getContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                break;
            case R.id.img_BackPress:
                getActivity().onBackPressed();
                break;
        }
    }

    // written by vicky
    public void changePassword() {

        if (edtOldPass.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Enter Old Password", Toast.LENGTH_SHORT).show();
        } else if (edtNewPass.getText().toString().isEmpty() && edtConfirmPass.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Enter New Password", Toast.LENGTH_SHORT).show();
        } else if (!edtNewPass.getText().toString().equals(edtConfirmPass.getText().toString())) {
            Toast.makeText(getContext(), "New Password and Confirm Password does not Match", Toast.LENGTH_SHORT).show();
        } else {
            int id = Integer.valueOf(SharedPref.getInstance(getContext()).getLoginDetails().get(0));
            retrofitInterface = APIClient.getClient().create(RetrofitInterface.class);
            Call<APIModel> apiModelCall = retrofitInterface.changePasswordAPI(edtOldPass.getText().toString(), edtNewPass.getText().toString(), id);

            apiModelCall.enqueue(new Callback<APIModel>() {
                @Override
                public void onResponse(Call<APIModel> call, Response<APIModel> response) {

                    if (response.isSuccessful()) {
                        if (response.body().getSuccess()) {
                            try {
                                Log.e("response ", response.isSuccessful() + "");
                                Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                SharedPref.getInstance(getContext()).setLogOut(true);
                                startActivity(new Intent(getContext(), Splash.class));
//                                getActivity().getSupportFragmentManager().popBackStack();
                                getActivity().finish();
                            } catch (Throwable throwable) {
                                throwable.printStackTrace();
                            }
                        } else {
                            Log.e("response ", response.isSuccessful() + "");

                            Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<APIModel> call, Throwable t) {

                }
            });
        }

    }
}
