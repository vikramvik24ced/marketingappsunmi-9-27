package com.sunmi.printerhelper.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.Interfaces.CommunicatorCustomer;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.R;

import org.json.JSONException;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Customer_Table_Name;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.Create_Table_Query;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Customer_Table_Name;

public class AddCustomerInfoFrag extends Fragment implements View.OnClickListener {

    //    View
    View view;
    ImageView imgBack;
    EditText edtCustomerName, edtAddress, edtMobileNumber, edtEmail;
    Button btnAddCustomer, btnDelete, btnUpdate;
    LinearLayout ll_bottomBtn;
    CardView cv_shadow;

    //String
    String userName, address, email;
    String TAG = "AddCustomerInfoFrag: ";
    Long mobNumber;

    //Model Class
    CustomerDetailsModel customerDetailsModel;
    //DataBase Referrence
    DBHelper dbHelper;
    // interfaces
    CommunicatorCustomer communicatorCustomer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_add_customer_info, container, false);
        init(view);
        interfaces();
        dbHelper = new DBHelper(getContext());
        updateCustomerInfo();
        return view;
    }

    // written by himanshu
    private void init(View v) {
        imgBack = v.findViewById(R.id.img_BackPress);
        edtCustomerName = v.findViewById(R.id.edtCustomerName);
        edtAddress = v.findViewById(R.id.edtAddress);
        edtMobileNumber = v.findViewById(R.id.edtMobileNo);
        edtEmail = v.findViewById(R.id.edtEmail);
        btnAddCustomer = v.findViewById(R.id.btnAddCustomer);
        ll_bottomBtn = v.findViewById(R.id.ll_bottomBtn);
        btnDelete = v.findViewById(R.id.btnDelete);
        btnUpdate = v.findViewById(R.id.btnUpdate);
        cv_shadow = v.findViewById(R.id.cv_shadow);
//        rg_gender = v.findViewById(R.id.rg_gender);
        imgBack.setOnClickListener(this);
        btnAddCustomer.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);

    }

    public void interfaces() {
        communicatorCustomer = (CommunicatorCustomer) getContext();
    }

    public boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // written by himanshu
    private boolean AddCustomer() {
        boolean status = false;
        if (getEditTextData()) {
            CustomerDetailsModel customerDetails = new CustomerDetailsModel(0, userName, address, mobNumber, email);
            status = dbHelper.insertCustomer(customerDetails);
        }
        return status;
    }

    //Update customer details written by himanshu
    private void updateCustomerInfo() {
        if (getArguments() != null) {
            customerDetailsModel = (CustomerDetailsModel) getArguments().getSerializable("customerInfo");
            if (customerDetailsModel != null) {
                edtCustomerName.setText(customerDetailsModel.getCustomerName());
                edtMobileNumber.setText(String.valueOf(customerDetailsModel.getCustomerMobileNumber()));
                edtEmail.setText(customerDetailsModel.getCustomerEmail());
                edtAddress.setText(customerDetailsModel.getCustomerAddress());
                btnAddCustomer.setVisibility(View.GONE);
                ll_bottomBtn.setVisibility(View.VISIBLE);
                cv_shadow.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.img_BackPress:
                getActivity().onBackPressed();
                break;

            case R.id.btnAddCustomer:

                if (edtCustomerName.getText().toString().isEmpty()) Toast.makeText(getContext(), "Enter customer name", Toast.LENGTH_SHORT).show();
                else if (edtMobileNumber.getText().toString().length()<10) Toast.makeText(getContext(), "Enter valid mobile number", Toast.LENGTH_SHORT).show();
                else if (edtEmail.getText().toString().isEmpty()) Toast.makeText(getContext(), "Enter email id", Toast.LENGTH_SHORT).show();
                else  if (emailValidator(edtEmail.getText().toString())  == false)Toast.makeText(getContext(), "Enter valid email id", Toast.LENGTH_SHORT).show();
                else if (edtAddress.getText().toString().isEmpty()) Toast.makeText(getContext(), "Enter address of customer", Toast.LENGTH_SHORT).show();
                else
                {
                    if (dbHelper.searchAvailability(Customer_Table_Name,"Mobile_Number",edtMobileNumber.getText().toString()) == 0)
                    {
                        userName = edtCustomerName.getText().toString();
                        mobNumber = Long.valueOf(edtMobileNumber.getText().toString());
                        email = edtEmail.getText().toString();
                        address =edtAddress.getText().toString();
                        CustomerDetailsModel customerDetails = new CustomerDetailsModel(0, userName, address, Long.valueOf(edtMobileNumber.getText().toString()), email);
                            if (dbHelper.insertCustomer(customerDetails))
                        {
                            Toast.makeText(getContext(), "Customer Added Successfully", Toast.LENGTH_SHORT).show();
                            communicatorCustomer.getCustomer(1);
                            getActivity().onBackPressed();
                            // backpress();
                        }
                    }
                    else {
                        edtMobileNumber.getText().clear();
                        Toast.makeText(getContext(), "This mobile number is already registered", Toast.LENGTH_SHORT).show();
                    }
                }

                break;

            case R.id.btnDelete:
                boolean status = dbHelper.change_Status(Customer_Table_Name, "delete_status", "Customer_id", customerDetailsModel.getCustomerId(), 1);
                if(status){
                    Toast.makeText(getContext(), "Customer Deleted", Toast.LENGTH_SHORT).show();
                    communicatorCustomer.getCustomer(1);
                    getActivity().onBackPressed();
                }
                break;

            case R.id.btnUpdate:
                if (getEditTextData()) {
                    try {
                        int value = dbHelper.updateCustomer(customerDetailsModel.getCustomerId(), userName, address,
                                mobNumber, email);
                        Log.e(TAG + " update", value + "");
                        if (value == 1){
                            Toast.makeText(getContext(), "Update Details Successfully", Toast.LENGTH_SHORT).show();
                            communicatorCustomer.getCustomer(1);
                            getActivity().onBackPressed();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                break;
        }}




    private boolean getEditTextData() {
        boolean check;
        try {
            userName = edtCustomerName.getText().toString();
            address = edtAddress.getText().toString();
            mobNumber = Long.valueOf(edtMobileNumber.getText().toString());
            email = edtEmail.getText().toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (userName.isEmpty() || address.isEmpty() || mobNumber == null || email.isEmpty()) {
            Toast.makeText(getContext(), "Please Fill All Details", Toast.LENGTH_SHORT).show();
            check = false;
        } else
            check = true;

        return check;
    }

    private void backpress() {
        Intent intent = new Intent();
        Bundle args = new Bundle();
        args.putSerializable("ARRAYLIST", (Serializable) dbHelper.getCustomerListFromDataBase());
        intent.putExtra("BUNDLE", args);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().onBackPressed();
    }
}
