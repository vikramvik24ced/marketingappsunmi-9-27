package com.sunmi.printerhelper.Helper;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.sunmi.printerhelper.Adapter.SideMenuAdapter;
import com.sunmi.printerhelper.R;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MakeView {

    private static SideMenuAdapter sideMenuAdapter;

    public static void setMakeSideMenu(RecyclerView recyclerView, Context context) {
        int[] images = {R.drawable.nav_home, R.drawable.nav_billing, R.drawable.home_inventory, R.drawable.nav_purchase, R.drawable.nav_receipt, R.drawable.nav_setting, R.drawable.nav_customer};
        sideMenuAdapter = new SideMenuAdapter(images, context);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(sideMenuAdapter);
    }

    public static boolean isTablet(Context context) {

        boolean config = false;

        int moble_size = context.getResources().getConfiguration().screenLayout;
        int moble_config = Configuration.SCREENLAYOUT_SIZE_MASK;
        int config_U = Configuration.SCREENLAYOUT_SIZE_XLARGE;

        if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_XLARGE)
        {
            config = true;
        }
        else
        {
            config = false;
        }


        return config;
    }

    //get current date
    public static String showCuurentDate() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        String months = new DateFormatSymbols().getMonths()[month];
        Log.e("months", months);
//        StringBuilder stringBuilder = new StringBuilder().append(months).append(" ").append(day).append("/")
        StringBuilder stringBuilder = new StringBuilder().append(day).append(" ").append(months).append(", ").append(year);
        String cuurentDate = stringBuilder.toString();
        Log.e("curretDate", cuurentDate);
        return cuurentDate;
    }

    public static String showCurrentTime() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        String formattedDate = simpleDateFormat.format(date);
        Log.e("Current_Time ", formattedDate);

        return formattedDate;
    }
}
