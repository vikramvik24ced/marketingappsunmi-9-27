package com.sunmi.printerhelper.Helper;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.sunmi.printerhelper.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;


/**
 * Created by Anurag on 24/7/17.
 */
public class FullImageDialog {
    // written by himanshu
    private static Dialog customProgressDialog;

    public static void ShowProgressDialog(Context context, Bitmap imagePath) {
        try {
            if (customProgressDialog != null) {
                if (customProgressDialog.isShowing()) {
                    customProgressDialog.dismiss();
                }
            }
        } catch (Exception e) {

        }

        customProgressDialog = new Dialog(context, R.style.AppThemeFull);
        customProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customProgressDialog.setContentView(R.layout.dialog_fullimage);
        customProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView imageview = customProgressDialog.findViewById(R.id.imgFullImage);
//        Bitmap yourbitmap = BitmapFactory.decodeFile(image);

//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        Bitmap decodedStringToBitmap = BitmapFactory.decodeFile(image);
//        decodedStringToBitmap.compress(Bitmap.CompressFormat.PNG, 90, byteArrayOutputStream);
//        for (int i = 0; i < imagePath.size(); i++) {
//            Bitmap decodedByteToBitmap = BitmapFactory.decodeFile(imagePath.get(i));
        Log.e("Dialog_bitmap", imagePath+"");
        imageview.setImageBitmap(imagePath);

//        }
        try {
            customProgressDialog.show();
        } catch (Exception e) {

        }
    }

    /**
     * This method is to dismiss the dialog box.
     */
    public static void DismissProgressDialog() {
        if (customProgressDialog == null) {
            return;
        }
        try {
            if (customProgressDialog.isShowing()) {
                customProgressDialog.dismiss();
            }
        } catch (Exception e) {

        }
    }

}
