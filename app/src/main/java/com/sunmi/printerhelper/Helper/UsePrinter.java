package com.sunmi.printerhelper.Helper;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.RemoteException;
import android.util.Log;

import com.sunmi.DataBaseSunmi.DBHelper;
import com.sunmi.printerhelper.Model.FinalBill;
import com.sunmi.printerhelper.Model.OrderModel;
import com.sunmi.printerhelper.Model.Tabledemo;
import com.sunmi.printerhelper.Model.TaxModel;
import com.sunmi.printerhelper.R;
import com.sunmi.printerhelper.utils.AidlUtil;

import java.util.ArrayList;
import java.util.IllegalFormatCodePointException;
import java.util.LinkedList;

public class UsePrinter {

    private static UsePrinter usePrinter;
    private FinalBill finalBill;
    private   Context context;
    private DBHelper dbHelper;
    private   ArrayList<OrderModel> orderArrayList = new ArrayList<>();
    private LinkedList<Tabledemo> allItemsLL = new LinkedList<>();
    private double totalQuantity = 0;
    private int[] widthh;
    private int[] alignn;
    private String[]selectedItems;
    private String paymentReceipt = "Payment Receipt";
    private String nice_day = "*** HAVE A NICE DAY ***";
    private String invoiceNo = "Invoie No.";
    private String under = "-------------------------" + "\n";
    private String totalProducts = null;
    private Bitmap bitmap;
    private  String customer_name;

    public UsePrinter(Context context, FinalBill finalBill) {
        this.finalBill = finalBill;
        this.context = context;
        this.orderArrayList.add(finalBill.getOrderModel());
        this.dbHelper = new DBHelper(context);
        Log.e("UsePrinter ", "orderArrayList " + orderArrayList.size() + "");
        Log.e("test ", finalBill.getItemsSelectedByUser().size() + " "+finalBill.getOrderModel().getPayment_mode()+" "+finalBill.getOrderModel().getBillStatus()+" "+orderArrayList.get(0).getPayment_mode()+" "+orderArrayList.get(0).getBillStatus());

        Log.e("UsePrinter ", " ItemsSelectedByUser "+finalBill.getItemsSelectedByUser().size());


    }

    public static final UsePrinter getInstance(Context context, FinalBill finalBill) {
        if (usePrinter == null) {
            usePrinter = new UsePrinter(context, finalBill);
        }
        return usePrinter;
    }

    public boolean printReceipt() throws RemoteException
    {
        topHeader();
//        itemsTableHeading();
       allItemsOfTable();
//        subTotalPrint();
//        discountPrint();
//        taxPrint();
//        totalAmountToPay();
//        footerPrinter();
        finalBill = null;
        return true;
        // dfds

    }

    // slipkode logo
    public void bitmapPrinter() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inTargetDensity = 160;
        options.inDensity = 160;
        if (bitmap == null) {
            bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.slipecodeblak, options);
        }
        AidlUtil.getInstance().printBitmapVicky(bitmap, 1);
    }

    // top header
    public void topHeader() throws RemoteException {
        bitmapPrinter();
        AidlUtil.getInstance().printTextVicky(paymentReceipt, 30,true,1,1);
        AidlUtil.getInstance().printTextVicky(orderArrayList.get(0).getBillDate(), 22,true,1,1);

        Log.e("bill status", orderArrayList.get(0).getPayment_mode()+" "+orderArrayList.get(0).getBillStatus());

        if (orderArrayList.get(0).getCus_Id()!=0) customer_name =   dbHelper.getCustomer(orderArrayList.get(0).getCus_Id()).getCustomerName();
        else if (orderArrayList.get(0).getCus_Id() == 0) customer_name = orderArrayList.get(0).getPayment_mode();
        AidlUtil.getInstance().printTextVicky(customer_name, 22,true,1,1);
        AidlUtil.getInstance().printTextVicky(invoiceNo+orderArrayList.get(0).getOrder_id(), 22,true,1,1);

        if (orderArrayList.get(0).getBillStatus() == 1) AidlUtil.getInstance().printTextVicky("C A N C E L L E D",40,true,1,1);
            //        AidlUtil.getInstance().printTextVicky("*******************",20,true,1,1);
        else if (orderArrayList.get(0).getBillStatus() == 2) AidlUtil.getInstance().printTextVicky("R E F U N D E D",40,true,1,1);
    }
    // headings
    public void itemsTableHeading() throws RemoteException {
        widthh = new int[]{3,1,1};
        alignn = new int[]{0,1,2};
        selectedItems = null;
        selectedItems = new String[3];
        selectedItems[0] = "ITEMS";
        selectedItems[1] = "QTY";
        selectedItems[2] = "PRICE";
        allItemsLL.add(new Tabledemo(selectedItems, widthh, alignn));
        AidlUtil.getInstance().printTableVicky(allItemsLL, false);
        AidlUtil.getInstance().vickyPrintText(under.toString(), false, 0);
        allItemsLL.clear();
    }
    // all items
    public void allItemsOfTable() throws RemoteException {
        // clearing string array and linked list
        widthh = null;
        alignn = null;
        selectedItems = null;
        widthh = new int[]{3,1,1};
        alignn = new int[]{0,1,2};
        selectedItems = new String[3];
        allItemsLL.clear();
        totalQuantity = 0;
        // ProductModel(int pro_id, String pro_name, String quantity, double price)
        // storing number of items with product - name, selected quantity, price

        Log.e("getItemsSelectedByUser ",finalBill.getItemsSelectedByUser().size()+"");
        for (int i = 0; i < finalBill.getItemsSelectedByUser().size(); i++) {
            selectedItems = null;
            selectedItems = new String[3];
            selectedItems[0] = finalBill.getItemsSelectedByUser().get(i).getPro_name();
            selectedItems[1] = UseSingleton.getInstance().decimalFormat(finalBill.getItemsSelectedByUser().get(i).getQty());
            selectedItems[2] = UseSingleton.getInstance().decimalFormat(finalBill.getItemsSelectedByUser().get(i).getPrice());
            totalQuantity = totalQuantity + finalBill.getItemsSelectedByUser().get(i).getQty();
            allItemsLL.add(new Tabledemo(selectedItems,widthh,alignn));
        }
        // printer method
        AidlUtil.getInstance().printTableVicky(allItemsLL, false);
        AidlUtil.getInstance().vickyPrintText(under.toString(), false, 0);
        selectedItems = null;
        allItemsLL.clear();
    }

    // subtotal
    public void subTotalPrint() throws RemoteException {
        widthh = null;
        alignn = null;
        selectedItems = null;
        widthh = new int[]{1,1};
        alignn = new int[]{0,2};
        selectedItems = new String[2];

        Log.e("subTotalPrint ",totalProducts+"    "+ orderArrayList.get(0).getAmtBeforeDiscount());
        totalProducts = String.valueOf(finalBill.getItemsSelectedByUser().size());
        selectedItems[0] = "Sub Total"+"("+totalProducts+")";
        selectedItems[1] = UseSingleton.getInstance().decimalFormat(orderArrayList.get(0).getAmtBeforeDiscount());
        allItemsLL.add(new Tabledemo(selectedItems, widthh, alignn));
        AidlUtil.getInstance().printTableVicky(allItemsLL, false);
        allItemsLL.clear();
    }

    // discount
    public void discountPrint() throws RemoteException {
        if (orderArrayList.get(0).getDiscountAmt() != 0.0)
        {
            widthh = null;
            alignn = null;
            selectedItems = null;
            widthh = new int[]{3,1,1};
            alignn = new int[]{0,1,2};
            selectedItems = new String[3];
            allItemsLL.clear();
            selectedItems[0] = "Discount";
            Log.e("sizeeeeee ", orderArrayList.size() + "   " + orderArrayList.get(0).getDiscountPerc() + "  " + orderArrayList.get(0).getDiscountAmt());
            if (orderArrayList.get(0).getDiscountPerc() == 0.0) {
                selectedItems[1] = "";
                selectedItems[2] = UseSingleton.getInstance().decimalFormat(orderArrayList.get(0).getDiscountAmt());
            }
            else if (orderArrayList.get(0).getDiscountPerc() > 0.0)
            {
                selectedItems[1] = UseSingleton.getInstance().decimalFormat(orderArrayList.get(0).getDiscountPerc()) + "%";
                selectedItems[2] = UseSingleton.getInstance().decimalFormat(orderArrayList.get(0).getDiscountAmt());
            }
            allItemsLL.add(new Tabledemo(selectedItems, widthh, alignn));
            AidlUtil.getInstance().printTableVicky(allItemsLL, false);
            allItemsLL.clear();
        }
    }

    // tax
    public void taxPrint() throws RemoteException {
        if (orderArrayList.get(0).getTaxType() == 2)
        {
            if (orderArrayList.get(0).getTaxID() != null)
            {
                widthh = null;
                alignn = null;
                selectedItems = null;
                widthh = new int[]{3,1,1};
                alignn = new int[]{0,1,2};
                selectedItems = new String[3];
                allItemsLL.clear();
                Log.e("taxxxx ",orderArrayList.get(0).getTaxID()+"");

                String[] taxId = orderArrayList.get(0).getTaxID().split(",");
                String[] taxAmt = orderArrayList.get(0).getAmtOfEach_Tax().split(",");

                // getting tax name from Tax Table
                for (int i = 0; i < taxId.length; i++) {
                    TaxModel taxModel = dbHelper.getTax_DB(Integer.valueOf(taxId[i]));
                    Log.e("taxxxx ", taxModel.getTaxname() + "  taxName " + taxAmt[i]);
                }
                for (int i = 0; i < taxId.length; i++) {
                    selectedItems = null;
                    selectedItems = new String[3];
                    TaxModel taxModel = dbHelper.getTax_DB(Integer.valueOf(taxId[i]));
                    Log.e("taxxxx ", taxModel.getTaxname());
                    selectedItems[0] = taxModel.getTaxname();
                    selectedItems[1] = UseSingleton.getInstance().decimalFormat(taxModel.getTaxPercent())+"%";
                    selectedItems[2] = taxAmt[i];
                    allItemsLL.add(new Tabledemo(selectedItems, widthh, alignn));
                }

                AidlUtil.getInstance().printTableVicky(allItemsLL, false);
                selectedItems = null;
                allItemsLL.clear();
                Log.e("PaymentFrag ", "amtOfEachTax " + orderArrayList.get(0).getAmtOfEach_Tax() + "  IDofTaxes " + orderArrayList.get(0).getTaxID());
                AidlUtil.getInstance().vickyPrintText(under.toString(), false, 0);
            }}}

    // total amt to pay
    public void totalAmountToPay() throws RemoteException {
        widthh = null;
        alignn = null;
        selectedItems = null;
        widthh = new int[]{1,1};
        alignn = new int[]{0,2};
        selectedItems = new String[2];
        totalProducts = String.valueOf(finalBill.getItemsSelectedByUser().size());
        selectedItems[0] = "Total Amount(SAR)";
        if (orderArrayList.get(0).getAmtAfterTax().indexOf(".")>=0)
            selectedItems[1] = orderArrayList.get(0).getAmtAfterTax().substring(0,orderArrayList.get(0).getAmtAfterTax().indexOf("."));
        else selectedItems[1] = orderArrayList.get(0).getAmtAfterTax();
        allItemsLL.add(new Tabledemo(selectedItems, widthh, alignn));
        AidlUtil.getInstance().printTableVicky(allItemsLL, false);
        AidlUtil.getInstance().vickyPrintText(under.toString(), false, 1);
        allItemsLL.clear();
    }

    // footer
    public void footerPrinter() throws RemoteException
    {
        AidlUtil.getInstance().printTextVicky(nice_day,28,true,1,5);
    }


















//    private static final UsePrinter ourInstance = new UsePrinter();
//
//    FinalBill finalBill;
//    OrderModel orderModel;
//   static Context context;
//    DBHelper dbHelper;
//
//
//
//
//
//
//
//
//    ArrayList<OrderModel> orderArrayList = new ArrayList<>();
//    String paymentReceipt = "Payment Receipt";
//
//
//
//   public static UsePrinter getInstance() {
//        return ourInstance;
//    }
//
//
//
//    public static printNow(FinalBill finalBill, Context context)
//    {
//        finalBill = finalBill;
//        context = context;
//        orderArrayList.add(finalBill.getOrderModel());
//        dbHelper = new DBHelper(context);
//    }
//
//    public void hello
//
//
//    public void topHeader() throws RemoteException
//    {
//        AidlUtil.getInstance().printTextVicky(paymentReceipt, 30,true,1,1);
//       // AidlUtil.getInstance().printTextVicky(orderArrayList.get(0).getBillDate(), 22,false,1,1);
//       // String customer_name =   dbHelper.getCustomer(orderArrayList.get(0).getCus_Id()).getCustomerName();
//      //  AidlUtil.getInstance().printTextVicky(customer_name, 22,false,1,1);
//    //    AidlUtil.getInstance().printTextVicky(invoiceNo+orderArrayList.get(0).getOrder_id(), 22,false,1,1);
////        AidlUtil.getInstance().vickyPrintText(invoiceNo+orderArrayList.get(0).getOrder_id(), false, false);
////
////
////        AidlUtil.getInstance().printTextWithSize("DESIGN O WEB", 40,false,0);
//
//    }





}
