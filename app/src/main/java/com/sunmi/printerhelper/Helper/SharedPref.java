package com.sunmi.printerhelper.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.sunmi.printerhelper.Model.ImageModel;

import java.util.ArrayList;

// this shared perferences class has been created to give default values to fields
public class SharedPref {

    private static SharedPref sharedPref;
    private static SharedPreferences sp;
    private Editor editor;
    private boolean isLogOut;
    public static boolean USER_ISLogin = false;


    public SharedPref(Context context) {
        sp = context.getSharedPreferences("LoginDetails", Context.MODE_PRIVATE);
        editor = sp.edit();
    }

    public static final SharedPref getInstance(Context context) {
        if (sharedPref == null) {
            sharedPref = new SharedPref(context);
        }
        return sharedPref;
    }

    public String getUserID() {
        return sp.getString("UserID", null);
    }

    public void setUserID(String UserID) {
        editor.putString("UserID", UserID);
        editor.apply();
    }


    public void setLoginDetails(Context context, String userId, String comp_name, String reg_no, String business_type,
                                String sub_type, String name, String mob_no, String email, String gender, String address, String login_time, String expire_time) {
        editor.putString("user_id", userId);
        editor.putString("Company_name", comp_name);
        editor.putString("Reg_no", reg_no);
        editor.putString("Business_Type", business_type);
        editor.putString("Sub_type", sub_type);
        editor.putString("name", name);
        editor.putString("Mobile No", mob_no);
        editor.putString("email", email);
        editor.putString("gender", gender);
        editor.putString("address", address);
        editor.putString("login_time", login_time);
        editor.putString("expire_time", expire_time);
        editor.apply();
    }


    public ArrayList<String> getLoginDetails() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(sp.getString("user_id", ""));                 //0
        arrayList.add(sp.getString("Company_name", ""));            //1
        arrayList.add(sp.getString("Reg_no", ""));                  //2
        arrayList.add(sp.getString("Business_Type", ""));           //3
        arrayList.add(sp.getString("Sub_type", ""));                //4
        arrayList.add(sp.getString("name", ""));                    //5
        arrayList.add(sp.getString("Mobile No", ""));               //6
        arrayList.add(sp.getString("email", ""));                   //7
        arrayList.add(sp.getString("gender", ""));                  //8
        arrayList.add(sp.getString("address", ""));                 //9
        arrayList.add(sp.getString("login_time", ""));              //10
        arrayList.add(sp.getString("expire_time", ""));             //11

        return arrayList;
    }

    public ArrayList<Integer> getQty() {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(sp.getInt("qty", 0));

        return arrayList;
    }

    public void setQty(int qty) {
        editor.putInt("qty", qty);
        editor.apply();
    }

    public boolean isLogOut() {

        return isLogOut;
    }

    public void setLogOut(boolean isLogOut) {
        editor.clear();
        editor.apply();
        this.isLogOut = isLogOut;
    }


    public void setCustID(int custID) {
        editor.putInt("custID", custID);
        editor.apply();
    }


    public void setBarCodeScannerActivity(boolean check){
        editor.putBoolean("check",check);
        editor.apply();
    }

    public boolean getBarCodeScannerActivity()
    {
        return sp.getBoolean("check",false);
    }



    public int getCustID() {
        return sp.getInt("custID", 0);
    }

    public void setDiscount(float discount) {
        editor.putFloat("discount", discount);
        editor.apply();
    }

    public void setCust_Name(String cust_name)
    {
        editor.putString("cust_name",cust_name);
        editor.apply();
    }

    public String getCust_Name()
    {
        return sp.getString("cust_name","");
    }

    public void setCust_Address(String cust_address)
    {
        editor.putString("cust_address",cust_address);
        editor.apply();
    }

    public String getCust_Address()
    {
        return sp.getString("cust_address","");
    }

    public void setCust_EmailID(String cust_emailID)
    {
        editor.putString("cust_emailID",cust_emailID);
        editor.apply();
    }

    public String getCust_EmailID()
    {
        return sp.getString("cust_emailID","");
    }

    public void setCust_Mobile(long cust_mobile)
    {
        editor.putLong("cust_mobile", cust_mobile);
        editor.apply();
    }
    public long getCust_Mobile() {
        return sp.getLong("cust_mobile", 0);
    }

    public float getDiscount() {
        return sp.getFloat("discount", 0);
    }

    public void setDiscountType(int discountType)
    {
        editor.putInt("discountType",discountType);
        editor.apply();
    }

    public int getDiscountType()
    {
        return sp.getInt("discountType", 0);
    }


    public void setCustomerSelected(int customerSelected)
    {
        editor.putInt("customerSelected",customerSelected);
        editor.apply();
    }

    public int getCustomerSelected()
    {
        return sp.getInt("customerSelected", 0);
    }

    public void setDeletePro_ID(int DeletePro_ID)
    {
        editor.putInt("DeletePro_ID", DeletePro_ID);
        editor.apply();
    }

    public int getDeletePro_ID() {
        return sp.getInt("DeletePro_ID", 0);
    }

}
