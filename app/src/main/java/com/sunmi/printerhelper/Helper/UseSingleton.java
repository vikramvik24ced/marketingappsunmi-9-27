package com.sunmi.printerhelper.Helper;

import android.util.Log;

public class UseSingleton {
    private static final UseSingleton ourInstance = new UseSingleton();

   public static UseSingleton getInstance() {
        return ourInstance;
    }


    public String decimalFormat(double d)
    {
        String numberAfterDecimal = String.format("%.2f",d);
        String s ="";
        numberAfterDecimal = numberAfterDecimal.substring(numberAfterDecimal.indexOf("."),numberAfterDecimal.length());
        if (Character.toString(numberAfterDecimal.charAt(1)).equals("0") && Character.toString(numberAfterDecimal.charAt(2)).equals("0"))
        {
            s = String.valueOf((int)Math.round(d));
        }
        else
        {
            s = String.format("%.2f",d);
        }
        return s;
    }

}
