package com.sunmi.printerhelper.Model;

public class SettingModel {


    String tittle;
    boolean state;

   public SettingModel(String tittle)
    {
        this.tittle = tittle;

    }


    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
