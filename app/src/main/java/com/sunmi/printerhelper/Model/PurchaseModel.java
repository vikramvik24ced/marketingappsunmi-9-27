package com.sunmi.printerhelper.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

// written by himanshu
public class PurchaseModel implements Serializable {

    @SerializedName("purchase_id")
    @Expose
    private Integer purchasedId;
    @SerializedName("vendor_name")
    @Expose
    private String vendorName;
    @SerializedName("purchase_date")
    @Expose
    private String purchased_Date;
    @SerializedName("purchase_amount")
    @Expose
    private double purchaseAmount;

    private int sync_status;
    private boolean isSelected;



    public PurchaseModel(int purchasedId, String vendorName, String purchased_date, double purchaseAmount) {
        this.purchasedId = purchasedId;
        this.vendorName = vendorName;
        purchased_Date = purchased_date;
        this.purchaseAmount = purchaseAmount;
    }

    public Integer getPurchasedId() {
        return purchasedId;
    }

    public void setPurchasedId(Integer purchasedId) {
        this.purchasedId = purchasedId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getPurchased_Date() {
        return purchased_Date;
    }

    public void setPurchased_Date(String purchased_Date) {
        this.purchased_Date = purchased_Date;
    }

    public double getPurchaseAmount() {
        return purchaseAmount;
    }

    public void setPurchaseAmount(double purchaseAmount) {
        this.purchaseAmount = purchaseAmount;
    }

    public int getSync_status() {
        return sync_status;
    }

    public void setSync_status(int sync_status) {
        this.sync_status = sync_status;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}