package com.sunmi.printerhelper.Model;

import android.content.Context;

import java.io.Serializable;

public class BarCodeModel implements Serializable {

    // written by himanshu
    public static int statu;

    private Context context;
    public BarCodeModel (Context context){
        this.context = context;
    }

    public static int getStatus() {
        return statu;
    }

    public static void setStatus(int status) {
        statu = status;
    }
}
