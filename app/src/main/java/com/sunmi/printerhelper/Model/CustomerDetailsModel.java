package com.sunmi.printerhelper.Model;

import java.io.Serializable;
import java.util.Comparator;

public class CustomerDetailsModel implements Serializable {

    private int customerId;
    private Long customerMobileNumber;
    private String customerName, customerAddress, customerEmail, customerGender;

    public CustomerDetailsModel(int customerId, String customerName, String customerAddress, Long customerMobileNumber, String customerEmail) {
        this.customerId = customerId;
        this.customerMobileNumber = customerMobileNumber;
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.customerEmail = customerEmail;
    }

    public CustomerDetailsModel(){}

    public static final Comparator<CustomerDetailsModel> BY_ALPHABATICALL = new Comparator<CustomerDetailsModel>() {
        @Override
        public int compare(CustomerDetailsModel o1, CustomerDetailsModel o2) {

            return o2.getCustomerName().toLowerCase().compareTo(o1.getCustomerName().toLowerCase());
        }
    };


    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Long getCustomerMobileNumber() {
        return customerMobileNumber;
    }

    public void setCustomerMobileNumber(Long customerMobileNumber) {
        this.customerMobileNumber = customerMobileNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerGender() {
        return customerGender;
    }

    public void setCustomerGender(String customerGender) {
        this.customerGender = customerGender;
    }
}
