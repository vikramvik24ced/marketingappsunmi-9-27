
package com.sunmi.printerhelper.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPro {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("user_id")
    @Expose
    private String userId;

    @SerializedName("pro_id")
    @Expose
    private String proId;

    @SerializedName("pro_name")
    @Expose
    private String proName;

    @SerializedName("quantity")
    @Expose
    private String quantity;

    @SerializedName("bar_code")
    @Expose
    private String barcode;

    @SerializedName("price")
    @Expose
    private String price;

    @SerializedName("category_id")
    @Expose
    private String categoryId;

    @SerializedName("soldby_id")
    @Expose
    private String soldbyId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSoldbyId() {
        return soldbyId;
    }

    public void setSoldbyId(String soldbyId) {
        this.soldbyId = soldbyId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
