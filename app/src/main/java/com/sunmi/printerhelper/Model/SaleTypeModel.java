package com.sunmi.printerhelper.Model;

import java.io.Serializable;

public class SaleTypeModel implements Serializable {

   private int soldById;
   private String saleType;

    public SaleTypeModel()
    {}

    public SaleTypeModel(int soldById, String saleType)
    {
        this.soldById = soldById;
        this.saleType = saleType;
    }

    public SaleTypeModel(String saleType)
    {
        this.saleType = saleType;
    }

    public int getSoldById() {
        return soldById;
    }

    public void setSoldById(int soldById) {
        this.soldById = soldById;
    }

    public String getSaleType() {
        return saleType;
    }

    public void setSaleType(String saleType) {
        this.saleType = saleType;
    }
}
