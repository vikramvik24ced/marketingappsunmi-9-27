package com.sunmi.printerhelper.Model;

import java.io.Serializable;
import java.util.Comparator;

public class CategoryModel implements Serializable {

    private String Categegoryname;
    private Integer id, syncStatus;
    private double quantityInEach_Cat;


    public CategoryModel(String Categegoryname, int syncStatus) {
        this.Categegoryname = Categegoryname;
        this.syncStatus = syncStatus;
    }

    // used at the time of billing
    public CategoryModel(Integer id, String Categegoryname, double quantityInEach_Cat) {
        this.id = id;
        this.Categegoryname = Categegoryname;
        this.quantityInEach_Cat = quantityInEach_Cat;
    }

    // used at the time of synchronization
    public CategoryModel(Integer id, String Categegoryname, int syncStatus) {
        this.id = id;
        this.Categegoryname = Categegoryname;
        this.syncStatus = syncStatus;
    }

    public static final Comparator<CategoryModel> BY_TOTAL_QUANTITY = new Comparator<CategoryModel>() {
        @Override
        public int compare(CategoryModel o1, CategoryModel o2) {
            int p = 0;
            if (Double.compare(Double.valueOf(o2.getQuantityInEach_Cat()), Double.valueOf(o1.getQuantityInEach_Cat())) == 0)
            {
                p = o1.getCategegoryname().toLowerCase().compareTo(o2.getCategegoryname().toLowerCase());
            }
            else {
                p = Double.compare(Double.valueOf(o2.getQuantityInEach_Cat()), Double.valueOf(o1.getQuantityInEach_Cat()));
            } return p;
        }
    };

    public String getCategegoryname() {
        return Categegoryname;
    }

    public void setCategegoryname(String categegoryname) {
        Categegoryname = categegoryname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSyncStatus() {
        return syncStatus;
    }

    public void setSyncStatus(Integer syncStatus) {
        this.syncStatus = syncStatus;
    }

    public double getQuantityInEach_Cat() {
        return quantityInEach_Cat;
    }

    public void setQuantityInEach_Cat(double quantityInEach_Cat) {
        this.quantityInEach_Cat = quantityInEach_Cat;
    }


}
