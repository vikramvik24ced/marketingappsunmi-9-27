package com.sunmi.printerhelper.Model;

import android.net.Uri;

/**
 * Created by Oclemy on 8/4/2016 for ProgrammingWizards Channel and http://www.camposha.com.
 */
public class ImageModel {
    String uri;

   public ImageModel(String uri)
    {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
