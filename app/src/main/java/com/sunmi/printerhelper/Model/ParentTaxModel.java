package com.sunmi.printerhelper.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class ParentTaxModel implements Serializable {

    TaxModel taxModel;

    private ArrayList<TaxModel> taxOnEachItemm = null;

    public ParentTaxModel(ArrayList<TaxModel> taxOnEachItemm)
    {
        this.taxOnEachItemm = taxOnEachItemm;
    }


    public ArrayList<TaxModel> getTaxOnEachItemm() {
        return taxOnEachItemm;
    }

    public void setTaxOnEachItemm(ArrayList<TaxModel> taxOnEachItemm) {
        this.taxOnEachItemm = taxOnEachItemm;
    }
}
