package com.sunmi.printerhelper.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

public class ProductModel implements Serializable {

    @SerializedName("id")
    @Expose
    private int pro_id;
    @SerializedName("pro_name")
    @Expose
    private String pro_name;
    @SerializedName("quantity")
    @Expose
    private double qty;
    @SerializedName("price")
    @Expose
    private double price;
    @SerializedName("category_id")
    @Expose
    private int category_id;
    @SerializedName("soldby_Id")
    @Expose
    private int soldby_Id;
    @SerializedName("barcode")
    @Expose
    private String barcode;
    @SerializedName("barcodeStatus")
    @Expose
    private int barcodeStatus;

    private double totalAmt;
    private int  tax_id, sync_status;
    private String taxes, quantity_sold;
    private boolean isSelected;


    public ProductModel(String pro_name) {
        this.pro_name = pro_name;
    }

    public ProductModel(int pro_id, String pro_name, double qty, double price, String barcode) {
        this.pro_id = pro_id;
        this.qty = qty;
        this.pro_name = pro_name;
        this.price = price;
        this.barcode = barcode;
    }

    // (vicky longadge) used to get data from temp table
    public ProductModel(int pro_id, String pro_name, double qty, double price, double totalAmt ) {
        this.pro_id = pro_id;
        this.pro_name = pro_name;
        this.qty = qty;
        this.price = price;
        this.totalAmt = totalAmt;
    }



    public ProductModel(int pro_id, String pro_name, double qty, double price, String barcode, int barcodeStatus, int category_ID, int soldby_ID) {
        this.pro_id = pro_id;
        this.qty = qty;
        this.pro_name = pro_name;
        this.price = price;
        this.barcode = barcode;
        this.barcodeStatus = barcodeStatus;
        this.category_id = category_ID;
        this.soldby_Id = soldby_ID;

    }

    public ProductModel(String pro_name, double qty, double price, String barcode, int barcodeStatus, int category_id, int soldby_Id) {
        this.qty = qty;
        this.pro_name = pro_name;
        this.price = price;
        this.barcode = barcode;
        this.barcodeStatus = barcodeStatus;
        this.category_id = category_id;
        this.soldby_Id = soldby_Id;

    }

    public ProductModel(String pro_name, double qty, double price, String barcode, int barcodeStatus, int category_id, int soldby_Id, int sync_status) {
        this.qty = qty;
        this.pro_name = pro_name;
        this.price = price;
        this.barcode = barcode;
        this.barcodeStatus = barcodeStatus;
        this.category_id = category_id;
        this.soldby_Id = soldby_Id;
        this.sync_status = sync_status;
    }

    // (vicky longadge) selected qantity by user and total amount of each product (Array - quantityAndTotalAmtt)
    public ProductModel(double qty, double price) {
        this.qty = qty;
        this.price = price;
    }

    //  (vicky longadge) Payment Fragment (Array - ItemSelectedByUser // FinalBill Model Class)
    public ProductModel(int pro_id, String pro_name, double qty, double price) {
        this.pro_id = pro_id;
        this.pro_name = pro_name;
        this.qty = qty;
        this.price = price;
    }

    // (vicky longadge) used to get data from temp table
    public ProductModel(int pro_id, double qty, double price) {
        this.pro_id = pro_id;
        this.qty = qty;
        this.price = price;
    }


    // search product
    public ProductModel(int pro_id, String pro_name, double qty, double price, int category_id, int soldby_Id, String quantity_sold) {
        this.pro_id = pro_id;
        this.pro_name = pro_name;
        this.qty = qty;
        this.price = price;
        this.category_id = category_id;
        this.soldby_Id = soldby_Id;
        this.quantity_sold = quantity_sold;
    }

    public ProductModel() {
    }

    public static final Comparator<ProductModel> BY_QUANTITY_SOLD = new Comparator<ProductModel>() {
        @Override
        public int compare(ProductModel o1, ProductModel o2) {
            int p = 0;
            if (Double.compare(Double.valueOf(o2.getQuantity_sold()), Double.valueOf(o1.getQuantity_sold())) == 0) {
                p = o1.getPro_name().toLowerCase().compareTo(o2.getPro_name().toLowerCase());

            } else {
                p = Double.compare(Double.valueOf(o2.getQuantity_sold()), Double.valueOf(o1.getQuantity_sold()));
            }

            return p;
        }
    };







    public int getPro_id() {
        return pro_id;
    }

    public void setPro_id(int pro_id) {
        this.pro_id = pro_id;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public double getQty() {
        return qty;
    }

    public void setQty(double qty) {
        this.qty = qty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getSoldbyId() {
        return soldby_Id;
    }

    public void setSoldbyId(int soldby_Id) {
        this.soldby_Id = soldby_Id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public int getBarcodeStatus() {
        return barcodeStatus;
    }

    public void setBarcodeStatus(int barcodeStatus) {
        this.barcodeStatus = barcodeStatus;
    }

    public int getTax_id() {
        return tax_id;
    }

    public void setTax_id(int tax_id) {
        this.tax_id = tax_id;
    }

    public int getSync_status() {
        return sync_status;
    }

    public void setSync_status(int sync_status) {
        this.sync_status = sync_status;
    }

    public String getTaxes() {
        return taxes;
    }

    public void setTaxes(String taxes) {
        this.taxes = taxes;
    }

    public String getQuantity_sold() {
        return quantity_sold;
    }

    public void setQuantity_sold(String quantity_sold) {
        this.quantity_sold = quantity_sold;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public double getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(double totalAmt) {
        this.totalAmt = totalAmt;
    }
}
