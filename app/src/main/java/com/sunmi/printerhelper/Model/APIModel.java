
package com.sunmi.printerhelper.Model;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class APIModel implements Serializable {

    @SerializedName("success")
    @Expose
    private Boolean success;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<LoginModel> data = null;

    @SerializedName("cat_data")
    @Expose
    private List<CatDatum> cat_Data = null;

    @SerializedName("cat_update_data")
    @Expose
    private List<CatDatum> cat_update_data = null;

    @SerializedName("cat_delete_data")
    @Expose
    private List<CatDatum> cat_delete_data = null;

    @SerializedName("datas")
    @Expose
    private List<GetPro> datas = null;

    @SerializedName("purchase_data")
    @Expose
    private List<PurchaseModel> purchaseData = null;

    @SerializedName("new_product")
    @Expose
    private List<ProductModel> productModel = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<LoginModel> getData() {
        return data;
    }

    public void setData(List<LoginModel> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CatDatum> getCat_Data() {
        return cat_Data;
    }

    public void setCat_Data(List<CatDatum> cat_Data) {
        this.cat_Data = cat_Data;
    }

    public List<CatDatum> getCat_update_data() {
        return cat_update_data;
    }

    public void setCat_update_data(List<CatDatum> cat_update_data) {
        this.cat_update_data = cat_update_data;
    }

    public List<GetPro> getDatas() {
        return datas;
    }

    public void setDatas(List<GetPro> datas) {
        this.datas = datas;
    }

    public List<PurchaseModel> getPurchaseData() {
        return purchaseData;
    }

    public void setPurchaseData(List<PurchaseModel> purchaseData) {
        this.purchaseData = purchaseData;
    }

    public List<CatDatum> getCat_delete_data() {
        return cat_delete_data;
    }

    public void setCat_delete_data(List<CatDatum> cat_delete_data) {
        this.cat_delete_data = cat_delete_data;
    }


    public List<ProductModel> getNewProduct() {
        return productModel;
    }

    public void setNewProduct(List<ProductModel> productModel) {
        this.productModel = productModel;
    }
}
