package com.sunmi.printerhelper.Model;

import java.io.Serializable;

public class CartAlItemsModel implements Serializable {

    String ProductName;
    int Quantity;
    double TotalpriceOfitem;


    public CartAlItemsModel()
    {
        this.ProductName = ProductName;
        this.Quantity = Quantity;
        this.TotalpriceOfitem = TotalpriceOfitem;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public double getTotalpriceOfitem() {
        return TotalpriceOfitem;
    }

    public void setTotalpriceOfitem(double totalpriceOfitem) {
        TotalpriceOfitem = totalpriceOfitem;
    }
}
