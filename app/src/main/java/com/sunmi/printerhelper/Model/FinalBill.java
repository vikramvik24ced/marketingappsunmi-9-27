package com.sunmi.printerhelper.Model;

import java.io.Serializable;
import java.util.ArrayList;

public class FinalBill implements Serializable {

    ArrayList<ProductModel> ItemsSelectedByUser = new ArrayList<>();
    OrderModel orderModel;


    public FinalBill(ArrayList<ProductModel> ItemsSelectedByUser, OrderModel orderModel)
    {
        this.ItemsSelectedByUser = ItemsSelectedByUser;
        this.orderModel = orderModel;
    }

    public ArrayList<ProductModel> getItemsSelectedByUser() {
        return ItemsSelectedByUser;
    }

    public void setItemsSelectedByUser(ArrayList<ProductModel> itemsSelectedByUser) {
        ItemsSelectedByUser = itemsSelectedByUser;
    }

    public OrderModel getOrderModel() {
        return orderModel;
    }

    public void setOrderModel(OrderModel orderModel) {
        this.orderModel = orderModel;
    }
}
