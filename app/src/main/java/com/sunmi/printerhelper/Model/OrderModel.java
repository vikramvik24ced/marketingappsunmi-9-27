package com.sunmi.printerhelper.Model;

import java.io.Serializable;

public class OrderModel implements Serializable {
    private int order_id, cus_Id, taxType, billStatus;
    private double amtBeforeDiscount, discountAmt, amtBeforeTax, taxPercent, taxAmt, discountPerc;
    private String billDate, amtAfterTax, amtOfEach_Tax, taxID, payment_mode;

    //--himanshu--  used in Payment fragment and in receipt fragment
    public OrderModel(int order_id, String payment_mode, int cus_Id, int taxType, double amtBeforeDiscount, double discountPerc, double discountAmt, double amtBeforeTax, String taxID, String amtAfterTax, String billDate, String amtOfEach_Tax, int billStatus) {

        this.order_id = order_id;
        this.cus_Id = cus_Id;
        this.taxType = taxType;
        this.amtBeforeDiscount = amtBeforeDiscount;
        this.discountPerc = discountPerc;
        this.discountAmt = discountAmt;
        this.amtBeforeTax = amtBeforeTax;
        this.amtAfterTax = amtAfterTax;
        this.taxID = taxID;
        this.billDate = billDate;
        this.amtOfEach_Tax = amtOfEach_Tax;
        this.billStatus = billStatus;
        this.payment_mode = payment_mode;
    }

    // (vicky longadge) used in CartalItems fragment
    public OrderModel(int cus_Id, int taxType, double amtBeforeDiscount, double discountPerc, double discountAmt, double amtBeforeTax, String taxID, double taxAmt, String amtAfterTax, String amtOfEach_Tax) {
        this.cus_Id = cus_Id;
        this.taxType = taxType;
        this.amtBeforeDiscount = amtBeforeDiscount;
        this.discountPerc = discountPerc;
        this.discountAmt = discountAmt;
        this.amtBeforeTax = amtBeforeTax;
        this.taxID = taxID;
        this.taxAmt = taxAmt;
        this.amtAfterTax = amtAfterTax;
        this.amtOfEach_Tax = amtOfEach_Tax;
    }

    public String getTaxID() {
        return taxID;
    }

    public void setTaxID(String taxID) {
        this.taxID = taxID;
    }

    public String getAmtOfEach_Tax() {
        return amtOfEach_Tax;
    }

    public void setAmtOfEach_Tax(String amtOfEach_Tax) {
        this.amtOfEach_Tax = amtOfEach_Tax;
    }

    public int getCus_Id() {
        return cus_Id;
    }

    public void setCus_Id(int cus_Id) {
        this.cus_Id = cus_Id;
    }

    public double getAmtBeforeDiscount() {
        return amtBeforeDiscount;
    }

    public void setAmtBeforeDiscount(double amtBeforeDiscount) {
        this.amtBeforeDiscount = amtBeforeDiscount;
    }

    public double getDiscountAmt() {
        return discountAmt;
    }

    public void setDiscountAmt(double discountAmt) {
        this.discountAmt = discountAmt;
    }

    public double getAmtBeforeTax() {
        return amtBeforeTax;
    }

    public void setAmtBeforeTax(double amtBeforeTax) {
        this.amtBeforeTax = amtBeforeTax;
    }

    public String getAmtAfterTax() {
        return amtAfterTax;
    }

    public void setAmtAfterTax(String amtAfterTax) {
        this.amtAfterTax = amtAfterTax;
    }

    public double getTaxAmt() {
        return taxAmt;
    }

    public void setTaxAmt(double taxAmt) {
        this.taxAmt = taxAmt;
    }

    public int getTaxType() {
        return taxType;
    }

    public void setTaxType(int taxType) {
        this.taxType = taxType;
    }

    public double getDiscountPerc() {
        return discountPerc;
    }

    public void setDiscountPerc(double discountPerc) {
        this.discountPerc = discountPerc;
    }

    public int getOrder_id() {
        return order_id;
    }

    public void setOrder_id(int order_id) {
        this.order_id = order_id;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public int getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(int billStatus) {
        this.billStatus = billStatus;
    }

    public double getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(double taxPercent) {
        this.taxPercent = taxPercent;
    }

    public String getPayment_mode() {
        return payment_mode;
    }

    public void setPayment_mode(String payment_mode) {
        this.payment_mode = payment_mode;
    }
}
