package com.sunmi.printerhelper.Model;

import java.io.Serializable;

public class Tabledemo implements Serializable {

    private String[] text;
    private int[] widthh;
    private int[] alignn;

// headings, all items
    public Tabledemo(String[] text, int[] widthh, int[] alignn)
    {

        this.text = text;
        this.widthh = widthh;
        this.alignn = alignn;
    }




    public String[] getText() {
        return text;
    }

    public void setText(String[] text) {
        this.text = text;
    }

    public int[] getWidthh() {
        return widthh;
    }

    public void setWidthh(int[] widthh) {
        this.widthh = widthh;
    }

    public int[] getAlignn() {
        return alignn;
    }

    public void setAlignn(int[] alignn) {
        this.alignn = alignn;
    }
}
