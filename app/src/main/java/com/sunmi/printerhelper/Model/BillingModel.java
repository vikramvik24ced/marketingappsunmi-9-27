package com.sunmi.printerhelper.Model;

import java.io.Serializable;

public class BillingModel implements Serializable {

    private int id, product_id;
    private double total_quantity, total_amount;

    public BillingModel(int id, int product_id, double total_quantity, double total_amount) {
        this.id = id;
        this.product_id = product_id;
        this.total_quantity = total_quantity;
        this.total_amount = total_amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public double getTotal_quantity() {
        return total_quantity;
    }

    public void setTotal_quantity(double total_quantity) {
        this.total_quantity = total_quantity;
    }

    public double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(double total_amount) {
        this.total_amount = total_amount;
    }
}
