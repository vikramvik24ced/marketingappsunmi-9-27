package com.sunmi.printerhelper.Model;

import java.io.Serializable;

public class TotalAmountModel implements Serializable {

    double totalAmount;

    public TotalAmountModel(double totalAmount)
    {
        this.totalAmount = totalAmount;
    }


    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }
}
