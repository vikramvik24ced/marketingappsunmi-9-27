package com.sunmi.printerhelper.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CatDatum {

@SerializedName("category_name")
@Expose
private String categoryName;

    @SerializedName("category_id")
    @Expose
    private int category_id;


public String getCategoryName() {
return categoryName;
}

public void setCategoryName(String categoryName) {
this.categoryName = categoryName;
}

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }
}