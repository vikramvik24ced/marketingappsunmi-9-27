package com.sunmi.printerhelper.Model;

import java.io.Serializable;

public class TaxModel implements Serializable {

    private   String Taxname;
    private Double taxPercent;
    private int id, taxType, selected;


    public TaxModel(){}
    public TaxModel(Integer id, String Taxname, Double taxPercent, int taxType, int selected )
    {
        this.Taxname = Taxname;
        this.taxPercent = taxPercent;
        this.id = id;
        this.taxType = taxType;
        this.selected = selected;
    }

    public TaxModel( String Taxname, Double taxPercent)
    {
        this.Taxname = Taxname;
        this.taxPercent = taxPercent;
    }

    public TaxModel( String Taxname, Double taxPercent, int selected)
    {
        this.Taxname = Taxname;
        this.taxPercent = taxPercent;
        this.selected = selected;
    }

    public TaxModel( Integer id )
    {
        this.id = id;
    }


    public String getTaxname() {
        return Taxname;
    }

    public void setTaxname(String taxname) {
        Taxname = taxname;
    }

    public Double getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(Double taxPercent) {
        this.taxPercent = taxPercent;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getTaxType() {
        return taxType;
    }

    public void setTaxType(int taxType) {
        this.taxType = taxType;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSelected() {
        return selected;
    }
    public void setSelected(int selected) {
        this.selected = selected;
    }
}