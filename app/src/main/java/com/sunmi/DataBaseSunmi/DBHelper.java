package com.sunmi.DataBaseSunmi;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.sunmi.printerhelper.Model.BillingModel;
import com.sunmi.printerhelper.Model.CategoryModel;
import com.sunmi.printerhelper.Model.CustomerDetailsModel;
import com.sunmi.printerhelper.Model.FinalBill;
import com.sunmi.printerhelper.Model.OrderModel;
import com.sunmi.printerhelper.Model.ProductModel;
import com.sunmi.printerhelper.Model.SaleTypeModel;
import com.sunmi.printerhelper.Model.PurchaseModel;
import com.sunmi.printerhelper.Model.TaxModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import static com.sunmi.DataBaseSunmi.TablesAndColumns.AddToCart_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.BillingProduct_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Category_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Create_Table_Query;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Customer_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.DATABASE_NAME;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.DATABASE_VERSION;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Image_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Order_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Product_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Purchased_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.SoldBy_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Tax_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Temp_Tab_Table_Name;
import static com.sunmi.DataBaseSunmi.TablesAndColumns.Temp_Table_Name;

public class DBHelper extends SQLiteOpenHelper {

    private ProductModel productModel;
    private String TAG = "DBHelper";
    private int rowID = 0;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Create_Table_Query + Tax_Table_Name +
                "(Tax_id integer PRIMARY KEY AUTOINCREMENT, Tax_Name text, Tax_Precentage real, taxType integer default 2, selected integer default 0)");

        db.execSQL(Create_Table_Query + Category_Table_Name +
                "(Category_id integer PRIMARY KEY AUTOINCREMENT, Category_Name text, sync_status integer)");

        db.execSQL(Create_Table_Query + SoldBy_Table_Name + "(SoldBy_id integer PRIMARY KEY AUTOINCREMENT, SaleType text)");

        db.execSQL(Create_Table_Query + Product_Table_Name +
                "(Pro_id integer PRIMARY KEY AUTOINCREMENT, Pro_name text not null , Quantity real not null, Price real not null, barcode text unique, BarCode_Status integer," +
                "Category_ID integer REFERENCES Category(Category_id), Soldby_Id integer REFERENCES SoldBy(SoldBy_id), sync_status integer, delete_status integer default 0, quantity_sold text default '0')");

        db.execSQL(Create_Table_Query + Customer_Table_Name +
                "(Customer_id integer PRIMARY KEY AUTOINCREMENT, Customer_Name text, Address text, Mobile_Number long, " +
                "Email_id text, delete_status integer default 0)");

        db.execSQL(Create_Table_Query + Order_Table_Name + "(orderr_id integer PRIMARY KEY AUTOINCREMENT, " +
                "payment_mode text default null, cust_id integer REFERENCES Customer(Customer_id), tax_type integer, amt_before_dis real, " +
                "discount_perc real, discount_amt real, amt_after_dis real, tax_id string, amt_after_tax real, date text, amt_of_each_tax text, bill_status integer)");

        db.execSQL(Create_Table_Query + BillingProduct_Table_Name + "(Order_Id integer REFERENCES Orderr(Orderr_Id), Pro_Id integer,Quantity real, Pro_Price real )");

        db.execSQL(Create_Table_Query + Purchased_Table_Name +
                "(purchase_id integer PRIMARY KEY AUTOINCREMENT, vendor_name text, purchase_date text, purchase_amount real, sync_status integer, delete_status integer default 0)");

        db.execSQL(Create_Table_Query + Image_Table_Name + "(image_id integer REFERENCES Purchase(purchase_id), purchase_image text)");

        db.execSQL(Create_Table_Query + AddToCart_Table_Name + "(pro_idd integer, selected_quantity integer)");

        db.execSQL(Create_Table_Query + Temp_Table_Name + "(pro_idd integer, selected_quantity real, total_amt real)");

        db.execSQL(Create_Table_Query + Temp_Tab_Table_Name + "(pro_idd integer, pro_name text, selected_quantity real,price real, total_amt real)");


    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//        db.execSQL("DROP TABLE IF EXISTS " + Tax_Table_Name);
//        if (oldVersion<2) {
//            db.execSQL("alter table " + Order_Table_Name + " add column amt_of_each_tax text default 0");
//        }
//
//        Log.e("upgraded", "yoo");
    }


    //Insert Tax into Tax Table
    public void insertTax(TaxModel taxModel) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Tax_Name", taxModel.getTaxname());
        contentValues.put("Tax_Precentage", taxModel.getTaxPercent());
        contentValues.put("selected", taxModel.getSelected());
        sqLiteWritable.insert(Tax_Table_Name, null, contentValues);
        sqLiteWritable.close();
    }


    // update Tax Table
    public int updateTaxTable(int taxtype) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("taxType", taxtype);
        int a = sqLiteWritable.update(Tax_Table_Name, contentValues, "", null);
        return a;
    }

    // "(Tax_id integer PRIMARY KEY AUTOINCREMENT, Tax_Name text, Tax_Precentage integer, taxType integer default 2, selected integer default 0)");

    public TaxModel getTax_DB(int taxid) {
        TaxModel taxModel = null;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Tax_Table_Name + " where Tax_id = '" + taxid + "'", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                taxModel = new TaxModel(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2), cursor.getInt(3), cursor.getInt(4));
            }
        }
        Log.e("getTax_DB ", taxModel.getTaxname());
        cursor.close();
        return taxModel;
    }


    // getting number of inclusive and exclusive tax
    public int[] searchInclusiveExclusive() {
        int[] taxCount = new int[3];
        Cursor cur_TotalTax = null;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        cur_TotalTax = sqLiteReadable.rawQuery("select * from " + Tax_Table_Name, null);
        // total no of taxes
        taxCount[0] = cur_TotalTax.getCount();
        Log.e("DBTotalTaxAvailable ", cur_TotalTax.getCount() + "");
        // total no of in taxes
        cur_TotalTax = sqLiteReadable.rawQuery("select * from " + Tax_Table_Name + " where taxType = 1 ", null);
        Log.e("DBNoOfInclusiveTax ", cur_TotalTax.getCount() + "");
        taxCount[1] = cur_TotalTax.getCount();
        // total no of ex taxes
        cur_TotalTax = sqLiteReadable.rawQuery("select * from " + Tax_Table_Name + " where taxType = 2 ", null);
        Log.e("DBNoOfExclusiveTax ", cur_TotalTax.getCount() + "");
        taxCount[2] = cur_TotalTax.getCount();
        cur_TotalTax.close();
        return taxCount;
    }

    public ArrayList<TaxModel> getTaxPercentColumn(int taxTypee) {
        ArrayList<TaxModel> ListTaxPercent = new ArrayList<>();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select Tax_Name, Tax_Precentage from " + Tax_Table_Name + " where taxType = '" + taxTypee + "'", null);
        TaxModel taxModel;
        if (cursor != null) {
            //getting TAX ID from Local DB
            while (cursor.moveToNext()) {

                ListTaxPercent.add(new TaxModel(cursor.getString(0), cursor.getDouble(1)));
                Log.e("TaxPercent ", ListTaxPercent.toString());
            }
        }
        cursor.close();
        return ListTaxPercent;
    }


    //insert CategoryName in Database  
    public void insertCategory(CategoryModel categoryModel) {


        Log.e("status ", categoryModel.getSyncStatus() + "");

        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Category_Name", categoryModel.getCategegoryname());
        // contentValues.put("sync_status", 1);
        contentValues.put("sync_status", categoryModel.getSyncStatus());
        long l = sqLiteWritable.insert(Category_Table_Name, null, contentValues);
        Log.e("lCat", l + "");
    }

    //get CategoryName from category Id  
    public String getCategoryName(int cat_id) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String CategoryNames = null;
        Cursor cursor = sqLiteDatabase.rawQuery("select Category_Name from " + Category_Table_Name + " where  Category_ID =" + cat_id, null);
        ArrayList<CategoryModel> arrayList = new ArrayList<>();
        if ((cursor != null)) {
            while (cursor.moveToNext()) {
                CategoryNames = cursor.getString(0);
            }
        }
        cursor.close();
        sqLiteDatabase.close();
        return CategoryNames;
    }

    // getting Category details from Local DB  
    public ArrayList<CategoryModel> getCategoryData() {
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Category_Table_Name + " ", null);
        ArrayList<CategoryModel> categoryArrayList = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                CategoryModel categoryModel = new CategoryModel(cursor.getInt(0), cursor.getString(1), eachCategory_total_qyantity(cursor.getInt(0)));
                Log.e("eachCategory_total ", cursor.getString(1));
                // Adding user record to list
                categoryArrayList.add(categoryModel);
                Log.d("hi", categoryArrayList.toString());
            }
        }
        cursor.close();
        sqLiteReadable.close();
        return categoryArrayList;
    }

    // (VICKY LONGADGE) -- get total quantity in each category
    public double eachCategory_total_qyantity(int cat_id) {
        double total_quantity = 0;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select sum(quantity_sold) from " + Product_Table_Name + " where  Category_ID =" + cat_id, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                total_quantity = cursor.getDouble(0);
                Log.e("eachCategory_total ", total_quantity + "");
            }
        }
        cursor.close();
        return total_quantity;
    }


    // inserting data into product table
    public boolean insertProduct(ProductModel productModel) {
        Log.e("vicky: ", productModel.getPro_name());
        SQLiteDatabase db1 = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Pro_name", productModel.getPro_name());
        contentValues.put("Quantity", productModel.getQty());
        contentValues.put("Price", productModel.getPrice());
        contentValues.put("barcode", productModel.getBarcode());
        contentValues.put("BarCode_Status", productModel.getBarcodeStatus());
        contentValues.put("Category_ID", productModel.getCategory_id());
        contentValues.put("Soldby_Id", productModel.getSoldbyId());
        contentValues.put("sync_status", productModel.getSync_status());

        long result = db1.insert(Product_Table_Name, null, contentValues);
        db1.close();
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    // PRODUCT SYNC STATUS
    public ArrayList<ProductModel> search_SyncStatus() {
        ArrayList<ProductModel> list_productsWithStatusZero = new ArrayList<>();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Product_Table_Name + " where sync_status = 0 ", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                ProductModel productModel = new ProductModel(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2),
                        cursor.getDouble(3), cursor.getString(4), cursor.getInt(5), cursor.getInt(6), cursor.getInt(7));
                // Adding user record to list
                list_productsWithStatusZero.add(productModel);
                Log.e("ZeroStatusProducts", list_productsWithStatusZero.size() + "");
            }
        }
        cursor.close();
        return list_productsWithStatusZero;
    }


    // CAETEGORY SYNC STATUS
    public ArrayList<CategoryModel> search_SyncStatusCate() {
        ArrayList<CategoryModel> list_categoriesWithStatusZero = new ArrayList<>();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Category_Table_Name + " where sync_status = 0 ", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                CategoryModel categoryModel = new CategoryModel(cursor.getInt(0), cursor.getString(1), cursor.getInt(2));
                list_categoriesWithStatusZero.add(categoryModel);

            }
            Log.e("ZeroStatusCategories", list_categoriesWithStatusZero.size() + "");
        }
        cursor.close();
        return list_categoriesWithStatusZero;
    }

    // CHANGE SYNC STATUS OF CATEGORY
    public void updateSyncStatus(JSONArray jsonArrayCate, String table_name, String column_name1, String column_name2) throws JSONException {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        int status = 1;
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < jsonArrayCate.length(); i++) {
            JSONObject jsonObject1 = jsonArrayCate.getJSONObject(i);
            int id = Integer.valueOf(jsonObject1.optString(column_name2));
            Log.e("Pro_id", id + "");
            contentValues.put(column_name1, status);
            sqLiteWritable.update(table_name, contentValues, column_name2 + "=" + id, null);
        }
    }

    public void updateCategoryName(int id, String data) throws JSONException {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Category_Name", data);
        sqLiteWritable.update(Category_Table_Name, contentValues, "Category_id=" + id, null);
    }

    // getting products Details from local DB  
    public ArrayList<ProductModel> getProductList() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + Product_Table_Name + " ", null);
        ArrayList<ProductModel> arrayList = new ArrayList<>();

        if ((cursor != null)) {
            while (cursor.moveToNext()) {
                ProductModel productModel = new ProductModel(cursor.getInt(0), cursor.getString(1),
                        cursor.getDouble(2), cursor.getDouble(3), cursor.getString(4));
                // Adding user record to list
                arrayList.add(productModel);
            }
        }
        cursor.close();
        db.close();

        return arrayList;
    }


    //Insert data in SoldBy Table
    public boolean insertDataInSoldBy(SaleTypeModel saleTypeModel) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("SaleType", saleTypeModel.getSaleType());
        long result = sqLiteWritable.insert(SoldBy_Table_Name, null, contentValues);
        sqLiteWritable.close();
        if (result == -1) return false;
        else return true;
    }

    // (vicky) get SoldBy_id ding where clause
    public ArrayList<SaleTypeModel> getSoldById() {
        ArrayList<SaleTypeModel> saleTypeModelArrayList = new ArrayList<>();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + SoldBy_Table_Name, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                SaleTypeModel saleTypeModel = new SaleTypeModel(cursor.getInt(0), cursor.getString(1));
                Log.e("soldBy DB ", saleTypeModel + "");
                saleTypeModelArrayList.add(saleTypeModel);
            }
        }
        cursor.close();
        sqLiteReadable.close();
        return saleTypeModelArrayList;

    }

    public String getSoldByType(int soldBy) {
        String saleType = "";
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select SaleType from " + SoldBy_Table_Name + " where SoldBy_id = '" + soldBy + "'", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                saleType = cursor.getString(0);
            }
        }
        cursor.close();
        return saleType;
    }

    // getting all specific products name by using category id
    public ArrayList<ProductModel> getProdcutName(int category_id) {
        ProductModel productModel;
        ArrayList<ProductModel> productList = new ArrayList<>();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select Pro_name from " + Product_Table_Name + " where Category_ID = '" + category_id + "'", null);
        if (cursor == null) Log.e("cursor count(Product)", cursor.getCount() + "");
        else if (cursor != null) {
            while (cursor.moveToNext()) {
                productModel = new ProductModel(cursor.getString(0));
                productList.add(productModel);
                Log.e("DB Prodct list: ", productList.toString());
            }
        }
        cursor.close();
        return productList;
    }


    //(vicky longadge) Billing - on clicking category
    // MODEL CLASS USED(int pro_id, String pro_name, String quantity, double price, int category_id, int soldby_Id)
    public ArrayList<ProductModel> getProductArrayList(int category_id) {
        ProductModel productModel;
        ArrayList<ProductModel> productList = new ArrayList<>();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select Pro_id,Pro_name,Quantity,Price,Category_ID,Soldby_Id,quantity_sold from " + Product_Table_Name + " where Category_ID = '" + category_id + "' AND delete_status =0", null);
        if (cursor == null) Log.e("cursor count(Product)", cursor.getCount() + "");
        else if (cursor != null) {
            while (cursor.moveToNext()) {
                productModel = new ProductModel(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3), cursor.getInt(4), cursor.getInt(5), cursor.getString(6));
                productList.add(productModel);
                Log.e(TAG + " Product_list: ", productList.toString());
            }
        }cursor.close();
        return productList;
    }


    //(vicky longadge) Billing - searching using edit text
    // MODEL CLASS USED(int pro_id, String pro_name, String quantity, double price, int category_id, int soldby_Id)
    public ArrayList<ProductModel> getProdcutSearched() {
        ProductModel productModel;
        ArrayList<ProductModel> productList = new ArrayList<>();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select Pro_id,Pro_name,Quantity,Price,Category_ID,Soldby_Id, quantity_sold from " + Product_Table_Name, null);
        if (cursor == null) Log.e("cursor count(Product)", cursor.getCount() + "");
        else if (cursor != null) {
            while (cursor.moveToNext()) {
                productModel = new ProductModel(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3), cursor.getInt(4), cursor.getInt(5), cursor.getString(6));
                productList.add(productModel);
                Log.e(TAG + " Product_list: ", productList.toString());
            }
        }cursor.close();
        return productList;
    }


    // getting Name by using id  
    public String getName(String Table_Name, String Col_Name, String Selected_Col_Name, int id) {
        String name = null;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select " + Selected_Col_Name + " from " + Table_Name + " where " + Col_Name + "= '" + id + "'", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                name = cursor.getString(0);
                Log.e("name", name);
            }
        }cursor.close();
        return name;
    }

    // insert data in customer table
    public boolean insertCustomer(CustomerDetailsModel customerDetails) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Customer_Name", customerDetails.getCustomerName());
        contentValues.put("Address", customerDetails.getCustomerAddress());
        contentValues.put("Mobile_Number", customerDetails.getCustomerMobileNumber());
        contentValues.put("Email_id", customerDetails.getCustomerEmail());
        long result = sqLiteWritable.insert(Customer_Table_Name, null, contentValues);
        sqLiteWritable.close();
        if (result == -1) {
            return false;
        } else
            return true;
    }

    // get all data from customer table
    public ArrayList<CustomerDetailsModel> getCustomerListFromDataBase() {
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Customer_Table_Name + " where delete_status = 0", null);
        ArrayList<CustomerDetailsModel> customerDetailsArrayList = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                CustomerDetailsModel customerDetails = new CustomerDetailsModel(cursor.getInt(0), cursor.getString(1),
                        cursor.getString(2), cursor.getLong(3), cursor.getString(4));
                customerDetailsArrayList.add(customerDetails);
                Log.d("CustomerDetailList", customerDetailsArrayList.toString());
            }
        }
        cursor.close();
        sqLiteReadable.close();
        return customerDetailsArrayList;
    }


    public long insertDataInorderTable
            (int cust_id, String payment_mode, ArrayList<OrderModel> orderArr, ArrayList<ProductModel> ItemsSelectedByUserr,
             ArrayList<ProductModel> quantityAndTotalAmt, String formattedDate, int bill_status) {
        long check_status = 0, check_BillProduct = 0;
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("cust_id", cust_id);
        contentValues.put("payment_mode", payment_mode);
        contentValues.put("tax_type", orderArr.get(0).getTaxType());
        contentValues.put("amt_before_dis", orderArr.get(0).getAmtBeforeDiscount());// percent
        contentValues.put("discount_perc", orderArr.get(0).getDiscountPerc());
        contentValues.put("discount_amt", orderArr.get(0).getDiscountAmt());
        contentValues.put("amt_after_dis", orderArr.get(0).getAmtBeforeTax());
        contentValues.put("tax_id", orderArr.get(0).getTaxID());
        contentValues.put("amt_after_tax", orderArr.get(0).getAmtAfterTax());
        contentValues.put("date", formattedDate);
        contentValues.put("amt_of_each_tax", orderArr.get(0).getAmtOfEach_Tax());
        contentValues.put("bill_status", bill_status);
        check_status = sqLiteWritable.insert(Order_Table_Name, null, contentValues);

// searching for last inserted row
        if (check_status > 0) {
            int orderId = 0;
            Cursor cursor = sqLiteReadable.rawQuery("select Orderr_Id from " + Order_Table_Name + " order by Orderr_Id desc limit 1", null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    Log.e("billing product ", cursor.getInt(0) + "");
                    orderId = cursor.getInt(0);
                }
            }
            check_BillProduct = insertDataInbillingPro(orderId, ItemsSelectedByUserr, quantityAndTotalAmt);
            Log.e("check_status ", check_status + "");
            cursor.close();
        }
        sqLiteWritable.close();
        sqLiteReadable.close();

        return check_BillProduct;
    }

    public long insertDataInbillingPro(int orderId, ArrayList<ProductModel> ItemsSelectedByUserr, ArrayList<ProductModel> quantityAndTotalAmt) {
        long count = 0;
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < ItemsSelectedByUserr.size(); i++) {
            Log.e("insertDataInbillingPro ", ItemsSelectedByUserr.get(i).getPro_name() + "  " + quantityAndTotalAmt.get(i).getQty());
            contentValues.put("Order_Id", orderId);
            contentValues.put("Pro_Id", ItemsSelectedByUserr.get(i).getPro_id());
            contentValues.put("Quantity", quantityAndTotalAmt.get(i).getQty());
            contentValues.put("Pro_Price", quantityAndTotalAmt.get(i).getPrice());
            count = sqLiteWritable.insert(BillingProduct_Table_Name, null, contentValues);
            if (count <= 0) break;
            Log.e("insertDataInbillingPro", "count " + count);
        }
        sqLiteWritable.close();
        Log.d("yooBilling", "insertDataInbillingPro: .e");
        return count;
    }

    //   (int pro_id, String pro_name, String quantity, double price)
    public ProductModel getProduct(int pro_id) {
        int available = 0;
        ProductModel productModel = null;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Product_Table_Name + " where Pro_id=" + pro_id, null);
        Log.e("available or not ", cursor.getCount() + "");
        if (cursor != null) {
            while (cursor.moveToNext()) {
                productModel = new ProductModel(cursor.getInt(0), cursor.getString(1), 0, 0);
            }
        }
        cursor.close();
        return productModel;
    }


    //get product from category id
    public ArrayList<ProductModel> getProduct_Tablet(int category_id) {
        // (int pro_id, String pro_name, String qty, double price, String barcode, int barcodeStatus, int category_ID, int soldby_ID)
        ProductModel productModel;
        ArrayList<ProductModel> productList = new ArrayList<>();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Product_Table_Name + " where Category_ID = '" + category_id + "' AND delete_status = 0", null);
        if (cursor == null) Log.e("cursor count(Product)", cursor.getCount() + "");
        else if (cursor != null) {
            while (cursor.moveToNext()) {
                productModel = new ProductModel(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3),
                        cursor.getString(4), cursor.getInt(5), cursor.getInt(6), cursor.getInt(7));
//                productModel = new ProductModel(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getDouble(3), cursor.getInt(4), cursor.getInt(5), cursor.getString(6));
                productList.add(productModel);
                Log.e(TAG + " Product_list: ", productList.toString());
            }
        }cursor.close();
        return productList;
    }

    public ArrayList<OrderModel> getOrderDetails() {
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Order_Table_Name + " ", null);
        ArrayList<OrderModel> orderDetails = new ArrayList<>();
        if (cursor != null) {
            int i = 0;
            while (cursor.moveToNext()) {
                OrderModel orderModel = new OrderModel(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3),
                        cursor.getDouble(4), cursor.getDouble(5), cursor.getDouble(6),
                        cursor.getDouble(7), cursor.getString(8), cursor.getString(9),
                        cursor.getString(10), cursor.getString(11), cursor.getInt(12));
                orderDetails.add(orderModel);
                Log.e(TAG + " status", orderDetails.get(i).getBillStatus() + "");
                i++;
            }
        }
        cursor.close();
        sqLiteReadable.close();
        return orderDetails;
    }

    //getting Purchase Details from Local DB  
    public ArrayList<PurchaseModel> getPurchaseDtailsFromDB() {
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Purchased_Table_Name + " where delete_status = 0", null);
        ArrayList<PurchaseModel> purchaseModelArrayList = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                PurchaseModel purchaseModel = new PurchaseModel(cursor.getInt(0), cursor.getString(1),
                        cursor.getString(2), cursor.getInt(3));
                // Adding user record to list
                purchaseModelArrayList.add(purchaseModel);
            }
        }
        cursor.close();
        sqLiteReadable.close();
        return purchaseModelArrayList;
    }

    //Search row from barcodes  
    public boolean searchBarCodeProduct(String barCodeResult) {
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        boolean check = false;
        ProductModel productModel = null;
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Product_Table_Name + " where barcode = '" + barCodeResult + "'", null);
        int value = cursor.getCount();
        if (value > 0) {
            check = true;
            for (int i = 0; i < value; i++) {

                if (cursor.moveToFirst()) {
                    productModel = new ProductModel(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2),
                            cursor.getDouble(3), cursor.getString(4), cursor.getInt(5), cursor.getInt(6), cursor.getInt(7));
                }
                setProductFromBraCode(productModel);
            }
            Log.e(TAG + "SeaerchProduct", productModel.getPro_name() + "");
        } else {
            check = false;
        }
        cursor.close();
        return check;
    }

    //add value from barcodes into model class  
    public void setProductFromBraCode(ProductModel productModel) {
        this.productModel = productModel;
    }


    //getting value from productModel  
    public ArrayList<ProductModel> getProductFromBraCode() {
        ArrayList<ProductModel> arrayList = new ArrayList<>();
        arrayList.add(productModel);
        return arrayList;
    }

    //Insert Tax into Tax Table
    public void insertData(TaxModel taxModel) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Tax_Name", taxModel.getTaxname());
        contentValues.put("Tax_Precentage", taxModel.getTaxPercent());
        sqLiteWritable.insert(Tax_Table_Name, null, contentValues);
        sqLiteWritable.close();
        Log.e("Tax Created", "insertData: ");
    }

    // (VICKY LONGADGE)get taxes from tax table
    public ArrayList<TaxModel> getTaxData() {
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
//        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Tax_Table_Name + " where Tax_id>1 ", null);
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Tax_Table_Name, null);
        ArrayList<TaxModel> arrayList = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                TaxModel taxModel = new TaxModel(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2), cursor.getInt(3), cursor.getInt(4));
                // Adding user record to list
                arrayList.add(taxModel);
            } while (cursor.moveToNext());
        }
        cursor.close();
        sqLiteReadable.close();
        return arrayList;
    }


    // update Tax Table
    public int updateTaxSelected(int taxId, int selectedStatus) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("selected", selectedStatus);
        int a = sqLiteWritable.update(Tax_Table_Name, contentValues, "Tax_id=" + taxId, null);
        return a;
    }

    public ArrayList<TaxModel> getSelectedTax() {
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Tax_Table_Name + " where selected=1 ", null);
        ArrayList<TaxModel> arrayList = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                TaxModel taxModel = new TaxModel(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2), cursor.getInt(3), cursor.getInt(4));
                arrayList.add(taxModel);
                Log.d("SelectedTaax", arrayList.size() + "");
            }
        }

        cursor.close();
        return arrayList;
    }

    public TaxModel getDefaultTax() {
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        TaxModel taxModel = null;
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Tax_Table_Name + " where Tax_id=1 ", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                taxModel = new TaxModel(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2), cursor.getInt(3), cursor.getInt(4));
            }
        }
        cursor.close();
        return taxModel;
    }


    //get last row from table
    public int getLastRowId(String Table_Name, String Col_Name) {

        Log.e("getLastRowId ", Table_Name + "    " + Col_Name);
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = null;
        try {
            cursor = sqLiteReadable.rawQuery("select " + Col_Name + " from " + Table_Name + " order by " + Col_Name + " desc limit 1 ", null);
            Log.e("lastRowIdDBc", cursor.getInt(0) + "");
        } catch (Exception e) {
            if (cursor.moveToNext()) {
                Log.e("lastRowIdDBe", cursor.getInt(0) + "");
                rowID = cursor.getInt(0);
                Log.e("lastRowIdDBr", rowID + "");
            } }
        rowID = rowID + 1;
        cursor.close();
        sqLiteReadable.close();
        return rowID;
    }

    // update method for purchase details by Himanshu
    public boolean updatePurchaseDetails(int id, String VenderName, String date, double amount, ArrayList<String> imagePath) throws JSONException {
        boolean check = false;
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("vendor_name", VenderName);
        contentValues.put("purchase_date", date);
        contentValues.put("purchase_amount", amount);
        int value = sqLiteWritable.update(Purchased_Table_Name, contentValues, "purchase_id = " + id, null);

        if (value == 1) {
            delete_Pro_From_Table(id, "Purchase_Image", "image_id");
            insertImage(imagePath, id);
            check = true;
        } else
            check = false;
        return check;
    }

    //insert Purchase Images Details Into Local DB  
    public boolean insertImage(ArrayList<String> arrayList, int img_id) {
        long check = 0;
        boolean status;
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        //insert new Images
        for (int i = 0; i < arrayList.size(); i++) {
            contentValues.put("image_id", img_id);
            contentValues.put("purchase_image", arrayList.get(i) + "");
            check = sqLiteWritable.insert(Image_Table_Name, null, contentValues);
            Log.e("statys", check + "");
        }
        //check weather data is insert or not
        if (check == 0) {
            status = false;
        } else
            status = true;

        sqLiteWritable.close();
        return status;
    }

    //insert Purchase Details Into Local DB 
    public int insertPurchaseDetails(PurchaseModel purchaseModel, ArrayList<String> imageModel) {

        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("vendor_name", purchaseModel.getVendorName());
        contentValues.put("purchase_date", purchaseModel.getPurchased_Date());
        contentValues.put("purchase_amount", purchaseModel.getPurchaseAmount());
        contentValues.put("sync_status", 0);

        long value = sqLiteWritable.insert(Purchased_Table_Name, null, contentValues);

        int purchasedId = 0;
        Cursor cursor = sqLiteReadable.rawQuery("select purchase_id from " + Purchased_Table_Name + " order by purchase_id desc limit 1", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                Log.e("Purchase ", cursor.getInt(0) + "");
                purchasedId = cursor.getInt(0);
            }
            Log.e("Purchase id", purchasedId + "");
        }
        sqLiteWritable.close();
        insertImage(imageModel, purchasedId);
        if (value == -1) {
            purchasedId = 0;
        }cursor.close();
        return purchasedId;
    }

    //getting Image from DB
    public ArrayList<String> getImage(int id) {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select Purchase_Image from Purchase join purchase_image" +
                " on purchase_id = image_id where purchase_id = " + id, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                arrayList.add(cursor.getString(0));
            }
        }cursor.close();
        return arrayList;
    }

    public int Pro_Available_To_AddToCart(int pro_id, String table_name, String col_name) {
        int available = 0;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + table_name + " where " + col_name + "=" + pro_id, null);
        Log.e("available or not ", cursor.getCount() + "");
        if (cursor.getCount() == 0) {
            available = 0;
        } else if (cursor.getCount() == 1) {
            available = 1;
        }
        cursor.close();
        return available;
    }

    // (vicky longadge) DELETE PRODUCT FROM ADD_TO_CART WHOSE QUANTITY IN 0
    public void delete_Pro_From_AddToCart(int ppro_id, String table_name, String col_name) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        sqLiteWritable.delete(table_name, col_name + "=" + ppro_id, null);
        Log.e("P ", "deleted ");
    }

    public void delete_category(int id) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Category_ID", 1);
        int updates = sqLiteWritable.update(Product_Table_Name, contentValues, "Category_ID=" + id, null);
        int delete = sqLiteWritable.delete(Category_Table_Name, "Category_id=" + id, null);
        Log.e("updateee: ", updates + " delete: " + delete + "");
    }

    public void truncate_AddToCart(String table_Name) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        sqLiteWritable.execSQL("delete from " + table_Name);
    }

    //Himanshu
    public CustomerDetailsModel getCustomer(int cust_id) {
        CustomerDetailsModel customerDetailsModel = null;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Customer_Table_Name + " where Customer_id='" + cust_id + "' ", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                customerDetailsModel = new CustomerDetailsModel(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                        cursor.getLong(3), cursor.getString(4));
            }
        }cursor.close();
        return customerDetailsModel;
    }

    public CustomerDetailsModel getNewCustomer() {
        CustomerDetailsModel customerDetailsModel = null;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Customer_Table_Name + " order by Customer_id desc limit 1", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                customerDetailsModel = new CustomerDetailsModel(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                        cursor.getLong(3), cursor.getString(4));
            }
        }cursor.close();
        return customerDetailsModel;
    }

    public ProductModel getProductRow(int pro_id) {
        ProductModel productModel = null;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Product_Table_Name + " where Pro_id='" + pro_id + "' ", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                productModel = new ProductModel(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2),
                        cursor.getDouble(3), cursor.getString(4), cursor.getInt(5), cursor.getInt(6)
                        , cursor.getInt(7));
            }
        }cursor.close();
        return productModel;
    }

    //GLOBAL METHOD FOR CHANGE STATUS
    public boolean change_Status(String Table_Name, String Col_Name, String Col_id, int id, int value) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        boolean status;
        ContentValues contentValues = new ContentValues();
        contentValues.put(Col_Name, value);
        int updates = sqLiteWritable.update(Table_Name, contentValues, Col_id + "=" + id, null);
        Log.e("Delete_status", updates + "");
        if (updates == 1) {
            status = true;
        } else {
            status = false;
        }return status;
    }

    // TABLET (VICKY LONGADGE) GLOBAL METHOD
    public void delete_Cust_From_Table(int cust_id, String table_name, String col_name) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        sqLiteWritable.delete(table_name, col_name + "=" + cust_id, null);
        Log.e("P ", "deleted ");
    }



    public PurchaseModel getPurchaseRow(int purchase_id) {
        PurchaseModel purchaseModel = null;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Purchased_Table_Name + " where purchase_id='" + purchase_id + "' ", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                purchaseModel = new PurchaseModel(cursor.getInt(0), cursor.getString(1),
                        cursor.getString(2), cursor.getInt(3));
            }
        }cursor.close();
        return purchaseModel;
    }

    //gettin total number of rows
    public int getTotalNumofRow(String tableName) {
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + tableName, null);
        Log.e("getTotalPurchased DB ", cursor.getCount() + "");
        cursor.close();
        return cursor.getCount();
    }


    public OrderModel getOrderRow(int order_id) {
        OrderModel orderModel = null;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Order_Table_Name + " where orderr_id='" + order_id + "' ", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                orderModel = new OrderModel(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3), cursor.getDouble(4),
                        cursor.getDouble(5), cursor.getDouble(6), cursor.getDouble(7), cursor.getString(8),
                        cursor.getString(9), cursor.getString(10), cursor.getString(11), cursor.getInt(12));
            }
        }
        Log.e("getOrderRow ", cursor.getCount() + " "+orderModel.getPayment_mode()+" "+orderModel.getBillStatus());
        cursor.close();
        return orderModel;
    }

    public ArrayList<ProductModel> getBillingData(int order_id) {
        ArrayList<ProductModel> billingModelArrayList = new ArrayList<>();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + BillingProduct_Table_Name + " where Order_Id='" + order_id + "' ", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                ProductModel productModel = new ProductModel(cursor.getInt(1), getProduct(cursor.getInt(1)).getPro_name(), cursor.getDouble(2), cursor.getDouble(3));
                Log.e("vicky_qty ", productModel.getPro_name() + "   " + productModel.getQty() + "    " + productModel.getPrice());
                billingModelArrayList.add(productModel);
            }
        }cursor.close();
        return billingModelArrayList;
    }

    public void update_ProductQuantity(int ppro_id, double Quantity) {
        String new_qty = null;
        ProductModel productModel = null;
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        productModel = getProductRow(ppro_id);
        new_qty = String.valueOf(productModel.getQty() - Quantity);
        ContentValues contentValues = new ContentValues();
        contentValues.put("Quantity", new_qty);
        int a = sqLiteWritable.update(Product_Table_Name, contentValues, "Pro_id=" + ppro_id, null);
    }


    public FinalBill getDataForBilling(int orderID) {
        FinalBill finalBill = new FinalBill(getBillingData(orderID), getOrderRow(orderID));
        return finalBill;
    }

    // (VICKY LONGADGE) update quantity_sold in product table
    public void update_ProductDB(ArrayList<ProductModel> ItemsSelectedByUser, ArrayList<ProductModel> quantityAndTotalAmtt) {
        Double quantity_sold = 0.0;
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        for (int i = 0; i < ItemsSelectedByUser.size(); i++) {
            Cursor cursor = sqLiteReadable.rawQuery("select quantity_sold from " + Product_Table_Name + " where Pro_id =" + ItemsSelectedByUser.get(i).getPro_id(), null);
            Log.e("update_ProductDB ", "coursor count: " + cursor.getCount());

            if (cursor != null) {
                while (cursor.moveToNext()) {
                    Log.e("update_ProductDBPro ", cursor.getString(0));

                    if (!cursor.getString(0).equals("0")) {
                        quantity_sold = Double.valueOf(cursor.getString(0));
                        Log.e("update_ProductDB ", "quantity sold previously" + quantity_sold);
                        quantity_sold = quantity_sold + quantityAndTotalAmtt.get(i).getQty();
                        contentValues.put("quantity_sold", quantity_sold);
                        int k = sqLiteWritable.update(Product_Table_Name, contentValues, "Pro_id=" + ItemsSelectedByUser.get(i).getPro_id(), null);
                        Log.e("update_ProductDB ", k + "");
                    } else {
                        contentValues.put("quantity_sold", quantityAndTotalAmtt.get(i).getQty() + "");
                        int k = sqLiteWritable.update(Product_Table_Name, contentValues, "Pro_id=" + ItemsSelectedByUser.get(i).getPro_id(), null);
                        Log.e("update_ProductDB ", k + "");
                    }
                }
            }cursor.close();
        }
    }

    // (VICKY LONGADGE) Insert Tax in Temp Table
    public void insertIntoTemp_vicky(int id, double selected_quantity, double total_amt) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("pro_idd", id);
        contentValues.put("selected_quantity", selected_quantity);
        contentValues.put("total_amt", total_amt);
        sqLiteWritable.insert(Temp_Table_Name, null, contentValues);
        sqLiteWritable.close();
    }

    public void updateTempTable(int id, double selected_quantity, double total_amt) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("selected_quantity", selected_quantity);
        contentValues.put("total_amt", total_amt);
        int a = sqLiteWritable.update(Temp_Table_Name, contentValues, "pro_idd=" + id, null);
        if (a == 1) {
            Log.e("P ", "product updated in TempTable ");
        } else {
            Log.e("P ", "product not updated in TempTable ");
        }
    }

    public ProductModel getProdcut_TempTable(int pro_id) {
        ProductModel productModel = null;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Temp_Table_Name + " where pro_idd='" + pro_id + "' ", null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                productModel = new ProductModel(cursor.getInt(0), cursor.getDouble(1), cursor.getDouble(2));
            }
            Log.e("getProdcut_TempTable ", productModel.toString());
        }

        cursor.close();
        return productModel;
    }


    public ArrayList<ProductModel> getDataFrom_TempTable() {
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Temp_Table_Name, null);
        ArrayList<ProductModel> tempTable_ArrayList = new ArrayList<>();
        if (cursor != null) {
            while (cursor.moveToNext()) {
                ProductModel productModel = new ProductModel(cursor.getInt(0), cursor.getDouble(1), cursor.getDouble(2));
                tempTable_ArrayList.add(productModel);
                Log.d("CustomerDetailList", tempTable_ArrayList.toString());
            }
        }
        Log.d("CustomerDetailList", tempTable_ArrayList.size() + "");
        cursor.close();
        sqLiteReadable.close();
        return tempTable_ArrayList;
    }

    // update method for Product details  
    public int updateProductDetails(int id, String pro_name, String qty, String amount, String categoryID, String soldId) throws JSONException {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Pro_name", pro_name);
        contentValues.put("Quantity", qty);
        contentValues.put("Price", amount);
        contentValues.put("Category_ID", categoryID);
        contentValues.put("Soldby_Id", soldId);
        int value = sqLiteWritable.update(Product_Table_Name, contentValues, "Pro_id = " + id, null);

        return value;
    }

    // insert data into TempTable  
    public void insertIntoTemp(int id, double selected_quantity, double total_amt) {
        int check;
        double lastQty;
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Temp_Table_Name + " where pro_idd = " + id, null);
        check = cursor.getCount();
        if (check > 0) {
            while (cursor.moveToNext()) {
                lastQty = cursor.getDouble(1);
                Log.e(TAG + " LastQty", lastQty + "");
                lastQty = lastQty + selected_quantity;
                ContentValues contentValues = new ContentValues();
                contentValues.put("selected_quantity", lastQty);
                contentValues.put("total_amt", lastQty * productModel.getPrice());
                sqLiteWritable.update(Temp_Table_Name, contentValues, "pro_idd = " + id, null);
            }
            sqLiteWritable.close();
        } else {
            ContentValues contentValues = new ContentValues();
            contentValues.put("pro_idd", id);
            contentValues.put("selected_quantity", selected_quantity);
            contentValues.put("total_amt", total_amt);
            sqLiteWritable.insert(Temp_Table_Name, null, contentValues);
            sqLiteWritable.close();
        }cursor.close();
    }


    public int getTotalRow(String Table_Name) {
        int totalRow;
        Log.e(TAG + " getTotalRow ", Table_Name);
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Table_Name + "", null);
        totalRow = cursor.getCount();
        cursor.close();
        sqLiteReadable.close();
        return totalRow;
    }

    // update method for Customer details by Himanshu
    public int updateCustomer(int id, String CustomerName, String address, long mobileNo, String email) throws JSONException {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("Customer_Name", CustomerName);
        contentValues.put("Address", address);
        contentValues.put("Mobile_Number", mobileNo);
        contentValues.put("Email_id", email);
        int status = sqLiteWritable.update(Customer_Table_Name, contentValues, "Customer_id = " + id, null);
        Log.e(TAG + " updateCustomerStatus", status + "");
        return status;
    }

    //(VICKY LONGADGE) weather mobile number is exist in database or not
    public int searchAvailability(String table_name, String column_name, String value) {
        Cursor cursor = null;
        int check = 0;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        if (table_name.equals(Customer_Table_Name)) {
            Long mobile = Long.valueOf(value);
            cursor = sqLiteReadable.rawQuery("select * from " + table_name + " where " + column_name + "='" + mobile + "' ", null);
        } else {
            cursor = sqLiteReadable.rawQuery("select * from " + table_name + " where " + column_name + "='" + value + "' ", null);
        }

        if (cursor.getCount() == 1) {
            check = cursor.getCount();
            Log.e("searchTaxAvailability ", "tax available " + cursor.getCount());
        } else if (cursor.getCount() == 0) {
            check = cursor.getCount();
            Log.e("searchTaxAvailability ", "tax available " + cursor.getCount());
        }
        cursor.close();
        return check;
    }

    //Getting Data from Db for update Quantity when Bill Cancel OR Refund
    public ArrayList<BillingModel> getBillingId(int order_id) {
        ArrayList<BillingModel> billingModelArrayList = new ArrayList<>();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + BillingProduct_Table_Name + " where Order_Id='" + order_id + "' ", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                BillingModel billingModel = new BillingModel(cursor.getInt(0), cursor.getInt(1), cursor.getDouble(2), cursor.getDouble(3));
                billingModelArrayList.add(billingModel);
            }
        }cursor.close();
        return billingModelArrayList;
    }

    // update method for Product quantity  
    public boolean updateProductQuantity(int id, double qty) {
        boolean status = false;
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        Log.e("quantity", getProductRow(id).getQty() + "");
        double new_qty = getProductRow(id).getQty() + qty;
        contentValues.put("Quantity", new_qty);
        long value = sqLiteWritable.update(Product_Table_Name, contentValues, "Pro_id = " + id, null);
        Log.e("value", value + "");
        if (value == 1) {
            status = true;
        } else
            status = false;
        return status;
    }

    public ArrayList<OrderModel> getFilterOrderDetails(long fromDate, long toDate) {
        ArrayList<OrderModel> orderDetails = new ArrayList<>();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Order_Table_Name + " ", null);
        if (cursor != null) {
            while (cursor.moveToNext()) {

                OrderModel orderModel = new OrderModel(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3),
                        cursor.getDouble(4), cursor.getDouble(5), cursor.getDouble(6),
                        cursor.getDouble(7), cursor.getString(8), cursor.getString(9),
                        cursor.getString(10), cursor.getString(11), cursor.getInt(12));

                String dateFromDB = orderModel.getBillDate().substring(0, orderModel.getBillDate().length() - 11);

                Log.e(TAG + " billDateFromUser", fromDate + " " + toDate);
                Log.e(TAG + " billDateFromDB", dateFromDB + "");
                if (getMilliSecofDate(dateFromDB) >= fromDate && getMilliSecofDate(dateFromDB) <= toDate) {
                    orderDetails.add(orderModel);
                }
            }
        }
        cursor.close();
        sqLiteReadable.close();
        return orderDetails;
    }

    //string date convert into millisecond  
    private long getMilliSecofDate(String dates) {
        long milliSeconds = 0;
        Date date;
        try {
            date = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH).parse(dates);
            milliSeconds = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return milliSeconds;
    }


    // (VICKY LONGADGE)drop table
    public void truncate_Table(String table_Name) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        sqLiteWritable.execSQL("delete from " + table_Name);
    }


    // db.execSQL(Create_Table_Query + Temp_Table_Name + "(pro_idd integer, pro_name text, selected_quantity real,price real, total_amt real)");
    // (VICKY LONGADGE) Insert Tax in Temp Table
    public void insertIntoTemp_TabletVicky(int id, String pro_name, double selected_quantity, double price, double total_amt) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("pro_idd", id);
        contentValues.put("pro_name",pro_name);
        contentValues.put("selected_quantity", selected_quantity);
        contentValues.put("price", price);
        contentValues.put("total_amt", total_amt);
        sqLiteWritable.insert(Temp_Tab_Table_Name, null, contentValues);
        sqLiteWritable.close();
    }


    // (VICKY LONAGADGE) get all products from temp table
    public ArrayList<ProductModel> getFromTemp_TabletVicky()
    {
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        ProductModel productModel;
        ArrayList<ProductModel> productList = new ArrayList<>();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Temp_Tab_Table_Name , null);
        if (cursor != null)
        {
            while (cursor.moveToNext()) {
                productModel = new ProductModel(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3), cursor.getDouble(4));
                productList.add(productModel);
                Log.e(TAG + " Product_list: ", productList.toString());
            }
        }cursor.close();
        return productList;     }


    // TABLET (VICKY LONGADGE)delete selected product from temp table
    public void delete_Pro_From_Table(int ppro_id, String table_name, String col_name) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        sqLiteWritable.delete(table_name, col_name + "=" + ppro_id, null);
        Log.e("P ", "deleted ");
    }


    // TABLET (VICKY LONGADGE) count products available in temp table
    public int count_Table(String table_name) {
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + table_name  , null);
        int count = cursor.getCount();
        Log.e("count ", cursor.getCount() + "");
        cursor.close();
        return count;
    }
    // TABLET (VICKY LONGADGE) update data in temp table
    public void updateTemp_TabTable(int id, double selected_quantity, double total_amt) {
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("selected_quantity", selected_quantity);
        contentValues.put("total_amt", total_amt);
        int a = sqLiteWritable.update(Temp_Tab_Table_Name, contentValues, "pro_idd=" + id, null);
        if (a == 1) {
            Log.e("P ", "product updated in TempTable ");
        } else {
            Log.e("P ", "product not updated in TempTable ");
        }
    }


    // TABLET (VICKY LONGADGE) get total items and subtotal for billing
    public ProductModel qty_subtotal_Temp()
    {
        int total_Items = 0;
        double sub_Total = 0;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        ArrayList<ProductModel> qty_subtotal_list = new ArrayList<>();
        ProductModel productModel = null;
        Cursor cursor = sqLiteReadable.rawQuery("select * from " + Temp_Tab_Table_Name , null);
        if (cursor != null)
        {
            while (cursor.moveToNext()) {
                productModel = new ProductModel(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3), cursor.getDouble(4));
                total_Items = total_Items + 1;
                sub_Total = sub_Total + productModel.getTotalAmt();
            }

            Log.e("qty_subtotal_Temp","total_Items: "+total_Items    + "sub_Total: "+sub_Total );

            productModel = new ProductModel(total_Items,sub_Total);
        }cursor.close();
        return productModel;
    }

    // (VICKY LONGADGE)  saving billing data in order table  ---- TABLET
    public int insertDataInorderTable_TAB(int cust_id, String payment_mode, OrderModel orderModel, String formattedDate, int bill_status)
    {
        long check_status = 0;
        int orderId = 0;
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("payment_mode", payment_mode);
        contentValues.put("cust_id", cust_id);
        contentValues.put("tax_type", orderModel.getTaxType());
        contentValues.put("amt_before_dis", orderModel.getAmtBeforeDiscount());// percent
        contentValues.put("discount_perc", orderModel.getDiscountPerc());
        contentValues.put("discount_amt", orderModel.getDiscountAmt());
        contentValues.put("amt_after_dis", orderModel.getAmtBeforeTax());
        contentValues.put("tax_id", orderModel.getTaxID());
        contentValues.put("amt_after_tax", orderModel.getAmtAfterTax());
        contentValues.put("date", formattedDate);
        contentValues.put("amt_of_each_tax", orderModel.getAmtOfEach_Tax());
        contentValues.put("bill_status", bill_status);
        check_status = sqLiteWritable.insert(Order_Table_Name, null, contentValues);

        Log.e("insertDataInorder_TAB, ","check_status:  "+check_status);
// searching for last inserted row
        if (check_status > 0) {

            Cursor cursor = sqLiteReadable.rawQuery("select Orderr_Id from " + Order_Table_Name + " order by Orderr_Id desc limit 1", null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    Log.e("billing product ", cursor.getInt(0) + "");
                    orderId = cursor.getInt(0);
                }
            }
            if (insertDataInbillingPro_TAB(orderId) <= 0)
            {
                orderId = 0;
            }

            Log.e("check_status ", check_status + "");
            cursor.close();
        }
        sqLiteWritable.close();
        sqLiteReadable.close();
        return orderId;
    }

    // (VICKY LONGADGE) saving billing data in billing table --- TABLET
    public long insertDataInbillingPro_TAB(int orderId)
    {
        long count = 0;
        SQLiteDatabase sqLiteWritable = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        for (int i = 0; i<getFromTemp_TabletVicky().size();i++)
        {
            contentValues.put("Order_Id", orderId);
            contentValues.put("Pro_Id", getFromTemp_TabletVicky().get(i).getPro_id());
            contentValues.put("Quantity", Double.valueOf(getFromTemp_TabletVicky().get(i).getQty()));
            contentValues.put("Pro_Price",getFromTemp_TabletVicky().get(i).getTotalAmt());
            count = sqLiteWritable.insert(BillingProduct_Table_Name, null, contentValues);
            if (count <= 0) break;
        }
        sqLiteWritable.close();
        return count;
    }

    public double noOfProductsSold() {
        double noOfProductsSold = 0;
        SQLiteDatabase sqLiteReadable = this.getReadableDatabase();
        Cursor cursor = sqLiteReadable.rawQuery("select sum(Quantity) from " + BillingProduct_Table_Name  , null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                noOfProductsSold = cursor.getDouble(0);
            }
        }
        cursor.close();
        Log.e("noOfProductsSold ", noOfProductsSold+"");
        return noOfProductsSold;
    }


}
