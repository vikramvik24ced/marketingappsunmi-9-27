package com.sunmi.DataBaseSunmi;

import java.util.ArrayList;

public class TablesAndColumns {

    //database version
    public static final String DATABASE_NAME = "DBMarketing";
    public static final int DATABASE_VERSION = 1;

    //Table Name
    public static final String Tax_Table_Name = "TaxCreate";
    public static final String Category_Table_Name = "Category";
    public static final String SoldBy_Table_Name = "SoldBy";
    public static final String Product_Table_Name = "Product";
    public static final String Customer_Table_Name = "Customer";
    public static final String BillingProduct_Table_Name = "BillingProduct";
    public static final String Purchased_Table_Name = "Purchase";
    public static final String Order_Table_Name = "Order_Details";
    public static final String Image_Table_Name = "Purchase_Image ";
    public static final String AddToCart_Table_Name = "Add_To_Cart ";
    public static final String Temp_Table_Name = "Temps";
    public static final String Temp_Tab_Table_Name = "Temp_Tablet";

    //Create Table querys
    public static final String Create_Table_Query = "CREATE TABLE IF NOT EXISTS ";
    // Variables
    public static final int Sync_StatusOff = 0;
    public static final int Sync_StatusOn = 1;
}
