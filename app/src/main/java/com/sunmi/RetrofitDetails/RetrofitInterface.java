package com.sunmi.RetrofitDetails;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sunmi.printerhelper.Model.APIModel;


import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface RetrofitInterface {

    // LOGIN
    @FormUrlEncoded
    @POST("pos/api/login")
    Call<APIModel> getLoginDetails(@Field("email") String email, @Field("password") String password, @Field("device_id") String device_id);

    // UPDATE USER
    @POST("pos/api/update_user")
    Call<APIModel> updateUser(@Body JsonObject jsonObject);

//    // ADD PRODUCT
//    @POST("pos/api/sync_product")
//    Call<Example> sync_ProductDataa(
//            @Body JsonObject jsonObject
//    );

//    // ADD PRODUCT
//    @POST("pos/api/add_product")
//   Call<APIModel> addproduct(@Body JsonArray jsonArray);

    // SEND AND GET CATEGORY
    @Headers("Content-Type: application/json")
    @POST("pos/api/sync_category")
    Call<APIModel> sendAndGetCategory(@Body RequestBody jsonObject);




    // SEND AND GET PRODUCTS
    @Headers("Content-Type: application/json")
    @POST("pos/api/sync_product")
    Call<APIModel> sendAndGetProducts(@Body RequestBody jsonObject);


//    // SEND AND GET CATEGORY
//    @Headers("Content-Type: application/json")
//    @POST("pos/api/sync_category")
//    Call<APIModel> sendAndGetCategory();

    // CHANGE PASSWORD
    @FormUrlEncoded
    @POST("pos/api/update_password")
    Call<APIModel> changePasswordAPI( @Field("old_password") String old_password, @Field("new_password") String new_password,
                                      @Field("user_id") int user_id
    );

    @Headers("Content-Type: application/json")
    @POST("pos/api/add_product")
    Call<APIModel> sendProduct(@Body JsonArray jsonObject);


    @GET("pos/api/get_product")
    Call<APIModel> getProduct();

    @FormUrlEncoded
    @POST("pos/api/forgot_password")
    Call<APIModel> setForgotPass(@Field("email") String email, @Field("device_id") String device_id);

    // SEND AND GET PURCHASE DATA
    @Headers("Content-Type: application/json")
    @POST("pos/api/add_purchase")
    Call<APIModel>  sendAndGetPurchase(@Body RequestBody purchaseBody);

}
